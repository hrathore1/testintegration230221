public without sharing class RACVF_LinkSFFilesHandler {
    
    public void linkFilestoLibraryFolder(List<ContentDocumentLink> newDocs) {
        
        Set<Id> setDocIds = new Set<Id>();
        
        Set<Id> setContenetLinkIds = new Set<Id>();
        Set<Id> setLinkedEntityIds = new Set<Id>();
        
        for( ContentDocumentLink contentDocLinkNew : newDocs ){
            
            setDocIds.add(contentDocLinkNew.ContentDocumentId);
            setLinkedEntityIds.add(contentDocLinkNew.LinkedEntityId);
           
        }
        
        for(Id keyRecordId : setLinkedEntityIds){
        
            String sObjName = keyRecordId.getSObjectType().getDescribe().getName();
            System.debug('sObjName---->'+sObjName);
            if(sObjName == 'genesis__Applications__c' || sObjName == 'loan__Loan_Account__c' 
               || sObjName == 'collect__Loan_Account__c' || sObjName == 'Case' 
               || sObjName == 'Deposit_Contract__c'|| sObjName == 'EmailMessage' )
            {
                setContenetLinkIds.add(keyRecordId);
                System.debug('sObjName---->'+sObjName+'----LinkedEntityId---->'+keyRecordId);
            }
        }
  
        Set<String> setContractNumber = new Set<String>();
        List<Case> caseList;
        if( setContenetLinkIds.size()>0 ){
            
            List<genesis__Applications__c> appList = [Select CRN_Number__c FROM genesis__Applications__c where id IN:setContenetLinkIds];
            
            for( genesis__Applications__c application : appList ){
                
                setContractNumber.add(application.CRN_Number__c);
            }
            
            if( setContractNumber.size() == 0 ){
                
                List<loan__Loan_Account__c> loanList = [Select CRN_Number__c FROM loan__Loan_Account__c where id IN:setContenetLinkIds];
                
                for( loan__Loan_Account__c loan : loanList ){
                    
                    setContractNumber.add(loan.CRN_Number__c);
                }
            }
            
            if( setContractNumber.size() == 0 ){
                
                List<collect__Loan_Account__c> collList = [Select CRN_Number__c FROM collect__Loan_Account__c where id IN:setContenetLinkIds];
                
                for( collect__Loan_Account__c collection : collList ){
                    
                    setContractNumber.add(collection.CRN_Number__c);
                }
            }
            
            if( setContractNumber.size() == 0 ){
                
                List<Deposit_Contract__c> depositList = [Select Contract_Number__c  FROM Deposit_Contract__c where id IN:setContenetLinkIds];
                
                for( Deposit_Contract__c deposit : depositList ){
                    
                    setContractNumber.add(deposit.Contract_Number__c);
                }
            }
            // This is for Case Attachment
            if( setContractNumber.size() == 0 ){
                System.debug('In case ---->');
                caseList = [Select CRN_Number__c,Application__c , Collection_Contract__c ,Loan_Account__c  FROM Case 
                            WHERE Id IN:setContenetLinkIds];
                
                System.debug('In caseList ---->'+caseList.size());
                for( Case caseObj : caseList ){
                     System.debug('In caseObj.CRN_Number__c ---->'+caseObj.CRN_Number__c);
                    setContractNumber.add(caseObj.CRN_Number__c);
                }
            }
            
            // This is for incomming email Attachment
            if( setContractNumber.size() == 0 ){
                System.debug('In Email ---->');
                caseList = [Select CRN_Number__c,Application__c , Collection_Contract__c ,Loan_Account__c  FROM Case 
                            WHERE Id IN(Select ParentId from EmailMessage WHERE Id IN:setContenetLinkIds)];
                
                System.debug('In caseList Email ---->'+caseList.size());
                for( Case caseObj : caseList ){
                     System.debug('In caseObj.CRN_Number__c ---->'+caseObj.CRN_Number__c);
                    setContractNumber.add(caseObj.CRN_Number__c);
                }
            }
            
        }
        
        if( setContractNumber.size()>0 )
        {
            ContentWorkspace ws = [SELECT Id,Name, RootContentFolderId FROM ContentWorkspace WHERE Name IN:setContractNumber LIMIT 1];
            
            List<ContentDocumentLink> contentDocLinktoInsert = new List<ContentDocumentLink>();
            
            // Getting Entity Id for Key object from Case
            Set<Id> entityIds = new Set<Id>();
            if(caseList!=null && caseList.size()> 0)
            {
                for(Case caseObj: caseList){
                    
                    if(!String.isBlank(caseObj.Application__c)){
                        entityIds.add(caseObj.Application__c);
                    }
                    if(!String.isBlank(caseObj.Collection_Contract__c)){
                        entityIds.add(caseObj.Collection_Contract__c);
                    }
                    if(!String.isBlank(caseObj.Loan_Account__c)){
                        entityIds.add(caseObj.Loan_Account__c);
                    }
                }
            }
            
            Map<String, String> contentLinkMap = new Map<String, String>();
            List<ContentDocumentLink> contentLinkList = [SELECT LinkedEntityId, ContentDocumentId FROM ContentDocumentLink WHERE ContentDocumentId IN:setDocIds];
           
            
            for(ContentDocumentLink contentLinked : contentLinkList){
                contentLinkMap.put(contentLinked.LinkedEntityId+','+contentLinked.ContentDocumentId, contentLinked.LinkedEntityId+','+contentLinked.ContentDocumentId);
            }
            
            
            for( Id contentDocId : setDocIds ){
                System.debug(' contentDocId -----> '+contentDocId);
               
                if( !contentLinkMap.containsKey( ws.Id+','+contentDocId) ){
                    
                    System.debug(' contentDocId2 -----> '+contentDocId);
                    ContentDocumentLink cdl = new ContentDocumentLink();
                    cdl.ContentDocumentId = contentDocId;
                    cdl.ShareType = 'I';
                    cdl.Visibility = 'AllUsers';
                    cdl.LinkedEntityId = ws.Id; 
                    contentDocLinktoInsert.add(cdl);
                }
                
                
                for( Id entityId: entityIds ){
                    System.debug(' entityId -----> '+entityId);
                    ContentDocumentLink cdl2 = new ContentDocumentLink();
                    cdl2.ContentDocumentId = contentDocId;
                    cdl2.ShareType = 'I';
                    cdl2.Visibility = 'AllUsers';
                    cdl2.LinkedEntityId = entityId; 
                    contentDocLinktoInsert.add(cdl2);
                }
            }
            
            System.debug('contentDocLinktoInsert.size()----->'+ contentDocLinktoInsert.size());
            System.debug('contentDocLinktoInsert----->'+ contentDocLinktoInsert);
            if( contentDocLinktoInsert.size()>0)
            	RACVF_ContentDocumentLink_TriggerHelper.shareFile(contentDocLinktoInsert); 
        }
    }

}