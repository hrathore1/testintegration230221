/**
 * @description       : 
 * @author            : chirag.gupta@q2.com
 * @group             : 
 * @last modified on  : 09-21-2020
 * @last modified by  : chirag.gupta@q2.com
 * Modifications Log 
 * Ver   Date         Author                Modification
 * 1.0   09-21-2020   chirag.gupta@q2.com   Initial Version
**/
trigger CustomApplicationTrigger on genesis__Applications__c (before insert, before update, after insert, after update) {
    if (!(genesis.CustomSettingsUtil.getOrgParameters().genesis__Disable_Triggers__c)) {
        if (trigger.isInsert && trigger.isAfter) {
            System.debug('Entering in IsAfter custom application trigger');
            CustomApplicationTriggerHandler appHandler = new CustomApplicationTriggerHandler();
            appHandler.createApplicationData(trigger.new);
        }
        if (trigger.isBefore) {
            System.debug('Entering in IsBefore custom application trigger');
            CustomApplicationTriggerHandler appHandler = new CustomApplicationTriggerHandler();
            appHandler.updateApplicationParams(trigger.new, trigger.oldMap);
        }
        /*if(trigger.isAfter){
            System.debug('Entering in IsAfter custom application trigger');
            CustomApplicationTriggerHandler appHandler = new CustomApplicationTriggerHandler();
            appHandler.runAfterActions(trigger.new, trigger.oldMap);
        }*/
    }
}