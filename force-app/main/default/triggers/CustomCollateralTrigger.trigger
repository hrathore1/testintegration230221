trigger CustomCollateralTrigger on clcommon__Collateral__c (before insert, after insert, after update, before update) {
    if (!(genesis.CustomSettingsUtil.getOrgParameters().genesis__Disable_Triggers__c)) {
        if(trigger.isInsert && trigger.isBefore){
            System.debug('Entering in IsBefore custom collateral trigger');
            CustomCollateralTriggerHandler collHandler = new CustomCollateralTriggerHandler();
            collHandler.parseJSON(trigger.new, trigger.oldMap);    
        }
        
        if((trigger.isInsert || trigger.isUpdate) && trigger.isBefore){
            CustomCollateralTriggerHandler collHandler = new CustomCollateralTriggerHandler();
            collHandler.updateAdjustmentKM(trigger.new, trigger.oldMap);
        }
        
        if((trigger.isInsert || trigger.isUpdate) && trigger.isAfter){
            System.debug('Entering in IsAfter custom collateral trigger');
            CustomCollateralTriggerHandler collHandler = new CustomCollateralTriggerHandler();
            collHandler.updatePledgeValue(trigger.new, trigger.oldMap);
            collHandler.createCollateralValuation(trigger.new, trigger.oldMap);
        }
    }
}