trigger PartyTriggerCustom on clcommon__Party__c (after insert,after update,after delete) {
	PartyTriggerHandler handler = new PartyTriggerHandler(trigger.new,trigger.newMap,trigger.oldMap);
    if(trigger.isafter){
        if(trigger.isInsert || trigger.isUpdate){
            handler.onPartyCreateOrUpdate();
        }
        else if(trigger.isDelete){
            handler.onPartyDelete();
        }
    }
}