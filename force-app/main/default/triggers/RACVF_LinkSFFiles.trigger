/**
 * @description       : 
 * @author            : Zain
 * @group             : 
 * @last modified on  : 11-11-2020
 * @last modified by  : Zain
 * Modifications Log 
 * Ver   Date         Author                Modification
 * 1.0   11-11-2020   Zain   				Initial Version
**/
trigger RACVF_LinkSFFiles on ContentDocumentLink (after insert) {
    
     if (!(genesis.CustomSettingsUtil.getOrgParameters().genesis__Disable_Triggers__c)) {
        
    	if (trigger.isInsert && trigger.isAfter) {
            
            System.debug('Entering in IsAfter Content Document trigger');
            RACVF_LinkSFFilesHandler fileHandler = new RACVF_LinkSFFilesHandler();
            fileHandler.linkFilestoLibraryFolder(trigger.new);
            
        }
        
    }

}