trigger CustomCashReceiptsTrigger on clcommon__Cash_Receipt__c (after update, after insert) {
    if (!(genesis.CustomSettingsUtil.getOrgParameters().genesis__Disable_Triggers__c)
        && trigger.isAfter 
        && (trigger.isInsert || trigger.isUpdate)) {
        
        Set<Id> setAccountIds = new Set<Id>();    
        for(clcommon__Cash_Receipt__c cashReceipt : trigger.new){
            if(cashReceipt.clcommon__Account__c != null){
                setAccountIds.add(cashReceipt.clcommon__Account__c );
            }            
        }
        
        if(setAccountIds.size() > 0){
            List<Account> listAccounts = [SELECT id,
                                                 clcommon__Default_Payment_Spread__c 
                                            FROM Account 
                                           WHERE id IN :setAccountIds
                                         ];
            List<clcommon__Payment_Spread__c> listPaymentSpread = [SELECT id,
                                                                          name 
                                                                     FROM clcommon__Payment_Spread__c 
                                                                    LIMIT 1
                                                                  ];
            
            String paymentSpreadId = '';                                                      
            if(listPaymentSpread != null && listPaymentSpread.size() > 0){
                paymentSpreadId = listPaymentSpread.get(0).Id;
            }
            else{
                clcommon__Payment_Spread__c ps = new clcommon__Payment_Spread__c();
                ps.clcommon__Enabled__c = 'Yes';
                ps.clcommon__Tax_Configuration__c = 'DISTRIBUTE';
                insert ps;
                paymentSpreadId = ps.Id;
            }
            
            List<Account> accountToBeUpdated = new List<Account>();
            for(Account acc : listAccounts){
                if(acc.clcommon__Default_Payment_Spread__c == null){
                    acc.clcommon__Default_Payment_Spread__c = paymentSpreadId;
                    accountToBeUpdated.add(acc);
                }
            }
            
            if(accountToBeUpdated.size() > 0){
                update accountToBeUpdated;
            }
        }
    }
}