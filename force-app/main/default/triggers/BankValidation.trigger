/********************************************************************************************
* @JIRA - 795
* @Description - The Trigger is used for validation purpose.
                 It will restrict to deactivate any Bank account if it is used for direct debit in cl contract.

@JIRA RFCL-1978
@Destription- Before insert trigger will put some dummyvalue in managed bank account number field since a 
custom bank account num er field will be in use.
*********************************************************************************************/
trigger BankValidation on loan__Bank_Account__c (before update, before insert) {
    if(Trigger.isBefore){
        if(Trigger.isUpdate){
            if(!loan__Trigger_Parameters__c.getOrgDefaults().Disable_BankValidation_Trigger__c){
                List <loan__Loan_Account__c> laccList = [SELECT id,
                                                                loan__Borrower_ACH__c  
                                                             FROM loan__Loan_Account__c
                                                             WHERE loan__Borrower_ACH__c in: Trigger.new];
                Set<id> bAccId = new Set<id>();
                for(loan__Loan_Account__c lacc : laccList)  {
                    bAccId.add(lacc.loan__Borrower_ACH__c);
                }                                           
               
                For(loan__Bank_Account__c ba : Trigger.new ){
                    if(Trigger.oldMap.get(ba.Id).loan__Active__c == True && ba.loan__Active__c == False   && bAccId.contains(ba.id)){
                        ba.adderror(System.label.BANK_VALIDATION_MESSAGE);
                    }
                } 
            }
        } 
        //Before insert trigger will put some dummyvalue in managed bank account number field since a 
        //custom bank account num er field will be in use.
        if(Trigger.isInsert){
            for(loan__Bank_Account__c ba : Trigger.new ){
                ba.loan__Bank_Account_Number__c = '00000000'; //Dummy Value
            }
        }
    }                                           
    
}