/*********************************************************************************
@ Description: Trigger on address for validations.
@ JIRA- RFCL-1005
**********************************************************************************/

trigger AddressValidation on clcommon__Address__c ( before insert, before update, after insert, after update ) {
    
    if ( loan__Trigger_Parameters__c.getOrgDefaults().Dissable_AddressValidation_Trigger__c == False &&
         genesis__Org_Parameters__c.getOrgDefaults().genesis__Disable_Triggers__c == False){
    
        List <clcommon__Address__c> addList = new  List <clcommon__Address__c>();
        List <clcommon__Address__c> resAddList = new  List <clcommon__Address__c>();
        
        if(trigger.isBefore && (trigger.isInsert || trigger.isUpdate) ){
            for (clcommon__Address__c add : Trigger.new){
                if(String.isBlank(add.Employer_Info__c) && ( (Trigger.oldMap == null || Trigger.oldMap.isEmpty()) || (Trigger.oldMap.containsKey(add.id) && Trigger.oldMap.get(add.id).Is_Current__c == False))){
                    addList.add(add);
                }
            }
            if(addList.size()>0){
            //call handler
            AddressValidationHandler.validateAddress(addList, Trigger.oldMap);
            }
        }
        
        if(trigger.isAfter && (trigger.isInsert || trigger.isUpdate) ){
            for (clcommon__Address__c addr : Trigger.new){
                if(String.isBlank(addr.Employer_Info__c) ){
                    if(String.isBlank(addr.Address_Type__c) ){
                        addr.addError(system.label.Address_validation_message_4);
                    }
                    else if(addr.Address_Type__c.equals('Residential')&& ( (Trigger.oldMap == null || Trigger.oldMap.isEmpty()) || (Trigger.oldMap.containsKey(addr.id) && Trigger.oldMap.get(addr.id).Is_Current_Mailing_Address__c == False))){
                        resAddList.add(addr);
                    }
                }
            }
            if(resAddList.size()>0){
                //call handler
                AddressValidationHandler.afetrTriggerAction(resAddList);
            }
        }
    }
}