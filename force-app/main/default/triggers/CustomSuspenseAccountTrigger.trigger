trigger CustomSuspenseAccountTrigger on peer__Bank_Recon_Exception__c (after insert) {
     if (!(genesis.CustomSettingsUtil.getOrgParameters().genesis__Disable_Triggers__c)) {
         if (trigger.isAfter) {
             System.debug('Entering in IsBefore custom bank statement exception trigger');
             //CustomApplicationTriggerHandler appHandler = new CustomApplicationTriggerHandler();
             //appHandler.updateApplicationParams(trigger.new, trigger.oldMap);
             List<String> listReasonCodes = System.label.BANK_STATEMENT_EXCEPTION_REASON_CODES.split(',');
             
             Set<Id> setIds = new Set<Id>();
             for(peer__Bank_Recon_Exception__c record : trigger.new){
                 setIds.add(record.Id);
             }
             
             List<peer__Bank_Recon_Exception__c> listRecords = [SELECT id,
                                                                       name,
                                                                       peer__Reason__c,
                                                                       peer__Transaction_Date__c,
                                                                       peer__Transaction_Amount__c, 
                                                                       peer__Reference_Number__c,
                                                                       Cash_Receipt__c,
                                                                       Cash_Receipt__r.External_Key__c,
                                                                       peer__Transaction_Handled__c 
                                                                  FROM peer__Bank_Recon_Exception__c 
                                                                 WHERE Id IN :setIds
                                                               ];
                                                               
             List<clcommon__Payment_Mode__c> paymentMode = [SELECT id,
                                                                   name 
                                                              FROM clcommon__Payment_Mode__c 
                                                             WHERE name = 'BPAY'
                                                           ];
                                                           
             String paymentModeId = '';
             if(paymentMode != null && paymentMode.size() > 0){
                 paymentModeId = paymentMode.get(0).Id;
             }
             else{
                 clcommon__Payment_Mode__c payMode = new clcommon__Payment_Mode__c();
                 payMode.name = 'BPAY';
                 insert payMode;
                 paymentModeId = payMode.Id;
             }
             
             List<clcommon__Cash_Receipt__c> cashReceiptToBeInserted = new List<clcommon__Cash_Receipt__c>();
             List<peer__Bank_Recon_Exception__c> bankStatementToBeUpdated = new List<peer__Bank_Recon_Exception__c>();
             for(peer__Bank_Recon_Exception__c record : listRecords){
                 if(!String.isBlank(record.peer__Reason__c) 
                        && !listReasonCodes.contains(record.peer__Reason__c)
                        && record.peer__Transaction_Amount__c != null 
                        && record.peer__Transaction_Amount__c > 0
                        && record.peer__Transaction_Date__c != null){
                     clcommon__Cash_Receipt__c cashReceipts = new clcommon__Cash_Receipt__c();
                     cashReceipts.clcommon__Receipt_Date__c = record.peer__Transaction_Date__c;
                     cashReceipts.clcommon__Receipt_Amount__c = record.peer__Transaction_Amount__c;
                     cashReceipts.clcommon__Unused_Amount__c = record.peer__Transaction_Amount__c;
                     cashReceipts.clcommon__Creation_Date__c = Date.today();
                     cashReceipts.clcommon__Creation_Date_Time__c = DateTime.now();
                     cashReceipts.External_Key__c = record.name;
                     cashReceipts.Reference_Number__c = record.peer__Reference_Number__c;
                     cashReceipts.clcommon__Payment_Mode__c = paymentModeId;
                     cashReceipts.System_Created__c = true;
                     
                     record.Cash_Receipt__r = new clcommon__Cash_Receipt__c(External_Key__c = record.name);
                     record.peer__Transaction_Handled__c = true;
                     
                     cashReceiptToBeInserted.add(cashReceipts);
                     bankStatementToBeUpdated.add(record);
                 }
             }
             
             mfiflexUtil.VOLog voLogInstance = mfiflexUtil.VOLog.getInstance('Neon');
             List<Database.SaveResult> results = Database.insert(cashReceiptToBeInserted, false);
             for(Database.SaveResult result : results) {
                 if(!result.isSuccess()) {
                     System.debug('Error record:' + result);
                     voLogInstance.logError(9001, '[Cash Receipt Commit error for external id] : ' + result); 
                 }
             }
            
             List<Database.UpsertResult> results1 = Database.upsert(bankStatementToBeUpdated, false);
             for(Database.UpsertResult result : results1) {
                 if(!result.isSuccess()) {
                     if(!result.isCreated()) {
                         System.debug('Error record:' + result);
                         voLogInstance.logError(9001, '[Bank Statement Update error] : ' + result);
                     } 
                 }
             }
             voLogInstance.commitToDb();
             
             /*if(cashReceiptToBeInserted.size() > 0){
                 insert cashReceiptToBeInserted;
             }
             if(bankStatementToBeUpdated.size() > 0){
                 update bankStatementToBeUpdated;
             }*/
         }
     }
}