trigger CustomApplicationCollateralTrigger on genesis__Application_Collateral__c (before insert, before update) {
    if (!(genesis.CustomSettingsUtil.getOrgParameters().genesis__Disable_Triggers__c)) {
        if((trigger.isInsert || trigger.isUpdate) && trigger.isBefore){
            System.debug('Entering in IsAfter custom application collateral trigger');
            Set<Id> setCollateralIds = new Set<Id>();
            Set<Id> setApplicationIds = new Set<Id>();
            //Map<Id, String> mapCollIdToPosition = new Map<Id, String>();
            for(genesis__Application_Collateral__c appCollateral : trigger.new){
                setCollateralIds.add(appCollateral.genesis__Collateral__c); 
                setApplicationIds.add(appCollateral.genesis__Application__c);
                //mapCollIdToPosition.put(appCollateral.genesis__Collateral__c, appCollateral.Security_Position__c);      
            }
            
            List<clcommon__CollateralLien__c> CollLienList = [SELECT id,
                                                                     name,
                                                                     clcommon__Lien_Amount__c,
                                                                     clcommon__Collateral__c,
                                                                     loan__Loan_Account__c,
                                                                     Residual_Value__c,
                                                                     clcommon__Lien_Position__c
                                                                FROM clcommon__CollateralLien__c 
                                                               WHERE clcommon__Collateral__c IN :setCollateralIds
                                                                 AND genesis__Application__c IN :setApplicationIds
                                                             ];
        
            //Calculating residual value on collateral for LVR 
            Map<String, Decimal> mapCollToResidual = new Map<String, Decimal>();
            for(clcommon__CollateralLien__c collLien : CollLienList){
                if(collLien.loan__Loan_Account__c != null){
                    mapCollToResidual.put(collLien.clcommon__Collateral__c, collLien.Residual_Value__c);
                }  
                
                if(collLien.clcommon__Lien_Position__c == null){
                       //&& mapCollIdToPosition.containsKey(collLien.clcommon__Collateral__c)
                       //&& mapCollIdToPosition.get(collLien.clcommon__Collateral__c) != null){
                    //if('Primary'.equalsIgnoreCase(mapCollIdToPosition.get(collLien.clcommon__Collateral__c))){
                        collLien.clcommon__Lien_Position__c = 1;
                    //}
                    //else{
                    //    collLien.clcommon__Lien_Position__c = 2;
                    //}
                }          
            }
            
            if(CollLienList != null && CollLienList.size() > 0){
                update CollLienList;
            }
        
            for(genesis__Application_Collateral__c appColl : trigger.new){
                //if(appColl.Security_Position__c != null){
                    if(mapCollToResidual.containsKey(appColl.genesis__Collateral__c)){
                        appColl.genesis__Pledge_Amount__c = mapCollToResidual.get(appColl.genesis__Collateral__c);
                    }
                //}
            }        
        }
    }
}