global class CalculatePricing{
    //mfiflexUtil.VOLog voLogInstance = mfiflexUtil.VOLog.getInstance('Genesis');
    
    //************** to call from Skuid *****************
    /*webservice static String pricingDetails(String appId){
        mfiflexUtil.VOLog voLogInstance1 = mfiflexUtil.VOLog.getInstance('Genesis');
        String retVal = '';
        try{
            CalculatePricing cp = new CalculatePricing();
            cp.generatePricing(appId);
            return 'Pricing generated and applied.';
        }
        catch(Exception e){
            voLogInstance1.logException(1001, '[CalculatePricing.pricingDetails] Exception at line : ' + e.getLineNumber(), e);
            voLogInstance1.commitToDB();
            return 'Something went wrong ' + e;
        }
    }*/
    
    //************** get discount from skuid *********************
    /*webservice static String getDiscountFromSkuid(String appId){
        String loanChannel = '';
        String franchise = '';
        Set<String> membershipSet = new Set<String>();
        Decimal baseInterestRate = 0;
        Decimal discretion=0;
        List<genesis__Applications__c> thisApp = [SELECT id,
                                                         Franchise__c,
                                                         Loan_Channel__c,
                                                         genesis__Interest_Rate__c,
                                                         Pricing_Interest__c,
                                                         Discretion__c,
                                                         (SELECT id,
                                                                 Name,
                                                                 clcommon__Contact__c,
                                                                 clcommon__Contact__r.id,
                                                                 clcommon__Account__r.Membership_card_colour__c,
                                                                 clcommon__Account__r.Membership_Number__c
                                                            FROM genesis__Parties__r
                                                           WHERE clcommon__Type__r.Name IN ('BORROWER','CO-BORROWER','GUARANTOR')
                                                        ORDER BY CreatedDate DESC
                                                         )
                                                    FROM genesis__Applications__c
                                                   WHERE id =: appId];
                                                   
        if(thisApp != null && thisApp.size() > 0){
            
            if(thisApp[0].genesis__Parties__r.size() > 0)
            {
                for(clcommon__Party__c party : thisApp[0].genesis__Parties__r)
                {
                    membershipSet.add(party.clcommon__Account__r.Membership_card_colour__c);
                }
            }
            
            else
            {
                throw new NoPartyException('No party associated with the application found!!!');
            }
            
            Decimal pricingInterest = thisApp[0].Pricing_Interest__c != null ? thisApp[0].Pricing_Interest__c : 0;
            franchise = thisApp[0].Franchise__c;
            loanChannel=thisApp[0].Loan_Channel__c;
            discretion = thisApp[0].Discretion__c;
            baseInterestRate = pricingInterest;
            List<clcommon__Party__c> party = thisApp[0].genesis__Parties__r;

        }
        if(loanChannel != null &&  String.isNotBlank(loanChannel) && franchise != null && String.isNotBlank(franchise) && membershipSet.size() > 0 )
        {
            CalculatePricing cp = new CalculatePricing();
            Decimal discount = 0;
            discount = cp.getDiscount(franchise, loanChannel, membershipSet,baseInterestRate) ;
            
            if(discount == null){
                discount = 0;
            }
            else if(discount == -99999)
            {
                discount = 0;
                return 'Failed to calculate Discount :  Multiple configurations found in Interest Discount Object';
            }
            
            thisApp[0].genesis__Discount_Rate__c = discount;
            thisApp[0].genesis__Interest_Rate__c = new CalculatePricing().getFinalInterest(baseInterestRate, discount, discretion);
            update thisApp[0];
            
            return 'Discount calculated.';
        }
        else{
            return 'Franchise or Member colour details is missing.';
        }
    }*/
    
    //************** generate pricing records on Application *********
    /*public void generatePricing(String appId){
        String res = genesis.SkuidPricingCtrl.generatePricing(appId);
        system.debug(res);
        Set<String> membershipSet = new Set<String>();
        
        List<genesis__Applications__c> thisApp = [SELECT id,
                                                         genesis__Term__c,
                                                         genesis__Interest_Rate__c,
                                                         genesis__Loan_Amount__c,
                                                         Pricing_Interest__c,
                                                         Discretion__c,
                                                         genesis__Discount_Rate__c,
                                                         genesis__Payment_Frequency__c,
                                                         Loan_Channel__c,
                                                         Franchise__c,
                                                         Application_Term__c,
                                                         (SELECT id,
                                                                 Name,
                                                                 clcommon__Contact__c,
                                                                 clcommon__Contact__r.id,
                                                                 clcommon__Account__r.Membership_card_colour__c,
                                                                 clcommon__Account__r.Membership_Number__c
                                                            FROM genesis__Parties__r
                                                           WHERE clcommon__Type__r.Name IN ('BORROWER','CO-BORROWER','GUARANTOR')
                                                        ORDER BY CreatedDate DESC
                                                         )
                                                    FROM genesis__Applications__c
                                                   WHERE id =: appId]; 
        if(thisApp != null && thisApp.size() > 0){
            
            if(thisApp[0].genesis__Parties__r.size() > 0)
            {
                for(clcommon__Party__c party : thisApp[0].genesis__Parties__r)
                {
                    membershipSet.add(party.clcommon__Account__r.Membership_card_colour__c);
                }
            }
            
            else
            {
                throw new NoPartyException('No party associated with the application found!!!');
            }

            thisApp[0] = applyPricing(thisApp[0]);
            thisApp[0].genesis__Discount_Rate__c = getDiscount(thisApp[0].Franchise__c, thisApp[0].Loan_Channel__c,membershipSet, thisApp[0].Pricing_Interest__c);
               
            if(thisApp[0].genesis__Discount_Rate__c == -99999)
                    {
                        thisApp[0].genesis__Discount_Rate__c =0;
                        throw new DiscountException('Failed to calculate Discount :  Multiple configurations found in Interest Discount Object');
                    }
            
            thisApp[0].genesis__Interest_Rate__c = getFinalInterest(thisApp[0].Pricing_Interest__c, thisApp[0].genesis__Discount_Rate__c, thisApp[0].Discretion__c);
            system.debug(thisApp[0].genesis__Discount_Rate__c);
            thisApp[0].genesis__Status__c = 'NEW - PRICING GENERATED';
            update thisApp[0];
            
            generateScheduleAndSavings(thisApp[0].id);
        }
    }*/
    
    //**************** assign Term and Interest Rate *****************
    
    /*public genesis__Applications__c applyPricing(genesis__Applications__c app){
        Integer term = 0;
        //WebserviceAPIUtil wb = new WebserviceAPIUtil();
        if(app!= null && app.Application_Term__c != null){
            term = Integer.valueOf(app.Application_Term__c);
        }
        list<genesis__Application_Pricing_Detail__c> queryPricingDets = [SELECT id,
                                                                                name,
                                                                                genesis__Term__c,
                                                                                genesis__Interest_Rate__c,
                                                                                genesis__Payment_Amount_Derived__c
                                                                           FROM genesis__Application_Pricing_Detail__c
                                                                          WHERE genesis__Application__c =: app.id
                                                                            AND genesis__Term__c =: term
                                                                        ];
                                                                          
        if(queryPricingDets != null && queryPricingDets.size() > 0){
            app.Pricing_Interest__c = queryPricingDets[0].genesis__Interest_Rate__c ;
            if(queryPricingDets[0].genesis__Term__c != null){
                app.Pricing_Term__c = queryPricingDets[0].genesis__Term__c;
            }
            app.Application_Term__c = app.Pricing_Term__c;
        }
        else{
            app.Pricing_Interest__c = 0;
            app.Pricing_Term__c = 0;
        }
        return app;
    }*/
    
     //**************** assign Term and Interest Rate *****************
    
    /*webservice static String applyPricingFromSkuid(Id pricingId, Decimal term){
        
        WebserviceAPIUtil wb = new WebserviceAPIUtil();
        
        
        Set<String> membershipSet = new Set<String>();
        
        list<genesis__Application_Pricing_Detail__c> queryPricingDets = [SELECT id,
                                                                                name,
                                                                                genesis__Term__c,
                                                                                genesis__Interest_Rate__c,
                                                                                genesis__Application__c,
                                                                                genesis__Application__r.id
                                                                                
                                                                           FROM genesis__Application_Pricing_Detail__c
                                                                          WHERE id =: pricingId];
                                                                            //AND genesis__Term__c =: term];
                                                                         
        if(queryPricingDets != null && queryPricingDets.size() > 0 && queryPricingDets[0].genesis__Application__c != null && queryPricingDets[0].genesis__Application__r.id != null){
            List<genesis__Applications__c> thisApp = [SELECT id,
                                                             genesis__Term__c,
                                                             genesis__Interest_Rate__c,
                                                             genesis__Loan_Amount__c,
                                                             Pricing_Interest__c,
                                                             Franchise__c,
                                                             Discretion__c,
                                                             genesis__Discount_Rate__c,
                                                             genesis__Payment_Frequency__c,
                                                             Loan_Channel__c,
                                                             Application_Term__c,
                                                             (SELECT id,
                                                                     Name,
                                                                     clcommon__Contact__c,
                                                                     clcommon__Contact__r.id,
                                                                     clcommon__Account__r.Membership_card_colour__c,
                                                                     clcommon__Account__r.Membership_Number__c
                                                                FROM genesis__Parties__r
                                                               WHERE clcommon__Type__r.Name IN ('BORROWER','CO-BORROWER','GUARANTOR')
                                                            ORDER BY CreatedDate DESC
                                                             )
                                                        FROM genesis__Applications__c
                                                       WHERE id =: queryPricingDets[0].genesis__Application__r.id];
            
            if(thisApp != null && thisApp.size() > 0){
                List< Pricing_Configuration__mdt > pricingMeta = [SELECT id,
                                                                         Pricing_Term__c,
                                                                         Min_Application_term__c,
                                                                         Max_Application_term__c
                                                                    FROM Pricing_Configuration__mdt
                                                                   WHERE Pricing_Term__c =: queryPricingDets[0].genesis__Term__c];
                                                                     //AND Min_Application_term__c <=: thisApp[0].Application_Term__c
                                                                     //AND Max_Application_term__c >=: thisApp[0].Application_Term__c];
                if(pricingMeta == null || pricingMeta.size() == 0){
                    return 'Pricing Configuration metadata setup is missing.';
                }
                else{
                    if(thisApp[0].Application_Term__c > pricingMeta[0].Max_Application_term__c ||
                       thisApp[0].Application_Term__c < pricingMeta[0].Min_Application_term__c){
                        return 'Please choose an option with valid Pricing term.';
                    }
                }
                if(thisApp[0].genesis__Parties__r.size() > 0)
                {
                for(clcommon__Party__c party : thisApp[0].genesis__Parties__r)
                                    membershipSet.add(party.clcommon__Account__r.Membership_card_colour__c);
                }
            
                else                
                throw new NoPartyException('No party associated with the application found!!!');
                 
            
                thisApp[0].Pricing_Interest__c = queryPricingDets[0].genesis__Interest_Rate__c ;
                if(queryPricingDets[0].genesis__Term__c != null)
                thisApp[0].Pricing_Term__c = queryPricingDets[0].genesis__Term__c;
                thisApp[0].Application_Term__c = thisApp[0].Pricing_Term__c;
                CalculatePricing cp = new CalculatePricing();
                thisApp[0] = cp.applyPricing(thisApp[0]);
                
                if(membershipSet.size() > 0){
                    thisApp[0].genesis__Discount_Rate__c = cp.getDiscount(thisApp[0].Franchise__c, thisApp[0].Loan_Channel__c,membershipSet, thisApp[0].Pricing_Interest__c);
                }
                else{
                    thisApp[0].genesis__Discount_Rate__c = 0;
                }
                
                thisApp[0].genesis__Interest_Rate__c = cp.getFinalInterest(thisApp[0].Pricing_Interest__c, thisApp[0].genesis__Discount_Rate__c, thisApp[0].Discretion__c);
                
                if(thisApp[0].genesis__Discount_Rate__c == -99999)
                    {
                        thisApp[0].genesis__Discount_Rate__c =0;
                        throw new DiscountException('Failed to calculate Discount :  Multiple configurations found in Interest Discount Object');
                    }
                
                update thisApp[0];
                
                genesis.SkuidNewApplication.generateSchedule(thisApp[0].id);
                return 'Pricing applied!';
            }
            else{
                // throw
                return 'No rate card found for this term.';
            }
        }
        else{
            //return 'There is no Pricing detail with this Term.';
            return 'There is no Pricing detail with this ID.';
        }
    }*/
    
    //*****************calculate Payment amount ********************
    
    public Decimal calculateEMIAmount(Decimal loanAmount,Decimal interestRate, Decimal terms, String loanFrequency){
        mfiflexUtil.VOLog voLogInstance = mfiflexUtil.VOLog.getInstance('Genesis');
        Date systemDate;
        String thisFrequency = loanFrequency;
        if(!Test.isRunningTest()){
            loan.GlobalLoanUtilFacade loanUtil= new loan.GlobalLoanUtilFacade();
            systemDate = loanUtil.getCurrentSystemDate();
        }
        else{
            systemDate = Date.Today() ;
        }
        
        String dayConvention = genesis.LendingConstants.ACCRUAL_METHOD_CODE_30_360; 
        Date expectedStartDate =  systemDate; 
        Date firstPaymentDate;// = systemDate.addMonths(1);
        
        if('DAILY'.equalsIgnoreCase(loanFrequency)){
            firstPaymentDate = systemDate.addDays(1);
        }else if('WEEKLY'.equalsIgnoreCase(loanFrequency)){
            firstPaymentDate = systemDate.addDays(7);
        }else if('BI-WEEKLY'.equalsIgnoreCase(loanFrequency) || 'FORTNIGHTLY'.equalsIgnoreCase(loanFrequency)){
            firstPaymentDate = systemDate.addDays(14);
            thisFrequency = 'BI-WEEKLY';
        }else if('MONTHLY'.equalsIgnoreCase(loanFrequency)){
            firstPaymentDate = systemDate.addMonths(1);
        }else{
            firstPaymentDate = systemDate.addMonths(1);
        }
        genesis__Lending_Calculator__c calc = new genesis__Lending_Calculator__c(genesis__Accrual_Base_Method_Code__c = dayConvention,
                                                                                 genesis__Action__c = 'CALCULATE_ALL',
                                                                                 genesis__Additional_Interest_Amount__c = 0,
                                                                                 genesis__Amortization_Calculation_Method_Code__c = 'NONE',
                                                                                 genesis__APR__c = 0,
                                                                                 genesis__Balance_Amount__c = 0,
                                                                                 genesis__Balloon_Method_Code__c = 'DUMMY',
                                                                                 genesis__Balloon_Payment_Amount__c = 0,
                                                                                 genesis__Billing_Method_Code__c = genesis.LendingConstants.REPAYMENT_INT_CALC_METHOD_DEC_BAL,
                                                                                 genesis__Contract_Date__c = expectedStartDate ,
                                                                                 genesis__Final_Payment_Amount__c = 0,
                                                                                 genesis__Residual_Amount__c = 0,
                                                                                 genesis__Financed_Amount__c = 0,
                                                                                 genesis__Financed_Fees__c = 0,
                                                                                 genesis__First_Payment_Date__c = firstPaymentDate,
                                                                                 genesis__First_Period_Calender_Days__c = 0,
                                                                                 genesis__Calculator_type__c = 'LOAN',
                                                                                 genesis__First_Period_Interest__c = 0,
                                                                                 genesis__Flexible_Repayment_Flag__c = false,
                                                                                 genesis__Installment_Method_Code__c = 'UNDEFINED',
                                                                                 genesis__Interest_Amount__c = 0,
                                                                                 genesis__Interest_Only_Period__c = 0,
                                                                                 genesis__Loan_amount__c = loanAmount,
                                                                                 genesis__Payment_Amount__c =  0,
                                                                                 genesis__Payment_Frequency_Code__c = thisFrequency,
                                                                                 genesis__Prepaid_Fees__c = 0,
                                                                                 genesis__Principal_Payment_Amount__c = 0,
                                                                                 genesis__Rate__c = interestRate,
                                                                                 genesis__Repayment_Type_Code__c = 'UNDEFINED',
                                                                                 genesis__Term__c = terms,
                                                                                 genesis__Time_Counting_Method_Code__c = 'Actual Days (366)',//genesis.LendingConstants.TIME_COUNTING_ACTUAL_DAYS_366,
                                                                                 genesis__Total_Finance_Charge__c = 0,
                                                                                 genesis__Total_Financed_Amount__c = 0,
                                                                                 genesis__Total_Of_Payments__c = 0,
                                                                                 genesis__Due_Day__c = firstPaymentDate.day());          
        try{     
            List<genesis__Amortization_Schedule__c> emiScheduleNew = genesis.LendingCalculator.calculate(calc, NULL); 
            if(emiScheduleNew.size() > 0){
                return emiScheduleNew.get(0).genesis__Total_Due_Amount__c;
            }
            return 0;
        }
        catch(Exception e){
            voLogInstance.logException(1001, '[CalculatePricing.calculateEMIAmount] Exception at line : ' + e.getLineNumber(), e);
            voLogInstance.commitToDB();
            return 0;    
        }
    }
    
    //*************** Get discount rate ***********************
    
    /*public Decimal getDiscount(String franchise, String loanChannel, Set<String> membershipSet , Decimal baseInterestRate){
        Decimal discount = 0;
        if(baseInterestRate == null){
            baseInterestRate = 0;
        }
        baseInterestRate = baseInterestRate/100 ;
        Boolean isDiscountonBaseRate=false;
        List<Interest_Discount__c> intDiscounts = [SELECT id,
                                                          Discount__c,
                                                          Is_Discount_on_Base_Rate__c,
                                                          Membership_Colour__c
                                                     FROM Interest_Discount__c
                                                    WHERE Franchise__c =: franchise
                                                    AND 
                                                    (Loan_Channel__c = : loanChannel
                                                    OR
                                                    Loan_Channel__c = : 'All')
                                                    AND Membership_Colour__c in :membershipSet
                                                   order by Discount__c desc];
        
        if(intDiscounts != null && intDiscounts.size() > 0)
        {
            Set<String> colourSet=new Set<String>();
            for(Interest_Discount__c interest: intDiscounts)
            {
              colourSet.add(interest.Membership_Colour__c);
            }
            if(colourSet.size() < intDiscounts.size())    
            {
                return -99999;
            }   
            else 
            {
            discount = intDiscounts[0].Discount__c != null ? intDiscounts[0].Discount__c : 0;
            isDiscountonBaseRate = intDiscounts[0].Is_Discount_on_Base_Rate__c!=null ? intDiscounts[0].Is_Discount_on_Base_Rate__c : false;
            if(isDiscountonBaseRate) {discount = discount * baseInterestRate ;}
            }
        }
    
        
        return discount;
    }*/
    
    //*************** Get final interest rate ***********************
    
    /*public Decimal getFinalInterest(Decimal pricingInterest, Decimal discountRate, Decimal discretion){
        Decimal finalInterest = 0;
        
        if(pricingInterest == null){
            pricingInterest = 0;
        }
        if(discountRate == null){
            discountRate = 0;
        }
        if(discretion == null){
            discretion = 0;
        }
        finalInterest = pricingInterest - discountRate + discretion ;
        
        return finalInterest;
    }
    
    @future
    public static void generateScheduleAndSavings(String appId){
        
            genesis.SkuidNewApplication.generateSchedule(appId);
            
            List< genesis__Application_Pricing_Detail__c > updatePricingDets = new List< genesis__Application_Pricing_Detail__c >();
            genesis__Applications__c requeryApp = [SELECT id,
                                                          genesis__Loan_Amount__c,
                                                          Pricing_Interest__c,
                                                          genesis__Term__c,
                                                          genesis__Payment_Frequency__c,
                                                          genesis__Total_Estimated_Interest__c,
                                                          genesis__Payment_Amount__c
                                                     FROM genesis__Applications__c 
                                                    WHERE id =: appId];
            CalculatePricing cp = new CalculatePricing();
            
            Decimal baseRepaymentAmt = cp.calculateEMIAmount(requeryApp.genesis__Loan_Amount__c, requeryApp.Pricing_Interest__c, requeryApp.genesis__Term__c, requeryApp.genesis__Payment_Frequency__c);
            Decimal savings = 0;
            if(baseRepaymentAmt != null && requeryApp.genesis__Payment_Amount__c != null && requeryApp.genesis__Term__c != null){
                if(requeryApp.genesis__Payment_Amount__c != 0){
                    savings = (baseRepaymentAmt - requeryApp.genesis__Payment_Amount__c) * requeryApp.genesis__Term__c;
                }
                else{
                    savings = 0;
                }
                requeryApp.Estimated_Savings__c = savings;
                update requeryApp;
            }
    }*/
    
    //public class DiscountException extends Exception{}
    //public class NoPartyException extends Exception{}
}