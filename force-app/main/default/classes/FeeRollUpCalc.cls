/**Class to calculate Fee Roll up**/
global class FeeRollUpCalc{

    @InvocableMethod(label='Calculate roll up' description='Calculate roll up')
    webservice static void calculateRollUp(List<Id> appId) {
       mfiflexUtil.VOLog voLogInstance1 = mfiflexUtil.VOLog.getInstance('Genesis');
       List<genesis__applications__c> apps = [select id,
                                                     name, 
                                                     Capitalised_Fee__c,
                                                     (select id,
                                                             Final_Fee_Amount__c,
                                                             clcommon__Fee_Definition__c,
                                                             clcommon__Fee_Definition__r.clcommon__Capitalize__c 
                                                        from genesis__Fees__r) 
                                                from genesis__applications__c 
                                               where id in :appId
                                             ];
       for(genesis__applications__c app: apps) {
           app.genesis__Total_Fee_Amount__c = 0;
           app.Capitalised_Fee__c = 0;
           for(clcommon__Fee__c fee: app.genesis__Fees__r) {
               app.genesis__Total_Fee_Amount__c += fee.Final_Fee_Amount__c;
               if(fee.clcommon__Fee_Definition__c != null && fee.clcommon__Fee_Definition__r.clcommon__Capitalize__c){
                   app.Capitalised_Fee__c += fee.Final_Fee_Amount__c;
               }
           }
           System.debug('DIsplay Collection ' + app.id +' '+ app.genesis__Total_Fee_Amount__c );
       }

       //Insert total Fees into associated application object
       try {
           update apps;       
       } 
       catch(DmlException e) {
           System.debug('An unexpected error has occurred: ' + e.getMessage());
           voLogInstance1.logException(1001, '[FeeRollUpCalc.calculateRollUp] Exception at line : ' + e.getLineNumber(), e);
           voLogInstance1.commitToDB();
       }
       System.debug('CALCULATION CALLED');
    }
}