/**
 * @File Name          : JWF_RunScorecard.cls
 * @Description        : 
 * @Author             : chirag.gupta@q2.com
 * @Group              : 
 * @Last Modified By   : chirag.gupta@q2.com
 * @Last Modified On   : 6/9/2020, 2:35:39 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    6/4/2020   chirag.gupta@q2.com     Initial Version
**/
public class JWF_RunScorecard implements IFlowExecution{
    public void execute(Id applicationId,  JWF_Application_Execution_Log__c appExecutionLog){
        try{
            String result = genesis.ScorecardAPI.generateScorecard(applicationId);
            appExecutionLog.Message__c += ('Scorecard response:' + result);
            Map<String, Object> mapExecutionLog = new Map<String, Object>();
            mapExecutionLog.put('appExecutionLog', appExecutionLog);

            ApplicationExecutionAction action = new ApplicationExecutionAction(applicationId);
            action.changeStatus(mapExecutionLog);
            action.doInvokeAction(null);                           
        }
        catch(Exception e){ExceptionLog.insertExceptionLog(e,'Scorecard generation failed'+'---'+e.getMessage()+'---'+e.getStackTraceString(),applicationId);}          
    }
}