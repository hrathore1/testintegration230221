/**
 * @File Name          : SAT_SubmitToCredit.cls
 * @Description        : Stages and task - While submitting to credit stage
 * @Author             : chirag.gupta@q2.com
 * @Group              : 
 * @Last Modified By   : chirag.gupta@q2.com
 * @Last Modified On   : 6/25/2020, 12:37:18 PM
 * @Modification Log   : 
 * Ver       Date            Author                 Modification
 * 1.0    6/9/2020   chirag.gupta@q2.com     Initial Version
**/
public class SAT_SubmitToCredit implements StageExecutionInterface {
    public List<String> executeValidations(Id applicationId){
        try{
            List<String> returnList = new List<String>();
            if(String.isNotBlank(applicationId)){
                List<genesis__Applications__c> appList = [SELECT Id, 
                                                                 name,
                                                                 genesis__CL_Product__c,
                                                                 genesis__CL_Product__r.Allow_Collateral__c,
                                                                 genesis__CL_Product__r.Max_Count_of_Collaterals__c,
                                                                 genesis__CL_Product_Name__c,
                                                                 (SELECT Id,
                                                                         genesis__Collateral__r.Final_Collateral_Value__c
                                                                    FROM genesis__Application_Collaterals__r
                                                                 ),
                                                                 (SELECT id,
                                                                         name,
                                                                         genesis__Contact__c,
                                                                         genesis__Contact__r.accountId,
                                                                         Total_Months_on_Job__c,
                                                                         genesis__Is_Current_Employer__c
                                                                    FROM genesis__Employment_Information__r
                                                                   WHERE Total_Months_on_Job__c > 0
                                                                     AND genesis__Contact__c != null
                                                                 )
                                                            FROM genesis__Applications__c 
                                                           WHERE Id = :applicationId
                                                         ];
                                                         
                Map<Id, List<genesis__Employment_Information__c>> mapAccountIdVsEmployment = new Map<Id, List<genesis__Employment_Information__c>>();
                for(genesis__Applications__c appInst : appList){
                    List<genesis__Employment_Information__c> employmentList = appInst.genesis__Employment_Information__r;
                    if(employmentList != null && employmentList.size() > 0){
                        for(genesis__Employment_Information__c empInfo : employmentList){
                            if(empInfo.genesis__Contact__c != null
                                && !mapAccountIdVsEmployment.containsKey(empInfo.genesis__Contact__r.accountId)){
                                mapAccountIdVsEmployment.put(empInfo.genesis__Contact__r.accountId, new List<genesis__Employment_Information__c>());
                            }
                            mapAccountIdVsEmployment.get(empInfo.genesis__Contact__r.accountId).add(empInfo);
                        }
                    }
                    
                    if(appInst.genesis__CL_Product__c != null && appInst.genesis__CL_Product__r.Allow_Collateral__c){
                        List<genesis__Application_Collateral__c> listAppColls = appInst.genesis__Application_Collaterals__r;
                        if(listAppColls == null || listAppColls.size() == 0){
                            returnList.add(Label.Application_Security_Validation_Error_Message);
                        }
                        else{
                            Integer numberOfcolls = 0;
                            for(genesis__Application_Collateral__c appColls : listAppColls){
                                numberOfcolls++;
                            }
                            Integer maxNoOfSecurities = (appInst.genesis__CL_Product__r.Max_Count_of_Collaterals__c == null ? 0 : Integer.valueOf(appInst.genesis__CL_Product__r.Max_Count_of_Collaterals__c));
                            if(numberOfcolls == 0){
                                returnList.add('System should have at least one primary security.');
                            }
                            else if(numberOfcolls > maxNoOfSecurities){
                                returnList.add('Number of securities can not exceed count of ' + maxNoOfSecurities);
                            }
                        }
                    }
                }
                
                Set<String> partyTypeSet = new Set<String>{'GUARANTOR','CO-BORROWER','BORROWER'};
                Set<String> addressTypeSet = new Set<String>{'Residential', 'Mailing'};
                List<clcommon__Party__c> partyList = [SELECT Id,
                                                             clcommon__Account__c,
                                                             genesis__Credit_Check_Consent__c,
                                                             Electronic_Identification_Consent__c,
                                                             clcommon__Account__r.Name,
                                                             clcommon__Account__r.Age__pc,
                                                             Party_Type_Name__c,
                                                             Number_of_Dependents__c,
                                                             PEP__c,
                                                             (SELECT id,
                                                                     Address_Type__c,
                                                                     Tenure_Total_Months__c,
                                                                     clcommon__Account__c,
                                                                     Is_Current__c
                                                                FROM Addresses__r
                                                               WHERE Address_Type__c IN :addressTypeSet
                                                                 AND Tenure_Total_Months__c > 0
                                                             )
                                                        FROM clcommon__Party__c
                                                       WHERE genesis__Application__c =: applicationId
                                                         AND Party_Type_Name__c IN :partyTypeSet
                                                         AND clcommon__Account__c != null
                                                     ];
                
                Integer countForCOBORROWER = 0;
                Integer countForBORROWER = 0;
                genesis__Org_Parameters__c orgParams = genesis__Org_Parameters__c.getOrgDefaults();
                Decimal validationMonthsAddress = (orgParams.Address_Validation_Months__c == null ? 36 : orgParams.Address_Validation_Months__c);
                Decimal validationMonthsEmployment = (orgParams.Employment_Validation_Months__c == null ? 36 : orgParams.Employment_Validation_Months__c);
                
                if(partyList != null && partyList.size() > 0){
                    for(clcommon__Party__c partyRecord :  partyList){
                        String partyType = partyRecord.Party_Type_Name__c;
                        String customerName = partyRecord.clcommon__Account__r.Name;
                        String messagePrefix = 'For ' + customerName + ' (Party Type: '+ partyType +'), ';
                        
                        //Validating party data
                        if('BORROWER'.equalsIgnoreCase(partyRecord.Party_Type_Name__c)){
                            countForBORROWER++;
                        }
                        if('CO-BORROWER'.equalsIgnoreCase(partyRecord.Party_Type_Name__c)){
                            countForCOBORROWER++;    
                        }
                        
                        if(!partyRecord.genesis__Credit_Check_Consent__c){
                            returnList.add(messagePrefix + Label.Application_Account_Credit_Check_Consent_Validation_Error_Message);
                        }
                        if(partyRecord.Number_of_Dependents__c == null){
                            returnList.add(messagePrefix + 'information about number of dependents is not provided.');
                        }
                        if(partyRecord.PEP__c == null){
                            returnList.add(messagePrefix + 'information about PEP is not provided.');
                        }
                        if(partyRecord.clcommon__Account__c != null 
                            && (partyRecord.clcommon__Account__r.Age__pc == null 
                            || partyRecord.clcommon__Account__r.Age__pc <= 0)){
                            returnList.add(messagePrefix + Label.Application_Account_DOB_Validation_Error_Message);
                        }
                        
                        //Validating address records
                        List<clcommon__Address__c> listAddresses = partyRecord.Addresses__r;
                        Decimal countCurrentResidentialAdds = 0;
                        Decimal countOfCurrentMailingAdds = 0;
                        Decimal sumOfCurrentResidentialMonths = 0;
                        Decimal sumOfPreviousResidentialMonths = 0;
                        if(listAddresses != null && listAddresses.size() > 0){
                            for(clcommon__Address__c addr : listAddresses){
                                if('Residential'.equalsIgnoreCase(addr.Address_Type__c)){
                                    if(addr.Is_Current__c){
                                        countCurrentResidentialAdds++;
                                        sumOfCurrentResidentialMonths += addr.Tenure_Total_Months__c;
                                    }
                                    else{
                                        sumOfPreviousResidentialMonths += addr.Tenure_Total_Months__c;
                                    }
                                }
                                else if('Mailing'.equalsIgnoreCase(addr.Address_Type__c)){
                                    if(addr.Is_Current__c){
                                        countOfCurrentMailingAdds++;
                                    }
                                }
                            }
                        }
                        
                        if(countCurrentResidentialAdds == 0){
                            returnList.add(messagePrefix + Label.Application_Account_Residential_Address_Validation_Error_Message_2);
                        }
                        else if(countCurrentResidentialAdds > 1){
                            returnList.add(messagePrefix + Label.Address_validation_message_1);
                        }
                        else if(sumOfCurrentResidentialMonths < validationMonthsAddress && sumOfPreviousResidentialMonths == 0){
                            returnList.add(messagePrefix + 'require at least one previous residential address as current provided tenure is less than 36 months.');
                        }
                        //else if((sumOfCurrentResidentialMonths + sumOfPreviousResidentialMonths) < validationMonthsAddress){
                        //    returnList.add(messagePrefix + 'require at least one more previous residential address as overall tenure is less than 36 months.');
                        //}
                        
                        if(countOfCurrentMailingAdds == 0){
                            returnList.add(messagePrefix + Label.Application_Account_Mailing_Address_Validation_Error_Message);
                        }
                        else if(countOfCurrentMailingAdds > 1) {
                            returnList.add(messagePrefix + Label.Address_validation_message_2);
                        }
                        
                        //Validating employment Information
                        List<genesis__Employment_Information__c> listEmpInfo = mapAccountIdVsEmployment.get(partyRecord.clcommon__Account__c);
                        Decimal countCurrentEmployment = 0;
                        Decimal sumCurrentEmploymentTenure = 0;
                        Decimal sumPreviousEmploymentTenure = 0;
                        if(listEmpInfo != null && listEmpInfo.size() > 0){
                            for(genesis__Employment_Information__c empInfo : listEmpInfo){
                                if(empInfo.genesis__Is_Current_Employer__c){
                                    countCurrentEmployment++;
                                    sumCurrentEmploymentTenure += empInfo.Total_Months_on_Job__c;
                                }
                                else{
                                    sumPreviousEmploymentTenure += empInfo.Total_Months_on_Job__c;
                                }
                            }
                        }
                        
                        if(countCurrentEmployment == 0){
                            returnList.add(messagePrefix + Label.Application_Employment_Data_Validation_Error_Message_2);
                        }
                        else if(countCurrentEmployment > 1){
                            returnList.add(messagePrefix + 'has more than one current employment information present.');
                        }
                        else if(sumCurrentEmploymentTenure < validationMonthsEmployment && sumPreviousEmploymentTenure == 0){
                            returnList.add(messagePrefix + 'require at least one previous employment as current provided tenure is less than 36 months.');
                        }
                        //else if((sumCurrentEmploymentTenure + sumPreviousEmploymentTenure) < validationMonthsEmployment){
                        //    returnList.add(messagePrefix + 'require at least one more previous employment as overall tenure is less than 36 months.');
                        //}
                    }
                }
                    
                if(countForBORROWER == 0){
                    returnList.add(Label.Application_Borrow_Mandatory_Validation_Error_Message);
                }
                else if(countForBORROWER > 1){
                    returnList.add('This Application has more than one BORROWER type party.');
                }

                if(countForCOBORROWER > 1){
                    returnList.add(Label.Application_COBORROWER_Validation_Error_Message);
                }
            }
            else{
                returnList.add('Passed application id is invalid.');
            }
             
            return returnList;            
        }
        catch(Exception e){throw e;}          
    }

    public String executeNextDeptActions(Id applicationId){
        try{
            return executeActions(applicationId);
            //return System.label.TAG_SUCCESS;                     
        }
        catch(Exception e){throw e;}          
    }

    public String executePreviousDeptActions(Id applicationId){
        try{
            //executeActions(applicationId);
            return System.label.NEXT_STAGE_SUBMIT_SUCCESS_MSG;
        }
        catch(Exception e){throw e;}          
    }

    public String executeActions(Id applicationId){
        try{
            genesis__Applications__c appInst = new genesis__Applications__c(Id = applicationId);
            appInst.genesis__Status__c = 'NEW - CREDIT SUBMISSION IN PROCESS';
            update appInst;

            ID jobID = System.enqueueJob(new ApplicationExecutionAction(applicationId));
            return System.label.NEXT_STAGE_SUBMIT_SUCCESS_MSG;                     
        }
        catch(Exception e){throw e;}
    }
}