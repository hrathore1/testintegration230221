/**
 * @description       : 
 * @author            : chirag.gupta@q2.com
 * @group             : 
 * @last modified on  : 09-25-2020
 * @last modified by  : chirag.gupta@q2.com
 * Modifications Log 
 * Ver   Date         Author                Modification
 * 1.0   09-21-2020   chirag.gupta@q2.com   Initial Version
**/
public class CalculateGrossIncome {
    public static void updateValues(genesis__Applications__c appInst){
    
        List<genesis__Income__c> listIncomes = [SELECT id,
                                                       name,
                                                       Net_Annual_Income__c,
                                                       genesis__Application__c,
                                                       Gross_Annual_Income__c,
                                                       Party_Type__c 
                                                  FROM genesis__Income__c 
                                                 WHERE genesis__Application__c = :appInst.Id
                                                   AND Incur_Tax__c = false
                                               ];
                                                      
        List<Income_Tax_Configuration__c> listITC = [SELECT id,
                                                            name,
                                                            Income_From__c,
                                                            Net_Income_To__c,
                                                            Cumulative_Max_Tax__c,
                                                            Net_Income_From__c,
                                                            Tax__c                                                             
                                                       FROM Income_Tax_Configuration__c 
                                                    ];
            
        Decimal nonTaxableIncomeBorrower = 0;
        Decimal nonTaxableIncomeCoBorrower = 0;
        Decimal nonTaxableIncomeGuarantor = 0;
                                                        
        for(genesis__Income__c income : listIncomes){
            if('BORROWER'.equalsIgnoreCase(income.Party_Type__c)){
                nonTaxableIncomeBorrower += income.Net_Annual_Income__c;
            }
            else if('CO-BORROWER'.equalsIgnoreCase(income.Party_Type__c)){
                nonTaxableIncomeCoBorrower += income.Net_Annual_Income__c;
            }
            else if('GUARANTOR'.equalsIgnoreCase(income.Party_Type__c)){
                nonTaxableIncomeGuarantor += income.Net_Annual_Income__c;
            }
        }
                                                    
        Decimal netYearlyTaxableIncomeBorrower = (appInst.Net_Annual_Income_Borrower__c == null ? 0 : (appInst.Net_Annual_Income_Borrower__c - nonTaxableIncomeBorrower));
        Decimal netYearlyTaxableIncomeCoBorrower = (appInst.Net_Annual_Income_Co_Borrower__c == null ? 0 : appInst.Net_Annual_Income_Co_Borrower__c - nonTaxableIncomeCoBorrower);
        Decimal netYearlyTaxableIncomeGuarantor = (appInst.Net_Annual_Income_Guarantor__c == null ? 0 : appInst.Net_Annual_Income_Guarantor__c - nonTaxableIncomeGuarantor);

        for(Income_Tax_Configuration__c itc : listITC){
            //For Borrower
            if(itc.Net_Income_From__c <= netYearlyTaxableIncomeBorrower && itc.Net_Income_To__c >= netYearlyTaxableIncomeBorrower){
                appInst.Gross_Annual_Income_Borrower__c = ((netYearlyTaxableIncomeBorrower 
                                                            + itc.Cumulative_Max_Tax__c 
                                                            - itc.Income_From__c 
                                                            * itc.Tax__c 
                                                            * 0.01) 
                                                            / (1 - (itc.Tax__c * 0.01))) + nonTaxableIncomeBorrower;    
            }

            //For Co-Borrower
            if(itc.Net_Income_From__c <= netYearlyTaxableIncomeCoBorrower && itc.Net_Income_To__c >= netYearlyTaxableIncomeCoBorrower){
                appInst.Gross_Annual_Income_Co_Borrower__c = ((netYearlyTaxableIncomeCoBorrower 
                                                               + itc.Cumulative_Max_Tax__c 
                                                               - itc.Income_From__c 
                                                               * itc.Tax__c 
                                                               * 0.01) 
                                                               / (1 - (itc.Tax__c * 0.01))) + nonTaxableIncomeCoBorrower;    
            }

            //For Guarantor
            if(itc.Net_Income_From__c <= netYearlyTaxableIncomeGuarantor && itc.Net_Income_To__c >= netYearlyTaxableIncomeGuarantor){
                appInst.Gross_Annual_Income_Guarantor__c = ((netYearlyTaxableIncomeGuarantor 
                                                             + itc.Cumulative_Max_Tax__c 
                                                             - itc.Income_From__c 
                                                             * itc.Tax__c 
                                                             * 0.01) 
                                                             / (1 - (itc.Tax__c * 0.01))) + nonTaxableIncomeGuarantor ;    
            }
        }
    }
}