//@isTest
public class CalculatePricing_Test {
  
    /*@TestSetUp
    public static void setCalculatePricing_Test()
    {
   genesis__Applications__c app = RACVTestHelper.getApplication(RACVTestHelper.getAccount());
    }

    public static testmethod void testDiscountFromSkuid()
    {
        genesis__Applications__c app = RACVTestHelper.getApplication(RACVTestHelper.getAccount());
        Test.startTest();
        CalculatePricing.getDiscountFromSkuid(app.Id);

        system.assertEquals(1,1,'Function_failed');
        Test.stopTest();
    }
    
    public static testmethod void testPricingDetails()
    {
      genesis__Applications__c app = RACVTestHelper.getApplication(RACVTestHelper.getAccount());
        Test.startTest();
        CalculatePricing.pricingDetails(app.Id);
        system.assertEquals(1,1,'Function_failed');
        Test.stopTest();
    }
    
    static testmethod void testGeneratePricing()
    {
        genesis__Applications__c app = RACVTestHelper.getApplication(RACVTestHelper.getAccount());
        Test.startTest();
        new CalculatePricing().generatePricing(app.Id);
        system.assertEquals(1,1,'Function_failed');
        Test.stopTest();
    }
    
    static testmethod void testApplyPricing()
    {
        genesis__Applications__c app = RACVTestHelper.getApplication(RACVTestHelper.getAccount());
        Test.startTest();
        new CalculatePricing().applyPricing(app);
        system.assertEquals(1,1,'Function_failed');
        Test.stopTest();
    }
    
        static testmethod void testApplyPricing_enhanced()
    {
        genesis__Applications__c app = RACVTestHelper.getApplication_Enhanced(RACVTestHelper.getAccount());
        Test.startTest();
        new CalculatePricing().applyPricing(app);
        system.assertEquals(1,1,'Function_failed');
        Test.stopTest();
    }
    
    static testmethod void testApplyPricingFromSkuid()
    {
        genesis__Applications__c app = RACVTestHelper.getApplication(RACVTestHelper.getAccount());
        Test.startTest();
        CalculatePricing.applyPricingFromSkuid(new genesis__Application_Pricing_Detail__c().id, app.genesis__Term__c);
        system.assertEquals(1,1,'Function_failed');
        Test.stopTest();
    }
    
        static testmethod void testApplyPricingFromSkuid_Enhanced()
    {
        genesis__Applications__c app = RACVTestHelper.getApplication_Enhanced(RACVTestHelper.getAccount());
        Test.startTest();
        CalculatePricing.applyPricingFromSkuid(new genesis__Application_Pricing_Detail__c().id, app.genesis__Term__c);
        system.assertEquals(1,1,'Function_failed');
        Test.stopTest();
    }
    
    
    
       static testmethod void testCalculateEMIAmount_Freq_Daily()
    {
        genesis__Applications__c app = RACVTestHelper.getApplication(RACVTestHelper.getAccount());
        Test.startTest();
        new CalculatePricing().calculateEMIAmount(app.genesis__Loan_Amount__c , app.genesis__Interest_Rate__c, app.genesis__Term__c, 'DAILY');        
        system.assertEquals(1,1,'Function_failed');
        Test.stopTest();
    }
    
     static testmethod void testCalculateEMIAmount_Freq_Monthly()
    {
        genesis__Applications__c app = RACVTestHelper.getApplication(RACVTestHelper.getAccount());
        Test.startTest();
        new CalculatePricing().calculateEMIAmount(app.genesis__Loan_Amount__c , app.genesis__Interest_Rate__c, app.genesis__Term__c, 'MONTHLY');        
        system.assertEquals(1,1,'Function_failed');
        Test.stopTest();
    }
    
         static testmethod void testCalculateEMIAmount_Freq_Weekly()
    {
        genesis__Applications__c app = RACVTestHelper.getApplication(RACVTestHelper.getAccount());
        Test.startTest();
        new CalculatePricing().calculateEMIAmount(app.genesis__Loan_Amount__c , app.genesis__Interest_Rate__c, app.genesis__Term__c, 'WEEKLY');        
        system.assertEquals(1,1,'Function_failed');
        Test.stopTest();
    }
    
             static testmethod void testCalculateEMIAmount_Freq_ByWeekly()
    {
        genesis__Applications__c app = RACVTestHelper.getApplication(RACVTestHelper.getAccount());
        Test.startTest();
        new CalculatePricing().calculateEMIAmount(app.genesis__Loan_Amount__c , app.genesis__Interest_Rate__c, app.genesis__Term__c, 'BI-WEEKLY');        
        system.assertEquals(1,1,'Function_failed');
        Test.stopTest();
    }
    
               static testmethod void testCalculateEMIAmount_Freq_FORTNIGHTLY()
    {
        genesis__Applications__c app = RACVTestHelper.getApplication(RACVTestHelper.getAccount());
        Test.startTest();
        new CalculatePricing().calculateEMIAmount(app.genesis__Loan_Amount__c , app.genesis__Interest_Rate__c, app.genesis__Term__c, 'FORTNIGHTLY');        
        system.assertEquals(1,1,'Function_failed');
        Test.stopTest();
    }
    
     static testmethod void testCalculateEMIAmount_Freq_null()
    {
        genesis__Applications__c app = RACVTestHelper.getApplication(RACVTestHelper.getAccount());
        Test.startTest();
        new CalculatePricing().calculateEMIAmount(app.genesis__Loan_Amount__c , app.genesis__Interest_Rate__c, app.genesis__Term__c, null);        
        system.assertEquals(1,1,'Function_failed');
        Test.stopTest();
    }
    static testmethod void testScheduleAndSavings()
    {
    genesis__Applications__c app = RACVTestHelper.getApplication(RACVTestHelper.getAccount());
    Test.startTest();
    CalculatePricing.generateScheduleAndSavings(app.Id);
    system.assertEquals(1,1,'Function_failed');
    Test.stopTest();
    }
    
    static testmethod void testDiscount()
    {
    Test.startTest();
    //Decimal discount = new CalculatePricing().getDiscount('RACV', '', 10);
    //system.assertEquals(1,1,'Function_failed');
    Test.stopTest();
    }
    
    static testmethod void testDiscount_BaseRateNull()
    {
    Test.startTest();
    //Decimal discount = new CalculatePricing().getDiscount('RACV', '', null);
    //system.assertEquals(1,1,'Function_failed');
    Test.stopTest();
    }
        

    static testmethod void testFinalInterest_neg_Discretion()
    {
    Test.startTest();
    //Decimal finalInterest = new CalculatePricing().getFinalInterest(10, 1, -1);
    //system.assertEquals(1,1,'Function_failed');
    Test.stopTest();
    }
    
    static testmethod void testFinalInterest_pos_Discretion()
    {
    Test.startTest();
    //Decimal finalInterest = new CalculatePricing().getFinalInterest(10, 1, 1);
    //system.assertEquals(1,1,'Function_failed');
    Test.stopTest();
    }
    
    static testmethod void testFinalInterest_all_NUll()
    {
    Test.startTest();
    //Decimal finalInterest = new CalculatePricing().getFinalInterest(null, null, null);
    //system.debug('final interest :: ' + finalInterest);
    //system.assertEquals(1,1,'Function_failed');
    Test.stopTest();
    }*/
}