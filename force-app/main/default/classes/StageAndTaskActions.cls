/**
 * @File Name          : StageAndTaskActions.cls
 * @Description        : 
 * @Author             : chirag.gupta@q2.com
 * @Group              : 
 * @Last Modified By   : chirag.gupta@q2.com
 * @Last Modified On   : 09-28-2020
 * @Modification Log   : 
 * Ver       Date            Author                 Modification
 * 1.0    6/9/2020   chirag.gupta@q2.com     Initial Version
**/
global class StageAndTaskActions{

    webservice static String submitTask(String taskId){
        try{
            String validationMessage = evaluateRulesForTask(taskId);
            
            if(System.label.TAG_SUCCESS.equalsIgnoreCase(validationMessage)){
                String apiMessage = genesis.LoanDashBoard.updateTaskList(new List<String>{taskId});
                if(apiMessage.equalsIgnoreCase('"SUCCESS"')){
                    return System.label.TAG_SUCCESS;
                }
                return apiMessage;
            }
            else{
                return validationMessage;
            }
        }
        catch(Exception e){
            return 'Error while submitting task: ' + e.getMessage() + ', at line :' + e.getLineNumber();
        }
    }
    
    webservice static String submitToNextDepartment(String appId){
        try{
            List<genesis__Application_Department__c> activeDept = [SELECT id,
                                                                          genesis__Department__c,
                                                                          genesis__Application__c,
                                                                          genesis__Application__r.genesis__CL_Product__c
                                                                     FROM genesis__Application_Department__c
                                                                    WHERE genesis__Status__c = 'Active'
                                                                      AND genesis__Application__c = :appId
                                                                  ];
            
            if(activeDept != null && activeDept.size() > 0){
            
                /*List<Task> taskList = [SELECT Id,
                                              Status,
                                              IsClosed,
                                              genesis__Completion_Mandatory__c,
                                              genesis__Application__c
                                         FROM Task
                                        WHERE WhatId = :appId
                                          AND genesis__Application__c = :appId
                                          AND genesis__Task_Setup__c != NULL
                                          AND genesis__Department__c = :activeDept.get(0).genesis__Department__c
                                          AND genesis__Is_Archived__c = false
                                          AND genesis__Completion_Mandatory__c = true
                                          AND Status != 'Completed'
                                      ];*/
                                      
                List<Task> taskList = [SELECT Id,
                                              Status,
                                              IsClosed,
                                              genesis__Completion_Mandatory__c,
                                              genesis__Application__c,
                                              genesis__Task_Setup__c,
                                              genesis__Department__c,
                                              Is_Exception_Task__c
                                         FROM Task
                                        WHERE WhatId = :appId
                                          AND genesis__Application__c = :appId
                                          AND genesis__Is_Archived__c = false
                                          AND Status != 'Completed'
                                      ];
                
                Integer countMandatoryActiveDeptTasks = 0;   
                Integer countIncompleteExceptionTasks = 0;                   
                for(Task tsk : taskList){
                    if(tsk.genesis__Completion_Mandatory__c 
                         && tsk.genesis__Task_Setup__c != null
                         && String.valueOf(tsk.genesis__Department__c).equalsIgnoreCase(String.valueOf(activeDept.get(0).genesis__Department__c))){
                        countMandatoryActiveDeptTasks++;
                    }
                    else if(tsk.Is_Exception_Task__c){
                        countIncompleteExceptionTasks++;
                    }
                }
                
                //Check if all the mandatory tasks for the department are complete or not
                //String isFineToSubmitFurther = genesis.LoanDashBoard.showSubmitToNxtDeptBtn(appId);
                String finalReturnMsg = '';
                if(countMandatoryActiveDeptTasks > 0){
                    finalReturnMsg += '=> Please complete all the mandatory tasks first.<br>';
                }
                //if(countIncompleteExceptionTasks > 0){
                //    finalReturnMsg += '=> Please complete all the exception tasks first.<br>';
                //}
                if(!String.isBlank(finalReturnMsg)){
                    return finalReturnMsg;
                }
                
                List<genesis__Product_Department_Junction__c> deptJunctionList = [SELECT id, 
                                                                                         name,
                                                                                         genesis__Department__c,
                                                                                         genesis__CL_Product__c,
                                                                                         genesis__Department_No__c,
                                                                                         genesis__Department__r.name,
                                                                                         Class_Name__c,
                                                                                         Is_REHASH_Required__c,
                                                                                         (SELECT id,
                                                                                                 name,
                                                                                                 Rule__c
                                                                                            FROM Product_Dept_Rule_Associations__r
                                                                                         )
                                                                                    FROM genesis__Product_Department_Junction__c
                                                                                   WHERE genesis__CL_Product__c = :activeDept.get(0).genesis__Application__r.genesis__CL_Product__c
                                                                                 ];
                                                                             
                if(deptJunctionList != null && deptJunctionList.size() > 0){
                    genesis__Product_Department_Junction__c currDeptJuncRec = null;
                    for(genesis__Product_Department_Junction__c pdj : deptJunctionList){
                        if(pdj.genesis__Department__c == activeDept.get(0).genesis__Department__c){
                            currDeptJuncRec = pdj;
                            break;
                        }
                    }

                    if(currDeptJuncRec != null){
                        String validationMessageFromCustomClass = System.label.TAG_SUCCESS;
                        String validationMessageFromRules = System.label.TAG_SUCCESS;

                        List<Product_Dept_Rule_Association__c> listPDRA = currDeptJuncRec.Product_Dept_Rule_Associations__r;
                        if(listPDRA != null && listPDRA.size() > 0){
                            Set<Id> setRuleIds = new Set<Id>();
                            for(Product_Dept_Rule_Association__c pdj : listPDRA){
                                setRuleIds.add(pdj.Rule__c);
                            }
                            validationMessageFromRules = evaluateRules(setRuleIds, (SObject) new genesis__Applications__c(Id = appId));
                        }
                        
                        StageExecutionInterface classImplInst = null;
                        if(!String.isBlank(currDeptJuncRec.Class_Name__c)){
                            Type classInst = Type.forName(currDeptJuncRec.Class_Name__c);
                            if(classInst != null){
                                classImplInst = (StageExecutionInterface) classInst.newInstance();
                                List<String> validationMsgs = classImplInst.executeValidations(appId);
                                validationMessageFromCustomClass = convertListToString(validationMsgs); 
                            }
                            else{
                                validationMessageFromCustomClass = ('Implementation for class is missing: ' + currDeptJuncRec.Class_Name__c);
                            }
                        }

                        if(!System.label.TAG_SUCCESS.equalsIgnoreCase(validationMessageFromRules)){
                            finalReturnMsg += validationMessageFromRules;
                        }
                        if(!System.label.TAG_SUCCESS.equalsIgnoreCase(validationMessageFromCustomClass)){
                            finalReturnMsg += validationMessageFromCustomClass;
                        }

                        if(!String.isBlank(finalReturnMsg)){
                            return finalReturnMsg;
                        }
                        else{
                            //Submit to next department action will happen
                            String returnMsg = System.label.NEXT_STAGE_SUBMIT_SUCCESS_MSG;//System.label.TAG_SUCCESS;
                            if(currDeptJuncRec.Is_REHASH_Required__c){
                                RehashUtility ru = new RehashUtility();
                                returnMsg = ru.triggerRehash(appId, currDeptJuncRec.genesis__Department__c, classImplInst);
                            }
                            
                            if(System.label.NEXT_STAGE_SUBMIT_SUCCESS_MSG.equals(returnMsg)){
                                returnMsg = genesis.LoanDashBoard.submitToNextDepartment(appId);
                                System.debug('Dept returnMsg:' + returnMsg);
                                if(returnMsg.equalsIgnoreCase(System.label.NEXT_STAGE_SUBMIT_SUCCESS_MSG)){
                                    System.enqueueJob(new CaptureApplicationSnapshot(appId, currDeptJuncRec.genesis__Department__r.name));
                                    triggerOmniWorkflow(appId);
                                    if(classImplInst != null){
                                        returnMsg = classImplInst.executeNextDeptActions(appId);
                                    }                                    
                                }                                
                            }
                            return returnMsg;
                        }
                    }
                    else{
                        return activeDept.get(0).genesis__Department__c + ' was the last department in application workflow!';
                    }
                }
                else{
                    return 'No junction record found for dept ' + activeDept.get(0).genesis__Department__c;
                }
            }
            else{
                return 'No current active stage found for Application!';
            }
        }
        catch(Exception e){
            return 'Error while submitting to next department: ' + e.getMessage() + ', at line :' + e.getLineNumber();
        }
    }

    webservice static String skipAndSubmitToNextStage(genesis__Applications__c appInstance){
        try{
            List<genesis__Product_Department_Junction__c> prodDeptList = [SELECT Id,
                                                                                 genesis__CL_Product__c,
                                                                                 genesis__Department__c,
                                                                                 genesis__Department__r.Name,
                                                                                 genesis__Department_No__c ,
                                                                                 (SELECT ID,
                                                                                         genesis__Task_Setup__c,
                                                                                         genesis__Task_Setup__r.ID,
                                                                                         genesis__Task_Setup__r.genesis__Auto_Close__c,
                                                                                         genesis__Task_Setup__r.genesis__Description__c,
                                                                                         genesis__Task_Setup__r.genesis__Mandatory__c,
                                                                                         genesis__Task_Setup__r.genesis__Priority__c,
                                                                                         genesis__Task_Setup__r.genesis__Retain_Completed_Task__c,
                                                                                         genesis__Task_Setup__r.genesis__Task_Name__c,
                                                                                         genesis__Task_Setup__r.genesis__Type__c
                                                                                    FROM genesis__ProductDepartment_Task_Junction__r
                                                                                   WHERE genesis__Task_Setup__r.genesis__Is_Parallel__c = false
                                                                                   ORDER BY genesis__Task_Order__c ASC
                                                                                 )
                                                                            FROM genesis__Product_Department_Junction__c
                                                                           WHERE genesis__CL_Product__c = :appInstance.genesis__CL_Product__c
                                                                           ORDER BY genesis__Department_No__c ASC
                                                                         ];
                                                                         
            List<genesis__Application_Department__c> appDeptList = [SELECT Id,
                                                                           Name,
                                                                           genesis__Application__c,
                                                                           genesis__Department__c,
                                                                           genesis__Department__r.Name,
                                                                           genesis__Status__c
                                                                      FROM genesis__Application_Department__c
                                                                     WHERE genesis__Application__c = :appInstance.Id
                                                                       AND genesis__Status__c = :genesis.LendingConstants.ACTIVE_DEPARTMENT
                                                                   ];
            
            genesis__Application_Department__c activeDept = appDeptList.get(0);
            //Mark the old department as inactive
            activeDept.genesis__Status__c = genesis.LendingConstants.INACTIVE_DEPARTMENT;
            update activeDept;          
            
            Integer activeDeptNumber = 0;
            for(genesis__Product_Department_Junction__c pdj : prodDeptList){
                if(String.valueOf(pdj.genesis__Department__c).equalsIgnoreCase(String.valueOf(activeDept.genesis__Department__c))){
                    activeDeptNumber = Integer.valueOf(pdj.genesis__Department_No__c);
                }
            }
            
            String nextDeptId = '';
            String nextDeptName = '';
            for(genesis__Product_Department_Junction__c pdj : prodDeptList){
                if(pdj.genesis__Department_No__c == (activeDeptNumber+1)){
                    nextDeptId = pdj.genesis__Department__c;
                    nextDeptName = pdj.genesis__Department__r.Name;
                }
            }
            
            genesis__Application_Department__c newAppDept = new genesis__Application_Department__c();
            newAppDept.genesis__Application__c = appInstance.Id;
            newAppDept.genesis__Department__c = nextDeptId;
            newAppDept.genesis__Status__c = genesis.LendingConstants.ACTIVE_DEPARTMENT;
            insert newAppDept;

            genesis.TaskAPI tsk = genesis.APIFactory.getTaskAPI();
            tsk.generateTask(new List<String>{appInstance.Id}, false);
            appInstance.genesis__Overall_Status__c = genesis.LendingConstants.APP_ASSIGNED_TO_USER + nextDeptName;
            //appInstance.ownerId = UserInfo.getUserId();
            update appInstance;
            
            triggerOmniWorkflow(appInstance.Id);
            
            return System.label.TAG_SUCCESS;
        }
        catch(Exception e){
            return 'Error while skipping to next department: ' + e.getMessage() + ', at line :' + e.getLineNumber();
        }
    }

    webservice static String submitToPreviousDepartment(String appId, String previousStageId, String applicationStatus){
        try{
           
            if(String.isBlank(previousStageId)){
                genesis__Org_Parameters__c orgParams = genesis__Org_Parameters__c.getOrgDefaults();
                String baseStageName = orgParams.Rehash_Base_Stage_Name__c;
                
                List<genesis__Department__c> deptId = [SELECT id,
                                                              name 
                                                         FROM genesis__Department__c 
                                                        WHERE name = :baseStageName 
                                                      ];
                                                      
                previousStageId = deptId.get(0).Id;
                 
            }
            
            List<genesis__Application_Department__c> activeDept = [SELECT id,
                                                                          genesis__Department__c,
                                                                          genesis__Application__c,
                                                                          genesis__Application__r.genesis__CL_Product__c
                                                                     FROM genesis__Application_Department__c
                                                                    WHERE genesis__Status__c = 'Inactive'
                                                                      AND genesis__Application__c = :appId
                                                                      AND genesis__Department__c = :previousStageId
                                                                  ];
            
            if(activeDept != null && activeDept.size() > 0){
                List<genesis__Product_Department_Junction__c> deptJunctionList = [SELECT id, 
                                                                                         name,
                                                                                         genesis__Department__c,
                                                                                         genesis__CL_Product__c,
                                                                                         genesis__Department_No__c,
                                                                                         genesis__Department__r.name,
                                                                                         Class_Name__c,
                                                                                         Is_REHASH_Required__c,
                                                                                         (SELECT id,
                                                                                                 name,
                                                                                                 Rule__c
                                                                                            FROM Product_Dept_Rule_Associations__r
                                                                                         )
                                                                                    FROM genesis__Product_Department_Junction__c
                                                                                   WHERE genesis__CL_Product__c = :activeDept.get(0).genesis__Application__r.genesis__CL_Product__c
                                                                                     AND genesis__Department__c = :activeDept.get(0).genesis__Department__c
                                                                                 ];
                                                                             
                if(deptJunctionList != null && deptJunctionList.size() > 0){
                    genesis__Product_Department_Junction__c currDeptJuncRec = deptJunctionList.get(0);
                    
                    StageExecutionInterface classImplInst = null;
                    if(!String.isBlank(currDeptJuncRec.Class_Name__c)){
                        Type classInst = Type.forName(currDeptJuncRec.Class_Name__c);
                        if(classInst != null){
                            classImplInst = (StageExecutionInterface) classInst.newInstance();
                        }
                    }
                    
                    String returnMsg = submitToPreviousDepartment(appId, previousStageId, classImplInst);
                    
                    if(returnMsg.startsWith('Application moved to') && !String.isBlank(applicationStatus)){
                        List<genesis__Applications__c> appInst = [SELECT id,
                                                                         name,
                                                                         genesis__Status__c 
                                                                    FROM genesis__Applications__c
                                                                   WHERE Id = :appId
                                                                 ];
                        
                        for(genesis__Applications__c gac : appInst){
                            gac.genesis__Status__c = applicationStatus;
                        }                        
                        update appInst;
                    }

                    return returnMsg;                 
                }
                else{
                    return 'No junction record found for dept ' + activeDept.get(0).genesis__Department__c;
                }
            }
            else{
                return 'No current active stage found for Application!';
            }
        }
        catch(Exception e){
            return 'Error while submitting to previous department: ' + e.getMessage() + ', at line :' + e.getLineNumber();
        }
    }

    public static String submitToPreviousDepartment(String appId, String previousStageId, StageExecutionInterface classImplInst){
        try{
            String returnMsg = genesis.LoanDashBoard.moveApplicationToPreviousDepartment(appId, previousStageId);
            triggerOmniWorkflow(appId);

            if(classImplInst != null){
                classImplInst.executePreviousDeptActions(appId);
            }

            return returnMsg;
        }
        catch(Exception e){
            return 'Error while submitting to previous department: ' + e.getMessage() + ', at line :' + e.getLineNumber();
        }
    }

    //@future
    public static void triggerOmniWorkflow(String applicationId){
        try{
            genesis__Org_Parameters__c orgParams = genesis__Org_Parameters__c.getOrgDefaults();
            if(!orgParams.Use_OMNI_Channel__c){
                return;
            }
            String userId = System.UserInfo.getUserId();
            // Call the Flow
            Map<String, Object> params = new Map<String, Object>();
            params.put('APEX_Application_ID', applicationId);
            params.put('APEX_UserID', userId);
            Flow.Interview routingFlow = Flow.Interview.createInterview('OMNI_Channel_Application_Routing', params);
            routingFlow.start();
 
            // Obtain the results
            //Double returnValue = (Double) routingFlow.getVariableValue('ReturnValue');
            //System.debug('Flow returned ' + returnValue);
        }
        catch(Exception e){
            ExceptionLog.insertExceptionLog(e, '[triggerOmniWorkflow]', applicationId);
        }
    }
    
    public static String evaluateRulesForTask(String taskId){
        try{
            List<Task> listTasks = [SELECT id,
                                           genesis__Application__c,
                                           genesis__Task_Setup__c
                                      FROM Task 
                                     WHERE genesis__Application__c != null 
                                       AND genesis__Task_Setup__c != null
                                       AND id = :taskId
                                   ];
            
            if(listTasks != null && listTasks.size() > 0){
                SObject appInstance = (SObject) new genesis__Applications__c(Id = listTasks.get(0).genesis__Application__c); 
                
                List<Task_Rule_Junction__c> listRules = [SELECT id,
                                                                Rule__c, 
                                                                name 
                                                           FROM Task_Rule_Junction__c 
                                                          WHERE Task_Setup__c = :listTasks.get(0).genesis__Task_Setup__c
                                                        ];
        
                Set<Id> setRuleIds = new Set<Id>();
                for(Task_Rule_Junction__c trj : listRules){
                    setRuleIds.add(trj.Rule__c);
                }
                
                return evaluateRules(setRuleIds, appInstance);
            }
            else{
                return ('Task not found with id: ' + taskId);
            }
        }
        catch(Exception e){
            return 'Error while evaluating rules for task: ' + taskId + ', Exception: ' + e.getMessage() + ', at line :' + e.getLineNumber();
        }
    }
    
    public static String evaluateRules(Set<Id> setRuleIds, SObject appInstance){
        try{
            if(setRuleIds != null && setRuleIds.size() > 0){
                Map<Id, genesis__Rule__c> mapRules = new Map<Id,genesis__Rule__c>([SELECT Id,
                                                                                          Name
                                                                                     FROM genesis__Rule__c
                                                                                    WHERE genesis__Enabled__c = true
                                                                                      AND Id = :setRuleIds
                                                                                 ]);
            
                if(mapRules == null || mapRules.size() == 0){
                    return System.label.TAG_SUCCESS;
                }
                                                                             
                List<genesis__Checklist__c> checkList = genesis.RulesAPI.evaluateRules(appInstance, mapRules.values(), false, false);
                List<String> errorList = new List<String>();
                
                if(checkList != null && checkList.size() > 0){
                    for(genesis__Checklist__c checkListRecord : checkList){
                        if(checkListRecord.genesis__Result__c == false){
                            errorList.add(checkListRecord.genesis__Message__c);
                        }
                    }
                }
                
                if(errorList.size() > 0){
                    String errorString = 'Please review below errors before submitting:' + '<br>';
                    errorString += convertListToString(errorList);                
                    return errorString;
                }
            }
            return System.label.TAG_SUCCESS;
        }
        catch(Exception e){
            return 'Error while evaluating rules: ' + e.getMessage() + ', at line :' + e.getLineNumber();
        }
    }

    public static String convertListToString(List<String> strList){
        try{
            if(strList != null && strList.size() > 0){
                String returnString = '';
                for(String str : strList){
                    returnString += '=> ' + str + '<br>';
                }                
                return returnString;
            }
            return '';
        }
        catch(Exception e){
            throw e;
        }
    }
    
    public class CaptureApplicationSnapshot implements Queueable{
        String appId;
        String stgName;
        public CaptureApplicationSnapshot (String applicationId, String stageName){
            appId = applicationId;
            stgName = stageName;
        }
        public void execute(QueueableContext context) {
            try{
                List<clcommon.SnapshotAPI.SnapshotParams> snapshotList = new List<clcommon.SnapshotAPI.SnapshotParams>();
                clcommon.SnapshotAPI.SnapshotParams snapshot = new clcommon.SnapshotAPI.SnapshotParams();
                snapshot.recordId = appId;
                snapshot.dynamicQuerySetName = 'Application Snapshot';
                snapshot.useLabelName = true;
                snapshot.saveSnapshot = true;
                snapshot.snapshotType = 'AUTOMATIC';
                snapshot.snapshotRemarks = 'Data snapshot for ' + stgName + ' stage.';
                snapshotList.add(snapshot);
                List<String> responseJsonString = clcommon.SnapshotAPI.generateSnapshot(snapshotList);
            }
            catch(Exception e){
                ExceptionLog.insertExceptionLog(e, 'Regenerating amortisation schedule failed for app: '+ appId, appId);
            }   
        }
    }
}