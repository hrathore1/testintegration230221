global class ContactPersonalInfoUtil{
    
    public class ReturnAccounts{
        public Account firstApplicant;
        public Account secondApplicant;
    }
    
    public static ReturnAccounts assignAccountVals(ApplicationCreationParser accountData, Boolean hasAdditionalBorrower, Account retAccount_First, Account retAccount_Second){
        mfiflexUtil.VOLog voLogInstance = mfiflexUtil.VOLog.getInstance('Genesis');
        ReturnAccounts returnAccs= new ReturnAccounts();
        
        try{
            
            String channel = '';
            if(accountData.loanChannel != null && String.isNotBlank(accountData.loanChannel)){
                channel = accountData.loanChannel;
            }
            else{
                channel = 'RACV';
            }
            
            //********************* First Applicant Details ***************************//
            
            if(accountData.title_First != null && String.isNotBlank(accountData.title_First)){
                if(accountData.title_First.equalsIgnoreCase(System.label.DEFAULT_ACCOUNT_TITLE_PORTAL)){
                    accountData.title_First = System.label.DEFAULT_ACCOUNT_TITLE_BE;
                }
                retAccount_First.Salutation = accountData.title_First;
            }
            if(accountData.firstName_First != null && String.isNotBlank(accountData.firstName_First)){
                retAccount_First.FirstName = accountData.firstName_First;
            }
            if(accountData.middleName_First != null && String.isNotBlank(accountData.middleName_First)){
                retAccount_First.MiddleName = accountData.middleName_First;
            }
            if(accountData.lastName_First != null && String.isNotBlank(accountData.lastName_First)){
                retAccount_First.LastName = accountData.lastName_First;
            }
            else{
                throw new CustomException('Error: '+ 'Last name of first applicant is missing.');
            }
            if(accountData.gender_First != null && String.isNotBlank(accountData.gender_First)){
                if(accountData.gender_First.equalsIgnoreCase(System.label.DEFAULT_GENDER_PORTAL)){
                    accountData.gender_First = System.label.DEFAULT_GENDER_BE;
                }
                retAccount_First.Gender__pc = accountData.gender_First;
            }
            if(accountData.dob_First != null && String.isNotBlank(accountData.dob_First)){
                retAccount_First.PersonBirthdate = Date.valueOf(accountData.dob_First);
            }
            if(accountData.email_First != null && String.isNotBlank(accountData.email_First)){
                retAccount_First.PersonEmail = accountData.email_First;
            }
            if(accountData.mobile_First != null && String.isNotBlank(accountData.mobile_First)){
                retAccount_First.Phone = accountData.mobile_First;
            }
            if(accountData.homePhone_First != null && String.isNotBlank(accountData.homePhone_First)){
                retAccount_First.PersonHomePhone = accountData.homePhone_First;
            }
            if(accountData.isRACVMember_First != null && String.isNotBlank(accountData.isRACVMember_First)){
                Boolean isRACVMember_FirstBool = 'Yes'.equalsIgnoreCase(accountData.isRACVMember_First) ? true: false;
                retAccount_First.Existing_Member__c = isRACVMember_FirstBool ;
            }
            if(accountData.racvMemberNo_First != null && String.isNotBlank(accountData.racvMemberNo_First)){
                retAccount_First.Membership_Number__c = accountData.racvMemberNo_First;
            }
            if(accountData.racvMemberCardColor_First != null && String.isNotBlank(accountData.racvMemberCardColor_First)){
                retAccount_First.Membership_card_colour__c = channel + ' ' + accountData.racvMemberCardColor_First;
            }
            else{
                retAccount_First.Membership_card_colour__c = System.label.DEFAULT_MEMBER_COLOUR;
            }
            if(accountData.loanTermsAndConditions != null && accountData.loanTermsAndConditions){
                //retAccount_First. = accountData.loanTermsAndConditions;
            }
            if(accountData.statementOfNotifiable != null && accountData.statementOfNotifiable){
                //retAccount_First. = accountData.statementOfNotifiable;
            }
            if(accountData.privacyConsent != null && accountData.privacyConsent){
                retAccount_First.Credit_Check_Consent__c = accountData.privacyConsent;
            }
            if(accountData.marketingConsent != null && accountData.marketingConsent){
                retAccount_First.Marketing_Consent__c = accountData.marketingConsent;
            }
            if(accountData.electronicCommConsent != null && accountData.electronicCommConsent){
                retAccount_First.PersonHasOptedOutOfEmail = accountData.electronicCommConsent;
            }
            if(accountData.electronicIdConsent != null && accountData.electronicIdConsent){
                retAccount_First.Electronic_Identification_Consent__c = accountData.electronicIdConsent;
            }
            if(accountData.smsUpdates != null && accountData.smsUpdates){
                retAccount_First.SMS_Updates__c = accountData.smsUpdates;
            }
            if(accountData.scoreseekerConsent != null && accountData.scoreseekerConsent){
                retAccount_First.Scoreseeker_Consent__c = accountData.scoreseekerConsent;
            }
            if(accountData.applicationSubmitted != null && accountData.applicationSubmitted){
                retAccount_First.PersonHasOptedOutOfEmail = true;
                retAccount_First.Electronic_Identification_Consent__c = true;
                retAccount_First.Credit_Check_Consent__c = true;
                retAccount_First.SMS_Updates__c = true;
                retAccount_First.Scoreseeker_Consent__c = true;
            }
            if(accountData.mailingSameAsCurrent_First != null){
                retAccount_First.clcommon__Mailing_Address_Same_as_Current_Address__pc = (accountData.mailingSameAsCurrent_First != null 
                                                                                          && String.isNotBlank(accountData.mailingSameAsCurrent_First)
                                                                                          && 'Yes'.equalsIgnoreCase(accountData.mailingSameAsCurrent_First)) ?
                                                                    true:
                                                                    false;
            }
            //********************* Second Applicant Details ***************************//
            
            if(hasAdditionalBorrower){
                if(accountData.title_Second != null && String.isNotBlank(accountData.title_Second)){
                    if(accountData.title_Second.equalsIgnoreCase(System.label.DEFAULT_ACCOUNT_TITLE_PORTAL)){
                        accountData.title_Second = System.label.DEFAULT_ACCOUNT_TITLE_BE;
                    }
                    retAccount_Second.Salutation = accountData.title_Second;
                }
                if(accountData.firstName_Second != null && String.isNotBlank(accountData.firstName_Second)){
                    retAccount_Second.FirstName = accountData.firstName_Second;
                }
                else{
                    retAccount_Second.FirstName = 'Divya';
                }
                if(accountData.middleName_Second != null && String.isNotBlank(accountData.middleName_Second)){
                    retAccount_Second.MiddleName = accountData.middleName_Second;
                }
                if(accountData.lastName_Second != null && String.isNotBlank(accountData.lastName_Second)){
                    retAccount_Second.LastName = accountData.lastName_Second;
                }
                else{
                    throw new CustomException('Error: '+ 'Last name of Second applicant is missing.');
                }
                if(accountData.gender_Second != null && String.isNotBlank(accountData.gender_Second)){
                    if(accountData.gender_Second.equalsIgnoreCase(System.label.DEFAULT_GENDER_PORTAL)){
                        accountData.gender_Second = System.label.DEFAULT_GENDER_BE;
                    }
                    retAccount_Second.Gender__pc = accountData.gender_Second;
                }
                if(accountData.dob_Second != null && String.isNotBlank(accountData.dob_Second)){
                    retAccount_Second.PersonBirthdate = Date.valueOf(accountData.dob_Second);
                }
                if(accountData.email_Second != null && String.isNotBlank(accountData.email_Second)){
                    retAccount_Second.PersonEmail = accountData.email_Second;
                }
                if(accountData.mobile_Second != null && String.isNotBlank(accountData.mobile_Second)){
                    retAccount_Second.Phone = accountData.mobile_Second;
                }
                if(accountData.homePhone_Second != null && String.isNotBlank(accountData.homePhone_Second)){
                    retAccount_Second.PersonHomePhone = accountData.homePhone_Second;
                }
                if(accountData.isRACVMember_Second != null && String.isNotBlank(accountData.isRACVMember_Second)){
                    Boolean isRACVMember_SecondBool = 'Yes'.equalsIgnoreCase(accountData.isRACVMember_Second) ? true: false;
                    retAccount_Second.Existing_Member__c = isRACVMember_SecondBool ;
                }
                if(accountData.racvMemberNo_Second != null && String.isNotBlank(accountData.racvMemberNo_Second)){
                    retAccount_Second.Membership_Number__c = accountData.racvMemberNo_Second;
                }
                if(accountData.racvMemberCardColor_Second != null && String.isNotBlank(accountData.racvMemberCardColor_Second)){
                    retAccount_Second.Membership_card_colour__c = channel + ' ' + accountData.racvMemberCardColor_Second;
                }
                else{
                    retAccount_Second.Membership_card_colour__c = System.label.DEFAULT_MEMBER_COLOUR;
                }
                
                if(accountData.relationshipToBorrower_Second != null && String.isNotBlank(accountData.relationshipToBorrower_Second)){
                    //retAccount_Second.__________ = accountData.relationshipToBorrower_Second;
                }
                if(accountData.applicationSubmitted != null && accountData.applicationSubmitted){
                    retAccount_Second.PersonHasOptedOutOfEmail = true;
                    retAccount_Second.Electronic_Identification_Consent__c = true;
                    retAccount_Second.Credit_Check_Consent__c = true;
                    retAccount_Second.SMS_Updates__c = true;
                    retAccount_Second.Scoreseeker_Consent__c = true;
                }
                if(accountData.marketingConsent != null && accountData.marketingConsent){
                    retAccount_Second.Marketing_Consent__c = accountData.marketingConsent;
                }
                if(accountData.mailingSameAsCurrent_Second != null){
                    retAccount_Second.clcommon__Mailing_Address_Same_as_Current_Address__pc = (accountData.mailingSameAsCurrent_Second != null 
                                                                                               && String.isNotBlank(accountData.mailingSameAsCurrent_Second)
                                                                                               && 'Yes'.equalsIgnoreCase(accountData.mailingSameAsCurrent_Second)) ?
                                                                        true:
                                                                        false;
                }
                returnAccs.secondApplicant = retAccount_Second;
            }
            else{
                returnAccs.secondApplicant = null;
            }
            returnAccs.firstApplicant = retAccount_First;
        }
        catch(Exception e){
            voLogInstance.logException(1001, '[ContactPersonalInfoUtil.assignAccountVals] Error: ' + e.getMessage() + ' (at line: ' + e.getLineNumber() + ')', e);
            voLogInstance.commitToDb();
            returnAccs= null;
            throw new CustomException('Error: '+ e.getMessage());
        }
        return returnAccs;
    }
}