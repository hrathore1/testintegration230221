/******************************************************************************
* @Description:- This is the handler class of AddressValidation Trigger.
*  JIRA - RFCL-1005
*******************************************************************************/

public class AddressValidationHandler{
    public static void validateAddress(List<clcommon__Address__c> addList, Map<Id,clcommon__Address__c> oldAddMap){
    
    
        List<clcommon__Address__c> rAddList = new List<clcommon__Address__c>();
        List<clcommon__Address__c> mAddList = new List<clcommon__Address__c>();
        Set<Id> resAccId = new Set<Id>();
        Set<Id> mailAccId = new Set<Id>();
        
        
        for(clcommon__Address__c add : addList){
            if(String.isBlank(add.Address_Type__c) ){
                add.addError(system.label.Address_validation_message_4);
            }
            else if(add.Address_Type__c.equals('Residential')){
                rAddList.add(add);
                resAccId.add(add.clcommon__Account__c);
            }
            else if(add.Address_Type__c.equals('Mailing')){
                mAddList.add(add);
                mailAccId.add(add.clcommon__Account__c);
            }
            
        }
        validateResidentialAddress(rAddList,resAccId, oldAddMap);
        validateMailingAddress(mAddList,mailAccId, oldAddMap);
        
    }
    
    public static void validateResidentialAddress(List<clcommon__Address__c> resAddList, Set<Id> accId, Map<Id,clcommon__Address__c> oldAddMap){
        Set <Id> currentAddAccountId = new Set <Id>();
        List <Account> accList = [Select Id ,
                                     (Select id 
                                     from clcommon__Addresses__r 
                                     where Address_Type__c = 'Residential' 
                                     and Is_Current__c = True)           
                              From Account
                              Where id in: accId];
        for (Account acc : accList){
            if(acc.clcommon__Addresses__r.Size()>0){
                currentAddAccountId.add(acc.id);
            }
        }
        for(clcommon__Address__c addr : resAddList){
            if(addr.Is_Current__c == True && currentAddAccountId.contains(addr.clcommon__Account__c)&& ( (oldAddMap == null || oldAddMap.isEmpty()) || (oldAddMap.containsKey(addr.id) && oldAddMap.get(addr.id).Is_Current__c == False)))   {
                addr.addError(system.label.Address_validation_message_1);
            }
            
        }
    }
    
    public static void validateMailingAddress(List<clcommon__Address__c> mailAddlist, Set<Id> accId, Map<Id,clcommon__Address__c> oldAddMap){
         Set <Id> currentAddAccountId = new Set <Id>();
        List <Account> accList = [Select Id ,
                                     (Select id 
                                     from clcommon__Addresses__r 
                                     where Address_Type__c = 'Mailing' 
                                     and Is_Current__c = True)           
                              From Account
                              Where id in: accId];
        for (Account acc : accList){
            if(acc.clcommon__Addresses__r.Size()>0){
                currentAddAccountId.add(acc.id);
            }
        }
        for(clcommon__Address__c addr : mailAddlist){
            if(addr.Is_Current__c == True && currentAddAccountId.contains(addr.clcommon__Account__c) && ( (oldAddMap == null || oldAddMap.isEmpty()) || (oldAddMap.containsKey(addr.id) && oldAddMap.get(addr.id).Is_Current__c == False)))   {
                addr.addError(system.label.Address_validation_message_2);
            }
            
        }
    }
    
    public static void afetrTriggerAction(List<clcommon__Address__c> resAddList){
        List <clcommon__Address__c> mailingAddrToInsert = new List <clcommon__Address__c>();
        for(clcommon__Address__c addr : resAddList){
            if(addr.Is_Current_Mailing_Address__c == True){
                clcommon__Address__c  clonedMailingAddr = addr.clone(false, false, false, false);
                clonedMailingAddr.Address_Type__c = 'Mailing';
                clonedMailingAddr.Is_Current__c = True;
                clonedMailingAddr.Is_Current_Mailing_Address__c  = False;
                clonedMailingAddr.Start_Date_Duplicate_Check__c = null;
                mailingAddrToInsert.add(clonedMailingAddr);
            }
            try{
                insert mailingAddrToInsert;
            }
            Catch( exception e){
                addr.addError(system.label.Address_validation_message_3+e);
            }
        }
    }
}