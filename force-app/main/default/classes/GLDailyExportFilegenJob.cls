/******************************************************************************

@Description :- Produce General Ledger daily export file to SAP. 
User Story:- RFCL-334
*******************************************************************************/

global class GLDailyExportFilegenJob implements DataBase.Batchable<sObject>, Database.Stateful, Schedulable{
    private mfiflexUtil.VOLog voLogInstance = mfiflexUtil.VOLog.getInstance('Neon');
    public Batch_Configs__c batchConfig = Batch_Configs__c.getOrgDefaults();
    public Banking_Config__c bankingConfig = Banking_Config__c.getOrgDefaults();
    
    public Integer counter = 0;
    global Database.QueryLocator start(Database.BatchableContext bc){

        return Database.getQueryLocator([SELECT id,
                                                Header_Record__c,
                                                Debit_Record__c,
                                                Credit_Record__c,
                                                Debit_Amount__c,
                                                Credit_Amount__c,
                                                Assignment_Number__c
                                           FROM Accounting_Summary__c 
                                          WHERE SAP_File_Generated__c = false
                                            AND SAP_GL_File_Name__c = null
                                        ]);                                        
    }
     
    global void execute(SchedulableContext sc){
        Database.executeBatch(new GLDailyExportFilegenJob(), (Integer) batchConfig.SAP_GL_Batchsize__c);
    }
         
    global void execute(Database.BatchableContext bc, List<sObject> scope){
        try{
            List<Accounting_Summary__c> eaRecords = (List<Accounting_Summary__c>) scope;
            Map<String, List<String>> mapLineItems = new Map<String, List<String>>();
            String fileName = String.isBlank(bankingConfig.SAP_GL_File_Name_Prefix__c) ? '' : bankingConfig.SAP_GL_File_Name_Prefix__c 
                                                                                            + CommonUtility.getDatetimeValue('YYYYMMDDHHMMSS') 
                                                                                            + '_' 
                                                                                            + String.valueOf(++counter);
            
            String fileBody = '';                                                                                    
            for(Accounting_Summary__c ea : eaRecords){
            
                String header = ea.Header_Record__c.replace('<REPLACE_ME>', fileName) + '\r\n';
                
                String debitAmount = String.valueOf(ea.Debit_Amount__c.setScale(2));
                String creditAmount = String.valueOf(ea.Credit_Amount__c.setScale(2));
                
                String entryDbt = ea.Debit_Record__c.replace('<REPLACE_ME>', debitAmount) + '\r\n';
                String entryCdt = ea.Credit_Record__c.replace('<REPLACE_ME>', creditAmount) + '\r\n';

                ea.SAP_GL_File_Name__c = fileName;
                ea.SAP_File_Generated__c = true;
                
                if(ea.Assignment_Number__c != null && !String.isBlank(ea.Assignment_Number__c)){
                    fileBody += header + entryDbt + entryCdt; continue;
                }
                
                if(mapLineItems.containsKey(header)){
                    mapLineItems.get(header).add(entryDbt); mapLineItems.get(header).add(entryCdt);
                }
                else{
                    mapLineItems.put(header, new List<String>{entryDbt, entryCdt});
                }
            }
            
            for(String header : mapLineItems.keySet()){
                fileBody += header;
                for(String lineItem : mapLineItems.get(header)){
                    fileBody += lineItem;
                }
            }
            
            String folderName = bankingConfig.SAP_GL_File_Folder__c;
            CommonUtility.createDocument(folderName, fileName, fileBody, 'BLANK_EXTN', null, null);
            Update eaRecords;
        }
        catch(Exception e) {
            voLogInstance.logException(7004, '[GLDailyExportFilegenJob.execute] Exception:' + e.getMessage() + ', at line:' + e.getLineNumber(), e);
            voLogInstance.commitToDb();
        }         
    }
     
    global void finish(Database.BatchableContext bc) {}
}