global class CreateMonthEndInterestAccrualJob implements DataBase.Batchable<sObject>, Database.Stateful, Schedulable{
    private mfiflexUtil.VOLog voLogInstance = mfiflexUtil.VOLog.getInstance('Neon');
    loan.GlobalLoanUtilFacade loanUtil= new loan.GlobalLoanUtilFacade();
    public Date systemDate;
    public Date lastDateOfMonth;
    
    global CreateMonthEndInterestAccrualJob(){
        systemDate = loanUtil.getCurrentSystemDate();
        Integer daysInMonth = Date.daysInMonth(systemDate.year(), systemDate.month());
        lastDateOfMonth = systemDate.toStartOfMonth().addDays(daysInMonth - 1);
    }
    
    global CreateMonthEndInterestAccrualJob(Date dt){
        if(dt == null){
            systemDate = loanUtil.getCurrentSystemDate();
        }
        else{
            systemDate = dt;
        }
        
        Integer daysInMonth = Date.daysInMonth(systemDate.year(), systemDate.month());
        lastDateOfMonth = systemDate.toStartOfMonth().addDays(daysInMonth - 1);
    }
    
    global Database.QueryLocator start(Database.BatchableContext bc){
        List<String> listActiveContractStatuses = System.label.CUSTINFO_LOAN_STATUS.split(',');
        
        return Database.getQueryLocator([SELECT id,
                                                name,
                                                loan__Last_Accrual_Date__c,
                                                loan__Interest_Remaining__c,
                                                loan__Accrual_Stop_Indicator__c,
                                                loan__Interest_Rate__c,
                                                loan__Frequency_of_Loan_Payment__c,
                                                loan__Interest_Calculation_Method__c,
                                                loan__Principal_Adjustment__c,
                                                loan__Principal_Adjustment_Subtract__c,
                                                loan__Loan_Amount__c,
                                                loan__Principal_Remaining__c,
                                                loan__Capitalized_Fee__c,
                                                loan__Capitalized_Interest__c,
                                                loan__Time_Counting_Method__c ,
                                                (SELECT id,
                                                        name,
                                                        loan__Type__c,
                                                        loan__Amount__c,
                                                        loan__Transaction_Date__c
                                                   FROM loan__Accrual_Entries__r
                                                  WHERE loan__Type__c = 'Interest Accrual'
                                                    AND loan__Transaction_Date__c = :lastDateOfMonth
                                                )
                                           FROM loan__Loan_Account__c 
                                          WHERE loan__Invalid_Data__c = False
                                            AND loan__Loan_Status__c = :listActiveContractStatuses 
                                       ]);                                        
    }
     
    global void execute(SchedulableContext sc){
        CreateMonthEndInterestAccrualJob  batchJob = new CreateMonthEndInterestAccrualJob();         
        Database.executeBatch(batchJob, 200);
    }
     
    global void execute(Database.BatchableContext bc, List<sObject> scope){
        
        List<loan__Loan_Account__c> loanRecords = (List<loan__Loan_Account__c>) scope;
        try{
            if(systemDate != lastDateOfMonth){
                throw new CustomException('Interest accrual can be generated only for end of month.');
            }
            
            List<loan__Accrual_Entry__c> listAccrualEntries = new List<loan__Accrual_Entry__c>();
            for(loan__Loan_Account__c lacc : loanRecords){
                
                loan__Accrual_Entry__c tempAccEntry = new loan__Accrual_Entry__c();
                
                List<loan__Accrual_Entry__c> lAccEntry = lacc.loan__Accrual_Entries__r;
                if(lAccEntry != null && lAccEntry.size() > 0){
                    tempAccEntry = lAccEntry.get(0);
                }
                
                Decimal accruedInterest = loan.InterestCalc.calcSI(lacc, systemDate);
                
                tempAccEntry.loan__Type__c = 'Interest Accrual';
                tempAccEntry.loan__Amount__c = loan.ValueUtil.round(accruedInterest);
                tempAccEntry.loan__Transaction_Date__c = systemDate;
                tempAccEntry.loan__Loan_Account__c = lacc.id;
                tempAccEntry.Last_Accrual_Date__c = lacc.loan__Last_Accrual_Date__c;
                
                listAccrualEntries.add(tempAccEntry);
            }
            
            if(listAccrualEntries != null && listAccrualEntries.size() > 0){
                UPSERT listAccrualEntries;
            }
        }
        catch(Exception e) {
            voLogInstance.logException(7003, '[CreateMonthEndInterestAccrualJob.execute] Exception:' + loanRecords+ ', '+ e.getMessage() + ', at line:' + e.getLineNumber(), e);
            voLogInstance.commitToDb();
        }         
    }
     
    global void finish(Database.BatchableContext bc) {}
    
    global class CustomException extends Exception{}
}