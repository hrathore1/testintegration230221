/**
 * @File Name          : JWF_RunFinalSystemDecision.cls
 * @Description        : 
 * @Author             : chirag.gupta@q2.com
 * @Group              : 
 * @Last Modified By   : chirag.gupta@q2.com
 * @Last Modified On   : 6/9/2020, 2:05:27 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    6/8/2020   chirag.gupta@q2.com     Initial Version
**/
public class JWF_RunFinalSystemDecision implements IFlowExecution {
    public void execute(Id applicationId,  JWF_Application_Execution_Log__c appExecutionLog){
        try{
            List<String> listReturnMsgs = CalculateDecisioning.runFinalSystemDecision(applicationId);
            System.debug('listReturnMsgs:'+ listReturnMsgs);

            Map<String, Object> mapExecutionLog = new Map<String, Object>();
            appExecutionLog.Message__c += ('Final System Decision:' + convertListToString(listReturnMsgs));
            mapExecutionLog.put('appExecutionLog', appExecutionLog);

            ApplicationExecutionAction action = new ApplicationExecutionAction(applicationId);
            action.changeStatus(null);
            action.doInvokeAction(null);                                        
        }
        catch(Exception e){ExceptionLog.insertExceptionLog(e, 'EA call failed:::' + e.getMessage()+'---'+e.getStackTraceString(),applicationId);}          
    }

    public String convertListToString(List<String> strList){
        try{
            if(strList != null && strList.size() > 0){
                String returnString = '';
                for(String str : strList){
                    returnString += '=> ' + str + '<br>';
                }                
                return returnString;
            }
            return '';
        }
        catch(Exception e){
            throw e;
        }
    }
}