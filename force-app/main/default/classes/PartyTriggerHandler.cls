public class PartyTriggerHandler {
    
    public List<clcommon__Party__c> newList;
    public Map<Id,clcommon__Party__c> oldMap;
    public Map<Id,clcommon__Party__c> newMap;
    final String BORR_NUM_OF_DEPND = 'Number of Dependents changed for Borrower';
    final String CO_BORR_NUM_OF_DEPND = 'Number of Dependents changed for Co-Borrower';
    final String GUAR_NUM_OF_DEPND = 'Number of Dependents changed for Guarantor';
    final String CO_BORR_RELATON_TO_BORR = 'Relationship to Borrower changed for Co-Borrower';
    final String GUAR_RELATON_TO_BORR = 'Relationship to Borrower changed for Guarantor';
    final String BORR_RELATON_STATS = 'Relationship Status changed for Borrower';
    final String CO_BORR_RELATON_STATS = 'Relationship Status changed for Co-Borrower';
    final String GUAR_RELATON_STATS = 'Relationship Status changed for Guarantor';
    
    mfiflexUtil.VOLog voLogInstance = mfiflexUtil.VOLog.getInstance('Genesis');    
    /****************
    *Constructor
     ***************/
    public PartyTriggerHandler(List<clcommon__Party__c> newList,Map<Id,clcommon__Party__c> newMap, Map<Id,clcommon__Party__c> oldMap) {
        this.newList = newList;
        this.newMap = newMap;
        this.oldMap = oldMap;
    }
    
    /**********************************************************************************************************************
    * Add to the count on Application field.
    **********************************************************************************************************************/  
    public void onPartyCreateOrUpdate(){
        updateApplication(newMap);
    }
    public void onPartyDelete(){
        updateApplication(oldMap);
    }
    public void updateApplication(Map<Id,clcommon__Party__c> partyMap){
        Savepoint sp = Database.setSavepoint();


        try{
            List<clcommon__Party__c> partyList = partyMap.values();
            List<genesis__Applications__c> updateAppList = new List<genesis__Applications__c>();
            List<genesis__Applications__c> updateAppErrorMsgAppList = new List<genesis__Applications__c>();
            Set<ID> appIds = new Set<ID>();
            Set<ID> appIdsList = new Set<ID>();
            Map<Id,List<String>> errorMsgMap = new Map<Id,List<String>>();
            String message='';
            for(clcommon__Party__c party: partyList){
                if(party.genesis__Application__c != null){
                    appIds.add(party.genesis__Application__c);
                }
            }
            //adding application in the set w.r.t their Party Id
            for(clcommon__Party__c partyId : newList){
                if(partyId.genesis__Application__c != null){
                    appIdsList.add(partyId.genesis__Application__c);
                }
            }
            Map<Id,genesis__Applications__c> partyAppMap = new Map<Id,genesis__Applications__c>([SELECT id,
                                                                                                        Name,
                                                                                                        HEM_Recalculate_Message__c,
                                                                                                        HEM_Value__c 
                                                                                                    FROM genesis__Applications__c 
                                                                                                    WHERE id in : appIdsList]);
            genesis__Applications__c partyApp;
            List<String> existingErrorMsg = new List<String>();
            for(clcommon__Party__c partyId: newList)
            {
                if(partyId.genesis__Application__c != null && oldMap!=null)
                {
                    partyApp = partyAppMap.get(partyId.genesis__Application__c);
                    message = partyApp.HEM_Recalculate_Message__c;
                    //Checking if the Application already has an Error message
                    if(String.isNotBlank(message))
                    {
                        existingErrorMsg = message.split('<br>');
                    }
                    else {
                        existingErrorMsg = new List<String>();
                    }

                    //Checking if the Application already has HEM value calcuated
                    if(partyApp.HEM_Value__c != null)
                    {
                        //Checking if the Application has already Error message in previous iterations
                        if(errorMsgMap.containsKey(partyId.genesis__Application__c))
                        {
                            existingErrorMsg.addall(errorMsgMap.get(partyId.genesis__Application__c));
                        }
                        if(partyId.Party_Type_Name__c == 'BORROWER')
                        {
                            if(partyId.Number_of_Dependents__c != oldMap.get(partyId.Id).Number_of_Dependents__c && !existingErrorMsg.contains(BORR_NUM_OF_DEPND))
                            {
                                existingErrorMsg.add(BORR_NUM_OF_DEPND);
                            }

                            if(partyId.Relationship_Status__c != oldMap.get(partyId.Id).Relationship_Status__c && !existingErrorMsg.contains(BORR_RELATON_STATS)){
                                existingErrorMsg.add(BORR_RELATON_STATS);
                            }
                        }
                        else if(partyId.Party_Type_Name__c == 'CO-BORROWER')
                        {
                            if(partyId.Number_of_Dependents__c != oldMap.get(partyId.Id).Number_of_Dependents__c && !existingErrorMsg.contains(CO_BORR_NUM_OF_DEPND))
                            {
                                existingErrorMsg.add(CO_BORR_NUM_OF_DEPND);
                            }
                            if(partyId.Relationship_to_Applicant__c != oldMap.get(partyId.Id).Relationship_to_Applicant__c && !existingErrorMsg.contains(CO_BORR_RELATON_TO_BORR)){
                                existingErrorMsg.add(CO_BORR_RELATON_TO_BORR);
                            }
                            if(partyId.Relationship_Status__c != oldMap.get(partyId.Id).Relationship_Status__c && !existingErrorMsg.contains(CO_BORR_RELATON_STATS)){
                                existingErrorMsg.add(CO_BORR_RELATON_STATS);
                            }
                        }
                        else if(partyId.Party_Type_Name__c == 'GUARANTOR')
                        {
                            if(partyId.Number_of_Dependents__c != oldMap.get(partyId.Id).Number_of_Dependents__c && !existingErrorMsg.contains(GUAR_NUM_OF_DEPND))
                            {
                                existingErrorMsg.add(GUAR_NUM_OF_DEPND);
                            }
                            if(partyId.Relationship_to_Applicant__c != oldMap.get(partyId.Id).Relationship_to_Applicant__c && !existingErrorMsg.contains(GUAR_RELATON_TO_BORR)){
                                existingErrorMsg.add(GUAR_RELATON_TO_BORR);
                            }
                            if(partyId.Relationship_Status__c != oldMap.get(partyId.Id).Relationship_Status__c && !existingErrorMsg.contains(GUAR_RELATON_STATS)){
                                existingErrorMsg.add(GUAR_RELATON_STATS);
                            }
                        }
                        //Storing Error message list with their respective Application Id
                        errorMsgMap.put(partyId.genesis__Application__c,existingErrorMsg);
                    }
                }
            }
            //Map<Id,genesis__Applications__c> partyAppMap = new Map<Id,genesis__Applications__c>([SELECT id from genesis__Applications__c where id in: appIds]);
            for(genesis__Applications__c app: [SELECT id,
                                                        Name,
                                                        HEM_Recalculate_Message__c,
                                                        (SELECT id,
                                                                Name,
                                                                Party_Type_Name__c
                                                            FROM genesis__Parties__r
                                                            WHERE Party_Type_Name__c =: System.label.COBORROWER_PARTY_TYPE_NAME)
                                                            FROM genesis__Applications__c
                                                            WHERE id IN: appIds]){
                system.debug(app.Id);
                app.Number_of_Co_Borrowers__c = app.genesis__Parties__r.size();
                app.genesis__Discount_Rate__c = GeneratePricing.getDiscountRate(app.id); 
                System.debug(app.genesis__Discount_Rate__c);
                updateAppList.add(app);
            }
            //Adding error msg according to their applications
            for(genesis__Applications__c appObj: [SELECT id,
                                                        Name,
                                                        HEM_Recalculate_Message__c
                                                    FROM genesis__Applications__c
                                                    WHERE id IN: errorMsgMap.keySet()]){
                if(errorMsgMap.containsKey(appObj.Id))
                {
                    if(!errorMsgMap.get(appObj.Id).isEmpty())
                    {
                        //Storing Error message in HEM Recalculate message
                        appObj.HEM_Recalculate_Message__c = String.join(errorMsgMap.get(appObj.Id),'<br>');
                        updateAppErrorMsgAppList.add(appObj);
                    }
                }
            }
            if(updateAppList != null && updateAppList.size() > 0){
                System.debug(updateAppList);
                update updateAppList;
            }
            if(updateAppErrorMsgAppList != null && updateAppErrorMsgAppList.size() > 0){
                update updateAppErrorMsgAppList;
            }
        } 
        catch(Exception e){
            Database.rollback(sp);
            voLogInstance.logException(1001, '[PartyTriggerHandler.updateApplication] Exception at line : ' + e.getLineNumber(), e);
            voLogInstance.commitToDB();
        } 
    }
}