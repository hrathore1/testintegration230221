/*
* Created By Zain from Cludo on 01-09-2020 
* This class has invocable method which can be use fron API or Processbuilder to create folder in Salesforce Libraries
* Test Class RACVF_LibraryCreateFolderTest
*/
public class RACVF_LibraryCreateFolder {
    /*
@InvocableMethod(label='Create Folder' description='Create Folder with name as Contract Number')
public static void createFolder(List<String> contractNumbers) {

List<ContentWorkspace> contentWorkspaceList = [SELECT Id, Name FROM ContentWorkspace WHERE Name IN:contractNumbers]; 

if(contentWorkspaceList.size() == 0)
{
List<ContentWorkspace> contentList = new List<ContentWorkspace>();
for (String contractNumber : contractNumbers) {
ContentWorkspace content = new ContentWorkspace();
content.Name = contractNumber;

contentList.add(content);
}
insert contentList;
}

}
*/  
    @future
    public static void createLibraryFolder(List<String> contractNumbers) {
        /*
        List<ContentWorkspace> contentWorkspaceList = [SELECT Id, Name FROM ContentWorkspace WHERE Name IN:contractNumbers]; 
        
        if(contentWorkspaceList.size() == 0)
        {
            List<ContentWorkspace> contentWorkspaceNewList = new List<ContentWorkspace>();
            for(String contractNumber: contractNumbers)
            {
                ContentWorkspace content = new ContentWorkspace();
                content.Name = contractNumber;
                contentWorkspaceNewList.add(content);
            }
            insert contentWorkspaceNewList;
            
            //Create Content Workspace Permission
            ContentWorkspacePermission contentPermission = [Select id, Description, Name, Type FROM ContentWorkspacePermission WHERE Type='Admin'];
            
            Id groupId = [SELECT Id,Name FROM Group WHERE Name='Asset Library Group'].Id;
            System.debug('groupId---->'+groupId);
            
            List<ContentWorkspaceMember> contentMemberList = new List<ContentWorkspaceMember>();
            for( ContentWorkspace content: contentWorkspaceNewList )
            {
                ContentWorkspaceMember contentWorkspaceMember = new ContentWorkspaceMember();
                contentWorkspaceMember.ContentWorkspaceId =content.Id;
                contentWorkspaceMember.ContentWorkspacePermissionId =contentPermission.Id;
                contentWorkspaceMember.MemberId = groupId;
                contentMemberList.add(contentWorkspaceMember);
            }
            
            insert contentMemberList;
        }
        */
    }
    // @future
    public static void createLibraryFolder(String contractNumber) {
        /*
        List<ContentWorkspace> contentWorkspaceList = [SELECT Id, Name FROM ContentWorkspace WHERE Name =:contractNumber]; 
        
        if(contentWorkspaceList.size() == 0)
        {
            
            ContentWorkspace content = new ContentWorkspace();
            content.Name = contractNumber;
            //content.ShouldAddCreatorMembership=false; 
            insert content;
            
            //Create Content Workspace Permission
            ContentWorkspacePermission contentPermission = [Select id, Description, Name, Type FROM ContentWorkspacePermission WHERE Type='Admin'];
            
            Id groupId = [SELECT Id,Name FROM Group WHERE Name='Asset Library Group'].Id;
            System.debug('groupId---->'+groupId);
            
            ContentWorkspaceMember contentWorkspaceMember = new ContentWorkspaceMember();
            contentWorkspaceMember.ContentWorkspaceId =content.Id;
            contentWorkspaceMember.ContentWorkspacePermissionId =contentPermission.Id;
            contentWorkspaceMember.MemberId = groupId;
            insert contentWorkspaceMember;
        }
     */   
    }
}