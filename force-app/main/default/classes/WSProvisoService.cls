/**
 * @description       : 
 * @author            : chirag.gupta@q2.com
 * @group             : 
 * @last modified on  : 07-10-2020
 * @last modified by  : chirag.gupta@q2.com
 * Modifications Log 
 * Ver   Date         Author                Modification
 * 1.0   07-10-2020   chirag.gupta@q2.com   Initial Version
**/
@RestResource(urlMapping='/provisoResponsePush/*')
global class WSProvisoService {
    
    global class Response{
        public String status;
        public String errorCode;
        public String errorMessage;
        public String statusCode;
    }

    @HttpPost
    global static Response postProvisoData(){
        try{
            RestRequest request = Restcontext.Request;
            //System.debug('request:'+ request);
            System.debug('request.body:'+ request.requestBody.toString());
            
            Response res = new Response();
            List <Attachment> attachmentList = new list<Attachment>();
            JSONParser parser = JSON.createParser(request.requestBody.toString());
            String referrerCode;
            List<id> attachmentIds = new list <id>();
            
            while (parser.nextToken() != null) {
                
                
                if ((parser.getCurrentToken() == JSONToken.FIELD_NAME) && (parser.getText().containsIgnoreCase('.PDF')) ) {
                    parser.nextValue();
                    System.debug('parser.getCurrentName()====='+ parser.getCurrentName());
                    System.debug('parser.getText()====='+ parser.getText());
                    attachmentList.add(createAttachment(parser.getText(),'application/pdf',parser.getCurrentName()));
                   
                }
              
                
                if ((parser.getCurrentToken() == JSONToken.FIELD_NAME) && (parser.getText().contains('referrerCode')) ) {
                    // Get the value.
                    parser.nextValue();
                    referrerCode = parser.getText();
                    
                }
                
                /*if (parser.getCurrentName()  == 'rawJson')) {
                    // Get the value.
                    parser.nextValue();
                    System.debug('rawJson parser.getText()====='+ parser.getText());
                    attachmentList.add(createAttachment(parser.getText(),'text/plain','rawJson'));
                    
                }*/
            }
           
           
           
           if (referrerCode!=null){
               referrerCode = referrerCode.substring(5);
               List < clcommon__Party__c > partyList = [Select id,
                                                               name ,
                                                               clcommon__Account__c,
                                                               genesis__Application__c
                                                               from clcommon__Party__c 
                                                         where name =: referrerCode];
               
               if(partyList.size()>0){
                   List< clcommon__Document_Category__c > dcList = [select id 
                                                                    from clcommon__Document_Category__c 
                                                                    where clcommon__Account__c =: partyList[0].clcommon__Account__c
                                                                    and genesis__Application__c =: partyList[0].genesis__Application__c];
                                                                 
                   for (attachment att : attachmentList){
                       att.parentId = partyList[0].genesis__Application__c;
                   }
                   if(dcList.size()>0){
                       if(attachmentList.size()>0){
                           insert attachmentList;
                           for (attachment atta : attachmentList){
                               attachmentIds.add(atta.id);
                           }
                           if(attachmentIds.size()>0){
                               genesis.Response result = genesis.ApplicationDocumentManagement.linkAttachments(dcList[0].id, attachmentIds );
                               System.debug('result====='+ result);
                           }
                           else {
                               throw new CustomException('No attachment Id found');
                           }
                       } 
                       else {
                           throw new CustomException('No attachment found');
                       }
                   } 
                   else {
                       throw new CustomException('No Document category found');
                   }                                                    
               } else {
                   throw new CustomException('No Party found');
               }
           } else {
               throw new CustomException('No referrerCode found');
           }
           
          
            



            res.status = System.label.TAG_SUCCESS + ': Service under development.';
            res.statusCode = '200';
            return res;
        }
        catch(Exception e){
            //insert Log
            insert new clcommon__Log__c( clcommon__Message__c = 'Response Body: WSProvisoService :'+e,
                                         clcommon__Module_Acronym__c = 'Genesis',
                                         clcommon__Time__c = datetime.now());
            
            
            Response res = new Response();
            res.status = System.label.TAG_EXCEPTION;
            res.errorCode = 'TECHNICAL_EXCEPTION';
            res.errorMessage = 'Exception: ' + e.getMessage();
            res.statusCode = '404';
            return res;
            
        }
    }
    global static Attachment createAttachment (String jsonString, string contentType, string fileName){
        
        Attachment attach = new Attachment();
        attach.contentType = contentType; //'application/pdf'
        attach.name = (filename.length()>79)? fileName.substring(0,75):fileName;
        //attach.parentId = parentId;
        if(fileName =='rawJson'){
            attach.body = blob.valueof(jsonString);
        }
        else{
            attach.body = EncodingUtil.base64Decode(jsonString);
            
           
        }
        return attach;
    }
}