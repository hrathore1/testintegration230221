global class EACreditReportBatch implements Schedulable, 
                                              Database.Batchable<sObject>, 
                                              Database.AllowsCallouts, 
                                              Database.Stateful {

    public String applicationId;
    public JWF_Application_Execution_Log__c appExecutionLog;
    public List<String> listReturnMsgs = new List<String>();
    public EACreditReportBatch(String applicationId, JWF_Application_Execution_Log__c appExecutionLog){
        this.applicationId = applicationId;
        this.appExecutionLog = appExecutionLog;
    }

    global Database.QueryLocator start(Database.BatchableContext BC) {
        try{
            List<String> listPartyTypes = System.label.INTEGRATION_PARTIES.split(',');
            mfiflexUtil.ExecutionContext ec = mfiflexUtil.ExecutionContext.getExecContext();
            mfiflexUtil.ObjectCache partyData = ec.getObject('partyData');
            if (partyData != null) {
                ec.deleteObject('partyData');
            }
            mfiflexUtil.ObjectCache entries = ec.createObject('partyData', 'clcommon__Party__c', true);
            SObjectType partySObj = Schema.getGlobalDescribe().get('clcommon__Party__c');    
            Map<String, Schema.SObjectField> partyfields = partySObj.getDescribe().fields.getMap();
            entries.addFields('clcommon__Account__r.name');
            entries.addFields('clcommon__Account__r.id');
            for(String field: partyfields.keyset()) {
                entries.addFields(field);
            }
            
            entries.addNamedParameter('applicationId', this.applicationId);
            entries.addNamedParameter('partyTypes', listPartyTypes);
            entries.setWhereClause('genesis__Application__c = :applicationId AND Party_Type_Name__c IN :partyTypes');
            entries.buildQuery();
            
            return Database.getQueryLocator(entries.getQuery());
        }
        catch(Exception e){throw e;}
    }
   
    global void execute(SchedulableContext sc) {
        EACreditReportBatch job = new EACreditReportBatch(this.applicationId, this.appExecutionLog);
        Database.executeBatch(job, 1);
    }

    public void Execute(Database.BatchableContext bc, List<sObject> scope) {
        List<clcommon__Party__c> partyList = (List<clcommon__Party__c>) scope;
        clcommon__Party__c party = partyList.get(0);    
        try {
            List<Equifax_Apply__c> eaRecords = [SELECT id,
                                                        name,
                                                        Is_Latest__c,
                                                        enquiry_Id__c
                                                   FROM Equifax_Apply__c
                                                  WHERE Application__c = :applicationId
                                                    AND Account__c = : party.clcommon__Account__r.id
                                                    AND Party_Type__c = :party.Party_Type_Name__c
                                                    AND Is_Latest__c = true
                                                ];
                           
            String message = '';
            if(eaRecords != null && eaRecords.size() > 0 && eaRecords[0].enquiry_Id__c != null){
                message = EquifaxSystemCallouts.calloutEACreditReport(eaRecords[0].enquiry_Id__c, applicationId, party.clcommon__Account__r.name);
            }
            listReturnMsgs.add('Message for party ' + party.clcommon__Account__r.name + '(' + party.Party_Type_Name__c + '):' + message);
        } 
        catch (Exception e) {
            String message = 'Exception for party ' + party.clcommon__Account__r.name + '(' + party.Party_Type_Name__c + '):' + e.getMessage();
            listReturnMsgs.add(message);
            ExceptionLog.insertExceptionLog(e, message, this.applicationId);
        }
    }
    
    public void Finish(Database.BatchableContext bc) {
        try {
            this.appExecutionLog.Message__c += convertListToString(listReturnMsgs);
            Map<String, Object> mapExecutionLog = new Map<String, Object>();
            mapExecutionLog.put('appExecutionLog', this.appExecutionLog);

            ApplicationExecutionAction action = new ApplicationExecutionAction(this.applicationId);
            action.changeStatus(mapExecutionLog);
            action.doInvokeAction(null);   
        } 
        catch (Exception e) {ExceptionLog.insertExceptionLog(e, '[EACreditReportBatch.Finish()]', this.applicationId);}           
    }

    public String convertListToString(List<String> strList){
        try{
            if(strList != null && strList.size() > 0){
                String returnString = '';
                for(String str : strList){
                    returnString += '=> ' + str + '<br>';
                }                
                return returnString;
            }
            return '';
        }
        catch(Exception e){throw e;}
    }
}