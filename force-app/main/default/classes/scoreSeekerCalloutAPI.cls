/**
 * @File Name          : scoreSeekerCalloutAPI.cls
 * @Description        : 
 * @Author             : chirag.gupta@q2.com
 * @Group              : 
 * @Last Modified By   : chirag.gupta@q2.com
 * @Last Modified On   : 6/4/2020, 5:32:38 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    6/4/2020   chirag.gupta@q2.com     Initial Version
**/
global without sharing class scoreSeekerCalloutAPI{
    /*webservice static ints.IntegrationResponseObject makeScoreSeekerCallout(Id accId, Id app, Id partyId){ 
        //System.debug('::::Application Id is::::'+accntId);       
        ints.IntegrationResponseObject responseObject; 
        try{           
            Map<String,Object> accountValues = new Map<String,Object>();            
            accountValues.put('accountId', accId);
            accountValues.put('appId', app);
            accountValues.put('Residential', 'Residential');
            accountValues.put('partyId', partyId);
            
            Map<String,Object> borrowerValues1 = new Map<String,Object>();
            borrowerValues1.put('Address',accountValues);
            borrowerValues1.put('Borrower',accountValues);
            borrowerValues1.put('PartyInfo',accountValues);
            borrowerValues1.put('Application',accountValues);
            borrowerValues1.put('EmploymentInfo',accountValues);
            borrowerValues1.put('ExecutionPriority', 2);
            system.debug(borrowerValues1);
            
            List<ints__Integration_Configuration__c> integConfig = [SELECT id, 
                                                                           name,
                                                                           ints__Integration_Service__c,
                                                                           ints__Type__c                                                                                                      
                                                                      FROM ints__Integration_Configuration__c                                                                                                      
                                                                     WHERE ints__Integration_Service__c ='Credit Check' 
                                                                       AND Name = 'ScoreSeekerCreditScore'
                                                                   ];
            
            System.debug('::::config data is::::'+integConfig);
            System.debug('::::borrowerValues are::::'+borrowerValues1);
            
            if(integConfig != null && integConfig.size() > 0){
                ints.AbstractMockService   mc  = ints.APIFactory.getPassthroughService();
                System.debug('mcmcmcmc::::'+mc);
                responseObject = mc.runPassthroughService(borrowerValues1,integConfig[0].ints__Integration_Service__c,'');
                system.debug(responseObject);
            }
            
        }
        catch(Exception ex){
            system.debug(ex.getMessage()+'~'+ex.getcause()+'~'+ex.getLineNumber()+'~'+ex.getStackTraceString()+'~'+ex.getTypeName());
        }
        return responseObject;
    }*/
}