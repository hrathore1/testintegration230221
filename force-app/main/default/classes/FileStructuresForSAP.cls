public class FileStructuresForSAP {
    /*
    public static String getRPDFileHeader(){
        String recordTypeId = 'H';
        String runNumber = '0';
        return (recordTypeId + ',' + CommonUtility.getDatetimeValue('CCYYMMDDHHMMSS') + ',' + runNumber);
    }

    public static String getPartyDetailRecord(Account acc){
        try{
            String recordIdentifier = 'P';
            String sovereignCrossRef = acc.Customer_Number__c;
            String title = String.isBlank(acc.Salutation) ? '' : acc.Salutation.substring(0, (acc.Salutation.length() - 1));
            String surname = String.isBlank(acc.LastName) ? '' : acc.LastName;
            String firstName = String.isBlank(acc.FirstName) ? '' : acc.FirstName;
            String middleName = String.isBlank(acc.MiddleName) ? '' : acc.MiddleName;
            String gender = String.isBlank(acc.Gender__pc) ? '' : (acc.Gender__pc.equalsIgnoreCase('Male') ? 'M' : 'F');
            String dateOfBirth = String.isBlank(String.valueOf(acc.PersonBirthdate)) ? '' : CommonUtility.getDateValue(acc.PersonBirthdate, 'DD_MM_YYYY');
            String membershipNumber = String.isBlank(acc.Membership_Number__c) ? '' : acc.Membership_Number__c;
            String homePhoneNumber = String.isBlank(acc.PersonHomePhone) ? '' : acc.PersonHomePhone;
            String workPhoneNumber = String.isBlank(acc.Phone) ? '' : acc.Phone;
            String fax = String.isBlank(acc.Fax) ? '' : acc.Fax;
            String abn = String.isBlank(acc.ABN__c) ? '' : acc.ABN__c;
            String maritalStatus = String.isBlank(acc.clcommon__Marital_Status__pc) ? '' : acc.clcommon__Marital_Status__pc;
            String driverLicence = String.isBlank(acc.ints__Driver_License__pc) ? '' : acc.ints__Driver_License__pc;
            
            return (recordIdentifier + ',' +
                    sovereignCrossRef + ',' +
                    title + ',' +
                    surname + ',' +
                    firstName + ',' +
                    middleName + ',' +
                    gender + ',' +
                    dateOfBirth + ',' +
                    membershipNumber + ',' +
                    homePhoneNumber + ',' +
                    workPhoneNumber + ',' +
                    fax + ',' +
                    abn + ',' +
                    maritalStatus + ',' +
                    driverLicence);
        }
        catch(Exception e){
            throw new CustomException('[FileStructuresForSAP.getPartyDetailRecord] Exception:' + e.getMessage() + ', at line: ' + e.getLineNumber());
        }
    }

    public static String getAddressDetails(List<clcommon__Address__c> listAddresses){
        try{
            String resBuildingName = '';
            String resApartmentCode = '';
            String resAddress1 = '';
            String resSuburb = '';
            String resPostcode = '';
            String mlgBuildingName = '';
            String mlgApartmentCode = '';
            String mlgAddress1 = '';
            String mlgSuburb = '';
            String mlgPostcode = '';

            for(clcommon__Address__c address : listAddresses){
                if(System.label.ADDRESS_TYPE_MAILING.equalsIgnoreCase(address.Address_Type__c)){
                    mlgBuildingName = String.isBlank(address.Building_Name__c) ? '' : address.Building_Name__c;
                    mlgApartmentCode = String.isBlank(address.Property_Type_Number__c) ? '' : address.Property_Type_Number__c;
                    mlgAddress1 = String.isBlank(address.RPD_Address_Format__c) ? '' : address.RPD_Address_Format__c;
                    mlgSuburb = String.isBlank(address.clcommon__County__c) ? '' : address.clcommon__County__c;
                    mlgPostcode = String.isBlank(address.Postcode__c) ? '' : address.Postcode__c;
                }
                else {
                    resBuildingName = String.isBlank(address.Building_Name__c) ? '' : address.Building_Name__c;
                    resApartmentCode = String.isBlank(address.Property_Type_Number__c) ? '' : address.Property_Type_Number__c;
                    resAddress1 = String.isBlank(address.RPD_Address_Format__c) ? '' : address.RPD_Address_Format__c;
                    resSuburb = String.isBlank(address.clcommon__County__c) ? '' : address.clcommon__County__c;
                    resPostcode = String.isBlank(address.Postcode__c) ? '' : address.Postcode__c;
                }
            }


            return (resBuildingName + ',' +
                    resApartmentCode + ',' +
                    resAddress1 + ',' +
                    resSuburb + ',' +
                    resPostcode + ',' +
                    mlgBuildingName + ',' +
                    mlgApartmentCode + ',' +
                    mlgAddress1 + ',' +
                    mlgSuburb + ',' +
                    mlgPostcode);
        }
        catch(Exception e){
            throw new CustomException('[FileStructuresForSAP.getAddressDetails] Exception:' + e.getMessage() + ', at line: ' + e.getLineNumber());
        }
    }

    public static List<String> getContractDetails(List<loan__Loan_Account__c> listClContracts, 
                                                  List<loan__Bank_Account__c> listBankAccounts){
        try{
            String recordIdentifier = 'C';
            String roleCode = 'PRIME BORROWER';
            String bankAccountNumber = '';
            String accountHolderName = '';
            String ddFlag = '';
            if(listBankAccounts != null && listBankAccounts.size() > 0){
                loan__Bank_Account__c bankAccount = listBankAccounts.get(0);
                String bsbValue = String.isBlank(bankAccount.BSB__c) ? '' : bankAccount.BSB__c;
                String bankAccountNo = String.isBlank(bankAccount.loan__Bank_Account_Number__c) ? '' : bankAccount.loan__Bank_Account_Number__c; 
                bankAccountNumber = bsbValue + '' + bankAccountNo;
                accountHolderName = bankAccount.Account_Holder_Name__c;

                if(!String.isBlank(bankAccountNumber) && !String.isBlank(accountHolderName)){
                    ddFlag = 'Y';
                }
                else{
                    ddFlag = 'N';
                }
            }   

            List<String> listContractRecords = new List<String>();
            for(loan__Loan_Account__c loanAcc : listClContracts){
                String productName = loanAcc.loan__Loan_Product_Name__c != null ? loanAcc.loan__Loan_Product_Name__r.name : '';
                String internalLoanNumber = loanAcc.name;
                String maturityDate = loanAcc.loan__Last_Installment_Date__c != null ? CommonUtility.getDateValue(loanAcc.loan__Last_Installment_Date__c, 'DD_MM_YYYY') : '';
                String loanStatus = String.isBlank(loanAcc.loan__Loan_Status__c) ? '' : loanAcc.loan__Loan_Status__c;
                String sovereignCrossRef = loanAcc.loan__Account__r.Customer_Number__c;
                String contractStatus = '';
                if(loanStatus.contains(System.label.RPD_FILE_SETTLED_STATUSES)){
                    contractStatus = System.label.RPD_FILE_CONTRACT_STATUS_SETTLED;
                }
                else{
                    contractStatus = System.label.RPD_FILE_CONTRACT_STATUS_ACTIVE;
                }

                String contractRec = recordIdentifier + ',' +
                                     sovereignCrossRef + ',' +
                                     productName + ',' +
                                     internalLoanNumber + ',' +
                                     maturityDate + ',' +
                                     roleCode + ',' +
                                     contractStatus + ',' +
                                     bankAccountNumber + ',' +
                                     accountHolderName + ',' +
                                     ddFlag;
                
                listContractRecords.add(contractRec);
            }
            
            return listContractRecords;
        }
        catch(Exception e){
            throw new CustomException('[FileStructuresForSAP.getContractDetails] Exception:' + e.getMessage() + ', at line: ' + e.getLineNumber());
        }
    }

    public static String getRPDFileTrailer(Integer countOfRec){
        try{
            String recordTypeId = 'T';
            return recordTypeId + ',' + countOfRec + '\r\n';
        }
        catch(Exception e){
            throw new CustomException('[FileStructuresForSAP.getRPDFileTrailer] Exception:' + e.getMessage() + ', at line: ' + e.getLineNumber());
        }
    }
    */
}