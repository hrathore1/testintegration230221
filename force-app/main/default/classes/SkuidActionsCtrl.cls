/**
 * @description       : 
 * @author            : chirag.gupta@q2.com
 * @group             : 
 * @last modified on  : 09-25-2020
 * @last modified by  : chirag.gupta@q2.com
 * Modifications Log 
 * Ver   Date         Author                Modification
 * 1.0   09-23-2020   chirag.gupta@q2.com   Initial Version
**/
global class SkuidActionsCtrl{
    global class AppOnCreateRequest {
        @InvocableVariable(required=true)
        global String appId;
        
        @InvocableVariable(required=true)
        global String status;
    }
    @InvocableMethod
    // here input param is smsRequest of List type
    global static void appList(AppOnCreateRequest[] requests){
        for (AppOnCreateRequest request : requests) {
            List<genesis__Applications__c> contextApps = [SELECT id,
                                                         Application_reference_number__c,
                                                         Loan_term_years__c,
                                                         Case__c,
                                                         genesis__Payment_Frequency__c,
                                                         genesis__Account__c,
                                                         Application_Term__c,
                                                         Loan_Channel__c,
                                                         genesis__CL_Product__c,
                                                         Pricing_Interest__c,
                                                         Franchise__c,
                                                         Broker_Originator__c,
                                                         (SELECT id,
                                                             Name,
                                                             clcommon__Type__r.Name,
                                                             clcommon__Account__r.Membership_card_colour__c 
                                                        FROM genesis__Parties__r
                                                       WHERE clcommon__Type__r.Name IN ('BORROWER','CO-BORROWER','GUARANTOR')
                                                     ),
                                                     (SELECT id
                                                        FROM genesis__Fees__r)
                                                    FROM genesis__Applications__c
                                                   WHERE id =: request.appId
                                                 ];
           if(contextApps != null && contextApps.size() > 0){
               genesis__Applications__c app = contextApps[0];
               if(request != null && request.status != null){
                   if('apply_fee'.equalsIgnoreCase(request.status)){
                       if(app.genesis__Fees__r == null || app.genesis__Fees__r.size() == 0){
                           applyFee(app);
                       }
                   }
                   else if('generate_crn'.equalsIgnoreCase(request.status)){
                       if(app.Application_reference_number__c != null && String.isBlank(app.CRN_Number__c)){
                            String crn = BpayNumberGenerator.generateBpayNumber(app.Application_reference_number__c,'',10);
                            if(crn != null){
                                app.CRN_Number__c = crn;
                            }
                        }
                        if(app.Loan_term_years__c != null){
                            app.Application_Term__c = Integer.valueOf(app.Loan_term_years__c) * 12;
                            app.Pricing_Term__c = Integer.valueOf(app.Loan_term_years__c) * 12;
                        }
                        app.genesis__Expected_Start_Date__c = CommonUtility.getSystemDate();
                        for(clcommon__Party__c party : app.genesis__Parties__r){
                            if('BORROWER'.equalsIgnoreCase(party.clcommon__Type__r.Name)){
                                app.Borrower_Party__c = party.id;//app.genesis__Parties__r[0].id;
                            }
                        }
                        createCase(app);
                        update app;
                   }
                   /*else if('populate_fields'.equalsIgnoreCase(request.status)){
                   
                   }
                   else if('create_case'.equalsIgnoreCase(request.status)){
                   
                   }
                   */
                   else if('generate_pricing'.equalsIgnoreCase(request.status)){
                       GeneratePricing.generateAndApplyPricing(app.Id);
                   }
                   else if('calculate_discount'.equalsIgnoreCase(request.status)){
                       genesis__Applications__c appInstance = new genesis__Applications__c(Id = app.Id);
                       appInstance.genesis__Discount_Rate__c = GeneratePricing.getDiscountRate(app.Id);
                       update appInstance;
                   }
                   else if('broker_hierarchy'.equalsIgnoreCase(request.status)){
                       BrokerHierarchyAPI.processBrokerHierarchy(app);
                   }
               }
           }
        }
    }
    /*@InvocableMethod
    public static void getApp(List<Id> appIds, String ){
        List<genesis__Applications__c> contextApps = [SELECT id,
                                                         Application_reference_number__c,
                                                         Loan_term_years__c,
                                                         Case__c,
                                                         genesis__Payment_Frequency__c,
                                                         genesis__Account__c,
                                                         Application_Term__c,
                                                         Loan_Channel__c,
                                                         genesis__CL_Product__c,
                                                         Pricing_Interest__c,
                                                         Franchise__c,
                                                         Broker_Originator__c,
                                                         (SELECT id,
                                                             Name,
                                                             clcommon__Type__r.Name,
                                                             clcommon__Account__r.Membership_card_colour__c 
                                                        FROM genesis__Parties__r
                                                       WHERE clcommon__Type__r.Name IN ('BORROWER','CO-BORROWER','GUARANTOR')
                                                     )
                                                    FROM genesis__Applications__c
                                                   WHERE id =: appIds
                                                 ];
        for(genesis__Applications__c app: contextApps){
            SkuidActionsCtrl.appOnCreateActions(appId);
        }
    }*/
    webservice static String appOnCreateActions(String appId){
        try{
            List<genesis__Applications__c> thisApp = [SELECT id,
                                                             Application_reference_number__c,
                                                             Loan_term_years__c,
                                                             Case__c,
                                                             genesis__Payment_Frequency__c,
                                                             genesis__Account__c,
                                                             Application_Term__c,
                                                             Loan_Channel__c,
                                                             genesis__CL_Product__c,
                                                             Pricing_Interest__c,
                                                             Franchise__c,
                                                             Broker_Originator__c,
                                                             CRN_Number__c,
                                                             (SELECT id,
                                                                 Name,
                                                                 clcommon__Type__r.Name,
                                                                 clcommon__Account__r.Membership_card_colour__c 
                                                            FROM genesis__Parties__r
                                                           WHERE clcommon__Type__r.Name IN ('BORROWER','CO-BORROWER','GUARANTOR')
                                                         )
                                                        FROM genesis__Applications__c
                                                       WHERE id =: appId
                                                     ];
            
            if(thisApp != null && thisApp.size() > 0){
                genesis__Applications__c app = thisApp.get(0);
                applyFee(app);
                
                //Generate CRN number
                if(app.Application_reference_number__c != null && String.isEmpty(app.CRN_Number__c)){
                    String crn = BpayNumberGenerator.generateBpayNumber(app.Application_reference_number__c,'',10);
                    if(crn != null){
                        app.CRN_Number__c = crn;
                    }
                }
                /*else{
                    return ('Application reference number found for application:' + app.Application_reference_number__c);
                }*/
                
                if(app.Loan_term_years__c != null){
                    app.Application_Term__c = Integer.valueOf(app.Loan_term_years__c) * 12;
                    app.Pricing_Term__c = Integer.valueOf(app.Loan_term_years__c) * 12;
                }
                app.genesis__Expected_Start_Date__c = CommonUtility.getSystemDate();
                //if(app.genesis__Parties__r != null && app.genesis__Parties__r.size() > 0){
                //    app.Borrower_Party__c = app.genesis__Parties__r[0].id;
                //}
                for(clcommon__Party__c party : app.genesis__Parties__r){
                    if('BORROWER'.equalsIgnoreCase(party.clcommon__Type__r.Name)){
                        app.Borrower_Party__c = party.id;//app.genesis__Parties__r[0].id;
                    }
                }
                createCase(app);
                
                update app;
                
                //System.debug('CRN Number: ' + app.CRN_Number__c);
                //if(!String.isBlank(app.CRN_Number__c)){
                //    RACVF_LibraryCreateFolder.createLibraryFolder(new List<String>{app.CRN_Number__c});
                //}
                GeneratePricing.generateAndApplyPricing(app.Id);
                
                genesis__Applications__c appInstance = new genesis__Applications__c(Id = app.Id);
                appInstance.genesis__Discount_Rate__c = GeneratePricing.getDiscountRate(app.Id);
                update appInstance;
                
                BrokerHierarchyAPI.processBrokerHierarchy(app);
            }
            else{
                return ('Application not found for id:' + appId);
            }
            
            return System.label.SUCCESS_MESSAGE;
        }
        catch(Exception e){
            return ('[SkuidActionsCtrl.appOnCreateActions] Exception:' + e.getMessage());
        }
    }
        
    webservice static void applyFee(genesis__Applications__c thisApp){
        try{
            String productId;
            List<clcommon__Fee__c> fees = new List<clcommon__Fee__c>();
                                                      
            if(thisApp != null && thisApp.genesis__CL_Product__c != null){
                productId = thisApp.genesis__CL_Product__c;
            }     
            else {
                throw new CommonUtility.CustomException('[SkuidActionsCtrl.applyFee] No Product found for this application. Automatic fees cannot be applied.');
            }
            
            Boolean isLoanChannelBroker = false;
            if(thisApp != null && !String.isBlank(thisApp.Loan_Channel__c) && 'BROKER'.equalsIgnoreCase(thisApp.Loan_Channel__c)){
                isLoanChannelBroker = true;
            }
            
            List<CL_Product_Fee_Junction__c> feeDefinition_List= [SELECT Id,
                                                                         Fee_Definition__c,
                                                                         Fee_Definition__r.Id,
                                                                         Fee_Definition__r.name, 
                                                                         Fee_Definition__r.clcommon__Amount__c,
                                                                         Is_Broker_Fee__c  
                                                                    FROM CL_Product_Fee_Junction__c 
                                                                   WHERE Apply_Automatically__c = true 
                                                                     AND CL_Product__c = :productId 
                                                                     AND Is_Broker_Fee__c = :isLoanChannelBroker 
                                                                 ];
        
            if(feeDefinition_List == null || feeDefinition_List.size() == 0){
                return;
            }
            
            for(CL_Product_Fee_Junction__c fee_def: feeDefinition_List) {
                if(fee_def.Fee_Definition__c == null){
                    continue;
                }
                
                Decimal feeAmount = fee_def.Fee_Definition__r.clcommon__Amount__c;
                String feeDefId = fee_def.Fee_Definition__c;
                clcommon__Fee__c fee = new clcommon__Fee__c(genesis__Application__c = thisApp.Id,
                                                            clcommon__Original_Amount__c = feeAmount,
                                                            clcommon__Date__c = Date.today(),
                                                            Applied_Automatically__c = true,
                                                            clcommon__Fee_Definition__c = feeDefId);
                fees.add(fee);  
            }
            
            if(fees.size() > 0) {
                //Insert Fees into associated Fees object
                insert fees;      
            }
        } 
        catch(Exception e) {
            throw new CommonUtility.CustomException('[SkuidActionsCtrl.applyFee] Exception: ' + e.getMessage());
        }
    }
    
    webservice static String mapLoanDisbursement(String appId){
        mfiflexUtil.VOLog voLogInstance = mfiflexUtil.VOLog.getInstance('Genesis');
        try{
            GenerateContract gc = new GenerateContract();
            String str = gc.settleApplicationToContract(appId);
            return str;
        }
        catch(Exception e){
            voLogInstance.logException(1001, '[SkuidActionsCtrl.mapLoanDisbursement] Error: ' + e.getMessage() + ' (at line: ' + e.getLineNumber() + ')', e);
            voLogInstance.commitToDb();
            return 'Exception: ' + e.getMessage();
        }
    }

    /*webservice static String validatePaymentDate(String applicationId, String paymentDate, String disbDate){
        mfiflexUtil.VOLog voLogInstance = mfiflexUtil.VOLog.getInstance('Genesis');
        try{
            GenerateContract gc = new GenerateContract();
            String retVal = gc.validatePaymentDate(applicationId, paymentDate, disbDate);
            return retVal;
        }
        catch(Exception e){
            voLogInstance.logException(1001, '[SkuidActionsCtrl.validatePaymentDate] Error: ' + e.getMessage() + ' (at line: ' + e.getLineNumber() + ')', e);
            voLogInstance.commitToDb();
            return 'Exception: ' + e.getMessage();
        }
    }
    
    webservice static String validationsOnSubmitToCredit(String applicationId){
        mfiflexUtil.VOLog voLogInstance = mfiflexUtil.VOLog.getInstance('Genesis');
        try{
            List<String> listErrors = ScreenValidations.onSubmitToCredit(applicationId);
            
            if(listErrors != null && listErrors.size() > 0){
                String errorString = '';
                Integer index = 1;
                for(String str : listErrors){
                    errorString += index++ + '. ' + str + '\r\n';
                }
                
                return errorString;
            }
            return 'SUCCESS';
        }
        catch(Exception e){
            voLogInstance.logException(1001, '[SkuidActionsCtrl.validationsOnSubmitToCredit] Error: ' + e.getMessage() + ' (at line: ' + e.getLineNumber() + ')', e);
            voLogInstance.commitToDb();
            return 'Exception: ' + e.getMessage();
        }
    }*/
    
    /*webservice static String validationsOnApprove(String applicationId){
        mfiflexUtil.VOLog voLogInstance = mfiflexUtil.VOLog.getInstance('Genesis');
        try{
            String returnStr = GenerateContract.createAppDisbDetails(applicationId);
            return returnStr;
        }
        catch(Exception e){
            voLogInstance.logException(1001, '[SkuidActionsCtrl.validationsOnSubmitToCredit] Error: ' + e.getMessage() + ' (at line: ' + e.getLineNumber() + ')', e);
            voLogInstance.commitToDb();
            return 'Exception: ' + e.getMessage();
        }
    }*/

    webservice static String approveApplication(String applicationId){
        mfiflexUtil.VOLog voLogInstance = mfiflexUtil.VOLog.getInstance('Genesis');
        try{
            
            List<genesis__Applications__c> listApps = [SELECT id,
                                                              Name,
                                                              genesis__Loan_Amount__c,
                                                              genesis__Status__c,
                                                              Final_Decision__c,
                                                              Initial_Decision__c,
                                                              Approval_Date__c,
                                                              genesis__CL_Product__r.clcommon__Asset_Class__c
                                                         FROM genesis__Applications__c
                                                        WHERE id = :applicationId
                                                      ];

            if(listApps != null && listApps.size() > 0){
                Date systemDate = CommonUtility.getSystemDate();
                genesis__Applications__c app = listApps.get(0);
                
                if('APPROVED'.equalsIgnoreCase(app.genesis__Status__c)){
                    return 'Application is already approved.';
                }

                List<ServiceResource> listServiceResource = [SELECT id,
                                                                    name,
                                                                    (SELECT id,
                                                                            name,
                                                                            DLA_Amount__c
                                                                       FROM Credit_Delegation_Lending_Authoritys__r
                                                                      WHERE Start_Date__c <= :systemDate
                                                                        AND End_Date__c >= :systemDate
                                                                        AND Asset_Class__c = :app.genesis__CL_Product__r.clcommon__Asset_Class__c
                                                                        AND DLA_Amount__c >= :app.genesis__Loan_Amount__c
                                                                    ) 
                                                               FROM ServiceResource
                                                              WHERE RelatedRecordId = :System.UserInfo.getUserId()
                                                                AND IsActive = true
                                                            ];

                System.debug('listServiceResource :' + listServiceResource );
                if(listServiceResource != null && listServiceResource.size() > 0){
                    ServiceResource serResource = listServiceResource.get(0);
                    List<Credit_Delegation_Lending_Authority__c> listDLA = serResource.Credit_Delegation_Lending_Authoritys__r;
                    
                    System.debug('listDLA :' + listDLA );

                    if(listDLA != null && listDLA.size() > 0){
                        if(listDLA.size() == 1){
                            app.genesis__Status__c = 'APPROVED';
                            app.Final_Decision__c = 'APPROVED';
                            //app.Initial_Decision__c = 'APPROVED';
                            app.Approval_Date__c = systemDate;
                            update app;
                            return System.label.SUCCESS_MESSAGE;//GenerateContract.createDisbursalRecordsForApplication(applicationId);                            
                        }
                        return 'User has duplicate Credit Authorisation Records.';
                    }
                    return 'User does not have Credit Authorisation to approve. Seek Assistance from Credit Manager';
                }
                return 'User does not have authority to perform approval action. Please contact system admin.';                
            }
            return 'No Valid Application found corresponding to ' + applicationId;            
        }
        catch(Exception e){
            voLogInstance.logException(1001, '[SkuidActionsCtrl.approveApplication] Error: ' + e.getMessage() + ' (at line: ' + e.getLineNumber() + ')', e);
            voLogInstance.commitToDb();
            return 'Exception: ' + e.getMessage();
        }
    }

    webservice static String validateDiscretionRate(String applicationId,
                                                    Decimal rateValue,
                                                    String discretionReason){
        mfiflexUtil.VOLog voLogInstance = mfiflexUtil.VOLog.getInstance('Genesis');
        try{
            System.debug('rateValue:' + rateValue);
            List<genesis__Applications__c> listApps = [SELECT id,
                                                              Name,
                                                              genesis__Loan_Amount__c,
                                                              genesis__CL_Product__r.clcommon__Asset_Class__c,
                                                              Discretion__c,
                                                              Discretion_Reason__c
                                                         FROM genesis__Applications__c
                                                        WHERE id = :applicationId
                                                      ];

            if(listApps != null && listApps.size() > 0){
                Date systemDate = CommonUtility.getSystemDate();
                genesis__Applications__c app = listApps.get(0);

                List<ServiceResource> listServiceResource = [SELECT id,
                                                                    name,
                                                                    (SELECT id,
                                                                            name
                                                                       FROM Interest_Rate_Discretion_Authoritys__r
                                                                      WHERE Start_Date__c <= :systemDate
                                                                        AND End_Date__c >= :systemDate
                                                                        AND Asset_Class__c = :app.genesis__CL_Product__r.clcommon__Asset_Class__c
                                                                        AND IRA_Percentage__c <= :rateValue
                                                                    ) 
                                                               FROM ServiceResource
                                                              WHERE RelatedRecordId = :System.UserInfo.getUserId()
                                                                AND IsActive = true
                                                            ];

                if(listServiceResource != null && listServiceResource.size() > 0){
                    ServiceResource serResource = listServiceResource.get(0);
                    List<Interest_Rate_Discretion_Authority__c> listDLA = serResource.Interest_Rate_Discretion_Authoritys__r;

                    if(listDLA != null && listDLA.size() > 0){
                        if(listDLA.size() == 1){
                            //app.Discretion__c = Decimal.valueOf(rateValue);
                            //app.Discretion_Reason__c = discretionReason;      
                            //update app;
                            
                            if(String.isBlank(discretionReason)){
                                return 'Please provide discretion reason.';
                            }
                            return System.label.SUCCESS_MESSAGE;        
                        }
                        return 'User has duplicate Interest Rate Delegation Records.';
                    }
                    return 'User does not have Discretion to discount Interest Rate. Seek Assistance from Manager.';
                }
                return 'User does not have authority to perform interest discount action. Please contact system admin.';                
            }
            return 'No Valid Application found corresponding to ' + applicationId;            
        }
        catch(Exception e){
            voLogInstance.logException(1001, '[SkuidActionsCtrl.applyDiscretionRate] Error: ' + e.getMessage() + ' (at line: ' + e.getLineNumber() + ')', e);
            voLogInstance.commitToDb();
            return 'Exception: ' + e.getMessage();
        }
    }
    
    webservice static String validateFeeWaiverDLA(String applicationId,
                                                  String feeDefinitionId,
                                                  String waiverAmount){
        mfiflexUtil.VOLog voLogInstance = mfiflexUtil.VOLog.getInstance('Genesis');
        try{
            System.debug('feeDefinitionId:' + feeDefinitionId);
            System.debug('waiverAmount:' + waiverAmount);
            List<genesis__Applications__c> listApps = [SELECT id,
                                                              Name,
                                                              genesis__Loan_Amount__c,
                                                              genesis__CL_Product__r.clcommon__Asset_Class__c
                                                         FROM genesis__Applications__c
                                                        WHERE id = :applicationId
                                                      ];

            if(listApps != null && listApps.size() > 0){
                Date systemDate = CommonUtility.getSystemDate();
                genesis__Applications__c app = listApps.get(0);
                System.debug('app :' + app.genesis__CL_Product__r.clcommon__Asset_Class__c);
                System.debug('systemDate :' + systemDate );
                List<ServiceResource> listServiceResource = [SELECT id,
                                                                    name,
                                                                    (SELECT id,
                                                                            name,
                                                                            Fee_Definition__c,
                                                                            Fee_Definition__r.name,
                                                                            Waiver_Amount__c
                                                                       FROM Fee_Waiver_DLA__r
                                                                      WHERE Start_Date__c <= :systemDate
                                                                        AND End_Date__c >= :systemDate
                                                                        AND Asset_Class__c = :app.genesis__CL_Product__r.clcommon__Asset_Class__c
                                                                        AND Fee_Definition__c = :feeDefinitionId
                                                                        AND Waiver_Amount__c >= : Decimal.valueOf(waiverAmount)
                                                                    ) 
                                                               FROM ServiceResource
                                                              WHERE RelatedRecordId = :System.UserInfo.getUserId()
                                                                AND IsActive = true
                                                            ];
                
                System.debug('listServiceResource :' + listServiceResource );
            
                if(listServiceResource != null && listServiceResource.size() > 0){
                    ServiceResource serResource = listServiceResource.get(0);
                    List<Fee_Waiver_DLA__c> listDLA = serResource.Fee_Waiver_DLA__r;
                    System.debug('listDLA :' + listDLA );
                    if(listDLA != null && listDLA.size() > 0){
                        if(listDLA.size() == 1){
                            return System.label.SUCCESS_MESSAGE;        
                        }
                        return 'User has duplicate Fee Waiver Delegation Records.';
                    }
                    return 'User does not have Discretion to waive fees. Seek Assistance from Manager.';
                }
                return 'User does not have authority to perform fee waiver action. Please contact system admin.';                
            }
            return 'No Valid Application found corresponding to ' + applicationId;            
        }
        catch(Exception e){
            voLogInstance.logException(1001, '[SkuidActionsCtrl.validateFeeWaiverDLA] Error: ' + e.getMessage() + ' (at line: ' + e.getLineNumber() + ')', e);
            voLogInstance.commitToDb();
            return 'Exception: ' + e.getMessage();
        }
    }

    webservice static String submitForAppeal(String applicationId){
        mfiflexUtil.VOLog voLogInstance = mfiflexUtil.VOLog.getInstance('Genesis');
        string returnStr;
        try{
        
            List<genesis__Application_Department__c> activeDept = [SELECT id,
                                                                          genesis__Department__c,
                                                                          genesis__Department__r.Name,
                                                                          genesis__Application__c,
                                                                          genesis__Application__r.genesis__CL_Product__c
                                                                     FROM genesis__Application_Department__c
                                                                    WHERE genesis__Status__c = 'Active'
                                                                      AND genesis__Application__c = :applicationId
                                                                  ];
            if(activeDept.get(0).genesis__Department__r.Name == genesis__Org_Parameters__c.getOrgDefaults().Before_Credit_Stage__c){
                returnStr = StageAndTaskActions.submitToNextDepartment(applicationId);
            }
            else if(activeDept.get(0).genesis__Department__r.Name == genesis__Org_Parameters__c.getOrgDefaults().After_Credit_Stage__c){
                returnStr = StageAndTaskActions.submitToPreviousDepartment(applicationId, null, 'DECLINED-APPEALED');
                
                //genesis__Applications__c app = new genesis__Applications__c (id = applicationId);
                //app.genesis__Status__c = ;
                //update app;
            }
            else{
                returnStr = 'Current stage mismatch.';
            }
                                                                  
            return returnStr;
        }
        catch(Exception e){
            voLogInstance.logException(1001, '[SkuidActionsCtrl.submitForAppeal] Error: ' + e.getMessage() + ' (at line: ' + e.getLineNumber() + ')', e);
            voLogInstance.commitToDb();
            return 'Exception: ' + e.getMessage();
        }
    }
    
    webservice static String generateSchedule(String applicationId){
        try{
            List<genesis__Applications__c> appList = [SELECT id,
                                                             name,
                                                             genesis__Loan_Amount__c,
                                                             genesis__Interest_Rate__c,
                                                             genesis__Term__c,
                                                             genesis__CL_Product__c,
                                                             genesis__Payment_Frequency__c,
                                                             genesis__Balloon_Payment__c,
                                                             genesis__Expected_Start_Date__c,
                                                             genesis__Expected_First_Payment_Date__c,
                                                             genesis__Total_Estimated_Interest__c
                                                        FROM genesis__Applications__c 
                                                       WHERE id = :applicationId
                                                     ];
                                                     
            if(appList == null || appList.size() == 0){
                return 'No valid application found';
            }
            
            genesis__Applications__c app = appList.get(0);
                                                     
            List<genesis__Amortization_Schedule__c> listAMZ = [SELECT id,
                                                                      name 
                                                                 FROM genesis__Amortization_Schedule__c
                                                                WHERE genesis__Application__c = :applicationId
                                                              ];
                                                              
            if(listAMZ != null && listAMZ.size() > 0){
                delete listAMZ;
            }
                        
            List<genesis__Amortization_Schedule__c> emiScheduleNew = GeneratePricing.getInstallments(app.genesis__Loan_Amount__c,
                                                                                                     app.genesis__Interest_Rate__c,
                                                                                                     app.genesis__Term__c,
                                                                                                     app.genesis__Payment_Frequency__c,
                                                                                                     app.genesis__Balloon_Payment__c,
                                                                                                     app.genesis__Expected_Start_Date__c,
                                                                                                     app.genesis__Expected_First_Payment_Date__c,
                                                                                                     app.genesis__CL_Product__c);
            
            if(emiScheduleNew != null && emiScheduleNew.size() > 0){
                app.genesis__Total_Estimated_Interest__c = 0;
                for(genesis__Amortization_Schedule__c amzScdl : emiScheduleNew){
                    amzScdl.genesis__Application__c = applicationId; 
                    app.genesis__Total_Estimated_Interest__c += amzScdl.genesis__Due_Interest__c;
                }
                insert emiScheduleNew;
                update app;
            }                                                       
                                                     
            return System.label.TAG_SUCCESS;
        }
        catch(Exception e){
            mfiflexUtil.VOLog voLogInstance = mfiflexUtil.VOLog.getInstance('Genesis');
            voLogInstance.logException(1001, '[SkuidActionsCtrl.generateSchedule] Error: ' + e.getMessage() + ' (at line: ' + e.getLineNumber() + ')', e);
            voLogInstance.commitToDb();
            return 'Exception: ' + e.getMessage();
        }
    }
    
    public static String createCase(genesis__Applications__c appInst){
        try{
            Case caseRec = new Case();
            caseRec.recordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Application').getRecordTypeId();
            caseRec.Type = 'Application';
            caseRec.Reason = 'New Application';
            caseRec.Origin = appInst.Loan_Channel__c;
            caseRec.Status = 'New';
            caseRec.Application__c = appInst.Id;
            caseRec.AccountId = appInst.genesis__Account__c;
            insert caseRec;
            
            appInst.Case__c = caseRec.Id;
            
            return System.label.TAG_SUCCESS;
        }
        catch(Exception e){
            mfiflexUtil.VOLog voLogInstance = mfiflexUtil.VOLog.getInstance('Genesis');
            voLogInstance.logException(1001, '[SkuidActionsCtrl.createCase] Error: ' + e.getMessage() + ' (at line: ' + e.getLineNumber() + ')', e);
            voLogInstance.commitToDb();
            throw e;
        }
    }
}