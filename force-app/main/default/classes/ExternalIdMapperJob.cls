global class ExternalIdMapperJob implements DataBase.Batchable<sObject>, Database.Stateful, Schedulable{
    public String dataSet;
    
    public ExternalIdMapperJob(){}
    public ExternalIdMapperJob(String dataSetName){
        dataSet = dataSetName;
    }
    
    global Database.QueryLocator start(Database.BatchableContext bc){
        return Database.getQueryLocator([SELECT id,
                                                Name,
                                                Data_Set_Name__c,
                                                External_ID__c,
                                                External_Id_Field__c,
                                                External_Key_Value__c,
                                                Object_API_Name__c,
                                                Sequence__c
                                           FROM Data_Import_Configurations__c  
                                          WHERE Data_Set_Name__r.name = :dataSet
                                          ORDER BY Sequence__c 
                                       ]);                                        
    }
     
    global void execute(SchedulableContext sc){
        ExternalIdMapperJob b = new ExternalIdMapperJob();         
        database.executeBatch(b, 1);
    }
     
    global void execute(Database.BatchableContext bc, List<sObject> scope){
        List<Data_Import_Configurations__c> dataRecords = (List<Data_Import_Configurations__c>) scope;
        Data_Import_Configurations__c dataImpConfig = dataRecords.get(0);
        try{            
            mfiflexUtil.ExecutionContext ec = mfiflexUtil.ExecutionContext.getExecContext();
            mfiflexUtil.ObjectCache objectData = ec.getObject('objData');
            if (objectData != null) {
                ec.deleteObject('objData');
            }
            mfiflexUtil.ObjectCache entries = ec.createObject('objData', dataImpConfig.Object_API_Name__c, true);
            entries.addFields('Id');
            entries.addFields(dataImpConfig.External_Id_Field__c);
            entries.addFields(dataImpConfig.External_Key_Value__c);
            entries.buildQuery();
            
            System.debug('Dynamic Query :'+ entries.getQuery());
            List<String> listExternalKeys = dataImpConfig.External_Key_Value__c.toLowerCase().split(',');
            List<SObject> listRecords = entries.executeQuery().getRecords();
            for(SObject sObj : listRecords){
                Map<String, Object> mapObject = (Map<String, Object>) JSON.deserializeUntyped(JSON.serialize(sObj).toLowerCase());
                String strExtKeyVal = '';
                for(String strExtFields : listExternalKeys){
                    strExtFields = strExtFields.trim();
                    if(strExtFields.contains('__r.')){
                        List<String> listStr = strExtFields.split('\\.');
                        Map<String, Object> innerMapObject = (Map<String, Object>) JSON.deserializeUntyped(JSON.serialize(mapObject.get(listStr.get(0))).toLowerCase());
                        strExtKeyVal += String.valueOf(innerMapObject.get(listStr.get(1))) + ' - ';
                    }
                    else{
                        strExtKeyVal += String.valueOf(mapObject.get(strExtFields)) + ' - ';
                    }
                }
                strExtKeyVal = strExtKeyVal.substring(0, strExtKeyVal.lastIndexOf(' - ')).trim();
                strExtKeyVal = strExtKeyVal.replaceAll(',', '');
                sObj.put(dataImpConfig.External_Id_Field__c, strExtKeyVal);
            }
            update listRecords;
        }
        catch(Exception e) {
            System.debug(e);
            ExceptionLog.insertExceptionLog(e, 'Exception ExternalIdMapperJob:' + dataImpConfig.name,'');
        }         
    }
     
    global void finish(Database.BatchableContext bc) {}
}