/**
 * @File Name          : CalculateDecisioning.cls
 * @Description        : 
 * @Author             : chirag.gupta@q2.com
 * @Group              : 
 * @Last Modified By   : chirag.gupta@q2.com
 * @Last Modified On   : 09-28-2020
 * @Modification Log   : 
 * Ver       Date            Author                 Modification
 * 1.0    6/8/2020   chirag.gupta@q2.com     Initial Version
**/
//*****************************************************************//
//Description: API to run rules for Initial and final decisioning
//Date: Septemper, 2019
//*****************************************************************//

public class CalculateDecisioning {
    
    public static List<String> runFinalSystemDecision(String applicationId){
        try{
            List<String> listReturnMsgs = new List<String>();
            String appStatus = 'NEW - IN CREDIT CHECK';
            String appSubStatus = 'SYSTEM REFERRED';

            List<genesis__Applications__c> appList = [SELECT id,
                                                             name,
                                                             genesis__Status__c,
                                                             Reason_for_appeal__c,
                                                             Initial_Decision__c,
                                                             Final_Decision__c,
                                                             genesis__CL_Product__c,
                                                             (SELECT id,
                                                                     name 
                                                                FROM genesis__Policy_Exceptions__r 
                                                               WHERE genesis__Status__c != 'Satisfied'
                                                             ) 
                                                        FROM genesis__Applications__c
                                                       WHERE Id = :applicationId
                                                     ];

            if(appList == null || appList.size() == 0){
                listReturnMsgs.add('Application not present corresponding to id passed:' + applicationId);
                return listReturnMsgs;
            }

            genesis__Applications__c appInstance = appList.get(0);
            List<genesis__Policy_Exception__c> policyExceptions = appInstance.genesis__Policy_Exceptions__r;

            if(policyExceptions != null && policyExceptions.size() > 0){
                listReturnMsgs.add('Application has policy exceptions, Please review them.');
            }

            Map<Id, genesis__Rule__c> mapRules = new Map<Id, genesis__Rule__c>([SELECT Id,
                                                                                       Name,
                                                                                       Rule_Classification__c
                                                                                  FROM genesis__Rule__C
                                                                                 WHERE genesis__Enabled__c = true
                                                                                   AND Rule_Classification__c IN ('Final Decision', 'Final Decision (Decline)')
                                                                              ]);
                                                                         
            if(mapRules != null && mapRules.size() > 0){
                mfiflexUtil.ExecutionContext ec = mfiflexUtil.ExecutionContext.getExecContext();
                mfiflexUtil.ObjectCache sObjOC = ec.getObject('tempRuleCache');
            
                if(sObjOC != null){
                    ec.deleteObject('tempRuleCache');
                }
                
                SObject appObj = (SObject) appInstance;   
                List<genesis__Checklist__c> checkList = genesis.RulesAPI.evaluateRules(appObj, mapRules.values(), true, false);
                
                Integer countAllItems = 0;//checkList.size();
                Integer countFailedItems_FD = 0;
                Integer countSuccessItems_FD = 0;
                Integer countFailedItems_FDD = 0;
                Integer countSuccessItems_FDD = 0;
                for(genesis__Checklist__c result : checkList){
                    genesis__Rule__c rule = mapRules.containsKey(result.genesis__Rule__c) ? mapRules.get(result.genesis__Rule__c) : null;
                    if(rule != null && !String.isBlank(rule.Rule_Classification__c)){
                        if('Final Decision'.equalsIgnoreCase(rule.Rule_Classification__c)){
                            countAllItems++;
                            if(!result.genesis__Result__c){
                                listReturnMsgs.add(String.valueOf(result.genesis__Message__c));
                                countFailedItems_FD++;
                                continue;
                            }
                            countSuccessItems_FD++;
                        }
                        else if('Final Decision (Decline)'.equalsIgnoreCase(rule.Rule_Classification__c)){
                            if(!result.genesis__Result__c){
                                listReturnMsgs.add(String.valueOf(result.genesis__Message__c));
                                countFailedItems_FDD++;
                                continue;
                            }
                            countSuccessItems_FDD++;
                        }
                    }
                }

                Boolean skipCreditStage = false;
                if(!String.IsBlank(appInstance.Reason_for_appeal__c) ){
                    appStatus = 'DECLINED-APPEALED';
                }
                else if(countFailedItems_FDD > 0){
                    appStatus = 'DECLINED';
                    appSubStatus = 'SYSTEM DECLINED';
                    skipCreditStage = true;
                }
                else if(countAllItems > 0 
                            && countAllItems == countSuccessItems_FD 
                            && listReturnMsgs.size() == 0){
                    appStatus = 'APPROVED';
                    appSubStatus = 'SYSTEM APPROVED';
                    skipCreditStage = true;
                }
                else{
                    appStatus = 'NEW - IN CREDIT CHECK';
                    appSubStatus = 'SYSTEM REFERRED';
                }
            
                appInstance.genesis__Status__c = appStatus;
                appInstance.Initial_Decision__c = appSubStatus;
                appInstance.Final_Decision__c = appSubStatus;
                appInstance.Approval_Date__c = CommonUtility.getSystemDate();
                update appInstance;  
                if(skipCreditStage){
                    //List<Group> listUserGrp = [Select Id,name,(Select UserOrGroupId From GroupMembers where UserOrGroupId  ='0056D000002EB9C') from Group where type='Queue' and Name='Credit'];
                    //List<GroupMember> grpmembers = listUserGrp.get(0).GroupMembers;
                    //appInstance.ownerId = '0056D000002EB9C';//grpmembers.get(0).UserOrGroupId;
                    //update appInstance;
                    listReturnMsgs.add('Message from Skip and Submit to Next Stage:' + StageAndTaskActions.skipAndSubmitToNextStage(appInstance));
                }           
            }            
            
            return listReturnMsgs;
        }
        catch(Exception e){
            throw e;
        }
    }
}