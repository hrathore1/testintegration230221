global class CustomCollateralValuesCalculation extends clcommon.AbstractCollateralLienCalculator{
    
    public List<loan__Loan_Account__c> loanAccounts;
    public Map<Id, loan__Loan_Account__c> loanId2LoanAccountMapForThisBatch;
       
    global CustomCollateralValuesCalculation(){
        System.debug('Custom constructor');
    }
        
    public void queryLoanAndRelatedData(Set<Id> loanIds){
        List<loan__Loan_Account__c> loanAccounts = [SELECT Id,
                                                           name,
                                                           loan__Capitalized_Fee__c,
                                                           loan__Capitalized_Interest__c,
                                                           loan__Principal_Remaining__c,
                                                           (SELECT id,
                                                                   name,
                                                                   loan__Loan_Account__c,
                                                                   clcommon__Collateral__c,
                                                                   clcommon__Loan_Amount__c,    
                                                                   clcommon__Lien_Position__c,
                                                                   clcommon__Lien_Amount__c
                                                              FROM loan__Collateral_Liens1__r
                                                             WHERE loan__Loan_Account__c != null
                                                           )
                                                      FROM loan__Loan_Account__c
                                                     WHERE Id IN :loanIds
                                                   ];         
        loanId2LoanAccountMapForThisBatch = new Map<Id, loan__Loan_Account__c>(loanAccounts);
    }
    
    global override void setParam(Map<String,Object> paramMap){
        List<loan__Loan_Account__c> loanAccounts1 = (List<loan__Loan_Account__c>) paramMap.get('appIds');        
        System.debug(logginglevel.DEBUG,'loanAccounts :: '+loanAccounts1);
        
        Set<Id> loanIds = new Set<Id>();
        for(loan__Loan_Account__c contract : loanAccounts1){
            loanIds.add(contract.Id);
        }
       
        queryLoanAndRelatedData(loanIds);
    }
    
    global override Map<Id, SObject> computeLoanToValueRatio(Set<Id> loanIds){
    
        Map<Id, loan__Loan_Account__c> resultMap = new Map<Id, loan__Loan_Account__c>(); 
        System.debug(logginglevel.DEBUG,'loanIds :: '+loanIds);
        for(ID loanId : loanIds){ 
            loan__Loan_Account__c loanAcc = loanId2LoanAccountMapForThisBatch.get(loanId);
            
            if(loanAcc != null){
                Decimal loanToValueRatio = 0.0;
                Decimal collateralLienAmount = 0;
                for(clcommon__CollateralLien__c colLien : loanAcc.loan__Collateral_Liens1__r){
                    if(colLien.clcommon__Lien_Position__c == 1){
                        loanToValueRatio = computeLoanBalance(loanAcc);   
                        collateralLienAmount += colLien.clcommon__Lien_Amount__c;
                    }
                }
                Decimal LTV = loanToValueRatio/collateralLienAmount;
                loanId2LoanAccountMapForThisBatch.get(loanAcc.Id).loan__LTV__c = LTV;
                resultMap.put(loanAcc.Id, loanId2LoanAccountMapForThisBatch.get(loanAcc.Id));
            }           
        }    
        return resultMap;
    }
    
    global override Map<Id,SObject> computeCombinedLoanToValueRatio(Set<Id> loanIds){
        Map<Id, loan__Loan_Account__c> resultMap = new Map<Id, loan__Loan_Account__c>(); 
        System.debug(logginglevel.DEBUG,'loanIds :: '+loanIds);
        for(ID loanId : loanIds){ 
            loan__Loan_Account__c loanAcc = loanId2LoanAccountMapForThisBatch.get(loanId);
            
            if(loanAcc != null){
                Decimal loanToValueRatio = 0.0;
                Decimal collateralLienAmount = 0;
                for(clcommon__CollateralLien__c colLien : loanAcc.loan__Collateral_Liens1__r){
                    loanToValueRatio = computeLoanBalance(loanAcc);   
                    collateralLienAmount += colLien.clcommon__Lien_Amount__c;
                }
                Decimal LTV = loanToValueRatio/collateralLienAmount;
                loanId2LoanAccountMapForThisBatch.get(loanAcc.Id).loan__CLTV__c  = LTV;
                resultMap.put(loanAcc.Id, loanId2LoanAccountMapForThisBatch.get(loanAcc.Id));
            }           
        }    
        return resultMap;        
    }
    
    public Decimal computeLoanBalance(loan__Loan_Account__c loanAccount){
        Decimal loanBalance = 0.00;
        if (loanAccount.loan__Capitalized_Fee__c == null) {
            loanAccount.loan__Capitalized_Fee__c = 0.0;
        }

        if (loanAccount.loan__Capitalized_Interest__c == null) {
            loanAccount.loan__Capitalized_Interest__c = 0.0;
        }
        loanBalance =  loanAccount.loan__Principal_Remaining__c + loanAccount.loan__Capitalized_Fee__c + loanAccount.loan__Capitalized_Interest__c;

        return loanBalance;
    }
}