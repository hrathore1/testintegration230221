/**
 * @description       : 
 * @author            : chirag.gupta@q2.com
 * @group             : 
 * @last modified on  : 09-16-2020
 * @last modified by  : chirag.gupta@q2.com
 * Modifications Log 
 * Ver   Date         Author                Modification
 * 1.0   09-15-2020   chirag.gupta@q2.com   Initial Version
**/
global class CalculateGSTValueDynamicJob extends clcommon.DynamicJob {
    
    private Date sodDate;
    private static final String JOB_NAME = 'Calculate GST Value Job';
    private static final Integer DEFAULT_BATCH_SIZE = 150;
    
    global CalculateGSTValueDynamicJob() {
        this(null);
    }
     
    public CalculateGSTValueDynamicJob(String query) {
        super(JOB_NAME, query);
    }
    
    /** 
      * execute(jobId)
      * This method gets invoked when the batch job executed and operates on one batch of records
      * @param  jobId     Database.BatchableContext
    */
    global override void doexecute(SchedulableContext sc) {
        try {
            CalculateGSTValueDynamicJob job = new CalculateGSTValueDynamicJob();
            Database.executeBatch(job);
        }
        catch (Exception e) {
            loan__Batch_Process_Log__c bpLog = new loan__Batch_Process_Log__c();
            bpLog.loan__Message__c = 'Unable to submit ' + JOB_NAME;
            bpLog.loan__Time__c = System.Now();
            bpLog.loan__Log_Level__c = 'ERROR';
            bpLog.loan__Date__c =  System.today();
            insert bpLog;
        }
    }

    global override void doInitialize() {} // do nothing
    global override String getRuntimeQuery() {
        //Add run time query when query passed in the constructor is null.
        
        return getQuery();
    }

    global override String getRuntimeQueryForPipelinedExecution(Set<Id> records) {
        System.debug('getRuntimeQueryForPipelinedExecution:' + records);
        return getQuery();
    }
    
    public String getQuery(){
        String allowabledLoanStatuses = '\'' + loan.LoanConstants.LOAN_STATUS_ACTIVE_GOOD_STANDING + '\'' + ',' +
                                        '\'' + loan.LoanConstants.LOAN_STATUSACTIVE_BAD_STANDING + '\'' + ',' +
                                        '\'' + loan.LoanConstants.LOAN_STATUS_ACTIVE_MATURED + '\'';
        
        String contractTypes = '\'Lease\'';

        String loanFields = 'ID, ' +
                            'Name ';
                            
        mfiflexUtil.ExecutionContext ec = mfiflexUtil.ExecutionContext.getExecContext();
        if (ec.getObject('GSTFee') != null) {
            ec.deleteObject('GSTFee');
        }
        mfiflexUtil.ObjectCache contractOC = ec.createObject('GSTFee', 'loan__Loan_Account__c');
        String whereClause = ' loan__Loan_Status__c IN (' + allowabledLoanStatuses + ') ' +
                             ' AND loan__Invalid_Data__c = false ' + 
                             ' AND loan__Loan_Product_Name__r.contract_type__c IN (' + contractTypes + ') ';
                            
        contractOC.addFields(loanFields);
        contractOC.setWhereClause(whereClause);
        contractOC.buildQuery();
        system.debug('Query :: ' + contractOC.getQuery());
        return contractOC.getQuery();
    }

    global override void doStart(Database.BatchableContext bc) {}
    
    /** 
        * doExecute(jobId,recordList)
        * This method calls writeOffImpl class to implement logic to write-off loans
        * @param  jobId         Database.BatchableContext 
        * @param  recordList    List<sObject> 
    */
    global override void doExecute(Database.BatchableContext BC, List<sObject> scope) {
        try{
            List<loan__Loan_Account__c> listBatchContracts = (List<loan__Loan_Account__c>) scope;
            Set<Id> setContractIds = new Set<Id>();
            for(loan__Loan_Account__c contract : listBatchContracts){
                setContractIds.add(contract.Id);
            }

            Id recordId_GST_Rebate = Schema.SObjectType.loan__Other_Transaction__c.getRecordTypeInfosByName().get('GST Rebate').getRecordTypeId();
            Id recordId_Interest_Rebate = Schema.SObjectType.loan__Other_Transaction__c.getRecordTypeInfosByName().get('Interest Rebate').getRecordTypeId();

            List<String> listTxnTypes = new List<String>();
            listTxnTypes.add('GST Rebate');
            listTxnTypes.add('Interest Rebate');
            
            List<String> listRecordTypes = new List<String>();
            listRecordTypes.add(recordId_GST_Rebate);
            listRecordTypes.add(recordId_Interest_Rebate);

            List<loan__Loan_Account__c> listContracts = [SELECT id,
                                                                name,
                                                                loan__Loan_Balance__c,
                                                                loan__Time_Counting_Method__c,
                                                                loan__Interest_Rate__c,
                                                                loan__Frequency_of_Loan_Payment__c,
                                                                loan__Capitalized_Fee__c,
                                                                loan__Capitalized_Interest__c,
                                                                loan__Principal_Remaining__c,
                                                                
                                                                loan__Next_Installment_Date__c,
                                                                (SELECT id,
                                                                        name,
                                                                        loan__Transaction_Type__c 
                                                                   FROM loan__Other_Loan_Transactions__r
                                                                  WHERE loan__Transaction_Type__c IN :listTxnTypes
                                                                    AND recordTypeId IN :listRecordTypes
                                                                )
                                                           FROM loan__Loan_Account__c
                                                          WHERE Id IN :setContractIds
                                                        ];

            AggregateResult[] groupedResults = [SELECT loan__Loan_Account__c, 
                                                       SUM(loan__Due_Fee1__c),
                                                       SUM(loan__Due_Interest__c) 
                                                  FROM loan__Repayment_Schedule__c
                                                 WHERE loan__Loan_Account__c IN :setContractIds
                                                   AND loan__Due_Date__c > :CommonUtility.getSystemDate()
                                                   AND loan__Is_Archived__c = false
                                                 GROUP BY loan__Loan_Account__c
                                               ];
            
            Map<String, Decimal> mapContractToGSTRebate = new Map<String, Decimal>();
            Map<String, Decimal> mapContractToInterestRebate = new Map<String, Decimal>();
            for (AggregateResult ar : groupedResults)  {
                System.debug('Loan Account ID:' + ar.get('loan__Loan_Account__c'));
                System.debug('Sum amount:' + ar.get('expr0'));
                mapContractToGSTRebate.put((String) ar.get('loan__Loan_Account__c'), (Decimal) ar.get('expr0'));
                mapContractToInterestRebate.put((String) ar.get('loan__Loan_Account__c'), (Decimal) ar.get('expr1'));
            }

            List<loan__Other_Transaction__c> listOltToBeUpdated = new List<loan__Other_Transaction__c>();
            for(loan__Loan_Account__c contract : listContracts){

                List<loan__Other_Transaction__c> listOLT = contract.loan__Other_Loan_Transactions__r;
                loan__Other_Transaction__c tempOltGSTRebate;
                loan__Other_Transaction__c tempOltInterestRebate;

                for(loan__Other_Transaction__c oltTxn : listOLT){
                    if(!String.isBlank(oltTxn.loan__Transaction_Type__c)){
                        if(oltTxn.loan__Transaction_Type__c.equalsIgnoreCase('GST Rebate')){
                            tempOltGSTRebate = oltTxn;
                        }
                        else if(oltTxn.loan__Transaction_Type__c.equalsIgnoreCase('Interest Rebate')){
                            tempOltInterestRebate = oltTxn;
                        }
                    }
                }
                
                if(tempOltGSTRebate == null){
                    tempOltGSTRebate = new loan__Other_Transaction__c();
                    tempOltGSTRebate.loan__Transaction_Creation_Date__c = CommonUtility.getSystemDate();
                    tempOltGSTRebate.loan__Loan_Account__c = contract.Id;
                    tempOltGSTRebate.loan__Transaction_Type__c = 'GST Rebate';
                    tempOltGSTRebate.recordTypeId = recordId_GST_Rebate;
                }
                if(tempOltInterestRebate == null){
                    tempOltInterestRebate = new loan__Other_Transaction__c();
                    tempOltInterestRebate.loan__Transaction_Creation_Date__c = CommonUtility.getSystemDate();
                    tempOltInterestRebate.loan__Loan_Account__c = contract.Id;
                    tempOltInterestRebate.loan__Transaction_Type__c = 'Interest Rebate';
                    tempOltInterestRebate.recordTypeId = recordId_Interest_Rebate;
                }
                
                Decimal gstAmount = (mapContractToGSTRebate.containsKey(contract.Id) ? mapContractToGSTRebate.get(contract.Id) : 0);
                Decimal interestAmount = (mapContractToInterestRebate.containsKey(contract.Id) ? mapContractToInterestRebate.get(contract.Id) : 0);

                if (contract.loan__Capitalized_Fee__c == null) {
                    contract.loan__Capitalized_Fee__c = 0.0;
                }
                if (contract.loan__Capitalized_Interest__c == null) {
                    contract.loan__Capitalized_Interest__c = 0.0;
                }
                Decimal balancePrincipal = contract.loan__Principal_Remaining__c + contract.loan__Capitalized_Interest__c + contract.loan__Capitalized_Fee__c;
                
                if(CommonUtility.getSystemDate() != contract.loan__Next_Installment_Date__c){
                    Date nextInstallmentDate = contract.loan__Next_Installment_Date__c;
                    String frequency = contract.loan__Frequency_of_Loan_Payment__c;
                    
                    Date previousInstallmentDate = CommonUtility.getSystemDate();
                    
                    if(frequency.equalsIgnoreCase('MONTHLY')){
                        previousInstallmentDate = nextInstallmentDate.addMonths(-1);
                    }
                    else if(frequency.equalsIgnoreCase('BI-WEEKLY')){
                        previousInstallmentDate = nextInstallmentDate.addDays(-14);
                    }
                    else if(frequency.equalsIgnoreCase('WEEKLY')){
                        previousInstallmentDate = nextInstallmentDate.addDays(-7);
                    }
                    
                    Decimal deltaInterestComponent = 0;
                    System.debug('previousInstallmentDate:' + previousInstallmentDate);
                    if(previousInstallmentDate.daysBetween(CommonUtility.getSystemDate()) > 0){
                        deltaInterestComponent = loan.InterestCalc.calcSIWithRounding(contract.loan__Interest_Rate__c,
                                                                                      loan.LoanTransactionUtil.getAccrualBaseMethodCode(contract),
                                                                                      contract.loan__Frequency_of_Loan_Payment__c,
                                                                                      balancePrincipal,
                                                                                      previousInstallmentDate,
                                                                                      CommonUtility.getSystemDate()
                                                                                     );
                    }
                    
                    interestAmount -= deltaInterestComponent;                                                                    
                }
                
                
                tempOltGSTRebate.loan__Txn_Date__c = CommonUtility.getSystemDate();
                tempOltGSTRebate.loan__Txn_Amt__c = gstAmount;
                tempOltGSTRebate.loan__Transaction_Time__c = System.now();
                listOltToBeUpdated.add(tempOltGSTRebate);

                tempOltInterestRebate.loan__Txn_Date__c = CommonUtility.getSystemDate();
                tempOltInterestRebate.loan__Txn_Amt__c = interestAmount;
                tempOltInterestRebate.loan__Transaction_Time__c = System.now();
                listOltToBeUpdated.add(tempOltInterestRebate);
            }
            
            System.debug(listOltToBeUpdated);
            
            if(listOltToBeUpdated.size() > 0){
                UPSERT listOltToBeUpdated;    
            }    
            UPDATE listContracts;  
        }
        catch(Exception e){
            loan__Batch_Process_log__c bpLog = new loan__Batch_Process_log__c();
            bpLog.loan__Message__c = 'CalculateGSTValueDynamicJob: ' + e.getStackTraceString();
            bpLog.loan__Time__c = System.Now();
            bpLog.loan__Log_Level__c = 'ERROR';
            bpLog.loan__Date__c =  System.today();
            insert bpLog;
        }
    }

    global override void doFinish(Database.BatchableContext bc) {}
}