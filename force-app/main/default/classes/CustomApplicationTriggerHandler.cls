/**
 * @File Name          : CustomApplicationTriggerHandler.cls
 * @Description        : 
 * @Author             : chirag.gupta@q2.com
 * @Group              : 
 * @Last Modified By   : chirag.gupta@q2.com
 * @Last Modified On   : 09-21-2020
 * @Modification Log   : 
 * Ver       Date            Author                 Modification
 * 1.0    6/10/2020   chirag.gupta@q2.com     Initial Version
**/
public class CustomApplicationTriggerHandler {
    public void updateApplicationParams(List<genesis__Applications__c> newApps, Map<Id, genesis__Applications__c> oldMap) {
        Id loanRecordId = Schema.SObjectType.genesis__Applications__c.getRecordTypeInfosByName().get('Loan').getRecordTypeId();
        for(genesis__Applications__c app : newApps){
            Boolean isInterestChanged = false;
            Boolean isDiscountParamChanged = false;
            Boolean isTermChanged = false;
            Boolean isLoanAmountChanged = false;
            Boolean isFrequencyChanged = false;
            Boolean isPurposeChanged = false;
            //Boolean isOriginatorChanged = false; commented by Abhishek & Chirag due to originator lookup change
            Boolean isFranchiseChanged = false;
            Boolean isPaymentAmountZero = false;
            
            genesis__Applications__c oldApp = (oldMap == null ? null : oldMap.get(app.Id));
            
            if(app.RecordTypeId != loanRecordId){
                continue;
            }

            //Updating Gross Annual Income
            if(oldApp != null && (app.Net_Annual_Income_Borrower__c != oldApp.Net_Annual_Income_Borrower__c 
                                    || app.Net_Annual_Income_Co_Borrower__c != oldApp.Net_Annual_Income_Co_Borrower__c
                                    || app.Net_Annual_Income_Guarantor__c != oldApp.Net_Annual_Income_Guarantor__c)){
                CalculateGrossIncome.updateValues(app);
            }

            if(oldApp != null && (app.Balloon_Percentage__c != oldApp.Balloon_Percentage__c 
                                    || app.Application_Term__c != oldApp.Application_Term__c
                                    || app.genesis__CL_Purpose__c != oldApp.genesis__CL_Purpose__c)){
                String validationMsg = validateApplicationData(app);
                if(!System.label.TAG_SUCCESS.equalsIgnoreCase(validationMsg)){
                    app.addError(validationMsg);
                    continue;
                }
            }

            if(app.genesis__Payment_Amount__c <= 0){
                isPaymentAmountZero = true;
            }
            
            if(oldApp == null
                  || app.Pricing_Interest__c != oldApp.Pricing_Interest__c 
                  || app.genesis__Discount_Rate__c != oldApp.genesis__Discount_Rate__c
                  || app.Discretion__c != oldApp.Discretion__c){
                isInterestChanged = true;
            }
            
            if(oldApp == null 
                  || app.Franchise__c != oldApp.Franchise__c
                  || app.Loan_Channel__c != oldApp.Loan_Channel__c){
                isDiscountParamChanged = true;
            }
            if(oldApp == null
                  || app.Primary_Loan_Amount__c != oldApp.Primary_Loan_Amount__c
                  || app.Secondary_Loan_Amount__c != oldApp.Secondary_Loan_Amount__c
                  || app.Vehicle_related_costs__c != oldApp.Vehicle_related_costs__c
                  || app.Trade_in_value__c != oldApp.Trade_in_value__c
                  || app.Trade_In_Payout__c!= oldApp.Trade_In_Payout__c
                  || app.Net_Cash_Deposit__c != oldApp.Net_Cash_Deposit__c
                  || app.Capitalised_Fee__c != oldApp.Capitalised_Fee__c
                  || app.Balloon_Percentage__c != oldApp.Balloon_Percentage__c){
                isLoanAmountChanged = true;
            }
            if(oldApp == null
                  || app.Loan_term_years__c != oldApp.Loan_term_years__c
                  || app.Application_Term__c != oldApp.Application_Term__c
                  || app.Pricing_Term__c != oldApp.Pricing_Term__c){
                isTermChanged = true;
            }
            if(oldApp == null
                  || app.genesis__Payment_Frequency__c != oldApp.genesis__Payment_Frequency__c){
                isFrequencyChanged = true;
            }
            if(oldApp == null
                  || app.genesis__CL_Purpose__c != oldApp.genesis__CL_Purpose__c){
                isPurposeChanged = true;
            }
            /*
            commented by Abhishek & Chirag due to originator lookup change
            if(oldApp == null 
                  || app.Originator__c != oldApp.Originator__c){
                isOriginatorChanged = true;
            }
            */
            if(oldApp == null 
                  || app.Franchise__c != oldApp.Franchise__c){
                isFranchiseChanged = true;
            }
            
            //isOriginatorChanged commented by Abhishek & Chirag due to originator lookup change
            
            if(isLoanAmountChanged || isPurposeChanged /*|| isOriginatorChanged*/ || isFranchiseChanged){
                app.genesis__Valid_Pricing_Flag__c = false;
            }
            
            if(!isInterestChanged && !isTermChanged && !isLoanAmountChanged && !isFrequencyChanged && !isPaymentAmountZero){
                continue;
            }
            
            if(isDiscountParamChanged){
                //Calculate Discount rate params
                GeneratePricing.recalculateDiscountRate(app);
            }
            if(isTermChanged || isFrequencyChanged){
                //Calculate number of installments
                GeneratePricing.recalculateNumberOfInstallments(app);
            }
            if(isInterestChanged){
                //Calculate updated interest basis discounts
                GeneratePricing.recalculateInterestRate(app);
            }
            if(isLoanAmountChanged){
                //Calculate updated loan amount
                GeneratePricing.recalculateLoanAmount(app);
            }
            
            if(isLoanAmountChanged || isInterestChanged || isTermChanged || isFrequencyChanged || isPaymentAmountZero){
                //Calculate Payment Amount
                System.debug('app.genesis__Loan_Amount__c>>> ' +app.genesis__Loan_Amount__c);
                List<genesis__Amortization_Schedule__c> emiSchedule = GeneratePricing.getInstallments(app.genesis__Loan_Amount__c,
                                                                                                      app.genesis__Interest_Rate__c,
                                                                                                      app.genesis__Term__c,
                                                                                                      app.genesis__Payment_Frequency__c,
                                                                                                      app.genesis__Balloon_Payment__c,
                                                                                                      app.genesis__Expected_Start_Date__c,
                                                                                                      app.genesis__Expected_First_Payment_Date__c,
                                                                                                      app.genesis__CL_Product__c); 
                
                if(emiSchedule.size() > 0){
                    app.genesis__Payment_Amount__c = emiSchedule.get(0).genesis__Total_Due_Amount__c;
                    app.genesis__Total_Estimated_Interest__c = 0;
                    for(genesis__Amortization_Schedule__c emi : emiSchedule){
                        if(emi.genesis__Due_Interest__c != null)
                        app.genesis__Total_Estimated_Interest__c += emi.genesis__Due_Interest__c;
                    }    
                }
                
                System.debug('app after payment amount change: ' + app);
            }
        }
    }
    
    /*public void runAfterActions(List<genesis__Applications__c> newApps, Map<Id, genesis__Applications__c> oldMap) {
        for(genesis__Applications__c app : newApps){
            genesis__Applications__c oldApp = (oldMap == null ? null : oldMap.get(app.Id));
            
            if(oldApp != null
                  && app.genesis__Overall_Status__c != oldApp.genesis__Overall_Status__c
                  && app.genesis__Overall_Status__c.startsWith('Being Processed By')
                  && oldApp.genesis__Overall_Status__c != null){
                  
                String stageName = 'Being Processed By ';
                if(oldApp.genesis__Overall_Status__c.startsWith('Being Processed By')){
                   stageName += oldApp.genesis__Overall_Status__c.substring('Being Processed By'.length()).trim();
                }
                else if(oldApp.genesis__Overall_Status__c.startsWith('Waiting for')){
                   stageName += oldApp.genesis__Overall_Status__c.substring('Waiting for'.length()).trim();
                }
                callApplicationRevisioning(app.Id, stageName);
            }
            
            //if(oldApp != null && app.genesis__Payment_Amount__c != oldApp.genesis__Payment_Amount__c){
            //    System.enqueueJob(new RegenerateAmortisationSchdlClass(app.Id));
            //}
        }
    }*/
    
    /*public class RegenerateAmortisationSchdlClass implements Queueable{
        String appId;
        public RegenerateAmortisationSchdlClass (String applicationId){
            appId = applicationId;
        }
        public void execute(QueueableContext context) {
            try{
                SkuidActionsCtrl.generateSchedule(appId);
            }
            catch(Exception e){
                ExceptionLog.insertExceptionLog(e, 'Regenerating amortisation schedule failed for app: '+ appId, appId);
            }   
        }
    }*/
    
    /*@future
    public static void callApplicationRevisioning(String applicationId, String stageName){
        try{
            List<clcommon.SnapshotAPI.SnapshotParams> snapshotList = new List<clcommon.SnapshotAPI.SnapshotParams>();
            clcommon.SnapshotAPI.SnapshotParams snapshot = new clcommon.SnapshotAPI.SnapshotParams();
            snapshot.recordId = applicationId;
            snapshot.dynamicQuerySetName = 'Application Snapshot';
            snapshot.useLabelName = true;
            snapshot.saveSnapshot = true;
            snapshot.snapshotType = 'AUTOMATIC';
            snapshot.snapshotRemarks = stageName;
            snapshotList.add(snapshot);
            List<String> responseJsonString = clcommon.SnapshotAPI.generateSnapshot(snapshotList);
        }
        catch(Exception e){
            ExceptionLog.insertExceptionLog(e, 'Application Revisioning snapshot failed for app: '+ applicationId + ' and stage: ' + stageName, applicationId);
        }
    }*/

    public String validateApplicationData(genesis__Applications__c app){
        try{
            List<Pricing_Configuration__mdt> pricingMetadata = [SELECT id,
                                                                       Pricing_Term__c,
                                                                       Min_Application_term__c,
                                                                       Max_Application_term__c,
                                                                       Balloon_Percentage_New__c,
                                                                       Balloon_Percentage_Old__c
                                                                  FROM Pricing_Configuration__mdt
                                                                 WHERE Pricing_Term__c =: app.Pricing_Term__c
                                                               ];
                                                            
            if(pricingMetadata == null || pricingMetadata.size() == 0){
                return System.label.TAG_SUCCESS;
            }
            else{
                Pricing_Configuration__mdt pricingData = pricingMetadata.get(0);
                if(app.Application_Term__c > pricingData.Max_Application_term__c 
                    || app.Application_Term__c < pricingData.Min_Application_term__c){
                    return 'Enter a loan term within your selected pricing option OR Apply Pricing for the new term from the table provided.';
                }

                //if(app.Pricing_Term__c != app.Application_Term__c && app.Balloon_Percentage__c > 0){
                //    return 'Balloon payment is only allowed for terms on full years.';
                //}

                //Boolean isPurposeNew = (app.CL_Purpose_Name__c.startsWith('New') ? true : false);
                //if(isPurposeNew && app.Balloon_Percentage__c > pricingData.Balloon_Percentage_New__c){
                //    return ('Balloon percentage should be less than ' + pricingData.Balloon_Percentage_New__c + '% for ' + app.CL_Purpose_Name__c + '.');
                //}
                //else if(!isPurposeNew && app.Balloon_Percentage__c > pricingData.Balloon_Percentage_Old__c){
                //    return ('Balloon percentage should be less than ' + pricingData.Balloon_Percentage_New__c + '% for ' + app.CL_Purpose_Name__c + '.');
                //}
                //else{
                    return System.label.TAG_SUCCESS;
                //}
            }
        }
        catch(Exception e){
            System.debug('Error ' +e.getMessage() + ' Line ' + e.getLineNumber());
            return ('Exception:' + e.getMessage());
        }
    }
    
    public void createApplicationData(List<genesis__Applications__c> newApps) {
        Set<Id> setAppIdsToBeUsed = new Set<Id>();
        for(genesis__Applications__c appInst : newApps){
            setAppIdsToBeUsed.add(appInst.Id);
        }
        
        System.debug('setAppIdsToBeUsed:' + setAppIdsToBeUsed);
        if(setAppIdsToBeUsed.size() > 0){
            List<genesis__Applications__c> appList = [SELECT id,Trade_In_Payout__c FROM genesis__Applications__c WHERE Id in: setAppIdsToBeUsed];
            Map<String, List<String>> mapPickListValues = getFieldDependencies('genesis__Liability__c', 'Type__c', 'genesis__Liability_Type__c');
            List<String> listExpensePickLists = mapPickListValues.get('Expense');
            List<genesis__Liability__c> listExpensesToBeCreated = new List<genesis__Liability__c>();
            for(String appId : setAppIdsToBeUsed){
                for(String strPicklistVal : listExpensePickLists){
                    genesis__Liability__c liability = new genesis__Liability__c();
                    liability.genesis__Application__c = appId;
                    liability.Party_Type__c = 'BORROWER';
                    liability.Type__c = 'Expense';
                    liability.genesis__Liability_Type__c = strPicklistVal;
                    liability.genesis__Payment_Amount__c = 0;
                    listExpensesToBeCreated.add(liability);
                }
            }
            insert listExpensesToBeCreated;
            //Creating default liability record for Trade In Payout value
            List<genesis__Liability__c> liabilityList = new List<genesis__Liability__c>();
            for(genesis__Applications__c app : appList){
                if(app.Trade_In_Payout__c > 0)
                {
                    genesis__Liability__c liability = new genesis__Liability__c();
                    liability.genesis__Application__c = app.Id;
                    liability.Party_Type__c = 'BORROWER';
                    liability.Type__c = 'Liability';
                    liability.genesis__Liability_Type__c = 'Trade In Payout';
                    liability.Monthly_Repayments__c = app.Trade_In_Payout__c;
                    liability.Refinanced__c = true;
                    liabilityList.add(liability);
                }
            }
            insert liabilityList;
        }        
    }
    
    public static Map<String, List<String>> getFieldDependencies(String objectName, String controllingField, String dependentField) {
        Map<String, List<String>> controllingInfo = new Map<String, List<String>>();
    
        Schema.SObjectType objType = Schema.getGlobalDescribe().get(objectName);
    
        Schema.DescribeSObjectResult describeResult = objType.getDescribe();
        Schema.DescribeFieldResult controllingFieldInfo = describeResult.fields.getMap().get(controllingField).getDescribe();
        Schema.DescribeFieldResult dependentFieldInfo = describeResult.fields.getMap().get(dependentField).getDescribe();
    
        List<Schema.PicklistEntry> controllingValues = controllingFieldInfo.getPicklistValues();
        List<Schema.PicklistEntry> dependentValues = dependentFieldInfo.getPicklistValues();
    
        for(Schema.PicklistEntry currControllingValue : controllingValues) {
            controllingInfo.put(currControllingValue.getLabel(), new List<String>());
        }
    
        for(Schema.PicklistEntry currDependentValue : dependentValues) {
            String jsonString = JSON.serialize(currDependentValue);
            MyPickListInfo info = (MyPickListInfo) JSON.deserialize(jsonString, MyPickListInfo.class);
            String hexString = EncodingUtil.convertToHex(EncodingUtil.base64Decode(info.validFor)).toUpperCase();
            Integer baseCount = 0;
    
            for(Integer curr : hexString.getChars()) {
                Integer val = 0;
    
                if(curr >= 65) {
                    val = curr - 65 + 10;
                }
                else {
                    val = curr - 48;
                }
    
                if((val & 8) == 8) {
                    controllingInfo.get(controllingValues[baseCount + 0].getLabel()).add(currDependentValue.getLabel());
                }
                if((val & 4) == 4) {
                    controllingInfo.get(controllingValues[baseCount + 1].getLabel()).add(currDependentValue.getLabel());                    
                }
                if((val & 2) == 2) {
                    controllingInfo.get(controllingValues[baseCount + 2].getLabel()).add(currDependentValue.getLabel());                    
                }
                if((val & 1) == 1) {
                    controllingInfo.get(controllingValues[baseCount + 3].getLabel()).add(currDependentValue.getLabel());                    
                }
    
                baseCount += 4;
            }            
        } 
    
        System.debug('ControllingInfo: ' + controllingInfo);
        return controllingInfo;
    }
    
    public class MyPickListInfo {
        public String validFor;
    }
}