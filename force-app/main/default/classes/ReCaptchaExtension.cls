global without sharing class ReCaptchaExtension {

    private static final genesis__Org_Parameters__c recaptchaConfig = genesis__Org_Parameters__c.getOrgDefaults();
    
    public ReCaptchaExtension(clcommon.PortalActions pa) {
        System.debug('Recaptcha');
    }
    @RemoteAction //the function to be called in remote action should use this annotation
    webservice static Decimal verify(final String response) {
    Boolean verif = false;
    Decimal score;
    final HttpResponse res = makeRequest(
      'secret=' + recaptchaConfig.ReCaptcha_Secret_Key__c +
      '&response='  + response 
    );
    if (res != null) {
      if ( res.getStatusCode() == 200 ) {
        final System.JSONParser parser = System.JSON.createParser(res.getBody());
        while (parser.nextToken() != null) {
          if ((parser.getCurrentToken() == JSONToken.FIELD_NAME) && (parser.getText() == 'score')) {
            parser.nextToken();
            system.debug(parser.getDecimalValue());
            score = parser.getDecimalValue();
            break;
          }
        }
      }
    }
    return score;
  }
  private static HttpResponse makeRequest(final string body)  {
    final HttpRequest req = new HttpRequest();
    mfiflexUtil.VOLog voLogInstance = mfiflexUtil.VOLog.getInstance('Genesis');
    req.setEndpoint(recaptchaConfig.ReCaptcha_Endpoint__c);
    req.setMethod('POST');
    req.setBody (body);
    system.debug(body);
    HttpResponse res;
    try {
      final Http http = new Http();
      res = http.send(req);
    } catch(System.Exception e) {
        voLogInstance.logException(1001, '[ReCaptchaExtension.makeRequest] Error: ' + e.getMessage() +
                                   ' (at line: ' + e.getLineNumber() + ')', e);
        voLogInstance.commitToDb();
    }
    return res;
  }
}