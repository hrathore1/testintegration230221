global class GenerateRPDExportForSAPJob implements DataBase.Batchable<sObject>, Database.Stateful, Schedulable{
    private mfiflexUtil.VOLog voLogInstance = mfiflexUtil.VOLog.getInstance('Neon');
    public List<String> listLineRecords = new List<String>();
    public Batch_Configs__c batchConfig = Batch_Configs__c.getOrgDefaults();
    public GenerateRPDExportForSAPJob(){}
    
    global Database.QueryLocator start(Database.BatchableContext bc){
        Id personAccountId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        List<String> listAddressTypes = System.label.RPD_FILE_ADDRESS_TYPES.split(',');
        List<String> listActiveContractStatuses = System.label.RPD_FILE_ACTIVE_CONTRACT_STATUSES.split(',');

        return Database.getQueryLocator([SELECT id,
                                                name,
                                                Salutation,
                                                FirstName,
                                                LastName, 
                                                MiddleName,
                                                PersonBirthdate, 
                                                PersonHomePhone, 
                                                Phone, 
                                                Fax,
                                                ABN__c,
                                                //Membership_Number__c,
                                                Customer_Number__c,
                                                Gender__pc 
                                                //ints__Driver_License__pc, 
                                                //clcommon__Marital_Status__pc,
                                                /*(SELECT id,
                                                        name,
                                                        Address_Type__c,
                                                        clcommon__County__c,
                                                        Postcode__c,
                                                        Property_Type__c,
                                                        Property_Type_Number__c,
                                                        RPD_Address_Format__c,
                                                        Building_Name__c
                                                   FROM clcommon__Addresses__r 
                                                  WHERE Is_Current__c = true
                                                    AND Address_Validated__c = true
                                                    AND Address_Type__c IN :listAddressTypes),
                                                (SELECT id,
                                                        name,
                                                        BSB__c,
                                                        loan__Bank_Account_Number__c,
                                                        Account_Holder_Name__c 
                                                   FROM loan__bank_accounts__r 
                                                  WHERE loan__Active__c = true),
                                                (SELECT id,
                                                        name,
                                                        loan__Loan_Product_Name__c,
                                                        loan__Account__r.Customer_Number__c,
                                                        loan__Loan_Product_Name__r.name,
                                                        loan__Last_Installment_Date__c,
                                                        loan__Loan_Status__c 
                                                   FROM loan__Loan_Accounts__r
                                                  WHERE loan__Loan_Status__c = :listActiveContractStatuses) 
                                                  */
                                           FROM Account 
                                          WHERE recordtypeid = :personAccountId
                                       ]);                                        
    }
     
    global void execute(SchedulableContext sc){
        /*GenerateRPDExportForSAPJob b = new GenerateRPDExportForSAPJob();         
        database.executeBatch(b, (Integer) batchConfig.BatchSize_SAP_RPD_File__c);
        */
    }
     
    global void execute(Database.BatchableContext bc, List<sObject> scope){
        /*List<Account> customerRecords = (List<Account>)scope;
        try{
            for(Account acc : customerRecords){
                List<clcommon__Address__c> listAddress = acc.clcommon__Addresses__r;
                List<loan__Bank_Account__c> listBankAccounts = acc.loan__bank_accounts__r;
                List<loan__Loan_Account__c> listClContracts = acc.loan__Loan_Accounts__r;

                String partyDetails = FileStructuresForSAP.getPartyDetailRecord(acc);
                String addressDetails = FileStructuresForSAP.getAddressDetails(listAddress);
                List<String> contractDetails = FileStructuresForSAP.getContractDetails(listClContracts, listBankAccounts);
                
                //Adding party records
                listLineRecords.add(partyDetails + ',' + addressDetails + '\r\n');

                //Adding contract records
                for(String contractRec : contractDetails){
                    listLineRecords.add(contractRec + ',' + addressDetails + '\r\n');
                }
            }
        }
        catch(Exception e) {
            voLogInstance.logException(7002, '[GenerateRPDExportForSAPJob.execute] Exception:' + e.getMessage() + ', at line:' + e.getLineNumber(), e);
        } 
        */        
    }
     
    global void finish(Database.BatchableContext bc) {
        /*try{
            String folderName = batchConfig.FolderName_SAP_RPD_File__c;
            String documentName = batchConfig.SAP_RPD_File_Name_Prefix__c + CommonUtility.getDatetimeValue('YYYYMMDDHHMMSS');

            String fileBody = FileStructuresForSAP.getRPDFileHeader() + '\r\n';
            for(String recordLine : listLineRecords){
                fileBody += recordLine;
            }
            fileBody += FileStructuresForSAP.getRPDFileTrailer(listLineRecords.size());

            CommonUtility.createDocument(folderName, documentName, fileBody, null, null, null);
        }
        catch(Exception e){
            voLogInstance.logException(7002, '[GenerateRPDExportForSAPJob.finish] Exception:' + e.getMessage() + ', at line:' + e.getLineNumber(), e);
            voLogInstance.commitToDb();
        }
        */
    }
}