global class WebserviceAPIUtil{
    public String jsonStart(String jsonStr, String objName, String objAPI, Boolean isChild){
        String retVal = jsonStr != null && String.isNotBlank(jsonStr) ? jsonStr : '' ;
        
        if(!isChild){
            retval = '{\"'+objName+'\" : \"{\\\"attributes\\\":{\\\"type\\\":\\\"'+objAPI+'\\\"},';
        }
        else{
            retval = '\"'+objName+'\" : \"{\"'+ objAPI +'\":[{\\\"attributes\\\":{\\\"type\\\":\\\"'+objAPI+'\\\"},';
        }
        
        return retval;
    }
    public String jsonAddFields(String jsonStr, String nsPrefix, String field, Object value, Boolean isEnd){
        String retVal = jsonStr != null && String.isNotBlank(jsonStr) ? jsonStr : '' ;
        
        retVal += '\\\"' + nsPrefix + field +'\\\":\\\"' + value +'\\\"';
        if(isEnd){
            retVal += '}"';
        }
        else{
            retval += ',';
        }
        return retval;
    }
    public String jsonEnd(String jsonStr, Boolean isChild){
        String retVal = jsonStr != null && String.isNotBlank(jsonStr) ? jsonStr : '' ;
        
        if(isChild){
            if(retval.endsWith('"')){
                retval = retval.removeEnd('"');
            }
            retval += ']}"';
        }
        else{
            retval += '"}';
        }
        return retval;
    }
    public clcommon__CL_Purpose__c getPurpose(String loanType, String purpose, Boolean isVehicleLessThan2years, Boolean isPersonalLoan, String camperType){
        List<clcommon__CL_Purpose__c> clPurpose;
        String purposeName = '';
        String vehicleCondition = '';
        camperType = camperType != null ? camperType : '';
        if(isPersonalLoan){
            if(purpose != null && String.isNotBlank(purpose)){
                purposeName = purpose;
                system.debug(purpose);
                system.debug(loanType);
                clPurpose = [SELECT ID,
                                    Name,
                                    Active__c,
                                    CL_Product__c,
                                    CL_Product__r.Name
                               FROM clcommon__CL_Purpose__c
                              WHERE Name =: purpose
                                AND CL_Product__r.clcommon__Product_Name__c =: loanType
                                AND Active__c = true LIMIT 1];
            }
        }
        else{
            purposeName = (purpose != null && String.isNotBlank(purpose)) ?
                          purpose :
                          loanType;
            
            //Date manufactureDateType;
            if(!isVehicleLessThan2years){
                 vehicleCondition = 'Used';
            }
            else{
                 vehicleCondition = 'New';
            }
            
            if(purposeName.contains('Camper')){
                clPurpose = [SELECT ID,
                                    Name,
                                    Active__c,
                                    CL_Product__c
                               FROM clcommon__CL_Purpose__c
                              WHERE CL_Product__r.clcommon__Product_Name__c =: loanType
                                AND Name LIKE: '%'+vehicleCondition+'%'
                                AND Name LIKE: '%'+camperType+'%'
                                AND Active__c = true];
            }
            else{
                clPurpose = [SELECT ID,
                                    Name,
                                    Active__c,
                                    CL_Product__c
                               FROM clcommon__CL_Purpose__c
                              WHERE CL_Product__r.clcommon__Product_Name__c =: loanType
                                AND Name LIKE: '%'+vehicleCondition+'%'
                                AND Active__c = true];
            }
            
        }
        if(clPurpose != null && clPurpose.size() > 0){
            return clPurpose[0];
        }
        else{
            return null;
        }
    }
    public Integer getTerm(Integer inputTerm, String frequency){
        Integer term = inputTerm;
        if(frequency != null & String.isNotBlank(frequency)){
            if('Monthly'.equalsIgnoreCase(frequency)){
                term = term * 12;
            }
            else if('Fortnightly'.equalsIgnoreCase(frequency) || 'BI-WEEKLY'.equalsIgnoreCase(frequency)){
                term = term * 26;
            }
            else if('Weekly'.equalsIgnoreCase(frequency)){
                term = term * 52;
            }
        }
        return term;
    }
    public Integer getTermInYears(Integer inputTerm, String frequency){
        Integer term = inputTerm;
        if(frequency != null & String.isNotBlank(frequency)){
            if('Monthly'.equalsIgnoreCase(frequency)){
                term = term / 12;
            }
            else if('Fortnightly'.equalsIgnoreCase(frequency) || 'BI-WEEKLY'.equalsIgnoreCase(frequency)){
                term = term / 26;
            }
            else if('Weekly'.equalsIgnoreCase(frequency)){
                term = term / 52;
            }
        }
        return term;
    }
    
    public class GoalsAndObjectives{
        public String whatCustomerWants;
        public String whyCustomerNeeds;
    }
    public GoalsAndObjectives getGNOMapping(String desciption){
        GoalsAndObjectives gno = new GoalsAndObjectives();
        if(desciption == null || String.isBlank(desciption)){
            return null;
        }
        else if(desciption.equalsIgnoreCase('Upgrading (to a newer asset)')){
            gno.whatCustomerWants = 'Buy a newer vehicle';
            gno.whyCustomerNeeds = 'To upgrade';
        }
        else if(desciption.equalsIgnoreCase('Downsize (to a smaller asset)')){
            gno.whatCustomerWants = 'Buy a smaller vehicle';
            gno.whyCustomerNeeds = 'To downsize';
        }
        else if(desciption.equalsIgnoreCase('Problems with existing asset')){
            gno.whatCustomerWants = 'Buy a replacement vehicle';
            gno.whyCustomerNeeds = 'There are problems with the existing vehicle';
        }
        else if(desciption.equalsIgnoreCase('Additional asset')){
            gno.whatCustomerWants = 'Buy an additional vehicle';
            gno.whyCustomerNeeds = 'Need another vehicle';
        }
        else if(desciption.equalsIgnoreCase('Work reasons/purpose')){
            gno.whatCustomerWants = 'Buy a vehicle for work';
            gno.whyCustomerNeeds = 'Required for work purpose';
        }
        else if(desciption.equalsIgnoreCase('Looking to lower repayments or save costs')){
            gno.whatCustomerWants = 'Buy a vehicle';
            gno.whyCustomerNeeds = 'To save costs or lower repayments';
        }
        else if(desciption.equalsIgnoreCase('Other')){
            gno.whatCustomerWants = 'Buy a vehicle';
            gno.whyCustomerNeeds = '';
        }
        else{
            gno.whatCustomerWants = '';
            gno.whyCustomerNeeds = '';
        }
        return gno;
    } 
    
    public String clcommonCollateralName;
    public String motorWebJson;
    public String glassesJson;
    public String manualJSON;
    public String vehicleRegisteredState;
    public String vehicleRegistrationNumber;
    public String yearOfManufacture;
    public String make;
    public String model;
    public String retailValue;
    
    public Decimal    folio;
    public Decimal    vol;
    //public Decimal    assesVal;
    public String   propertyAddress_First;
    public Boolean   propertyAddressManualEntry_First;
    public String   propertyAdPropertyType_First;
    public String   propertyAdBuildingName_First;
    public String   propertyAdStreetDirection_First;
    public String   propertyAdUnitNumber_First;
    public String   propertyAdStreetNumber_First;
    public String   propertyAdStreetName_First;
    public String   propertyAdStreetType_First;
    public String   propertyAdSuburb_First;
    public String   propertyAdState_First;
    public String   propertyAdPostalCode_First;
    public String   propertyAdCountry_First;
    public String   propertyAdPropertyType1;
    public String   propertyAdPropertyTypeNum1;
    public String   propertyAdPropertyType2;
    public String   propertyAdPropertyTypeNum2;
    public String   propertyAdPropertyType3;
    public String   propertyAdPropertyTypeNum3;
    public String   vinNumber;
    public String   hinNumber;
    public String   variantName;
    public String   seriesName;
    public Decimal   Value;
    public String   colour;
    public String   nvic;
    public String   EngineNumber;
    public Decimal   odometer;
    public String   TrailerBodyStyle;
    public String   GrossMass;
    public String   BoatType;
    public String   IsBroker;
    public String   position;
    public Boolean   collateralManualEntry;
    public String   collateralId;
    public String   appCollateralId;
    
    public void createCollateral(String json, String applicationId, String loanProduct){
        mfiflexUtil.VOLog voLogInstance = mfiflexUtil.VOLog.getInstance('Genesis');
        if(json != null && applicationId != null){
            WebserviceAPIUtil response = (WebserviceAPIUtil) System.JSON.deserialize(json, WebserviceAPIUtil.class);
            List<clcommon__Collateral_Type__c> collType = [SELECT id FROM clcommon__Collateral_Type__c WHERE Name =: loanProduct];
            
            String responseJson = (response.motorWebJson != null && String.isNotBlank(response.motorWebJson)) ? 
                                  response.motorWebJson : 
                                  ((response.glassesJson != null && String.isNotBlank(response.glassesJson)) ? response.glassesJson : '');
            String callType = 'Manual';
            String collId = '';
            String vinNumber = '';
            String hinNumber = '';
            Boolean duplicateColl = false;
            if(response.vinNumber != null && String.isNotBlank(response.vinNumber)){
                vinNumber = response.vinNumber;
            }
            else if(response.hinNumber != null && String.isNotBlank(response.hinNumber)){
                hinNumber = response.hinNumber;
            }
            else if (String.isNotBlank(responseJson)){
                VehicleSearchResponseParser.Vehicle parseVehicleData = (VehicleSearchResponseParser.Vehicle) System.JSON.deserialize(responseJson, VehicleSearchResponseParser.Vehicle.class);
                if(parseVehicleData != null && parseVehicleData.vinNumber != null){
                    vinNumber = parseVehicleData.vinNumber;
                }
            }
            
            if(String.isNotBlank(vinNumber)){
                List<clcommon__Collateral__c> dupVin = [SELECT id,
                                                         genesis__Application__c,
                                                         genesis__Application__r.Name
                                                    FROM clcommon__Collateral__c
                                                   WHERE vinNumber__c =: vinNumber
                                                     AND vinNumber__c !=: ''];
                if(dupVin != null && dupVin.size() > 0){
                    Task newTask = new Task(Description = ' VIN Number ' +vinNumber+ ' exists. Please check if collateral is used on existing loan.',
                                    Priority = 'Normal',
                                    Status = 'Not Started',
                                    Subject = 'Other',
                                    genesis__Application__c  = applicationId   
                    
                    );  
                    Database.insert(newTask, false);
                    collId = dupVin[0].id;
                    duplicateColl = true;
                }
            }
            else if(response.IsBroker != null && String.isNotBlank(response.IsBroker) && String.isNotBlank(hinNumber)){
                List<clcommon__Collateral__c> dupVin = [SELECT id,
                                                         genesis__Application__c,
                                                         genesis__Application__r.Name
                                                    FROM clcommon__Collateral__c
                                                   WHERE HIN_Number__c =: hinNumber
                                                     AND HIN_Number__c !=: ''];
                if(dupVin != null && dupVin.size() > 0){
                    Task newTask = new Task(Description = ' HIN Number ' +hinNumber+ ' exists. Please check if collateral is used on existing loan.',
                                    Priority = 'Normal',
                                    Status = 'Not Started',
                                    Subject = 'Other',
                                    genesis__Application__c  = applicationId   
                    
                    );  
                    Database.insert(newTask, false);
                    collId = dupVin[0].id;
                    duplicateColl = true;
                }
            }
            clcommon__Collateral__c coll = new clcommon__Collateral__c();
            if(response.motorWebJson != null && String.isNotBlank(response.motorWebJson)){
                //responseJson = response.motorWebJson;
                callType = 'MotorWeb';
                // user entered details
                if(response.vehicleRegisteredState != null)
                coll.clcommon__Registration_State__c = response.vehicleRegisteredState;
                if(response.vehicleRegistrationNumber != null)
                coll.Registration_Number__c = response.vehicleRegistrationNumber;
                
            } 
            else if(response.glassesJson != null && String.isNotBlank(response.glassesJson)){
                //responseJson = response.glassesJson;
                callType = 'Glasses';
                
                // user entered details
                if(response.yearOfManufacture != null)
                coll.clcommon__Year__c = response.yearOfManufacture;
                if(response.make != null)
                coll.manufacturerName__c = response.make;
                if(response.model != null)
                coll.modelName__c = response.model;
                if(response.retailValue != null)
                coll.Retail__c = Decimal.valueOf(response.retailValue);
                
            }
            else {
                if(response.collateralId != null && String.isNotBlank(response.collateralId) &&
                response.collateralManualEntry != null && response.collateralManualEntry){
                    collId = response.collateralId;
                }
                //Property type collateral
                coll.FOLIO__c = response.folio;//response.folio != null ? response.folio : 0;
                coll.VOL__c = response.vol;//response.vol != null ? response.vol : 0;
                //coll.clcommon__Value__c = response.assesVal != null ? response.assesVal : 0;
                
                //Manual entry from broker portal
                coll.vinNumber__c = response.vinNumber != null ? response.vinNumber : '';
                coll.HIN_Number__c = response.hinNumber != null ? response.hinNumber : '';
                coll.variantName__c = response.variantName != null ? response.variantName : '';
                coll.seriesName__c = response.seriesName != null ? response.seriesName : '';
                coll.clcommon__Value__c = response.Value != null ? response.Value : 0;
                coll.colour__c = response.colour != null ? response.colour : '';
                coll.nvic__c = response.nvic != null ? response.nvic : '';
                coll.Engine_Number__c = response.EngineNumber != null ? response.EngineNumber : '';
                coll.odometer__c = response.odometer != null ? response.odometer : 0;
                coll.clcommon__Trailer_Body_Style__c = response.TrailerBodyStyle != null ? response.TrailerBodyStyle : '';
                coll.Gross_Mass__c = response.GrossMass != null ? response.GrossMass : '';
                coll.Boat_Type__c = response.BoatType != null ? response.BoatType : '';
                coll.clcommon__Registration_State__c = response.vehicleRegisteredState != null ? response.vehicleRegisteredState : '';
                coll.Registration_Number__c = response.vehicleRegistrationNumber != null ? response.vehicleRegistrationNumber : '';
                
                //Vehicle type collateral
                //if(response.vehicleRegisteredState != null)
                //coll.clcommon__Registration_State__c = response.vehicleRegisteredState;
                //if(response.vehicleRegistrationNumber != null)
                //coll.Registration_Number__c = response.vehicleRegistrationNumber;
                if(response.yearOfManufacture != null)
                coll.clcommon__Year__c = response.yearOfManufacture;
                if(response.make != null)
                coll.manufacturerName__c = response.make;
                if(response.model != null)
                coll.modelName__c = response.model;
                if(response.retailValue != null){
                coll.Retail__c = Decimal.valueOf(response.retailValue);
                }
            }
            // update in the case of duplicate VIN
            if(collId != null && String.isNotBlank(collId)){
                coll.id = collId;
            }
            coll.genesis__Application__c = applicationId;
            if(collType != null && collType.size() > 0){
                coll.clcommon__Collateral_Type__c = collType[0].id;
            }
            coll.clcommon__Collateral_Name__c = response.clcommonCollateralName != null ? response.clcommonCollateralName :loanProduct;
            
            coll.JSON_Response__c = responseJson;
            coll.Call_Type__c = callType;
            //VehicleSearchResponseParser parser = new VehicleSearchResponseParser();
            //VehicleSearchResponseParser.parseJSON(responseJson,coll);
            Database.UpsertResult collSr = Database.upsert(coll, false);
            if (collSr.isSuccess()) {
                List< clcommon__Collateral__c > colList = [Select id, 
                                                                  Vehicle_State__c,
                                                                  Final_Collateral_Value__c,
                                                                  genesis__Application__c,
                                                                  genesis__Application__r.genesis__Account__c
                                                           from clcommon__Collateral__c 
                                                           where id =: collSr.getId()];
               
               if(colList.size() > 0){      
                   Decimal finalColVal = colList[0].Final_Collateral_Value__c;
                   genesis__Application_Collateral__c appColl = new genesis__Application_Collateral__c();
                   if(response.appCollateralId != null && String.isNotBlank(response.appCollateralId)){
                       appColl.id = response.appCollateralId;
                   }
                   appColl.genesis__Application__c = applicationId;
                   appColl.genesis__Collateral__c = collSr.getId();
                   //appColl.Security_Position__c = response.position != null ? response.position : 'Primary';
                   appColl.genesis__Pledge_Amount__c = finalColVal;
                    //insert appColl;
                   Database.SaveResult appCollSrList = Database.insert(appColl, false); 
                   
                   if(response.collateralId == null || String.isBlank(response.collateralId)){
                       clcommon__CollateralLien__c colLien = new clcommon__CollateralLien__c();
                       colLien.genesis__Application__c = applicationId;
                       colLien.clcommon__Lien_Amount__c =  finalColVal; 
                       colLien.clcommon__Lien_Position__c = 1;  
                       colLien.clcommon__Status__c = 'Proposed';
                       colLien.clcommon__Collateral__c = collSr.getId();
                       if(duplicateColl){
                           colLien.Collateral_Existence__c = 'Duplicate';
                       }
                       Database.SaveResult insertColLien = Database.insert(colLien, false);
                       
                       clcommon__Collateral_Owner__c colOwner = new clcommon__Collateral_Owner__c();
                       colOwner.clcommon__Collateral__c = collSr.getId();
                       colOwner.clcommon__Ownership__c = 100;
                       if(colList[0].genesis__Application__c != null && colList[0].genesis__Application__r.genesis__Account__c != null){
                           colOwner.clcommon__Account__c = colList[0].genesis__Application__r.genesis__Account__c;
                       }
                       Database.SaveResult insertColOwner = Database.insert(colOwner, false);
                   }
                   
                   
                   if(response.IsBroker == null || String.isBlank(response.IsBroker)){                     
                       List<clcommon__CL_Purpose__c> clPurpose = [SELECT ID,
                                                                        Name,
                                                                        Active__c,
                                                                        CL_Product__c
                                                                   FROM clcommon__CL_Purpose__c
                                                                  WHERE CL_Product__r.clcommon__Product_Name__c =: loanProduct
                                                                    AND Name LIKE: '%'+colList[0].Vehicle_State__c+'%'
                                                                    AND Active__c = true];   
                       
                       if(clPurpose.size()>0){                                             
                           genesis__Applications__c appl = new genesis__Applications__c(id = applicationId);    
                           appl.genesis__CL_Purpose__c  =   clPurpose[0].id;
                           update appl; 
                       }
                   }
               } 
               //Capture Property type Collateral address
               If((response.propertyAddress_First != null && String.isNotBlank(response.propertyAddress_First))||
                   (response.propertyAdState_First != null && String.isNotBlank(response.propertyAdState_First))){
                   
                   BrokerContactRelatedInfoUtil cru = new BrokerContactRelatedInfoUtil();
                   clcommon__Address__c currAddressFirst = new clcommon__Address__c();
                   
                   if(response.propertyAddressManualEntry_First != null && !response.propertyAddressManualEntry_First){
                        currAddressFirst.Address_Validated__c = true;
                    }
                    if(response.propertyAddress_First != null && String.isNotBlank(response.propertyAddress_First)){
                        currAddressFirst = cru.parseAddress(currAddressFirst, response.propertyAddress_First);
                    }
                    else{
                        if(response.propertyAdPropertyType_First != null && String.isNotBlank(response.propertyAdPropertyType_First)){
                            currAddressFirst.Property_Type_Picklist__c = response.propertyAdPropertyType_First;
                        }
                        if(response.propertyAdBuildingName_First != null && String.isNotBlank(response.propertyAdBuildingName_First)){
                            currAddressFirst.Building_Name__c = response.propertyAdBuildingName_First;
                        }
                        if(response.propertyAdStreetDirection_First != null && String.isNotBlank(response.propertyAdStreetDirection_First)){
                            currAddressFirst.Street_Direction__c = response.propertyAdStreetDirection_First;
                        }
                        if(response.propertyAdUnitNumber_First != null && String.isNotBlank(response.propertyAdUnitNumber_First)){
                            currAddressFirst.Property_Type_Number__c = response.propertyAdUnitNumber_First;
                            currAddressFirst.Property_Type_Picklist__c = 'APARTMENT';
                        }
                        if(response.propertyAdStreetNumber_First != null && String.isNotBlank(response.propertyAdStreetNumber_First)){
                            currAddressFirst.Street_Number__c = response.propertyAdStreetNumber_First;
                        }
                        if(response.propertyAdStreetName_First != null && String.isNotBlank(response.propertyAdStreetName_First)){
                            currAddressFirst.Street_Name__c = response.propertyAdStreetName_First;
                        }
                        if(response.propertyAdStreetType_First != null && String.isNotBlank(response.propertyAdStreetType_First)){
                            currAddressFirst.Street_Type__c = response.propertyAdStreetType_First;
                        }
                        if(response.propertyAdSuburb_First != null && String.isNotBlank(response.propertyAdSuburb_First)){
                            currAddressFirst.clcommon__County__c = response.propertyAdSuburb_First;
                        }
                        if(response.propertyAdState_First != null && String.isNotBlank(response.propertyAdState_First)){
                            currAddressFirst.clcommon__State_Province__c = response.propertyAdState_First;
                        }
                        if(response.propertyAdPostalCode_First != null && String.isNotBlank(response.propertyAdPostalCode_First)){
                            currAddressFirst.Postcode__c = response.propertyAdPostalCode_First;
                        }
                        if(response.propertyAdPropertyType1 != null && String.isNotBlank(response.propertyAdPropertyType1)){
                            currAddressFirst.Property_Type_Picklist__c = response.propertyAdPropertyType1;
                        }
                        if(response.propertyAdPropertyTypeNum1 != null && String.isNotBlank(response.propertyAdPropertyTypeNum1)){
                            currAddressFirst.Property_Type_Number__c = response.propertyAdPropertyTypeNum1;
                        }
                        if(response.propertyAdPropertyType2 != null && String.isNotBlank(response.propertyAdPropertyType2)){
                            currAddressFirst.Property_Type_Picklist_2__c = response.propertyAdPropertyType2;
                        }
                        if(response.propertyAdPropertyTypeNum2 != null && String.isNotBlank(response.propertyAdPropertyTypeNum2)){
                            currAddressFirst.Property_Type_Number_2__c = response.propertyAdPropertyTypeNum2;
                        }
                        if(response.propertyAdPropertyType3 != null && String.isNotBlank(response.propertyAdPropertyType3)){
                            currAddressFirst.Property_Type_Picklist_3__c = response.propertyAdPropertyType3;
                        }
                        if(response.propertyAdPropertyTypeNum3 != null && String.isNotBlank(response.propertyAdPropertyTypeNum3)){
                            currAddressFirst.Property_Type_Number_3__c = response.propertyAdPropertyTypeNum3;
                        }
                        
                        if(response.propertyAdCountry_First != null && String.isNotBlank(response.propertyAdCountry_First)){
                            currAddressFirst.Country_Picklist__c = response.propertyAdCountry_First;
                        }
                    }
                    if(currAddressFirst != null){
                        //insert allAddresses;
                        Database.SaveResult sr = Database.insert(currAddressFirst, false);
                        if (sr.isSuccess()) {
                            clcommon__Collateral_Location__c colloc = new clcommon__Collateral_Location__c();
                            colloc.clcommon__Collateral__c = collSr.getId();
                            colloc.Address__c = sr.getId();
                            insert colloc;
                        }
                        else{
                            
                                for(Database.Error err : sr.getErrors()) {                   
                                System.debug(err.getStatusCode() + ': ' + err.getMessage());
                                System.debug('Address fields that affected this error: ' + err.getFields());
                                voLogInstance.logError(1001, '[WebserviceAPIUtil.createCollateral] Error: ' + err.getMessage() + ' (statusCode: ' + err.getStatusCode() + ': ' + err.getMessage());
                                voLogInstance.commitToDb();
                            }
                        }
                    }
               }                              
                
            }
            else {
                Task newTask;
                // Operation failed, so get all errors                
                for(Database.Error err : collSr.getErrors()) {
                    voLogInstance.logError(1001, '[WebserviceAPIUtil.createCollateral] Error: ' + err.getStatusCode() + ': ' + err.getMessage()+
                                                'Account fields that affected this error: ' + err.getFields());
                     
                    newTask = new Task(Description = 'callType : '+callType+' Error : '+err.getMessage(),
                                        Priority = 'Normal',
                                        Status = 'Not Started',
                                        Subject = 'Other',
                                        genesis__Application__c  = applicationId   
                        
                        );             
                
                    
                }
                voLogInstance.commitToDb();
                insert newTask;
            }
        }
    }
    
    public class PartyParams{
        public String accountId;
        public String contactId;
        public String applicationId;
        public String partyType;
        public String relationshipToBorr;
        public String relationshipWith;
        public Boolean applicationSubmitted;
        public Boolean scoreseekerConsent;
        public Boolean privacyConsent;
        public Boolean electronicIdConsent;
        
    }
    public clcommon__Party__c createParty(PartyParams params){
        List<clcommon__Party_Type__c> partyType;
        clcommon__Party__c createParty; 
        mfiflexUtil.VOLog voLogInstance = mfiflexUtil.VOLog.getInstance('Genesis');
        try{
            if(params.partyType != null && String.isNotBlank(params.partyType)){
                partyType = [SELECT id, Name FROM clcommon__Party_Type__c WHERE Name =: params.partyType];
            }
            if(partyType != null && partyType.size() > 0){
                createParty = new clcommon__Party__c();
                createParty.clcommon__Account__c = params.accountId;
                createParty.clcommon__Contact__c = params.contactId;
                createParty.genesis__Application__c = params.applicationId;
                createParty.clcommon__Type__c = partyType[0].id;
                if(params.relationshipToBorr != null && String.isNotBlank(params.relationshipToBorr)){
                    createParty.Relationship_to_Applicant__c = params.relationshipToBorr;
                }
                if(params.relationshipWith != null && String.isNotBlank(params.relationshipWith)){
                    createParty.Relationship_to_Applicant__c = params.relationshipWith;
                }
                /*if((params.applicationSubmitted != null && params.applicationSubmitted) || (params.scoreseekerConsent != null && params.scoreseekerConsent)){
                    createParty.Scoreseeker_Consent__c = true;
                }
                if((params.applicationSubmitted != null && params.applicationSubmitted) || (params.privacyConsent != null && params.privacyConsent)){
                    createParty.genesis__Credit_Check_Consent__c = true;
                }
                if((params.applicationSubmitted != null && params.applicationSubmitted) || (params.electronicIdConsent != null && params.electronicIdConsent)){
                    createParty.Electronic_Identification_Consent__c = true;
                }*/
                if(params.scoreseekerConsent != null){
                    createParty.Scoreseeker_Consent__c = params.scoreseekerConsent;
                }
                if(params.privacyConsent != null){
                    createParty.genesis__Credit_Check_Consent__c = params.privacyConsent;
                }
                if(params.electronicIdConsent != null){
                    createParty.Electronic_Identification_Consent__c = params.electronicIdConsent;
                }
            }
        }
        catch(Exception e){
            System.debug(e.getMessage());
            voLogInstance.logException(1001, '[ApplicationCreationAPI.createParty] Error: ' + e.getMessage() + ' (at line: ' + e.getLineNumber() + ')', e);
            voLogInstance.commitToDb();
        }
        return createParty;
    }
}