public class CustomCollateralTriggerHandler {
    //Before Insert always
    public void parseJSON(List<clcommon__Collateral__c> newColls, Map<Id, clcommon__Collateral__c> oldMap) {
        for(clcommon__Collateral__c collateral : newColls){
            if(collateral.JSON_Response__c != null){
                String returnMsg = VehicleSearchResponseParser.parseJSON(collateral.JSON_Response__c, collateral);
                if(!System.label.TAG_SUCCESS.equalsIgnoreCase(returnMsg)){
                    collateral.addError(returnMsg);
                }
            }
        }
    }
    
    //Post Insert/Update
    public void createCollateralValuation(List<clcommon__Collateral__c> newColls, Map<Id, clcommon__Collateral__c> oldMap) {
        List<clcommon__Collateral_Valuation__c> newCollValList = new List<clcommon__Collateral_Valuation__c>();
        for(clcommon__Collateral__c collateral : newColls){
            clcommon__Collateral__c oldColl = (oldMap == null ? null : oldMap.get(collateral.Id));
            if(oldColl == null
                  || collateral.retail__c != oldColl.retail__c 
                  || collateral.rrp__c != oldColl.rrp__c
                  || collateral.trade__c != oldColl.trade__c
                  || collateral.tradeLow__c != oldColl.tradeLow__c){
                clcommon__Collateral_Valuation__c collVal = new clcommon__Collateral_Valuation__c();
                collVal.clcommon__Collateral__c = collateral.Id;
                collVal.clcommon__Appraised_Value__c = collateral.Final_Collateral_Value__c;
                collVal.rrp__c = collateral.rrp__c;
                collVal.trade__c = collateral.trade__c;
                collVal.tradeLow__c = collateral.tradeLow__c;
                collval.clcommon__Appraisal_Date__c = Date.today();
                newCollValList.add(collVal);
            }
        }
        insert newCollValList;
    }
    
    public void updateAdjustmentKM(List<clcommon__Collateral__c> newColls, Map<Id, clcommon__Collateral__c> oldMap) {
        List<clcommon__Collateral__c> newCollList = new List<clcommon__Collateral__c>();
        Set<String> setCategoryCode = new Set<String>();
        Set<String> setVehicleClass = new Set<String>();
        
        List<String> listCallTypes = new List<String>();
        listCallTypes.add('Glasses');
        listCallTypes.add('MotorWeb');
        
        for(clcommon__Collateral__c collateral : newColls){
            clcommon__Collateral__c oldColl = (oldMap == null ? null : oldMap.get(collateral.Id));
            if(listCallTypes.contains(collateral.Call_Type__c)//.equalsIgnoreCase('Glasses')  
                  && (oldColl == null 
                  || collateral.odometer__c != oldColl.odometer__c
                  || collateral.releaseDate__c != oldColl.releaseDate__c
                  || collateral.averageKM__c != oldColl.averageKM__c
                  || collateral.Vehicle_Class__c != oldColl.Vehicle_Class__c
                  || collateral.guideKMCategoryCode__c != oldColl.guideKMCategoryCode__c)){
                
                if(collateral.releaseDate__c != null){
                    String releaseDateStr = collateral.releaseDate__c;
                    if(releaseDateStr.contains('T')){
                        releaseDateStr = releaseDateStr.split('T').get(0);    
                    }
                    Date dt;
                    if(releaseDateStr.contains('-')){
                        dt = Date.valueOf(releaseDateStr);
                    }
                    else{
                        dt = Date.parse(releaseDateStr);
                    }
                    collateral.clcommon__Age__c = dt.daysBetween(CommonUtility.getSystemDate())/365;
                }
                else{
                    collateral.clcommon__Age__c = 0;         
                }
                      
                //if(collateral.clcommon__Age__c >= 10){
                //    collateral.clcommon__Age__c = 0;
                //}
                
                setCategoryCode.add(collateral.guideKMCategoryCode__c);
                setVehicleClass.add(collateral.Vehicle_Class__c);
                newCollList.add(collateral);
            }
        }
        
        List<KM_Adjustment_Table__c> kmAdjustmentTable = [SELECT id,
                                                                 name,
                                                                 Kilometre_Value__c,
                                                                 Adjustment_Value__c,
                                                                 Category_Code__c,
                                                                 Vehicle_Class__c
                                                            FROM KM_Adjustment_Table__c
                                                           WHERE Category_Code__c IN :setCategoryCode
                                                             //AND Vehicle_Class__c IN :setVehicleClass
                                                         ];
        
        Map<String, Decimal> mapKmAdjustmentTable = new Map<String, Decimal>();
        for(KM_Adjustment_Table__c tableContent : kmAdjustmentTable){
            //String keyValue = tableContent.Vehicle_Class__c + ':' + tableContent.Category_Code__c + ':' + tableContent.Kilometre_Value__c;
            String keyValue = tableContent.Category_Code__c + ':' + tableContent.Kilometre_Value__c;
            mapKmAdjustmentTable.put(keyValue, tableContent.Adjustment_Value__c);
        }
        
        for(clcommon__Collateral__c collateral : newCollList){
            Decimal odometerValue = collateral.odometer__c != null ? collateral.odometer__c : 0;
            
            if(odometerValue < 0 || collateral.clcommon__Age__c <= 0 || collateral.clcommon__Age__c >= 10){
                collateral.Adjustment_KMs__c = 0;
                collateral.Adjustment_KM_Value_Rounded__c = 0;
                collateral.Retail_Adjustment__c = 0;
                continue;
            }
            Decimal averageKMValue = collateral.averageKM__c != null ? (collateral.averageKM__c * 1000) : 0;
            Decimal adjustmentVal = ((averageKMValue * collateral.clcommon__Age__c) - odometerValue);
            collateral.Adjustment_KMs__c = adjustmentVal != null ? adjustmentVal : 0;
            collateral.Adjustment_KM_Value_Rounded__c = getNearestThousandValue(Math.abs(Integer.valueOf(adjustmentVal)));
                
            //String keyValue = collateral.Vehicle_Class__c + ':' + collateral.guideKMCategoryCode__c + ':' + collateral.Adjustment_KM_Value_Rounded__c;
            String keyValue = collateral.guideKMCategoryCode__c + ':' + collateral.Adjustment_KM_Value_Rounded__c;
            collateral.Retail_Adjustment__c = mapKmAdjustmentTable.containsKey(keyValue) ? mapKmAdjustmentTable.get(keyValue) : 0;
        }
    }
    
    public Decimal getNearestThousandValue(Integer dec){
        if(dec == 0){
            return 0;
        }
        else if(dec < 10000){
            return 0;
        }
        else if(dec > 100000){
            return 100000;
        }
        else{
            Integer modVal = Math.mod(dec, 1000);
            if(modVal  < 500){
                return (dec - modVal);
            }
            else{
                return (dec - modVal) + 1000;
            }
        }
    }
    
    //Post Insert/Update
    public void updatePledgeValue(List<clcommon__Collateral__c> newColls, Map<Id, clcommon__Collateral__c> oldMap) {
        
        Set<String> setCollateralIds = new Set<String>();
        Map<Id, Decimal> mapCollToValue = new Map<Id, Decimal>();
        for(clcommon__Collateral__c collateral : newColls){
            setCollateralIds.add(collateral.Id);    
            mapCollToValue.put(collateral.Id, collateral.Final_Collateral_Value__c);            
        }
        
        List<clcommon__CollateralLien__c> CollLienList = [SELECT id,
                                                                 name,
                                                                 clcommon__Lien_Amount__c,
                                                                 clcommon__Collateral__c,
                                                                 loan__Loan_Account__c,
                                                                 Residual_Value__c,
                                                                 clcommon__Status__c,
                                                                 genesis__Application__c
                                                            FROM clcommon__CollateralLien__c 
                                                           WHERE clcommon__Collateral__c IN :setCollateralIds
                                                         ];
        
        //Calculating residual value on collateral for LVR 
        Map<String, Decimal> mapCollToResidual = new Map<String, Decimal>();
        Set<Id> applicationsIds = new Set<Id>();
        for(clcommon__CollateralLien__c collLien : CollLienList){
            if(collLien.loan__Loan_Account__c != null){
                mapCollToResidual.put(collLien.clcommon__Collateral__c, collLien.Residual_Value__c);
            }        
            
            if('Proposed'.equalsIgnoreCase(collLien.clcommon__Status__c)){
                collLien.clcommon__Lien_Amount__c = mapCollToValue.get(collLien.clcommon__Collateral__c);
                applicationsIds.add(collLien.genesis__Application__c);
            }
        }
        
        List<genesis__Application_Collateral__c> appCollList = [SELECT id,
                                                                       name,
                                                                       genesis__Pledge_Amount__c,
                                                                       genesis__Collateral__c//,
                                                                       //Security_Position__c
                                                                  FROM genesis__Application_Collateral__c 
                                                                 WHERE genesis__Collateral__c IN :setCollateralIds
                                                                   AND genesis__Application__c IN :applicationsIds
                                                               ];
        
        for(genesis__Application_Collateral__c appColl : appCollList){
            //if(appColl.Security_Position__c != null){
                Decimal pleadgeAmount = (mapCollToValue.get(appColl.genesis__Collateral__c) != null ? mapCollToValue.get(appColl.genesis__Collateral__c) : 0);
                /*if(appColl.Security_Position__c.equalsIgnoreCase('Primary')){
                    pleadgeAmount = (mapCollToValue.get(appColl.genesis__Collateral__c) != null ? mapCollToValue.get(appColl.genesis__Collateral__c) : 0);
                }
                else{
                    pleadgeAmount = (mapCollToResidual.get(appColl.genesis__Collateral__c) != null ? mapCollToResidual.get(appColl.genesis__Collateral__c) : 0);
                }*/  
                
                appColl.genesis__Pledge_Amount__c = pleadgeAmount;
            //}
        }
        
        if(appCollList != null && appCollList.size() > 0){
            update appCollList;
        }
            
        if(CollLienList != null && CollLienList.size() > 0){
            update CollLienList;
        }
    }
}