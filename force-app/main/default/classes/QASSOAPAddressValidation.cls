/******************************************************************************************

*****************************************************************************************/

global with sharing class QASSOAPAddressValidation {
    
    public static final String CLASS_NAME = QASSOAPAddressValidation.class.getName();
    public static final System.LoggingLevel LOGGING_LEVEL = LoggingLevel.ERROR;
    
    public static List<Map<String, String>> xmlDataMapList; 
    public static Map<String, String> xmlDataMap;
      public QASSOAPAddressValidation(clcommon.PortalActions pa) {
        System.debug('Hello World');
    }
    
    public class mexException extends Exception {}
    
    @RemoteAction 
    webservice static List < String > getselectOptions(sObject objObject, string fld) {
        system.debug('objObject --->' + objObject);
        system.debug('fld --->' + fld);
        List < String > allOpts = new list < String > ();
        // Get the object type of the SObject.
        Schema.sObjectType objType = objObject.getSObjectType();
        
        // Describe the SObject using its object type.
        Schema.DescribeSObjectResult objDescribe = objType.getDescribe();
        
        // Get a map of fields for the SObject
        map < String, Schema.SObjectField > fieldMap = objDescribe.fields.getMap();
        
        // Get the list of picklist values for this field.
        list < Schema.PicklistEntry > values =
            fieldMap.get(fld).getDescribe().getPickListValues();
        
        // Add these values to the selectoption list.
        for (Schema.PicklistEntry a: values) {
            allOpts.add(a.getValue());
        }
        system.debug('allOpts ---->' + allOpts);
        allOpts.sort();
        return allOpts;
    }
    
    @RemoteAction 
    webservice static Boolean doCanSearch() {
        Boolean canSearch = false;
        String action = 'DoCanSearch';
        String response = QASSOAPAddressValidation.getSoapResponse(action, null);
        
        System.debug('@@@doCanSearch::resBody::'+response);
        
        xmlDataMap = new Map<String, String>();
        
        if (!canSearch) {
            QASSOAPAddressValidation.parserXML(response);
        }
        
        system.debug('@@@xmlDataMap::'+xmlDataMap);
        
        if (xmlDataMap != null && !xmlDataMap.isEmpty() && xmlDataMap.get('CanSearch') != null) {
            canSearch = Boolean.valueOf(xmlDataMap.get('CanSearch'));
        }
        
        system.debug('@@@canSearch::'+canSearch);
        
        return canSearch;
    }
    
    @RemoteAction 
    webservice static String doSearch(String input) {
        String action = 'DoSearch';
        String response = QASSOAPAddressValidation.getSoapResponse(action, input);
        
        /*String response = '<QAPicklist>' +
                            '<PicklistEntry FullAddress="true">' +
                            '<Moniker>AUS|b324cf12-c38e-4927-9ee6-2696d64f92c1|0hOAUSHAziBwAAAAAIAwEAAAAAMrD2AAAAAAAAAAAA..9kAAAAAP....8AAAAAAAAAAAAAAAAAdmljdG9yaWEA$8</Moniker>' +
                            '<PartialAddress>Victoria Park, 100 Barkers Road, GREENETHORPE  NSW  2809</PartialAddress>' +
                            '<Picklist>Victoria Park, 100 Barkers Road, GREENETHORPE  NSW</Picklist>' +
                            '<Postcode>2809</Postcode>' +
                            '<Score>100</Score>' +
                            '</PicklistEntry>' +
                            '<PicklistEntry FullAddress="true">' +
                            '<Moniker>AUS|b324cf12-c38e-4927-9ee6-2696d64f92c1|0sOAUSHAziBwAAAAAIAwEAAAAAaEQqgAAAAAAAAAAA..9kAAAAAP....8AAAAAAAAAAAAAAAAAdmljdG9yaWEA$8</Moniker>' +
                            '<PartialAddress>Victoria Tower, 197-199 Castlereagh Street, SYDNEY  NSW  2000</PartialAddress>' +
                            '<Picklist>Victoria Tower, 197-199 Castlereagh Street, SYDNEY  NSW</Picklist>' +
                            '<Postcode>2000</Postcode>' +
                            '<Score>100</Score>' +
                            '</PicklistEntry>' +
                            '<PicklistEntry FullAddress="true">' +
                            '<Moniker>AUS|b324cf12-c38e-4927-9ee6-2696d64f92c1|0BOAUSHAziBwAAAAAIAwEAAAAAaEQwEBAhAIIAAAAAAAAAODAxAAD..2QAAAAA.....wAAAAAAAAAAAAAAAAB2aWN0b3JpYQA-$8</Moniker>' +
                            '<PartialAddress>Victoria Tower, Unit 801  197-199 Castlereagh Street, SYDNEY  NSW  2000</PartialAddress>' +
                            '<Picklist>Victoria Tower, Unit 801  197-199 Castlereagh Street, SYDNEY  NSW</Picklist>' +
                            '<Postcode>2000</Postcode>' +
                            '<Score>100</Score>' +
                            '</PicklistEntry>' +
                            '<PicklistEntry FullAddress="true">' +
                            '<Moniker>AUS|b324cf12-c38e-4927-9ee6-2696d64f92c1|0SOAUSHAziBwAAAAAIAwEAAAAAaEQwEBAhAIIAAAAAAAAAODAyAAD..2QAAAAA.....wAAAAAAAAAAAAAAAAB2aWN0b3JpYQA-$8</Moniker>' +
                            '<PartialAddress>Victoria Tower, Unit 802  197-199 Castlereagh Street, SYDNEY  NSW  2000</PartialAddress>' +
                            '<Picklist>Victoria Tower, Unit 802  197-199 Castlereagh Street, SYDNEY  NSW</Picklist>' +
                            '<Postcode>2000</Postcode>' +
                            '<Score>100</Score>' +
                            '</PicklistEntry>' +
                            '<PicklistEntry FullAddress="true">' +
                            '<Moniker>AUS|b324cf12-c38e-4927-9ee6-2696d64f92c1|0mOAUSHAziBwAAAAAIAwEAAAAAaEQwEBAhAIIAAAAAAAAAODAzAAD..2QAAAAA.....wAAAAAAAAAAAAAAAAB2aWN0b3JpYQA-$8</Moniker>' +
                            '<PartialAddress>Victoria Tower, Unit 803  197-199 Castlereagh Street, SYDNEY  NSW  2000</PartialAddress>' +
                            '<Picklist>Victoria Tower, Unit 803  197-199 Castlereagh Street, SYDNEY  NSW</Picklist>' +
                            '<Postcode>2000</Postcode>' +
                            '<Score>100</Score>' +
                            '</PicklistEntry>' +
                            '</QAPicklist>';*/
        
        System.debug('@@@doSearch::resBody::'+response);
        
        xmlDataMapList = new List<Map<String, String>>();
        QASSOAPAddressValidation.parserXML(response);
        String strList = JSON.serialize(xmlDataMapList);
        
        system.debug('@@@xmlDataMapList::'+strList);
        
        return strList;
    }
    
    @RemoteAction 
    webservice static String doGetAddress(String input) {
        String mappedData = '';
        String action = 'DoGetAddress';
        String response = QASSOAPAddressValidation.getSoapResponse(action, input);
        
        /*String response = '<QAAddress>' +
                                    '<AddressLine>' +
                                        '<Label />' +
                                        '<Line>Victoria Tower</Line>' +
                                    '</AddressLine>' +
                                    '<AddressLine>' +
                                        '<Label>Flat/Unit (Type)</Label>' +
                                        '<Line />' +
                                    '</AddressLine>' +
                                    '<AddressLine>' +
                                        '<Label>Building level (Type)</Label>' +
                                        '<Line />' +
                                    '</AddressLine>' +
                                    '<AddressLine>' +
                                        '<Label>Allotment (Lot)</Label>' +
                                        '<Line />' +
                                    '</AddressLine>' +
                                    '<AddressLine>' +
                                        '<Label>All postal delivery types (Type)</Label>' +
                                        '<Line />' +
                                    '</AddressLine>' +
                                    '<AddressLine>' +
                                        '<Label>Flat/Unit (Number)</Label>' +
                                        '<Line />' +
                                    '</AddressLine>' +
                                    '<AddressLine>' +
                                        '<Label>Building level (Number)</Label>' +
                                        '<Line />' +
                                    '</AddressLine>' +
                                    '<AddressLine>' +
                                        '<Label>Allotment (Number)</Label>' +
                                        '<Line />' +
                                    '</AddressLine>' +
                                    '<AddressLine>' +
                                        '<Label>All postal delivery types (Number)</Label>' +
                                        '<Line />' +
                                    '</AddressLine>' +
                                    '<AddressLine>' +
                                        '<Label>Sub-building number</Label>' +
                                        '<Line />' +
                                    '</AddressLine>' +
                                    '<AddressLine>' +
                                        '<Label>Building number</Label>' +
                                        '<Line>197-199</Line>' +
                                    '</AddressLine>' +
                                    '<AddressLine>' +
                                        '<Label>Street (Name)</Label>' +
                                        '<Line>Castlereagh</Line>' +
                                    '</AddressLine>' +
                                    '<AddressLine>' +
                                        '<Label>Street (Type)</Label>' +
                                        '<Line>Street</Line>' +
                                    '</AddressLine>' +
                                    '<AddressLine>' +
                                        '<Label>Street (Type Suffix)</Label>' +
                                        '<Line />' +
                                    '</AddressLine>' +
                                    '<AddressLine>' +
                                        '<Label>Locality</Label>' +
                                        '<Line>SYDNEY</Line>' +
                                    '</AddressLine>' +
                                    '<AddressLine>' +
                                        '<Label>State code</Label>' +
                                        '<Line>NSW</Line>' +
                                    '</AddressLine>' +
                                    '<AddressLine>' +
                                        '<Label>Postcode</Label>' +
                                        '<Line>2000</Line>' +
                                    '</AddressLine>' +
                                    '<AddressLine LineContent="Ancillary">' +
                                        '<Label>DPID/DID</Label>' +
                                        '<Line>57700812</Line>' +
                                    '</AddressLine>' +
                                    '<AddressLine>' +
                                        '<Label>All postal delivery types</Label>' +
                                        '<Line />' +
                                    '</AddressLine>' +
                                '</QAAddress>';*/
        
        //System.debug('@@@doGetAddress::resBody::'+response);
        
        xmlDataMap = new Map<String, String>();
        
        QASSOAPAddressValidation.parserXML(response);
        mappedData = JSON.serialize(xmlDataMap);
        
        System.debug('@@@mappeData::'+mappedData);
        
        return mappedData;
    }
    
    public static String getSoapResponse(String action, String input){
        Http h = new Http();
        HttpRequest req = new HttpRequest();
        req = setSoapRequest(action, input);
        
        HttpResponse res = new HttpResponse();
        
        try {
            system.debug('###'+req);
            res = h.send(req); 
            
            if (Test.isRunningTest()) {
                //throw new mexException();
            }
        } catch (Exception ex) {
            //UTIL_LoggingService.logHandledException(ex, ORG_ID, APP_NAME, CLASS_NAME, 'getSoapResponse', null, LOGGING_LEVEL);
            System.debug('@@@Error::getSoapResponse:'+ex.getMessage());
        }
        
        String body;
        //error handling
        if (res.getStatusCode() != 200) {
            System.debug('The status code returned was not expected: ' +
                res.getStatusCode() + ' ' + res.getStatus());
        } else {
            System.debug('@@@Response::'+res.getBody());
            body = res.getBody();
        }
         
        return body;
    }

    public static HttpRequest setSoapRequest(String action, String input) {
        String soapAction = 'http://www.qas.com/OnDemand-2011-03/' + action;
        String body = setSoapBody(action, input);
        
        HttpRequest req = new HttpRequest();
        req.setMethod('POST');
        //req.setEndpoint('callout:QASSoapSetting');
        req.setEndpoint('https://ws.ondemand.qas.com/ProOnDemand/V3/ProOnDemandService.asmx?WSDL=');
        req.setHeader('SOAPAction', soapAction);
        req.setHeader('Content-Type', 'text/xml');
        //req.setHeader('Auth-token', '{!$Credential.Password}');
        req.setHeader('Auth-token', '2e9ec27d-dead-400f-af70-f9b153d10da9');
        req.setBody(body);
        
        return req;
    }
    
    public static String setSoapBody(String action, String input) {
        //QAS_Setting__c qasSetting = getQASAuthToken();
        
        String actionRequest = '';
        Integer maxNumResults = 10 ;//Integer.valueOf(qasSetting.MaxReturnedResult__c);
        String soapAction = 'http://www.qas.com/OnDemand-2011-03/' + action;
        
        if (action.equalsIgnoreCase('DoSearch')) {
            actionRequest = '<web:QASearch>'
                            + '<web:Country>AUS</web:Country>'
                            + '<web:Engine Threshold="'+ maxNumResults +'">Intuitive</web:Engine>'
                            + '<web:Search>' + input + '</web:Search>'
                            + '<web:Layout>'+system.label.QAS_LAyout+'</web:Layout>'
                            + '</web:QASearch>';
        }  else if (action.equalsIgnoreCase('DoGetAddress')) {
            actionRequest = '<web:QAGetAddress>'
                            + '<web:Moniker>' + input + '</web:Moniker>'
                            + '<web:Layout>'+system.label.QAS_LAyout+'</web:Layout>'
                            + '</web:QAGetAddress>';
        } else if (action.equalsIgnoreCase('DoCanSearch')) {
            actionRequest = '<web:QACanSearch>'
                            + '<web:Country>AUS</web:Country>'
                            + '<web:Engine>Intuitive</web:Engine>'
                            + '<web:Layout>'+system.label.QAS_LAyout+'</web:Layout>'
                            + '</web:QACanSearch>';
        }
        
        String body = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:web="http://www.qas.com/OnDemand-2011-03">'
                        + '<soapenv:Header/>'
                        + '<soapenv:Body>'
                        + actionRequest
                        + '</soapenv:Body>'
                        + '</soapenv:Envelope>';
        
        return body;
    }
    
    public static void parserXML(String toParse){
        system.debug('@@@parsing::'+toParse);
        DOM.Document doc = new DOM.Document();
        
        try{
            doc.load(toParse);
            DOM.XMLNode root = doc.getRootElement();
            iterateThroughXML(root);
            
            if (Test.isRunningTest()) {
                //throw new mexException();
            }
        }catch(Exception ex){
            //UTIL_LoggingService.logHandledException(ex, ORG_ID, APP_NAME, CLASS_NAME, 'parserXML', null, LOGGING_LEVEL);
            system.debug('@@@ERROR::'+ex.getMessage());
       } 
    }
    
    public static String label, line;
    
    public static void iterateThroughXML(DOM.XMLNode node){
        if (node.getNodeType() == DOM.XMLNodeType.ELEMENT){
            if (node.getName().equalsIgnoreCase('PicklistEntry')) {
                if (xmlDataMap != null && !xmlDataMap.isEmpty()) {
                    xmlDataMapList.add(xmlDataMap);
                }
                
                xmlDataMap = new Map<String,String>(); 
            } else if (node.getName().equalsIgnoreCase('AddressLine')) {
                if (String.isNotBlank(label)) {
                    xmlDataMap.put(label, line);
                }
                
                label = '';
                line = '';
            } else if (node.getName().equalsIgnoreCase('Label')) {
                label = String.isBlank(node.getText()) ? 'Site/Building Name' : node.getText().trim();
            } else if (node.getName().equalsIgnoreCase('Line')) {
                line = String.isBlank(node.getText()) ? '' : node.getText().trim();
            } else if(node.getName().equalsIgnoreCase('Moniker')){
                xmlDataMap.put('Id', node.getText().trim());
            } else if(node.getName().equalsIgnoreCase('Picklist')){
                xmlDataMap.put('Name', node.getText().trim());
            } else if(node.getName().equalsIgnoreCase('Postcode')){
                xmlDataMap.put('PostCode', node.getText().trim());
            } else if(node.getName().equalsIgnoreCase('IsOk')){
                xmlDataMap.put('CanSearch', node.getText().trim());
            } /*else if (node.getName() != null) {
                xmlDataMap.put(node.getName().trim(), node.getText() == null ? '' : node.getText().trim());
            }*/
            // Added by DinoBrinas - June 04 2019 - Bug MP2-12825, MP2-12690
            // Need to populate the xmlDataMapList variable on the last record in the child node
            if(node.getName().equalsIgnoreCase('VerificationFlags')){
                if (xmlDataMap != null && !xmlDataMap.isEmpty()) {
                    xmlDataMapList.add(xmlDataMap);
                }
            }

            for (Dom.XMLNode child: node.getChildElements()) {                
                iterateThroughXML(child);
            }
        }
    }
    
    @RemoteAction
    webservice static List<String> getStreetTypePicklistValues() {
        List<String> pickListValuesList = new List<String>();
        Schema.DescribeFieldResult fieldResult = clcommon__Address__c.Street_Type__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for (Schema.PicklistEntry pickListVal : ple) {
            pickListValuesList.add(pickListVal.getLabel());
        }
        return pickListValuesList;
    }
    @RemoteAction
    webservice static List<String> getCountryPicklistValues() {
        List<String> pickListValuesList = new List<String>();
        Schema.DescribeFieldResult fieldResult = clcommon__Address__c.Country_Picklist__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for (Schema.PicklistEntry pickListVal : ple) {
            pickListValuesList.add(pickListVal.getLabel());
        }
        return pickListValuesList;
    }
}