/************************************************************************************************************************************************
 * Authour      : CLS
 * Created Date : 14-11-2019
 * Description  : Vechicle callout class
************************************************************************************************************************************************/
global class VehicleSearchService{
    
    webservice static String makeCalloutToThirdParty(String collateralId){
        try{
            List<clcommon__Collateral__c> collDetails = [SELECT id,
                                                                name,
                                                                clcommon__Year__c,
                                                                modelTypeCode__c,
                                                                Manufacturer_Code__c,
                                                                Family_Code__c,
                                                                VariantName__c,
                                                                Series_Code__c,
                                                                Call_Type__c
                                                           FROM clcommon__Collateral__c 
                                                          WHERE id = :collateralId
                                                        ];

            if(collDetails == null || collDetails.size() == 0){
                return 'No collateral found!';
            }
            
            clcommon__Collateral__c collDtls = collDetails.get(0);
            
            if('Glasses'.equalsIgnoreCase(collDtls.Call_Type__c)){
                return makeCalloutForGlassesGuide(collateralId);
            }
            else if('MotorWeb'.equalsIgnoreCase(collDtls.Call_Type__c)){
                return makeCalloutForMotorWeb(collateralId);
            }
            else{
                return System.label.TAG_SUCCESS;    
            }            
        }
        catch(Exception e){
            return 'Exception: ' + e.getMessage();
        }
    }

    webservice static String makeCalloutForGlassesGuide(String collateralId){
        try{
            List<clcommon__Collateral__c> collDetails = [SELECT id,
                                                                name,
                                                                clcommon__Year__c,
                                                                modelTypeCode__c,
                                                                Manufacturer_Code__c,
                                                                Family_Code__c,
                                                                VariantName__c,
                                                                Series_Code__c
                                                           FROM clcommon__Collateral__c 
                                                          WHERE id = :collateralId
                                                         ];

            if(collDetails == null || collDetails.size() == 0){
                return 'No collateral found!';
            }
                                            
            Vehicle_Service__c  vehicleConfigData = Vehicle_Service__c.getOrgDefaults();
            String endPointUrl = vehicleConfigData.Base_URL_Glasses_Guide__c;

            String yearValue = collDetails.get(0).clcommon__Year__c;
            String modelTypeCode = collDetails.get(0).modelTypeCode__c;
            String manufactureCode = collDetails.get(0).Manufacturer_Code__c;
            String familyCode = collDetails.get(0).Family_Code__c; 
            String variantName = collDetails.get(0).VariantName__c;
            String seriesCode = collDetails.get(0).Series_Code__c;

            if(!String.isBlank(yearValue)){
                endPointUrl = endPointUrl + 'yearCreate=' + yearValue;
            }
            else{
                endPointUrl = endPointUrl + 'yearCreate = ';
            }

            if(!String.isBlank(modelTypeCode)){
                endPointUrl = endPointUrl + '&modelCode=' + modelTypeCode;
            }
            else{
                endPointUrl = endPointUrl + '&modelCode=';
            }

            if(!String.isBlank(manufactureCode)){
                endPointUrl = endPointUrl + '&manufacturerCode=' + manufactureCode;
            }
            else{
                endPointUrl = endPointUrl + '&manufacturerCode=';
            }

            if(!String.isBlank(familyCode)){
                endPointUrl = endPointUrl + '&familyCode=' + familyCode;
            }
            else{
                endPointUrl = endPointUrl + '&familyCode=';
            }
        
            if(!String.isBlank(variantName)){
                endPointUrl = endPointUrl + '&variantName=' + variantName;
            }
            else{
                endPointUrl = endPointUrl + '&variantName=';
            }
        
            if(!String.isBlank(seriesCode)){
                endPointUrl = endPointUrl + '&seriesCode=' + seriesCode;
            }
            else{
                endPointUrl = endPointUrl + '&seriesCode=';
            }
            
            if(vehicleConfigData.go_to_advsearch__c != null){
                endPointUrl=endPointUrl + '&goToAdvSearch=' + vehicleConfigData.go_to_advsearch__c;
            }
            else{
                endPointUrl=endPointUrl + '&goToAdvSearch=';
            }
        
            System.debug(':::endPointUrlendPointUrl::endPointUrl::is::' + endPointUrl);
            
            //Creating request
            HttpRequest req = new HttpRequest();
            req.setMethod('GET');
            req.setEndpoint(endPointUrl.trim().replaceAll(' ', '+'));        
            req.setHeader('client_id', vehicleConfigData.client_id__c);  
            req.setHeader('client_secret', vehicleConfigData.secret_id__c);  
            req.setHeader('X-InitialSystem', vehicleConfigData.X_InitialSystem__c);          
            req.setTimeout(120000);     
            system.debug('*** req is '  +  req);
            
            HttpResponse res = new HttpResponse();
            Http objhttp = new Http();
            res = objhttp.send(req);
            String response = res.getBody();
            Integer statusCode = res.getStatusCode();
            
            if(statusCode == 200){
                VehicleSearchResponseParser.VehicleParser parseJsonData = (VehicleSearchResponseParser.VehicleParser) JSON.deserializeStrict(response, VehicleSearchResponseParser.VehicleParser.class);
                //createAttachment('Request', 'GlassesGuide', String.valueOf(req), collateralId);
                //createAttachment('Response', 'GlassesGuide',response, collateralId);
                //System.debug(':::parseJsonDataparseJsonData::::' + parseJsonData);

                /*List<Vehicle_Model_Details__c> listExistingVMC = [SELECT id,
                                                                         name
                                                                    FROM Vehicle_Model_Details__c
                                                                   WHERE Collateral__c = :collateralId
                                                                     AND Is_Selected__c = false
                                                                 ];

                if(listExistingVMC.size() > 0){
                    delete listExistingVMC;
                }*/
                
                List<clcommon__Collateral__c> listCollateral = new List<clcommon__Collateral__c>();
                if(modelTypeCode == 'B'){
                    for(VehicleSearchResponseParser.Vehicles veh : parseJsonData.Bike){
                        VehicleSearchResponseParser.Vehicle vehicle = veh.bike;
                        listCollateral.add(VehicleSearchResponseParser.getVehicleInstanceWithMappings(vehicle, collateralId));
                    }
                }   
                else if(modelTypeCode == 'C'){
                    for(VehicleSearchResponseParser.Vehicles veh : parseJsonData.Caravan){
                        VehicleSearchResponseParser.Vehicle vehicle = veh.caravan;
                        listCollateral.add(VehicleSearchResponseParser.getVehicleInstanceWithMappings(vehicle, collateralId));
                    }
                }   
                else{
                    for(VehicleSearchResponseParser.Vehicles veh : parseJsonData.vehicles){
                        VehicleSearchResponseParser.Vehicle vehicle = veh.vehicle;
                        listCollateral.add(VehicleSearchResponseParser.getVehicleInstanceWithMappings(vehicle, collateralId));
                    }
                }

                if(listCollateral.size() > 0){
                    for(clcommon__Collateral__c coll : listCollateral){
                        coll.Call_Type__c = 'Glasses';
                    }
                    update listCollateral;
                }

                return System.label.TAG_SUCCESS;
            }
            else{
                if(statusCode == 403){   
                    return statusCode + ': Server error';
                }
                else{
                    return statusCode + ': Internal server error';
                }
            }
        }
        catch(Exception e){
            return 'Exception: ' + e.getMessage();
        }
    }

    webservice static String makeCalloutForMotorWeb(String collateralId){
        
        try{
            List<clcommon__Collateral__c> collDetails = [SELECT id,
                                                                name,
                                                                Registration_Number__c,
                                                                clcommon__Registration_State__c
                                                           FROM clcommon__Collateral__c 
                                                          WHERE id = :collateralId
                                                         ];

            if(collDetails == null || collDetails.size() == 0){
                return 'No collateral found!';
            }
                                            
            Vehicle_Service__c  vehicleConfigData = Vehicle_Service__c.getOrgDefaults();
            String endPointUrl = vehicleConfigData.Base_URL_MotorWeb__c;

            String jurisdiction = collDetails.get(0).clcommon__Registration_State__c;
            String plate = collDetails.get(0).Registration_Number__c;

            if(!String.isBlank(jurisdiction)){
                endPointUrl = endPointUrl + 'jurisdiction=' + jurisdiction;
            }
            else{
                endPointUrl = endPointUrl + 'jurisdiction= ';
            }

            if(!String.isBlank(plate)){
                endPointUrl = endPointUrl + '&plate=' + plate;
            }
            else{
                endPointUrl = endPointUrl + '&plate=';
            }

            System.debug(':::endPointUrlendPointUrl::endPointUrl::is::' + endPointUrl);
            
            //Creating request
            HttpRequest req = new HttpRequest();
            req.setMethod('GET');
            req.setEndpoint(endPointUrl.trim().replaceAll(' ', '+'));        
            req.setHeader('client_id', vehicleConfigData.client_id__c);  
            req.setHeader('client_secret', vehicleConfigData.secret_id__c);  
            req.setHeader('X-InitialSystem', vehicleConfigData.X_InitialSystem__c);          
            req.setTimeout(120000);     
            system.debug('*** req is '  +  req);
            
            HttpResponse res = new HttpResponse();
            Http objhttp = new Http();
            res = objhttp.send(req);
            String response = res.getBody();
            Integer statusCode = res.getStatusCode();
            
            if(statusCode == 201){
                VehicleSearchResponseParser.VehicleParser parseJsonData = (VehicleSearchResponseParser.VehicleParser) JSON.deserializeStrict(response, VehicleSearchResponseParser.VehicleParser.class);
                //createAttachment('Request', 'MotorWeb', String.valueOf(req), collateralId);
                //createAttachment('Response', 'MotorWeb', response, collateralId);
                //System.debug(':::parseJsonDataparseJsonData::::' + parseJsonData.vehicles);
                
                //clcommon__Collateral__c coll = collDetails.get(0);
                
                //List<Vehicle_Model_Details__c> listVMC = new List<Vehicle_Model_Details__c>();
                List<clcommon__Collateral__c> listCollateral = new List<clcommon__Collateral__c>();
                for(VehicleSearchResponseParser.Vehicles veh : parseJsonData.vehicles){
                    VehicleSearchResponseParser.Vehicle vehicle = veh.vehicle;
                    listCollateral.add(VehicleSearchResponseParser.getVehicleInstanceWithMappings(vehicle, collateralId));
                    //vmd.Is_Selected__c = true;
                    //listVMC.add(vmd);
                }

                if(listCollateral.size() > 0){
                    for(clcommon__Collateral__c coll : listCollateral){
                        coll.Call_Type__c = 'MotorWeb';
                    }
                    update listCollateral;
                    //coll.Selected_Collateral__c = listVMC.get(0).id;
                    //update coll;
                }

                return System.label.TAG_SUCCESS;
            }
            else{
                if(statusCode == 403){   
                    return statusCode + ': Server error, ' + response;
                }
                else{
                    return statusCode + ': Internal server error, ' + response;
                }
            }
        }
        catch(Exception e){
            return 'Exception: ' + e.getMessage();
        }
    }
  
    /*public static void createAttachment(String callType, String integrationName, String fileBody, ID parent){
        try{
            Attachment attachment = new Attachment();
            attachment.Body = Blob.valueOf(fileBody);
            attachment.Name = callType + '_' + integrationName + '_' + CommonUtility.getDatetimeValue('');
            attachment.ParentId = parent;
            insert attachment;
        }
        catch(Exception e){
            throw new ints.IntegrationException('[ScoreSeekerAPIAdapter.createAttachment]Exception: ' +  e.getMessage());
        }
    }*/
}