public class RACVF_ContractDocumentViewerController {
    
    @AuraEnabled
    public static List<RACVF_ContentDocumentModel> getContentDocument(Id recordId)
    {
  
        String strContractNumber = getContractNumber(recordId);
        // [Select CRN_Number__c, Application__c FROM Case WHERE Id=:caseId].CRN_Number__c;
        
        List<Contract_Document__c> contractDocList = [Select Id, Name From Contract_Document__c WHERE Name=:strContractNumber LIMIT 1];
        
        Set<Id> contractDocId = new Set<Id>();
        for(Contract_Document__c contratDoc : contractDocList)
        {
            contractDocId.add(contratDoc.Id);
        }
        
        
        List<ContentDocumentLink> contentDocLinks = [SELECT Id, ContentDocument.Id, ContentDocument.title, ContentDocument.FileType, ContentDocument.ContentSize, ContentDocument.CreatedDate, ContentDocument.CreatedBy.Name, LinkedEntityId 
                                                     FROM ContentDocumentLink
                                                     WHERE LinkedEntityId IN:contractDocId];
        
        
        List<RACVF_ContentDocumentModel> contentDocumentModelList = setContentDocument(contentDocLinks);
        return contentDocumentModelList;
    }
    
    private static String getContractNumber(Id recordId)
    {
        String sObjName = recordId.getSObjectType().getDescribe().getName();
        String cols;
        
        if(sObjName == 'Deposit_Contract__c')
            cols = 'Contract_Number__c';
        else
            cols ='CRN_Number__c';
        
        String query = 'SELECT '+cols+' FROM '+sObjName+' WHERE Id=:recordId';
        System.debug('query----->'+query);
        List<sObject> records = Database.query(query);
        
        if(sObjName == 'Deposit_Contract__c')
        {
            List<Deposit_Contract__c> decopitContracts = (List<Deposit_Contract__c>)records;
            return decopitContracts[0].Contract_Number__c;
            
        }
        else
        {
            List<Case> cases = (List<Case>)records;
            return cases[0].CRN_Number__c;
        }
        
    }
    
    public static List<RACVF_ContentDocumentModel> setContentDocument(List<ContentDocumentLink> contentDocLinks)
    {
        List<RACVF_ContentDocumentModel> contentDocumentModelList = new List<RACVF_ContentDocumentModel>();
        
        
        
        if(contentDocLinks!=null && contentDocLinks.size()> 0)
        {
            for(ContentDocumentLink contentDocLink: contentDocLinks)
            {
                
                RACVF_ContentDocumentModel model = new RACVF_ContentDocumentModel();
                model.RecordId = contentDocLink.ContentDocument.Id;
                model.ContentDocLinkId = contentDocLink.Id;
                model.Title = contentDocLink.ContentDocument.title;
                model.CreatedBy = contentDocLink.ContentDocument.CreatedBy.Name;
                model.CreatedDate = formatDate(contentDocLink.ContentDocument.CreatedDate);
                model.LinkedEntityId = contentDocLink.LinkedEntityId;  
                contentDocumentModelList.add(model);
               
            }
           
        }
        // contentDocumentModelList.sort();
        return contentDocumentModelList;
    }
    
    // Helper Method to format date
    public static String formatDate(DateTime formateDate)
    {
        String dateStr;
        if(formateDate!=null)
        {
            dateStr = formateDate.format('dd/MM/yyyy hh:mm:ss');
            //DateTime dt = DateTime.newInstance(formateDate.year(), formateDate.month(),formateDate.day());
            //dateStr = dt.format('dd/MM/yyyy');
        }
        return dateStr;
    }
}