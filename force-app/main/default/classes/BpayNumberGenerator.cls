//*****************************************************************//
//Description: API to generate BPAY/CRN number that is later used for banking.
//Date: Septemper, 2019
//*****************************************************************//

global class  BpayNumberGenerator{
    //*****weight that is to be multiplied with each digit in sequence**********
    
    webservice Static String generateBpayNumber(String autoNumber, String bankNumber, Integer modNumber){
    mfiflexUtil.VOLog voLogInstance = mfiflexUtil.VOLog.getInstance('Genesis');
        try{
            String weightCS = Banking_Config__c.getInstance().BPAY_Weights_Finance_Ltd__c;
            List<String> weight = new List<String>();
            try{
                Integer.valueOf(autoNumber);
                weight  = String.valueOf(weightCS).split(',');
            }catch(Exception e){
                return 'INPUT_PARAMETERS_ARE_NOT_VALID_NUMBERS:'+autoNumber;
            }
            
            String generatedBpayNum = '';
            String finalBpayNumber = '';
            String formattedAutoNum = '';
            //*******fill the missing number of digits with 0*****
            formattedAutoNum =autoNumber.trim().leftpad(9,'0');
            //*****evaluate only for 9 digit input String******
            if(formattedAutoNum.length()==9){
                Integer weightedScore=0;
                bankNumber = '';
                //Integer startingNumAdd = 200100000 + Integer.valueOf(formattedAutoNum);
                generatedBpayNum = bankNumber + String.valueOf(formattedAutoNum);
                for(integer i=0;i<generatedBpayNum.length();i++){
                    Integer tempScore = Integer.valueOf(generatedBpayNum.subString(i,i+1))*Integer.valueOf(weight.get(i));
                    
                    weightedScore+=(Math.mod(tempScore,modNumber) + (tempScore/modNumber));
                }
                Integer mod10=math.mod(weightedScore,modNumber);//*****mod10
                Integer checksumDigit;
                if(mod10 == 0)
                checksumDigit = 0;
                else
                checksumDigit = modNumber-mod10;
                
                finalBpayNumber = generatedBpayNum + String.valueOf(checksumDigit);
                
            }else{
                return 'INPUT_STRING_LENGTH_IS_INVALID :'+generatedBpayNum.length();
            }
            return finalBpayNumber ;
        }catch(Exception e){
            voLogInstance.logException(1001, '[BpayNumberGenerator.generateBpayNumber] Error: ' + e.getMessage() + ' (at line: ' + e.getLineNumber() + ')', e);
            voLogInstance.commitToDb();
            return 'Exception at Line: '+e.getLineNumber()+'Exception Msg: '+e.getMessage();
        }
    }
}