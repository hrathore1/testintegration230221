global without sharing class AppTrackingPortalAPI implements clcommon.PortalCustomRemoteAPI1 {
    global clcommon.Response invokeAction(String componentStrName, String[] disclosureNames, Map<String, Object> params) {           
        // Init parameter.
        SavePoint dbSavePoint = Database.setSavepoint();
        clcommon.Response response = new clcommon.Response();
        Map<String,Object> fields = new Map<String,Object>();
        List<clcommon.PortalCustomRemoteActionRecord> respList = new List<clcommon.PortalCustomRemoteActionRecord>();
        String applicationId = (String)params.get('applicationId');
        String apiMsg = System.Label.CONSUMER_PORTAL_TRACKING_MSG_1;
        Boolean showDetails = false;
        Boolean uploadDocuments = true;
        String crnNumber = '';
        List<genesis__Applications__c> apps = [SELECT id,
                                                      CRN_Number__c, 
                                                      Portal_Decision__c,
                                                      Franchise__c
                                                 FROM genesis__Applications__c 
                                                WHERE id =: applicationId];
        /*String defaultAccOwnerStr = genesis__Org_Parameters__c.getOrgDefaults().Default_Owner_for_Portal_Accounts__c != null ?
                                 genesis__Org_Parameters__c.getOrgDefaults().Default_Owner_for_Portal_Accounts__c :
                                 'RACV Financial';
        List<User> defaultAdminUser = [select id from User where name =: defaultAccOwnerStr];
        */
        Boolean isValidApp = (apps != null && apps.size() > 0) ? true : false ; 
        if(isValidApp){
            if(apps[0].CRN_Number__c != null)
            crnNumber = apps[0].CRN_Number__c;
            try {
                genesis__Applications__c app = apps[0];
                apiMsg += ' '+ app.CRN_Number__c + '.';
                app.Generate_Pricing_from_Consumer_Portal__c = false;
                // Run rules of type Consumer Portal Decline 
                Boolean portalDeclined = false;
                Boolean portalReferred = false;
                List<genesis__Checklist__c> checkList1 = runRules(app, 'Consumer Portal Decline');
                if(checkList1 != null){
                    for(genesis__Checklist__c result : checkList1){
                        if(result.genesis__Result__c){
                            portalDeclined = true;
                            break;
                        }
                    }
                }
                //if(apps[0].Portal_Decision__c != 'ScoreSeeker Failed'){
                if(portalDeclined && apps[0].Portal_Decision__c != 'ScoreSeeker Failed'){
                    app.Portal_Decision__c = 'Portal Declined';
                    app.genesis__Status__c = 'Declined';
                    update app;
                    uploadDocuments = false;
                    fields.put('refNumber', crnNumber);
                    fields.put('showDetails', false);
                    fields.put('message', apiMsg);
                    fields.put('franchise',apps[0].Franchise__c);
                    fields.put('uploadDocuments', false);
                    fields.put('callNextAPI', String.valueOf(false));
                    respList.add(new clcommon.PortalCustomRemoteActionRecord('response', fields));
                    response = clcommon.PortalActions.getCustomRemoteActionResponse(respList);
                    return response;
                }
                else{
                    // Run rules of type Consumer Portal Approve
                    apiMsg = System.Label.CONSUMER_PORTAL_TRACKING_MSG_2;
                    List<genesis__Checklist__c> checkList2 = runRules(app, 'Consumer Portal Approve');
                    if(checkList2 != null){
                        for(genesis__Checklist__c result : checkList2){
                            if(!result.genesis__Result__c){
                                portalReferred = true;
                                break;
                            }
                        }
                    }
                    if(portalReferred){
                       app.Portal_Decision__c = 'Portal Referred';
                    }
                    else{
                       app.Portal_Decision__c = 'Portal Approved';
                    }
                    update app;
                    
                    System.enqueueJob(new OnAppCreateActionsQueueable(applicationId));
                }
                //}
                //fields.put('submittedDateTime', app.Application_Submitted_On__c);
                //fields.put('submittedDateTime', String.valueOf(app.Application_Submitted_On__c));
                
            } 
            catch (Exception e) {
                Database.rollback(dbSavePoint);
                response.status = clcommon.Constants.API_EXCEPTION;
                response.errorMessage = 'Something went wrong.';
                //return response;               
            }
        }else{
            return getErrorResponse('Invalid Application Id');
        }   
        fields.put('refNumber', crnNumber);
        fields.put('showDetails', showDetails);
        fields.put('message', apiMsg);
        fields.put('uploadDocuments', uploadDocuments);
        fields.put('callNextAPI',String.valueOf(true));
        system.debug(fields);
        
        respList.add(new clcommon.PortalCustomRemoteActionRecord('response', fields));
        response = clcommon.PortalActions.getCustomRemoteActionResponse(respList);
        return response;
    }
    private clcommon.Response getErrorResponse(String errorMessage) {
        clcommon.Response response = new clcommon.Response();
        response.status = clcommon.Constants.ERROR;
        response.errorMessage = errorMessage;
        response.errorCode = clcommon.Constants.API_EXCEPTION;
        return response;
    }
    public List<genesis__Checklist__c> runRules(genesis__Applications__c app, String ruleClassif){
        Map<Id, genesis__Rule__c> mapRules = new Map<Id,genesis__Rule__c>([SELECT Id,
                                                                                  Name
                                                                             FROM genesis__Rule__C
                                                                            WHERE genesis__Enabled__c = true
                                                                              AND Rule_Classification__c =: ruleClassif
                                                                         ]);
        List<genesis__Checklist__c> checkList;                                                             
        if(mapRules != null && mapRules.size() > 0){
            mfiflexUtil.ExecutionContext ec = mfiflexUtil.ExecutionContext.getExecContext();
            mfiflexUtil.ObjectCache sObjOC = ec.getObject('tempRuleCache');
        
            if(sObjOC != null){
                ec.deleteObject('tempRuleCache');
            }
            
            SObject appObj = (SObject) app; 
            //delete [Select id FROM genesis__Checklist__c WHERE genesis__Application__c =: app.id];       
            checkList = genesis.RulesAPI.evaluateRules(app, mapRules.values(), true, true);
        }
        return checkList;
    } 
}