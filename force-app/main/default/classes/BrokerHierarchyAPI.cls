/**
 * Company: CloudKaptan Consultancy Services Pvt. Ltd.
 * Description: The API serves the following purposes - 
                 * Fill the originator lookup when application is created from consumer or broker portal.
                 * Insert appropriate parties in the application according to the originator lookup and from where the application is created.
                 * Fill the Parent and Grandparent lookup in application.
 * Developer : Atanu Saha.
 * Created Date : 24/11/2020
 * Last Modified By : Atanu Saha.
 * Last Modified Date : 09/12/2020
 */
 
global class BrokerHierarchyAPI
{
    //Party type
    public static final string PARTY_TYPE_ORIGINATOR = 'ORIGINATOR';
    public static final string PARTY_TYPE_BROKER = 'BROKER';
    
    //Community Profile Name
    public static final string BROKER_COMMUNITY_PROFILE_NAME = 'BrokerCommunityPlus';

    
    //Roles
    public static final string PARENT_RELATION = 'Parent';
    public static final string GRANDPARENT_RELATION = 'Grandparent';
    public static final string ORIGINATOR_TYPE_CHILD_BROKER = 'Child Broker';
    public static final string CHILD_RELATION = 'Child';
    public static final string BROKER_RELATION = 'Broker';
    public static final string BORROWER_RELATION = 'Borrower';
    
    //Loan Channel
    public static final string LOAN_CHANNEL_ONLINE = 'Online';
    public static final string LOAN_CHANNEL_BROKER = 'Broker';
    
    //Constant messages used
    public static final string APPLICATION_NULL = 'Application Instance passed is null';
    public static final string ORIGINATOR_FIELD_NULL = 'Originator field in Application is null';
    public static final string PARTY_TYPE_NULL = 'Party Type not found';
    public static final string ACCOUNT_ID_NULL = 'AccountId passed in blank';
    public static final string RELATION_NULL = 'Relation to search passed is blank';
    public static final string PROFILE_NOT_FOUND = 'Profile of the current user not found';
    public static final string CHILD_BROKER_ACCOUNT_NOT_FOUND = 'Child Broker Account not found';
    public static final string LOAN_CHANNEL_NOT_FOUND = 'Loan Channel not filled';
    public static final string FRANCHISE_NOT_FOUND = 'Franchise not filled';
    public static final string ORIGINATOR_ACCOUNT_NOT_FOUND = 'No Originator Account found';
    public static final string ORIGINATOR_LOOKUP_INCORRECT_PARAMETER = 'Incorrect parameter passed';
    public static final string BROKER_BORROWER_ROLE_NOT_FOUND = 'Broker and Borrower Role not found';
       
    //method for finding the and assigning broker hierarchy  
    webService static void processBrokerHierarchy(genesis__Applications__c app)
    {
        mfiflexUtil.VOLog voLogInstance = mfiflexUtil.VOLog.getInstance('Genesis');
        Savepoint sp = Database.setSavepoint();

        try
        {           
            if(app == null)
            {
                throw new CustomException(APPLICATION_NULL);
            }            
            if(app.Loan_Channel__c == null)
            {
                throw new CustomException(LOAN_CHANNEL_NOT_FOUND);
            }
            if(app.Franchise__c == null)
            {
                throw new CustomException(FRANCHISE_NOT_FOUND);
            }
            
            //getting the current user profile name for checking if it is called from protal or not 
            List<Profile> currentUserProfile = [Select Name from Profile where Id =: UserInfo.getProfileId()];
            String currentUserProfileName;
            if(currentUserProfile.size()>0)
            {
                currentUserProfileName = currentUserProfile[0].Name;
            }
            else
            {
                throw new CustomException(PROFILE_NOT_FOUND);
            }

            //List of parties that are to be inserted
            List<clcommon__Party__c> partiesToAddList = new List<clcommon__Party__c>();
            
            //for consumer portal loan channel is Online
            if(app.Loan_Channel__c == LOAN_CHANNEL_ONLINE)
            {
                //filling the originator lookup with the franchise account id
                app.Broker_Originator__c = fillOriginatorLookUp(app.Franchise__c,null);

                UPDATE app;
            }
            //for broker portal loan channel is broker
            else if(app.Loan_Channel__c == LOAN_CHANNEL_BROKER)
            {            
                //if class called from Broker portal then creating another fieldAgentParty and attaching it to Application.  
                if(currentUserProfileName == BROKER_COMMUNITY_PROFILE_NAME)
                { 
                    //Fetch current user account
                    User u = [SELECT Id,
                                     accountId,
                                     contactId 
                              FROM User 
                              WHERE Id=:userInfo.getUserId()];
                    
                    //filling the originator lookup with the child broker account id          
                    app.Broker_Originator__c = fillOriginatorLookUp(null,u.accountId);

                    UPDATE app;
                    
                    //create field agent party    
                    clcommon__Party__c fieldAgentParty = createParty(app.Id, u.accountId, PARTY_TYPE_BROKER, u.contactId);              
                    partiesToAddList.add(fieldAgentParty);                                               
                }
                
                //Insert child broker and borrower relation
                clcommon__Relationship__c borrowerBrokerRelation = new clcommon__Relationship__c();
                borrowerBrokerRelation.clcommon__Entity__c = app.Broker_Originator__c;
                borrowerBrokerRelation.clcommon__Related_Entity__c = app.genesis__Account__c;
                
                List<clcommon__Reciprocal_Role__c> brokerBorrowerRole = [SELECT Id,
                                                                                Name,
                                                                                clcommon__Inverse_Role__c
                                                                         FROM clcommon__Reciprocal_Role__c
                                                                         WHERE Name =: BROKER_RELATION
                                                                         AND clcommon__Inverse_Role__c =: BORROWER_RELATION
                                                                         ORDER BY CreatedDate DESC];
                
                if(brokerBorrowerRole.size()>0)
                {
                    borrowerBrokerRelation.clcommon__Relationship__c = brokerBorrowerRole[0].Id;
                }
                else
                {
                    throw new CustomException(BROKER_BORROWER_ROLE_NOT_FOUND);
                } 
                
                INSERT borrowerBrokerRelation;
            }

            //for backend if Originator is not filled
            if(app.Broker_Originator__c == null)
            {
                 throw new CustomException(ORIGINATOR_FIELD_NULL);
            }
            
            //create child broker party
            clcommon__Party__c childBrokerParty = createParty(app.Id, app.Broker_Originator__c, PARTY_TYPE_ORIGINATOR, null);
            partiesToAddList.add(childBrokerParty);
            
            if(partiesToAddList.size()>0)
            {
                INSERT partiesToAddList;
            }
            
            //getting the originator type for the child broker account
            List<Account> childBrokerAccountList = [SELECT Originator_Type__c FROM Account WHERE Id =: app.Broker_Originator__c];
            String OriginatorType;
            if(childBrokerAccountList.size()>0)
            {
                OriginatorType = childBrokerAccountList[0].Originator_Type__c;
            }
            else
            {
                throw new CustomException(CHILD_BROKER_ACCOUNT_NOT_FOUND); 
            }
            
            //continues only for child broker type originator     
            if(OriginatorType == ORIGINATOR_TYPE_CHILD_BROKER)
            {        
                //Finding the parent broker account of the child broker by calling the searchParentBroker method.
                Id parentBrokerAccount = searchParentBroker(app.Broker_Originator__c,PARENT_RELATION);
                if(parentBrokerAccount != null)
                {
                    app.Parent_Broker__c = parentBrokerAccount;
                    
                    //if parent broker found,searching for the grandparent broker.
                    Id grandParentBrokerAccount = searchParentBroker(parentBrokerAccount,GRANDPARENT_RELATION);
                    if(grandParentBrokerAccount != null)
                    {
                        app.Grandparent_Broker__c = grandParentBrokerAccount;
                    }   
                }
                
                UPDATE app; 
            }
        }
        catch(Exception e) 
        {
            Database.rollback(sp);
            voLogInstance.logException(1001, '[BrokerHierarchyAPI.processBrokerHierarchy] Error: ' + e.getMessage() + ' (at line: ' + e.getLineNumber() + ')', e);
            voLogInstance.commitToDb();
            throw new CustomException('Error Occured in BrokerHierarchyAPI.processBrokerHierarchy, Error : '+e.getMessage()+' Line : '+e.getLineNumber());
        }
    }
    
    /*method to find the accountId of the broker account. 
      brokerAccountId is the id whose related Account is to be found.
      toSearchRelation is the relation to which the related Account exists.*/
    public static Id searchParentBroker(Id brokerAccountId, String toSearchRelation)
    {
        mfiflexUtil.VOLog voLogInstance = mfiflexUtil.VOLog.getInstance('Genesis');
        try
        { 
            if(brokerAccountId == null)
            {
                throw new CustomException(ACCOUNT_ID_NULL);
            }
            if(String.isBlank(toSearchRelation))
            {
                throw new CustomException(RELATION_NULL);
            }
            
            //finding the broker accountId which is in relation to the accountId passed
            List<clcommon__Relationship__c> higherLevelAccount = [SELECT Id,
                                                                          Name,
                                                                          clcommon__Entity__c,
                                                                          clcommon__Entity_Role__c,
                                                                          clcommon__Related_Entity__c
                                                                   FROM clcommon__Relationship__c
                                                                   WHERE clcommon__Entity_Role__c =: toSearchRelation
                                                                   AND clcommon__Related_Entity__c =: brokerAccountId
                                                                   ORDER BY CreatedDate DESC];    
            
            //if account exists then returning                                                        
            if(higherLevelAccount.size()>0)
            {
                return higherLevelAccount[0].clcommon__Entity__c;
            }
            else
            {
                return null;
            }
        }
        catch(Exception e)
        {
            voLogInstance.logException(1001, '[BrokerHierarchyAPI.searchParentBroker] Error: ' + e.getMessage() + ' (at line: ' + e.getLineNumber() + ')', e);
            voLogInstance.commitToDb();
            throw new CustomException('Error Occured in BrokerHierarchyAPI.searchParentBroker, Error : '+e.getMessage()+' Line : '+e.getLineNumber());
        }
    }
    
    //method to create party
    public static clcommon__Party__c createParty(Id appId, Id accountId, String relationName, Id contactId)
    {
        mfiflexUtil.VOLog voLogInstance = mfiflexUtil.VOLog.getInstance('Genesis');
        try
        {
            //Creatting child broker account and attaching it to application
            clcommon__Party__c party = new clcommon__Party__c();
            
            party.clcommon__Account__c = accountId;
            party.genesis__Application__c = appId;
            party.clcommon__Contact__c = contactId;
            
            //getting the originator party type to assign it to the party 
            List<clcommon__Party_Type__c> originatorPartyType = [SELECT Id,
                                                                        Name
                                                                 FROM clcommon__Party_Type__c        
                                                                 WHERE Name =: relationName];   
            if(originatorPartyType.size()>0)
            {                                                        
                party.clcommon__Type__c = originatorPartyType[0].Id;   
            }
            else
            {
                throw new CustomException((relationName+' '+PARTY_TYPE_NULL));
            }   
            
            return party;
        }
        catch(Exception e)
        {
            voLogInstance.logException(1001, '[BrokerHierarchyAPI.createParty] Error: ' + e.getMessage() + ' (at line: ' + e.getLineNumber() + ')', e);
            voLogInstance.commitToDb();
            throw new CustomException('Error Occured in BrokerHierarchyAPI.createParty, Error : '+e.getMessage()+' Line : '+e.getLineNumber());
        }
    }
    
    //method to fill the Originator lookup field
    public static Id fillOriginatorLookUp(String franchiseName, Id accountId)
    {
        mfiflexUtil.VOLog voLogInstance = mfiflexUtil.VOLog.getInstance('Genesis');
        try
        {
            //if class called from consumer portal then returning the id of the franchise account
            if(String.isNotBlank(franchiseName) && accountId == null)
            { 
                List<Account> originatorAccountList = [SELECT Id,
                                                              Name
                                                       FROM Account
                                                       WHERE Name =: franchiseName
                                                       ORDER BY CreatedDate DESC];
                                                       
                if(originatorAccountList.size()>0)
                {
                    return originatorAccountList[0].Id;
                }  
                else
                {
                    throw new CustomException(ORIGINATOR_ACCOUNT_NOT_FOUND);
                }                                     
            }
            //if class called from broker portal then returning the id of child broker account related to the logged in user
            else if(String.isBlank(franchiseName) && accountId != null)
            {
                          
                List<clcommon__Relationship__c> childBrokerAccountList = [SELECT Id,
                                                                                 clcommon__Entity__c,
                                                                                 clcommon__Entity_Role__c,
                                                                                 clcommon__Related_Entity__c
                                                                          FROM clcommon__Relationship__c
                                                                          WHERE clcommon__Related_Entity__c =: accountId
                                                                          AND clcommon__Entity_Role__c =: CHILD_RELATION
                                                                          ORDER BY CreatedDate DESC];
                                                                          
                if(childBrokerAccountList.size()>0) 
                {
                    return childBrokerAccountList[0].clcommon__Entity__c;
                }
                else
                {
                    throw new CustomException(CHILD_BROKER_ACCOUNT_NOT_FOUND);
                }                                                         
            }
            else
            {
                throw new CustomException(ORIGINATOR_LOOKUP_INCORRECT_PARAMETER);
            }   
        } 
        catch(Exception e)
        {
            voLogInstance.logException(1001, '[BrokerHierarchyAPI.fillOriginatorLookUp] Error: ' + e.getMessage() + ' (at line: ' + e.getLineNumber() + ')', e);
            voLogInstance.commitToDb();
            throw new CustomException('Error Occured in BrokerHierarchyAPI.fillOriginatorLookUp, Error : '+e.getMessage()+' Line : '+e.getLineNumber());
        }   
    }
}