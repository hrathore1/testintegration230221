public class CustomFeeCalculator implements loan.CustomFeeCalculator {
    
    /*public Decimal computeFee(loan__Fee__c fee, loan__Loan_Account__c loan) {
        System.debug('==loan====='+loan);  
        System.debug('==fee====='+fee); 
        Decimal calfeeAmount = 0;
        mfiflexUtil.VOLog voLogInstance = mfiflexUtil.VOLog.getInstance('Genesis');
        String feeToConsider = System.label.PREPAID_FEE_NAMES != null ? 
                               System.label.PREPAID_FEE_NAMES :
                               '';
        try{  
            loan = [SELECT id, Application__c FROM loan__Loan_Account__c WHERE id =: loan.id];
            //fee = [SELECT id, Name FROM loan__Fee__c WHERE id =: fee.id];
            List<genesis__Applications__c> app = [SELECT ID,
                                                       (SELECT id,
                                                               Name,
                                                               clcommon__Fee_Definition__c,
                                                               clcommon__Fee_Definition__r.Name,
                                                               Final_Fee_Amount__c,
                                                               clcommon__Original_Amount__c,
                                                               clcommon__Type__c
                                                          FROM genesis__Fees__r)
                                                FROM genesis__Applications__c
                                                WHERE ID =: loan.Application__c];   
            if(app == null || app.size() == 0){
                return 0;
            }     
            if(app[0].genesis__Fees__r != null && app[0].genesis__Fees__r.size() > 0){
                for(clcommon__Fee__c thisFee: app[0].genesis__Fees__r){
                    system.debug(System.label.PREPAID_FEE_NAMES);
                    system.debug(thisFee.clcommon__Fee_Definition__r.Name);
                    //system.debug(fee.Name);
                    if(System.label.PREPAID_FEE_NAMES.contains(thisFee.clcommon__Fee_Definition__r.Name) &&
                       thisFee.clcommon__Fee_Definition__r.Name.equalsIgnoreCase(fee.Name)){
                        calfeeAmount = thisFee.Final_Fee_Amount__c;
                    }
                }
                calfeeAmount = calfeeAmount.setScale(2);
            }
            else{
                calfeeAmount = 0;
            }
        }
        catch(Exception e){
            voLogInstance.logException(1001, '[CustomFeeCalculator.computeFee] Error: ' + e.getMessage() + ' (at line: ' + e.getLineNumber() + ')', e);
            voLogInstance.commitToDb();
        }
        return calfeeAmount;
    }*/
    
    public Decimal computeFee(loan__Fee__c fee, loan__Loan_Account__c loan) {
        mfiflexUtil.VOLog voLogInstance = mfiflexUtil.VOLog.getInstance('Genesis');
        try{  
            loan = [SELECT id, Application__c FROM loan__Loan_Account__c WHERE id =: loan.id];
            fee = [SELECT id, Name FROM loan__Fee__c WHERE id =: fee.id];
            
            List<clcommon__Fee__c> applicationFee = [SELECT id,
                                                            Name,
                                                            clcommon__Fee_Definition__c,
                                                            clcommon__Fee_Definition__r.Name,
                                                            Final_Fee_Amount__c,
                                                            clcommon__Original_Amount__c,
                                                            clcommon__Type__c
                                                       FROM clcommon__Fee__c
                                                      WHERE genesis__Application__c = :loan.Application__c 
                                                        AND clcommon__Fee_Definition__r.Name = :fee.Name
                                                    ];
                                                    
            if(applicationFee != null && applicationFee.size() > 0){
                return applicationFee.get(0).Final_Fee_Amount__c;
            }
            return 0;
        }
        catch(Exception e){
            voLogInstance.logException(1001, '[CustomFeeCalculator.computeFee] Error: ' + e.getMessage() + ' (at line: ' + e.getLineNumber() + ')', e);
            voLogInstance.commitToDb();
            return 0;
        }
    }
}