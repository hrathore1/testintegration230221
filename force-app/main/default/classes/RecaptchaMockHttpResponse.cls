@isTest
global class RecaptchaMockHttpResponse implements HttpCalloutMock {

    global HTTPResponse respond(HTTPRequest req) {
        
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        res.setBody('{"score":1}');
        res.setStatusCode(200);
        return res;
    }
}