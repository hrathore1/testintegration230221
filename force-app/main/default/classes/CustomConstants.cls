public class CustomConstants{

    /*public static String APPLICATION_ID = 'applicationId'; 
    public static String BORROWER = 'BORROWER';
    public static String CO_BORROWER = 'CO-BORROWER';
    public static String COSIGNER = 'COSIGNER';
    public static String HYPOTHECATOR = 'HYPOTHECATOR';
    public static String INDIVIDUAL = 'Individual'; 
    public static String REJECTED = 'REJECTED';
    public static String NEWCHECKLISTGENERATED = 'NEW - CHECKLIST GENERATED';
    public static String BORROWERAGECHECK = 'Applicant is under legal age of 18 to enter into contract';
    public static String COBORROWERAGECHECK = 'Co-Applicant is under legal age of 18 to enter into contract';
    public static String BORROWERCITIZENSHIPCHECK = 'Applicant is neither a Citizen nor a Permanent Resident of United States.';
    public static String COBORROWERCITIZENSHIPCHECK = 'Co-Applicant is neither a Citizen nor a Permanent Resident of United States.';
    public static String INSIDER = 'Insider';
    public static String RETIRED = 'Retired';
    public static String MILITARY_ENLISTED = 'Military Enlisted';
    public static String MILITARY_OFFICER = 'Military officer';
    public static String CREDIT_DISABILITY = 'CreditDisability';
    public static String CREDIT_LIFE = 'CreditLife';
    public static String GAP = 'GAP';
    public static String INVOLUNTARY = 'Involuntary';
    public static String WAIVE_PROTECTION = 'WaiveProtection';
    public static String PORTAL_STAGE = 'portalStage';
    public static String PORTAL_SUB_STAGE = 'portalSubstage';
    public static String NEWSUBMITTED = 'NEW-SUBMITTED';
    
    //Portal constants
    public static String RETURN_MESSAGE = 'Something went wrong.';
    
    //Citizenship Country
    public static final String CITIZENSHIP_COUNTRY = 'US,USA,United States,United States of America,UNITED STATES,UNITED STATES OF AMERICA';
    
    //CL Products for Credit pull
    public static List<String> MLAProducts = new List<String>{'Consumer Secured Refinance','Consumer Secured Savings/CD Secured','Consumer Credit Card','Consumer Unsecured Loan'};
    public static List<String> nonMLAProducts = new List<String>{'Consumer Secured Purchase'};
    public static String CONSUMER_SECURED_PURCHASE = 'Consumer Secured Purchase';
    public static String CONSUMER_SECURED_REFINANCE = 'Consumer Secured Refinance';
    public static String CONSUMER_SECURED_SAVINGS_CD = 'Consumer Secured Savings/CD Secured';
    public static String CONSUMER_CREDIT_CARD = 'Consumer Credit Card';
    public static String CONSUMER_UNSECURED_LOAN = 'Consumer Unsecured Loan';*/
}