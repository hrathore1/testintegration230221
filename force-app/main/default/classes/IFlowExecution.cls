public Interface IFlowExecution{

    void execute(Id applicationId, JWF_Application_Execution_Log__c appExecutionLog);

}