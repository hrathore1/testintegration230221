/**
 * @File Name          : JWF_RunRehash.cls
 * @Description        : 
 * @Author             : chirag.gupta@q2.com
 * @Group              : 
 * @Last Modified By   : chirag.gupta@q2.com
 * @Last Modified On   : 6/9/2020, 2:35:39 PM
 * @Modification Log   : 
 * Ver       Date            Author              Modification
 * 1.0    6/4/2020   chirag.gupta@q2.com     Initial Version
**/
public class JWF_RunRehash implements IFlowExecution{
    public void execute(Id applicationId,  JWF_Application_Execution_Log__c appExecutionLog){
        try{
            List<genesis__Application_Department__c> currentAppDept = [SELECT id,
                                                                              genesis__Department__c 
                                                                         FROM genesis__Application_Department__c
                                                                        WHERE genesis__Application__c = :applicationId
                                                                          AND genesis__Status__c = 'Active'
                                                                      ];
            
            if(currentAppDept != null && currentAppDept.size() > 0){
                RehashUtility ru = new RehashUtility();
                String returnMsg = ru.triggerRehash(applicationId, currentAppDept.get(0).genesis__Department__c, null);
                appExecutionLog.Message__c += ('Rehash response:' + returnMsg);
            }
            else{
                appExecutionLog.Message__c += ('Rehash response: No active department present for application.');
            }
            
            Map<String, Object> mapExecutionLog = new Map<String, Object>();
            mapExecutionLog.put('appExecutionLog', appExecutionLog);

            ApplicationExecutionAction action = new ApplicationExecutionAction(applicationId);
            action.changeStatus(mapExecutionLog);
            action.doInvokeAction(null);                           
        }
        catch(Exception e){ExceptionLog.insertExceptionLog(e,'Rehash failed'+'---'+e.getMessage()+'---'+e.getStackTraceString(),applicationId);}          
    }
}