/**
 * @description       : 
 * @author            : chirag.gupta@q2.com
 * @group             : 
 * @last modified on  : 09-26-2020
 * @last modified by  : chirag.gupta@q2.com
 * Modifications Log 
 * Ver   Date         Author                Modification
 * 1.0   09-26-2020   chirag.gupta@q2.com   Initial Version
**/
public class CommonUtility {
    
    /*
     * This method will create an attachement under Documents folder.
     * Mandatory params: filebody
     */
    public static void createDocument(String folderName,
                                      String documentName,
                                      String fileBody,
                                      String extension,
                                      String contentType,
                                      String description){
        
        try{
            Document doc = getDocumentInstance(folderName, documentName, fileBody, extension, contentType, description, null);
            insert doc;
        }
        catch(Exception e){
            throw new CustomException('[CommonUtility.createDocument] Exception:' + e.getMessage() + ', at line: ' + e.getLineNumber());
        }
    }

    public static void createDocuments(String folderName,
                                       Map<String, Blob> mapDocs){
        
        try{
            String folderId;
            if(String.isBlank(folderName)){
                folderId = (Id) UserInfo.getUserId();
            }
            else{
                List<Folder> listFolders = [SELECT Id FROM Folder WHERE Name = :folderName];
                if (listFolders.size() > 0) {
                    folderId = listFolders.get(0).Id;
                } 
                else {
                    folderId = (Id) UserInfo.getUserId();
                }
            }

            List<Document> listDocs = new List<Document>();

            for(String strFileName : mapDocs.keySet()){
                Document doc = getDocumentInstance(folderName, strFileName, mapDocs.get(strFileName).toString(), 'TXT', '', '', folderId);
                listDocs.add(doc);
            }

            insert listDocs;
        }
        catch(Exception e){
            throw new CustomException('[CommonUtility.createDocument] Exception:' + e.getMessage() + ', at line: ' + e.getLineNumber());
        }
    }

    /***
     * getDocumentInstance description
     * @return   return description
     */
    public static Document getDocumentInstance(String folderName,
                                               String documentName,
                                               String fileBody,
                                               String extension,
                                               String contentType,
                                               String description,
                                               String folderIdVal
                                              ){
        try{
            if(String.isBlank(fileBody)){
                throw new CustomException('[CommonUtility.getDocumentInstance] File body blank');
            }
            
            String folderId = folderIdVal;
            if(String.isBlank(folderName)){
                folderId = (Id) UserInfo.getUserId();
            }
            else{
                if(folderId == null){
                    List<Folder> listFolders = [SELECT Id FROM Folder WHERE Name = :folderName];
                    if (listFolders.size() > 0) {
                        folderId = listFolders.get(0).Id;
                    } 
                    else {
                        folderId = (Id) UserInfo.getUserId();
                    }
                }
            }

            if(String.isBlank(extension)){
                extension = 'csv';
            }

            if(String.isBlank(ContentType)){
                ContentType = 'text/csv';
            }

            if(String.isBlank(description)){
                description = 'Create On:' + Datetime.now() + '; By:' + UserInfo.getUserId() + ';';
            }

            if(String.isBlank(documentName)){
                documentName = 'Doc_' + Datetime.now();
            }

            Document doc = new Document();
            doc.Name = documentName + ('BLANK_EXTN'.equals(extension) ? '' : '.' + extension);
            doc.Body = Blob.valueOf(fileBody);
            doc.ContentType = ContentType;
            doc.Type = ('BLANK_EXTN'.equals(extension) ? '' : extension);
            doc.Description = description;
            doc.folderid = folderId;

            return doc;
        }
        catch(Exception e){
            throw new CustomException('[CommonUtility.getDocumentInstance] Exception:' + e.getMessage() + ', at line: ' + e.getLineNumber());
        }
    }

    public static Date getSystemDate(){
        try{
            loan.GlobalLoanUtilFacade loanUtil = new loan.GlobalLoanUtilFacade();
            return loanUtil.getCurrentSystemDate();
        }
        catch(Exception e){
            throw new CustomException('[CommonUtility.getSystemDate] Exception:' + e.getMessage() + ', at line: ' + e.getLineNumber());
        }
    }

    /*
     * This method returns current datetime value in YYYYMMDDHHMMSS format
     */
    public static String getDatetimeValue(String format){
        try{
            if('CCYYMMDDHHMMSS'.equalsIgnoreCase(format)){
                return (getDateValue(null, 'CCYYMMDD') + getTimeValue());
            }
            return (getDateValue(null, 'YYYYMMDD') + getTimeValue());
        }
        catch(Exception e){
            throw new CustomException('[CommonUtility.getDatetimeValue] Exception:' + e.getMessage() + ', at line: ' + e.getLineNumber());
        }
    }
    
    /*
     * This method returns date value in String format.
     * Supported format: DD-MM-YYYY, YYYYMMDD, DDMMYY, DDMMYYYY
     */
    public static String getDateValue(Date dt, String dateFormat){
        try{
            if(dt == null){
                dt = getSystemDate();
            }

            String year = String.valueOf(dt.year());
            String month = dt.month() < 10 ? ('0' + dt.month()) : String.valueOf(dt.month());
            String date1 = dt.day() < 10 ? ('0' + dt.day()) : String.valueOf(dt.day());
 
            String finalDateValue = '';
            switch on dateFormat{
                when 'DD_MM_YYYY'{
                    finalDateValue = (date1 + '/' + month + '/' + year);
                }
                when 'YYYYMMDD'{
                    finalDateValue = (year + month + date1);
                }
                when 'CCYYMMDD'{
                    String ccValue = String.valueOf(Integer.valueOf(year.left(2)) + 1);
                    finalDateValue = (ccValue + year.right(2) + month + date1);
                }
                when 'DDMMYY'{
                    finalDateValue = (date1 + month + year.right(2));
                }
                when 'DDMMYYYY'{
                    finalDateValue = (date1 + month + year);
                }
                when else{
                    finalDateValue = String.valueOf(dt);
                }
            }
            System.debug('finalDateValue: ' + finalDateValue);
            return finalDateValue;
        }
        catch(Exception e){
            throw new CustomException('[CommonUtility.getDateValue_DD_MM_YYYY] Exception:' + e.getMessage() + ', at line: ' + e.getLineNumber());
        }
    }

    /***
     * getDateFromString description
     * @param  dateValue  dateValue description
     * @param  dateFormat dateFormat description
     * @return            return description
     */
    public static Date getDateFromString(String dateValue, String dateFormat){
        try{
            Date dateInstance;
            switch on dateFormat{
                when 'DDMMYYYY'{
                    dateInstance = Date.newInstance(Integer.valueOf(dateValue.substring(4, 8)), Integer.valueOf(dateValue.substring(2, 4)), Integer.valueOf(dateValue.substring(0, 2)));
                }
                when else{
                    dateInstance = Date.today();
                }
            }

            return dateInstance;
        }
        catch(Exception e){
            throw new CustomException('[CommonUtility.getDateFromString] Exception:' + e.getMessage() + ', at line: ' + e.getLineNumber());
        }
    }


    /*
     * This method returns current date value in HHMMSS format
     */
    public static String getTimeValue(){
        try{
            String hour = Datetime.now().hour() < 10 ? ('0' + Datetime.now().hour()) : String.valueOf(Datetime.now().hour());
            String minute = Datetime.now().minute() < 10 ? ('0' + Datetime.now().minute()) : String.valueOf(Datetime.now().minute());
            String second = Datetime.now().second() < 10 ? ('0' + Datetime.now().second()) : String.valueOf(Datetime.now().second());

            return (hour + minute + second);
        }
        catch(Exception e){
            throw new CustomException('[CommonUtility.getTimeValue] Exception:' + e.getMessage() + ', at line: ' + e.getLineNumber());
        }
    }
    
    /*
     * This method will evaluate rules basis the input application id and rule classification
     * Mandatory params: Application id, rule classification parameter
     * return: Map of PASSed and FAILed rules
     */
    public static Map<String, List<genesis__Checklist__c>> evaluateRules(Id appId, 
                                                                         String ruleClass){
        try{
            genesis__Applications__c app = new genesis__Applications__c(Id = appId);
            SObject appObj = (SObject) app;        

            List<genesis__Rule__c> listRules = [SELECT Id,
                                                       Name 
                                                  FROM genesis__Rule__c 
                                                 WHERE genesis__Enabled__c = true 
                                                   AND Rule_Classification__c =: ruleClass
                                               ];
            
            List<genesis__Checklist__c> checkList = genesis.RulesAPI.evaluateRules(appObj, listRules, true, false);
            Map<String, List<genesis__Checklist__c>> mapChecklist = new Map<String, List<genesis__Checklist__c>>();
            
            for(genesis__Checklist__c chkList : checkList){
                if(chkList.genesis__Result__c){
                    if(mapChecklist.containsKey('PASS')){
                        mapChecklist.get('PASS').add(chkList);
                    }
                    else{
                        mapChecklist.put('PASS', new List<genesis__Checklist__c>{chkList});
                    }
                }
                else{
                    if(mapChecklist.containsKey('FAIL')){
                        mapChecklist.get('FAIL').add(chkList);
                    }
                    else{
                        mapChecklist.put('FAIL', new List<genesis__Checklist__c>{chkList});
                    }
                }
            }
            return mapChecklist;
        }
        catch(Exception e){
            throw new CustomException('[CommonUtility.evaluateRules] Exception:' + e.getMessage() + ', at line: ' + e.getLineNumber());
        }
    }

    public class CustomException extends Exception{}
}