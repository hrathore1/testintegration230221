global class ScoreSeekerAPIAdapter1 extends ints.AbstractMockAdapter{
    public static String applicationId{get;set;}
    public static String accountId{get;set;}
    public static String partyTypeName{get;set;}
    public static String scRequest = '';
    global ScoreSeekerAPIAdapter1(){}
    
    global override ints.HttpRequestObject createRequest(Map<String, Object> scoreSeekerRequestMap, 
                                                         ints.IntegrationConfigurationDTO IntegrationConfiguration){
                                                             
        System.debug(':::::Inside the ScoreSeekerAPIAdapter:::::scoreSeekerRequestMap data is:::' + scoreSeekerRequestMap);
                                                             
        try{
            if(scoreSeekerRequestMap != null){
                ints.HttpRequestObject httpReq = new ints.HttpRequestObject();
                httpReq.endpoint = 'callout:'+ integrationConfiguration.apiNamedCredential;
                httpReq.timeout = 60000 ;
                httpReq.method = 'POST';    
                                                                     
                /************************Creating and assigning header values******************************/
                Map<String,String> headerMap = new Map<String,String>();
                headerMap.put('Content-type','text/xml');
                httpReq.headerMap = (headerMap);
                                                                     
                /****************************Constructing request body************************************/
                String requestBody = constructRequest(scoreSeekerRequestMap);
                //createAttachment('REQ', requestBody);
                if(requestBody != null && String.isNotBlank(requestBody)){
                    httpReq.body = requestBody;
                    return httpReq ;
                }
                else{
                    throw new ints.IntegrationException('Generated request is blank.');
                }
            }
            else{
                throw new ints.IntegrationException('Scoreseeker request map in blank.');
            }
        }
        catch(Exception e){
            throw new ints.IntegrationException(e.getMessage());
        }                                                         
    }
    
    global override ints.IntegrationResponseObject parseResponse(HttpResponse response, ints.ThirdPartyRequest thirdPartyRequest){
        IntegrationAPIResponseParser respParser = new IntegrationAPIResponseParser();
        ints.IntegrationResponseObject integrationRes;
        
        String resp = response.getBody();
        System.debug('=====RESPONSE======'+resp);
        //createAttachment('RES', String.valueOf(response));
        if(response.getStatusCode() != 200){
            throw new ints.IntegrationException('Status code is: ' + response.getStatusCode());
        }
        else{
            if(resp != null && String.isNotBlank(resp)){
                String parserResponse = responseXmlParseDML(resp);
                System.debug(':::parserResponse:::' + parserResponse);
                if(parserResponse != 'SUCCESS'){
                    throw new ints.IntegrationException(parserResponse);
                }
            }
            else{
                throw new ints.IntegrationException('Response Body is null or blank.');
            }
        }
        
        system.debug('::::respParserrespParser:::'+respParser);
        
        integrationRes = respParser;
        return integrationRes;
    }
    
    global Static String constructRequest(Map<String, Object> scoreSeekerRequestMap){
        String envelope1 = '';
        try{        
            /************************ Fetching otherParameters from Inegration Config ******************************/
            List<Map<String,Object>> applinPartyData = (List<Map<String,Object>>) scoreSeekerRequestMap.get(CLSIntegrationAPIConstant.applnpartydata);          
            System.debug(':::::applinPartyData applinPartyData is:::' + applinPartyData);
            Map<String,Object> applinPartyDataMap = applinPartyData.get(0);
            System.debug(':::::applinPartyDataMap data is:::' + applinPartyDataMap);
            
            applicationId = String.isNotBlank(String.valueOf(applinPartyDataMap.get(CLSIntegrationAPIConstant.applicationId))) ? String.valueOf(applinPartyDataMap.get(CLSIntegrationAPIConstant.applicationId)) : '' ;
            System.debug(':::applicationIdapplicationId:::'+applicationId);

            List<genesis__Applications__c> listApps = [SELECT id,
                                                              name,
                                                              genesis__CL_Product_Name__c 
                                                         FROM genesis__Applications__c 
                                                        WHERE id = :applicationId
                                                      ]; 
            
            String Accounttypecode = '';
            if(listApps == null || listApps.size() == 0){
                return '';
            }
            else{
                genesis__Applications__c gac = listApps.get(0);
                if('Car'.equalsIgnoreCase(gac.genesis__CL_Product_Name__c)){
                    Accounttypecode = 'AL';
                }
                else{
                    Accounttypecode = 'PF';
                }
            }

            String username = scoreSeekerRequestMap.get(CLSIntegrationAPIConstant.Username) != null ? String.valueOf(scoreSeekerRequestMap.get(CLSIntegrationAPIConstant.Username)) : '';
            String password = scoreSeekerRequestMap.get(CLSIntegrationAPIConstant.Password) != null ? String.valueOf(scoreSeekerRequestMap.get(CLSIntegrationAPIConstant.Password)) : '';
            String clientRef = scoreSeekerRequestMap.get(CLSIntegrationAPIConstant.ClientRef) != null ? String.valueOf(scoreSeekerRequestMap.get(CLSIntegrationAPIConstant.ClientRef)) : '';
            String operatorID = scoreSeekerRequestMap.get(CLSIntegrationAPIConstant.OperatorID) != null ? String.valueOf(scoreSeekerRequestMap.get(CLSIntegrationAPIConstant.OperatorID)) : '';
            String operatorName = scoreSeekerRequestMap.get(CLSIntegrationAPIConstant.OperatorName) != null ? String.valueOf(scoreSeekerRequestMap.get(CLSIntegrationAPIConstant.OperatorName)) : '';
            String permissionType = scoreSeekerRequestMap.get(CLSIntegrationAPIConstant.PermissionType) != null ? String.valueOf(scoreSeekerRequestMap.get(CLSIntegrationAPIConstant.PermissionType)) : '';
            String dataLevel = scoreSeekerRequestMap.get(CLSIntegrationAPIConstant.DataLevel) != null ? String.valueOf(scoreSeekerRequestMap.get(CLSIntegrationAPIConstant.DataLevel)) : '';
            String ScorecardId = scoreSeekerRequestMap.get(CLSIntegrationAPIConstant.ScorecardId) != null ? String.valueOf(scoreSeekerRequestMap.get(CLSIntegrationAPIConstant.ScorecardId)) : '';
            //String Accounttypecode = scoreSeekerRequestMap.get(CLSIntegrationAPIConstant.Accounttypecode) != null ? String.valueOf(scoreSeekerRequestMap.get(CLSIntegrationAPIConstant.Accounttypecode)) : '';
            String Currencycode = scoreSeekerRequestMap.get(CLSIntegrationAPIConstant.Currencycode) != null ? String.valueOf(scoreSeekerRequestMap.get(CLSIntegrationAPIConstant.Currencycode)) : '';
            //String RelationshipCode = scoreSeekerRequestMap.get(CLSIntegrationAPIConstant.RelationshipCode) != null ? String.valueOf(scoreSeekerRequestMap.get(CLSIntegrationAPIConstant.RelationshipCode)) : '';
            String Enquiryclientreference = scoreSeekerRequestMap.get(CLSIntegrationAPIConstant.Enquiryclientreference) != null ? String.valueOf(scoreSeekerRequestMap.get(CLSIntegrationAPIConstant.Enquiryclientreference)) : '';
            String EndPointURL = scoreSeekerRequestMap.get(CLSIntegrationAPIConstant.EndPointURL) != null ? String.valueOf(scoreSeekerRequestMap.get(CLSIntegrationAPIConstant.EndPointURL)) : '';
            String spEnv = scoreSeekerRequestMap.get(CLSIntegrationAPIConstant.SoapEnvironmentUrl) != null ? String.valueOf(scoreSeekerRequestMap.get(CLSIntegrationAPIConstant.SoapEnvironmentUrl)) : '';
            String spVh = scoreSeekerRequestMap.get(CLSIntegrationAPIConstant.SoapHeaderURL) != null ? String.valueOf(scoreSeekerRequestMap.get(CLSIntegrationAPIConstant.SoapHeaderURL)) : '';
            String spSk = scoreSeekerRequestMap.get(CLSIntegrationAPIConstant.ScoreSeekerVersionURL) != null ? String.valueOf(scoreSeekerRequestMap.get(CLSIntegrationAPIConstant.ScoreSeekerVersionURL)) : '';
            String spWsa = scoreSeekerRequestMap.get(CLSIntegrationAPIConstant.WSAUrl) != null ? String.valueOf(scoreSeekerRequestMap.get(CLSIntegrationAPIConstant.WSAUrl)) : '';
            String spWse = scoreSeekerRequestMap.get(CLSIntegrationAPIConstant.WSAsecurityURL) != null ? String.valueOf(scoreSeekerRequestMap.get(CLSIntegrationAPIConstant.WSAsecurityURL)) : '';
            String securedWsaUrl = scoreSeekerRequestMap.get(CLSIntegrationAPIConstant.WsaToURL) != null ? String.valueOf(scoreSeekerRequestMap.get(CLSIntegrationAPIConstant.WsaToURL)) : '';
            String securedWsaActionUrl = scoreSeekerRequestMap.get(CLSIntegrationAPIConstant.WsaActionUrl) != null ? String.valueOf(scoreSeekerRequestMap.get(CLSIntegrationAPIConstant.WsaActionUrl)) : '';
            
            /************************ Fetching borrower details from Inegration Config ******************************/
            List<Map<String,Object>> partyInfoList = (List<Map<String,Object>>) scoreSeekerRequestMap.get(CLSIntegrationAPIConstant.accountinfo);        
            System.debug(':::::partyInfoList is:::' + partyInfoList);
            Map<String,Object> partyInfoMap = partyInfoList.get(0);
            System.debug(':::::partyInfoMap data is:::' + partyInfoMap);
            
            List<Map<String,Object>> addressInfoList = (List<Map<String,Object>>) scoreSeekerRequestMap.get(CLSIntegrationAPIConstant.AddressInfo);          
            System.debug(':::::addressInfoList is:::' + addressInfoList);
            Map<String,Object> addressInfoListMap = addressInfoList.get(0);
            System.debug(':::::addressInfoListMap data is:::' + addressInfoListMap);
            
            
            
            partyTypeName = String.isNotBlank(String.valueOf(applinPartyDataMap.get(CLSIntegrationAPIConstant.partyTypeName))) ? String.valueOf(applinPartyDataMap.get(CLSIntegrationAPIConstant.partyTypeName)) : '' ;
            String familyName = String.isNotBlank(String.valueOf(partyInfoMap.get(CLSIntegrationAPIConstant.familyName))) ? String.valueOf(partyInfoMap.get(CLSIntegrationAPIConstant.familyName)) : '' ;
            String givenName = String.isNotBlank(String.valueOf(partyInfoMap.get(CLSIntegrationAPIConstant.givenName))) ? String.valueOf(partyInfoMap.get(CLSIntegrationAPIConstant.givenName)) : '' ;
            String middleName = String.isNotBlank(String.valueOf(partyInfoMap.get(CLSIntegrationAPIConstant.middleName))) ? String.valueOf(partyInfoMap.get(CLSIntegrationAPIConstant.middleName)) : '' ;
            String dob = String.isNotBlank(String.valueOf(partyInfoMap.get(CLSIntegrationAPIConstant.dob))) ? String.valueOf(partyInfoMap.get(CLSIntegrationAPIConstant.dob)).subString(0,10).trim() : '' ;
            String gender = String.isNotBlank(String.valueOf(partyInfoMap.get(CLSIntegrationAPIConstant.gender))) ? String.valueOf(partyInfoMap.get(CLSIntegrationAPIConstant.gender)) : '' ;
            String employment=String.isNotBlank(String.valueOf(partyInfoMap.get(CLSIntegrationAPIConstant.employment))) ? String.valueOf(partyInfoMap.get(CLSIntegrationAPIConstant.employment)) : '' ;
            accountId=String.isNotBlank(String.valueOf(partyInfoMap.get(CLSIntegrationAPIConstant.AccntId))) ? String.valueOf(partyInfoMap.get(CLSIntegrationAPIConstant.AccntId)) : '' ;
            
            String postcode=String.isNotBlank(String.valueOf(addressInfoListMap.get(CLSIntegrationAPIConstant.postcode))) ? String.valueOf(addressInfoListMap.get(CLSIntegrationAPIConstant.postcode)) : '' ;           
            String streetNumber = String.isNotBlank(String.valueOf(addressInfoListMap.get(CLSIntegrationAPIConstant.streetNumber))) ? String.valueOf(addressInfoListMap.get(CLSIntegrationAPIConstant.streetNumber)) : '' ;
            String streetName = String.isNotBlank(String.valueOf(addressInfoListMap.get(CLSIntegrationAPIConstant.streetName))) ? String.valueOf(addressInfoListMap.get(CLSIntegrationAPIConstant.streetName)) : '' ;
            String streetType = String.isNotBlank(String.valueOf(addressInfoListMap.get(CLSIntegrationAPIConstant.streetType))) ? String.valueOf(addressInfoListMap.get(CLSIntegrationAPIConstant.streetType)) : '' ;
            String suburb = String.isNotBlank(String.valueOf(addressInfoListMap.get(CLSIntegrationAPIConstant.suburb))) ? String.valueOf(addressInfoListMap.get(CLSIntegrationAPIConstant.suburb)) : '' ;
            String state = String.isNotBlank(String.valueOf(addressInfoListMap.get(CLSIntegrationAPIConstant.state))) ? String.valueOf(addressInfoListMap.get(CLSIntegrationAPIConstant.state)) : '' ;
            String countryCode = String.isNotBlank(String.valueOf(addressInfoListMap.get(CLSIntegrationAPIConstant.countryCode))) ? String.valueOf(addressInfoListMap.get(CLSIntegrationAPIConstant.countryCode)) : '' ;
            //Constructing the DOM structure
            DOM.Document doc = new DOM.Document(); 
            dom.XmlNode envelope = doc.createRootElement('Envelope', spEnv, 'soapenv');
            envelope.setNamespace('scor', spSk);
            envelope.setNamespace('vh', spVh);
            
            dom.XmlNode bodyhead = envelope.addChildElement('soapenv:Header', null, null);
            bodyhead.setNamespace('wsa', spWsa);
            dom.XmlNode security = bodyhead.addChildElement('wsse:Security', null, null);
            security.setNamespace('wsse', spWse);                    
            
            dom.XmlNode usernameTkn = security.addChildElement('wsse:UsernameToken',null,null);
            usernameTkn.addChildElement('wsse:Username',null,null).addTextNode(username);                                
            usernameTkn.addChildElement('wsse:Password',null,null).addTextNode(password);
            
            bodyhead.addChildElement('wsa:To',null,null).addTextNode(securedWsaUrl);
            bodyhead.addChildElement('wsa:Action',null,null).addTextNode(securedWsaActionUrl);
            
            dom.XmlNode bodyone= envelope.addChildElement('soapenv:Body',null,null); 
            dom.XmlNode scor=bodyone.addChildElement('scor:request', null, null);
            dom.XmlNode scorEnqHeader=scor.addChildElement('scor:enquiry-header', null, null);
            scorEnqHeader.addChildElement('scor:client-reference',null,null).addTextNode(clientRef);
            scorEnqHeader.addChildElement('scor:operator-id',null,null).addTextNode(operatorID);
            scorEnqHeader.addChildElement('scor:operator-name',null,null).addTextNode(operatorName);
            scorEnqHeader.addChildElement('scor:permission-type-code',null,null).addTextNode(permissionType);
            scorEnqHeader.addChildElement('scor:product-data-level-code',null,null).addTextNode(dataLevel);
            
            dom.XmlNode scorType=scor.addChildElement('scor:score-type', null, null);
            dom.XmlNode scoreWhatIf=scorType.addChildElement('scor:what-if', null, null);   
            dom.XmlNode scoreRequested=scoreWhatIf.addChildElement('scor:requested-scores', null, null);
            scoreRequested.addChildElement('scor:scorecard-id', null, null).addTextNode(ScorecardId);
            dom.XmlNode scoreEnquiry=scoreWhatIf.addChildElement('scor:enquiry', null, null);
            scoreEnquiry.addChildElement('scor:account-type-code',null,null).addTextNode(Accounttypecode);
            dom.XmlNode scoreEnquiryData=scoreEnquiry.addChildElement('scor:enquiry-amount',null,null);
            scoreEnquiryData.setAttribute('currency-code',Currencycode);
            scoreEnquiryData.addTextNode('0'); 

            String RelationshipCode = '';
            if('BORROWER'.equalsIgnoreCase(partyTypeName)){
                RelationshipCode = '1';
            }
            else if('CO-BORROWER'.equalsIgnoreCase(partyTypeName)){
                RelationshipCode = '2';
            }
            else if('GUARANTOR'.equalsIgnoreCase(partyTypeName)){
                RelationshipCode = '3';
            }
            else{
                RelationshipCode = '4';
            }

            scoreEnquiry.addChildElement('scor:relationship-code',null,null).addTextNode(RelationshipCode);
            scoreEnquiry.addChildElement('scor:enquiry-client-reference',null,null).addTextNode(Enquiryclientreference);
            
            dom.XmlNode subjectIdentity=scor.addChildElement('scor:subject-identity', null, null);
            dom.XmlNode currentName=subjectIdentity.addChildElement('scor:current-name',null,null);
            
            if(familyName!=null){
                currentName.addChildElement('scor:family-name',null,null).addTextNode(familyName);
            }
            else{
                currentName.addChildElement('scor:family-name',null,null).addTextNode('');
            }
            
            if(givenName !=null){
                currentName.addChildElement('scor:first-given-name',null,null).addTextNode(givenName);
            }
            else{
                currentName.addChildElement('scor:first-given-name',null,null).addTextNode('');
            }
            
            if(middlename !=null && !String.isBlank(middlename)){
                currentName.addChildElement('scor:other-given-name',null,null).addTextNode(middlename);
            }
            
            dom.XmlNode screAddrs=subjectIdentity.addChildElement('scor:addresses', null, null);
            dom.XmlNode screAddrsData=screAddrs.addChildElement('scor:address', null, null);
            screAddrsData.setAttribute('type','C');
            screAddrsData.addChildElement('scor:unit-number',null,null);                                                
            screAddrsData.addChildElement('scor:street-number',null,null).addTextNode(streetNumber);
            screAddrsData.addChildElement('scor:street-name',null,null).addTextNode(streetName);
            screAddrsData.addChildElement('scor:street-type',null,null).addTextNode(streetType);
            screAddrsData.addChildElement('scor:suburb',null,null).addTextNode(suburb);
            screAddrsData.addChildElement('scor:state',null,null).addTextNode(state.toUpperCase());
            screAddrsData.addChildElement('scor:postcode',null,null).addTextNode(postcode);
            screAddrsData.addChildElement('scor:country-code',null,null).addTextNode(countryCode);
            
            dom.XmlNode screDriveLic = subjectIdentity.addChildElement('scor:drivers-licence', null, null);
            //123788GT
            screDriveLic.addChildElement('scor:number',null,null).addTextNode('');                                                  
            //subjectIdentity.addChildElement('scor:gender-code', null, null).addTextNode(appData.gender__c);
            subjectIdentity.addChildElement('scor:gender-code', null, null).addTextNode(gender);
            //subjectIdentity.addChildElement('scor:date-of-birth', null, null).addTextNode(appData.date_of_birth__c);
            subjectIdentity.addChildElement('scor:date-of-birth', null, null).addTextNode(dob);
            
            dom.XmlNode empInfo=subjectIdentity.addChildElement('scor:employment', null, null); 
            dom.XmlNode scoreEmp= empInfo.addChildElement('scor:employer',null,null);
            scoreEmp.setAttribute('type','C');
            if(employment !=null){
                scoreEmp.addChildElement('scor:name',null,null).addTextNode(employment);
            }
            else{
                scoreEmp.addChildElement('scor:name',null,null).addTextNode('');
            }
            
            System.debug('::::doc::::::'+doc.toXmlString());
            envelope1=doc.toXmlString().remove('<?xml version="1.0" encoding="UTF-8"?>');
            
            system.debug('****Constructed request body is*****'+envelope1);
        }
        catch(Exception e){
            throw new ints.IntegrationException(e.getMessage());
        }
        scRequest = envelope1;
        return envelope1;
    }
    
    //XML parser starts here
    public static String responseXmlParseDML(String resp){
        
        scoreseeker__c scobj= new scoreseeker__c();
        DOM.Document doc1=new DOM.Document();   
        doc1.load(resp);
        dom.XmlNode envelopeResponse =doc1.getRootElement();
        System.debug(':::envelopeenvelope:::'+envelopeResponse);
        List<Dom.XMLNode> resbodyData=envelopeResponse.getChildren();
        System.debug('childelementdatatatatat'+resbodyData);
        for(Dom.XMLNode dataParse : resbodyData){
            System.debug(':::::dataParsedataParse in the body'+dataParse.getName()); 
            if(dataParse.getName()=='Body'){
                List<Dom.XMLNode> resBody1=dataParse.getChildren();
                System.debug(':::resBody1 for child is::::'+resBody1);
                for(Dom.XMLNode searchBody:resBody1){
                    System.debug(':::::searchBodysearchBody in the body'+searchBody.getName());
                    if(searchBody.getName()=='response'){
                        List<Dom.XMLNode> chData=searchBody.getChildren();
                        System.debug(':::resBody1 for child is::::'+chData);
                        for(Dom.XMLNode respChData:chData ){
                            System.debug('::::respChData:::'+respChData.getName());
                            Integer errorCount = 1;
                            if(respChData.getName()=='score-data'){
                                List<Dom.XMLNode> vsScoreData=respChData.getChildren();
                                System.debug(':::vsScoreData for child is::::'+vsScoreData);
                                
                                for(Dom.XMLNode childSData:vsScoreData){
                                    if(childSData.getName()=='score'){                                              
                                        List<Dom.XMLNode> ScoreData=childSData.getChildren();
                                        System.debug(':::vsScoreData for child is::::'+ScoreData); 
                                        for(Dom.XMLNode childScData:ScoreData){
                                            System.debug('::::ChildName is:::Scored data:::::'+childScData.getName());
                                            if(childScData.getName()=='risk-odds'){
                                                scobj.risk_odds__c=childScData.getText();
                                            }
                                            if(childScData.getName()=='score-masterscale'){
                                                scobj.score_masterscale__c = childScData.getText();
                                            }
                                            
                                            //System.debug('::childScDatachildScDatachildScData'+childScData.getAttribute('id', null));
                                            if(childScData.getName()=='scorecard'){
                                                System.debug('::childScDatachildScDatachildScData'+childScData.getAttribute('id', null));
                                                scobj.score_cardid__c=childScData.getAttribute('id', null);
                                                List<Dom.XMLNode> ScoreData1=childScData.getChildren();
                                                System.debug('::::ScoreData:::::'+ScoreData1);
                                                for(Dom.XMLNode scData:ScoreData1){
                                                    if(scData.getName()=='version'){
                                                        scobj.version__c=Decimal.valueOf(scData.getText());
                                                    }
                                                    if(scData.getName()=='name'){
                                                        scobj.score_name__c=scData.getText();
                                                    }
                                                    if(scData.getName()=='type'){
                                                        scobj.score_cardtype__c=scData.getText();
                                                    }
                                                    if(scData.getName()=='data-level'){
                                                        scobj.score_data_level__c=scData.getText();
                                                    }
                                                    System.debug('::::scData is:::scData data:::::'+scData.getName());
                                                }
                                                
                                            }
                                        }
                                    }
                                }
                            }
                            else if(respChData.getName() == 'errors'){
                                List<Dom.XMLNode> vsErrorData = respChData.getChildren();
                                System.debug(':::vsErrorData for child is::::'+vsErrorData);
                                List<String> listErrors = new List<String>();
                                for(Dom.XMLNode childSData : vsErrorData){
                                    if(childSData.getName() == 'error'){                                              
                                        List<Dom.XMLNode> errorData = childSData.getChildren();
                                        System.debug(':::errorData for child is::::'+errorData); 
                                        String errorStr = '';
                                        for(Dom.XMLNode childScData : errorData){
                                            System.debug('::::ChildName is:::Scored data:::::'+childScData.getName());
                                            if(childScData.getName()=='fault-code'){
                                                errorStr = '<ERR-' + errorCount++ + '>: ';
                                                errorStr += 'fault-code = ' + childScData.getText() + ' : ';
                                            }
                                            else if(childScData.getName() == 'detail'){
                                                errorStr += 'detail = ' + childScData.getText();
                                            }
                                        }
                                        if(!String.isBlank(errorStr)){
                                            listErrors.add(errorStr);
                                        }
                                    }
                                }
                                if(scobj.Error_Value__c == null){
                                    scobj.Error_Value__c = '';
                                }
                                scobj.Error_Value__c += string.join(listErrors,',');
                            }
                            else if(respChData.getName() == 'general-messages'){
                                List<Dom.XMLNode> vsErrorData = respChData.getChildren();
                                List<String> listErrors = new List<String>();
                                for(Dom.XMLNode childSData : vsErrorData){
                                    if(childSData.getName() == 'message'){                                              
                                        String str = '<GENMSG-' + errorCount++ + '>: ';
                                        str += 'message = ' + childSData.getText() + ' : ';
                                        listErrors.add(str);
                                    }
                                }
                                if(scobj.Error_Value__c == null){
                                    scobj.Error_Value__c = '';
                                }
                                scobj.Error_Value__c += string.join(listErrors,',');
                            }
                        }
                    }
                }// Parsing body ends here
            }
        }//Parsing response body for each ends here
        
        if(scobj !=null){
            scobj.application__c = applicationId;
            scobj.apipull_datetime__c = System.now();
            scobj.account__c = accountId;
            scobj.Party_Type__c = partyTypeName;
            scobj.Is_Latest__c = true;
            System.debug(':::Before adding the insert the score seeker::::'+scobj);                        
            
            //update recent Score seeker record's lookup on application
            List<scoreseeker__c> scoreseek = [SELECT id,
                                                     Name,
                                                     Is_Latest__c
                                                FROM scoreseeker__c
                                               WHERE application__c =: applicationId
                                                 AND account__c = :accountId
                                                 AND Party_Type__c = :partyTypeName
                                                 AND Is_Latest__c = true];
            if(scoreseek != null && scoreseek.size() > 0){
                for(scoreseeker__c ss : scoreseek){
                    ss.Is_Latest__c = false;
                }
                update scoreseek;
            }
            
            insert scobj;
            
            //insert request and response XML as attachment of Score seeker record
            ScoreSeekerAPIAdapter1 ssAdapter = new ScoreSeekerAPIAdapter1();
            ssAdapter.createAttachment('Request', scRequest, scobj.id);
            ssAdapter.createAttachment('Response', resp, scobj.id);
            
        }
        return 'SUCCESS';
    }
    
    public void createAttachment(String callType, String fileBody, ID parent){
        try{
            Attachment attachment = new Attachment();
            attachment.Body = Blob.valueOf(fileBody);
            attachment.Name = callType + '_' + 'ScoreSeeker_' + CommonUtility.getDatetimeValue('');
            attachment.ParentId = parent;
            insert attachment;
        }
        catch(Exception e){
            throw new ints.IntegrationException('[ScoreSeekerAPIAdapter.createAttachment]Exception: ' +  e.getMessage());
        }
    }
}