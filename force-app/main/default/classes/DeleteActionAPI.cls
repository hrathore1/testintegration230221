global without sharing class DeleteActionAPI implements clcommon.PortalCustomRemoteAPI1{
    global clcommon.Response invokeAction(String componentStrName,String[] disclosureNames,Map<String, Object> params){
        mfiflexUtil.VOLog voLogInstance = mfiflexUtil.VOLog.getInstance('Genesis');
        clcommon.Response response = new clcommon.Response();
        List<clcommon.PortalCustomRemoteActionRecord> records = new List<clcommon.PortalCustomRemoteActionRecord>();
        map<String,Object> fields=new map<String,Object>();
        List<String> validObjs = new List<String>();
        
        if(params == null){
            throw new CustomException('No parameters provided.');
        }
        if(params.get('recordId') == null || String.isBlank(String.valueOf(params.get('recordId')))){
            throw new CustomException('No recordId provided.');
        }
        String recordId = String.valueOf(params.get('recordId'));
        /*if(recordId.StartsWith('new_record_id_')){
            records.add ( new clcommon.PortalCustomRemoteActionRecord( 'response', fields));
            response = clcommon.PortalActions.getCustomRemoteActionResponse(records);
            return response;
        }
        */
        String applicationId = (params.containsKey('applicationId') && params.get('applicationId') != null)?
                                String.valueOf(params.get('applicationId')):
                                '';
        
        if(System.label.OBJECTS_TO_DELETE_FROM_PORTAL != null && String.isNotBlank(System.label.OBJECTS_TO_DELETE_FROM_PORTAL)){
            validObjs = System.label.OBJECTS_TO_DELETE_FROM_PORTAL.split(',');
        }
        try{
            List< clcommon__Category_Attachment_Association__c > categoryAssoc = new List< clcommon__Category_Attachment_Association__c >();
            if(String.isNotBlank(recordId)){
                Schema.SObjectType sobjectType = Id.valueOf(recordId).getSObjectType();
                String sobjectName = sobjectType.getDescribe().getName();
                List<SObject> record ;
                /*If(validObjs.contains(sobjectName)){
                    record = Database.query('Select Id, Name From ' + sobjectName + ' Where Id = :recordId AND genesis__Application__c = :applicationId');
                }
                else{
                */
                // delete attachment
                    if(!Test.isRunningTest()){
                         record = [SELECT Id,ParentId,Name FROM Attachment WHERE Id =: recordId AND OwnerId =: UserInfo.getUserId()];
                     }else{
                         record = [SELECT Id,ParentId,Name FROM Attachment WHERE Id =: recordId];
                     }
                     
                //}
                if(record != null && record.size() > 0){
                    categoryAssoc = [SELECT id 
                                        FROM clcommon__Category_Attachment_Association__c 
                                       WHERE clcommon__Attachment_Id__c =: record[0].id];
                    delete record;
                    if(categoryAssoc != null && categoryAssoc.size() > 0){
                        delete categoryAssoc;
                    }
                }
            }
            records.add ( new clcommon.PortalCustomRemoteActionRecord( 'response', fields));
            response = clcommon.PortalActions.getCustomRemoteActionResponse(records);
        }
        catch(Exception e){
            String exMessage = e.getMessage();
            response.status = clcommon.Constants.ERROR;
            response.errorCode = clcommon.Constants.API_EXCEPTION;
            response.errorMessage = exMessage;
            voLogInstance.logException(700, '[DeleteActionAPI.invokeAction] Exception Occured'+e.getLineNumber(), e);
        }
        voLogInstance.commitToDb();
        return response;
    }   
}