public class SAT_SubmitToCompliance implements StageExecutionInterface {
    public List<String> executeValidations(Id applicationId){
        try{
            List<String> returnList = new List<String>();
            return returnList;            
        }
        catch(Exception e){throw e;}          
    }

    public String executeNextDeptActions(Id applicationId){
        try{
            return executeActions(applicationId);
            //return System.label.TAG_SUCCESS;                     
        }
        catch(Exception e){throw e;}          
    }

    public String executePreviousDeptActions(Id applicationId){
        try{
            //executeActions(applicationId);
            return System.label.NEXT_STAGE_SUBMIT_SUCCESS_MSG;
        }
        catch(Exception e){throw e;}          
    }

    public String executeActions(Id applicationId){
        try{
            genesis__Applications__c appInst = new genesis__Applications__c(Id = applicationId);
            /*List<Task> tsList = [SELECT Id, 
                                        genesis__Application__c, 
                                        genesis__Department__c, 
                                        Subject 
                                   FROM Task
                                  WHERE genesis__Application__c =: applicationId
                                    AND genesis__Department__c = null
                                ];
                        
            if(tsList.size()>0){
                appInst.genesis__Status__c = 'COMPLIANCE-IRREGULARITY';
            }else{
                appInst.genesis__Status__c = 'APPROVED-COMPLIANCE';
            }*/
            
            appInst.genesis__Status__c = 'APPROVED-COMPLIANCE';
            update appInst;

            return System.label.NEXT_STAGE_SUBMIT_SUCCESS_MSG;                     
        }
        catch(Exception e){throw e;}
    }
}