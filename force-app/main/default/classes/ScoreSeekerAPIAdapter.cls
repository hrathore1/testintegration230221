global class ScoreSeekerAPIAdapter extends ints.AbstractMockAdapter{
    public static String applicationId{get;set;}
    public static String accountId{get;set;}
    public static String partyTypeName{get;set;}
    public static String partyId{get;set;}
    public static String reqBody = '';
    global ScoreSeekerAPIAdapter(){}
    
    global override ints.HttpRequestObject createRequest(Map<String, Object> scoreSeekerRequestMap, 
                                                         ints.IntegrationConfigurationDTO IntegrationConfiguration){
                                                             
        System.debug(':::::Inside Scoreseeker Adaptor:::');                                                       
        try{
            if(scoreSeekerRequestMap != null){
                ints.HttpRequestObject httpReq = new ints.HttpRequestObject();
                httpReq.endpoint = 'callout:'+ integrationConfiguration.apiNamedCredential;
                httpReq.timeout = 60000;
                httpReq.method = 'POST';    
                                                                     
                Map<String,String> headerMap = new Map<String,String>();
                headerMap.put('Content-type','text/xml;charset=utf-8');
                if(Test.isRunningTest()){
                    headerMap.put('Use-Mocker','TRUE');
                    headerMap.put('Execution-Priority','2');
                    headerMap.put('Control-Field-Value','3');
                }
                httpReq.headerMap = headerMap;
                                                                     
                /****************************Constructing request body************************************/
                reqBody = constructRequest(scoreSeekerRequestMap);
                if(reqBody != null && String.isNotBlank(reqBody)){
                    httpReq.body = reqBody;
                    return httpReq ;
                }
                else{
                    throw new ints.IntegrationException('[ScoreSeekerAPIAdapter.createRequest] Generated request is blank.');
                }
            }
            else{
                throw new ints.IntegrationException('[ScoreSeekerAPIAdapter.createRequest] Equifax Apply request map is blank.');
            }
        }
        catch(Exception e){
            throw new ints.IntegrationException('[ScoreSeekerAPIAdapter.createRequest] Exception:' + e.getMessage());
        }                                                         
    }
    
    global override ints.IntegrationResponseObject parseResponse(HttpResponse response, ints.ThirdPartyRequest thirdPartyRequest){
        IntegrationAPIResponseParser respParser = new IntegrationAPIResponseParser();
        ints.IntegrationResponseObject integrationRes;
        
        String resp = response.getBody();
        System.debug('=====RESPONSE======'+resp);
        //createAttachment('RES', String.valueOf(response));
        if(response.getStatusCode() == 404){
            throw new ints.IntegrationException('Status code is: ' + response.getStatusCode());
        }
        else{
            if(resp != null && String.isNotBlank(resp)){
                String parserResponse = parseScoreSeekerResponse(resp,response.getStatusCode());
                System.debug(':::parserResponse:::' + parserResponse);
                if(parserResponse != 'SUCCESS'){
                    throw new ints.IntegrationException(parserResponse);
                }
            }
            else{
                throw new ints.IntegrationException('Response Body is null or blank.');
            }
        }
        
        system.debug('::::respParserrespParser:::'+respParser);
        
        integrationRes = respParser;
        return integrationRes;
    }

    global Static String constructRequest(Map<String, Object> scorSeekerMap){
        DOM.Document doc = new DOM.Document(); 
        try{
            if(scorSeekerMap == null){
                return 'No content found.';
            }

            //Other parameters
            String soapEnvironmentUrl = (String) scorSeekerMap.get('SoapEnvironmentUrl');
            String equifaxApplyVersionURL = (String) scorSeekerMap.get('equifaxApplyVersionURL');
            String soapHeaderURL = (String) scorSeekerMap.get('soapHeaderURL');
            String wsaUrl = (String) scorSeekerMap.get('wsaUrl');
            String wsaSecurityURL = (String) scorSeekerMap.get('wsaSecurityURL');
            String wsaToURL = (String) scorSeekerMap.get('wsaToURL');
            String wsaActionURL = (String) scorSeekerMap.get('wsaActionURL');
            String clientReference = (String) scorSeekerMap.get('clientReference');
            String operatorId = (String) scorSeekerMap.get('operatorId');
            String operatorName = (String) scorSeekerMap.get('operatorName');
            String permissionTypeCode = (String) scorSeekerMap.get('permissionTypeCode');
            String productDataLevelCode = (String) scorSeekerMap.get('productDataLevelCode');
            String currenyCode = (String) scorSeekerMap.get('currenyCode');
            String namespace = (String) scorSeekerMap.get('namespace');
            String scorecardId = (String) scorSeekerMap.get('scorecardId');
            String addressTableTag = (String) scorSeekerMap.get('addressTableTag');
            String borrowerTableTag = (String) scorSeekerMap.get('borrowerTableTag');
            String partyTableTag = (String) scorSeekerMap.get('partyTableTag');
            String applicationTableTag = (String) scorSeekerMap.get('applicationTableTag');
            String employmentTableTag = (String) scorSeekerMap.get('employmentTableTag');
            String enquiryClientReference = (String) scorSeekerMap.get('enquiryClientReference');

            //User credentials
            String username = '{!HTMLENCODE($Credential.Username)}';
            String password = '{!HTMLENCODE($Credential.Password)}';
            
            //Dynamic data
            List<Map<String, Object>> addressList = (List<Map<String, Object>>) scorSeekerMap.get(addressTableTag);
            List<Map<String, Object>> borrowerList = (List<Map<String, Object>>) scorSeekerMap.get(borrowerTableTag);
            List<Map<String, Object>> partyList = (List<Map<String, Object>>) scorSeekerMap.get(partyTableTag);
            List<Map<String, Object>> applicationList = (List<Map<String, Object>>) scorSeekerMap.get(applicationTableTag);
            List<Map<String, Object>> employmentList = (List<Map<String, Object>>) scorSeekerMap.get(employmentTableTag);

            Map<String, Object> partyInfo = partyList.get(0);
            Map<String, Object> borrowerInfo = borrowerList.get(0);
            Map<String, Object> applicationInfo = applicationList.get(0);
            Map<String, Object> currentAddress;
            Map<String, Object> previousAddress;
            Map<String, Object> currentEmployment;
            Map<String, Object> previousEmployment;
            
            applicationId = (String) applicationInfo.get('applicationId');
            accountId = (String) borrowerInfo.get('AccId'); 
            partyTypeName = (String) partyInfo.get('partyTypeName'); 
            partyId = (String)  partyInfo.get('partyId1');

            if(addressList != null && addressList.size() > 0){
                for(Map<String, Object> mapAddress : addressList){
                    Map<String, Object> addr = mapAddress;
                    if(addr.containsKey('isCurrentAddress')){
                        if((Boolean) addr.get('isCurrentAddress')){
                            currentAddress = addr;
                        }
                        else{
                            previousAddress = addr;
                        }
                    }
                }
            }
            
            if(employmentList != null && employmentList.size() > 0){
                for(Map<String, Object> mapEmployment : employmentList){
                    Map<String, Object> employment = mapEmployment;
                    if(employment.containsKey('isCurrentEmployment')){
                        if((Boolean) employment.get('isCurrentEmployment')){
                            currentEmployment = employment;
                        }
                        else{
                            previousEmployment = employment;
                        }
                    }
                }
            }

            String salutation = (String) borrowerInfo.get('title');
            String firstName = (String) borrowerInfo.get('first-given-name');
            String lastName = (String) borrowerInfo.get('family-name');
            String middleName = (String) borrowerInfo.get('other-given-name');

            String genderCode = (String) borrowerInfo.get('gender-code');
            String dateOfBirth = (String) borrowerInfo.get('date-of-birth');
            String accountTypeCode = (String) applicationInfo.get('accountTypeCode');
            String loanAmount = String.valueOf((Decimal) applicationInfo.get('enquiry-amount')); 
            String relationshipCode = (String) partyInfo.get('equifaxRelationshipCode');
            String drivingLicense = (String) partyInfo.get('drivers-licence');
            
            //Envelop Record
            dom.XmlNode envelopRec = doc.createRootElement('Envelope', soapEnvironmentUrl, 'soapenv');
            envelopRec.setNamespace(namespace, equifaxApplyVersionURL);
            envelopRec.setNamespace('vh', soapHeaderURL);
            
            //Envelop Record -> Header Record
            dom.XmlNode headerRec = envelopRec.addChildElement('soapenv:Header', null, null);
            headerRec.setNamespace('wsa', wsaUrl);
            
            //Envelop Record -> Header Record -> Security Record
            dom.XmlNode securityRec = headerRec.addChildElement('wsse:Security', null, null);
            securityRec.setNamespace('wsse', wsaSecurityURL);   
            
            //Envelop Record -> Header Record -> Security Record -> Username Token Record
            dom.XmlNode usernameTknRec = securityRec.addChildElement('wsse:UsernameToken', null, null);
            usernameTknRec.addChildElement('wsse:Username', null, null).addTextNode(username);                                
            usernameTknRec.addChildElement('wsse:Password', null, null).addTextNode(password);
            
            //Envelop Record -> Header Record -> To Record
            headerRec.addChildElement('wsa:To', null, null).addTextNode(wsaToURL);

            //Envelop Record -> Header Record -> Action Record
            headerRec.addChildElement('wsa:Action', null, null).addTextNode(wsaActionURL);

            //Envelop Record -> Body Record
            dom.XmlNode bodyRecord = envelopRec.addChildElement('soapenv:Body', null, null); 

            //Envelop Record -> Body Record -> Request Record
            dom.XmlNode requestRecord = bodyRecord.addChildElement(namespace + ':request', null, null);

            //Envelop Record -> Body Record -> Request Record -> Enquiry Header Record
            dom.XmlNode enquiryHeaderRecord = requestRecord.addChildElement(namespace + ':enquiry-header', null, null);
            enquiryHeaderRecord.addChildElement(namespace + ':client-reference', null, null).addTextNode(clientReference);
            enquiryHeaderRecord.addChildElement(namespace + ':operator-id', null, null).addTextNode(operatorId);
            if(!String.isBlank(operatorName)){
                enquiryHeaderRecord.addChildElement(namespace + ':operator-name', null, null).addTextNode(operatorName);
            }
            enquiryHeaderRecord.addChildElement(namespace + ':permission-type-code', null, null).addTextNode(permissionTypeCode);
            enquiryHeaderRecord.addChildElement(namespace + ':product-data-level-code', null, null).addTextNode(productDataLevelCode);

            //Envelop Record -> Body Record -> Request Record -> Score Type Record
            dom.XmlNode scoreTypeRecord = requestRecord.addChildElement(namespace + ':score-type', null, null);

            //Envelop Record -> Body Record -> Request Record -> Score Type Record -> What If Record
            dom.XmlNode whatIfRecord = scoreTypeRecord.addChildElement(namespace + ':what-if', null, null);

            //Envelop Record -> Body Record -> Request Record -> Score Type Record -> What If Record
            // -> Requested Score Record
            dom.XmlNode requestedScoreRecord = whatIfRecord.addChildElement(namespace + ':requested-scores', null, null);
            requestedScoreRecord.addChildElement(namespace + ':scorecard-id', null, null).addTextNode(scorecardId);

            //Envelop Record -> Body Record -> Request Record -> Score Type Record -> What If Record
            // -> Enquiry Record
            dom.XmlNode enquiryRecord = whatIfRecord.addChildElement(namespace + ':enquiry', null, null);

            //Envelop Record -> Body Record -> Request Record -> Score Type Record -> What If Record
            // -> Enquiry Record -> Account Type Record
            if(!String.isBlank(accountTypeCode)){
                enquiryRecord.addChildElement(namespace + ':account-type-code', null, null).addTextNode(accountTypeCode);
            }
            
            //Envelop Record -> Body Record -> Request Record -> Score Type Record -> What If Record
            // -> Enquiry Record -> Enquiry Amount Record
            if(!String.isBlank(loanAmount)){
                dom.XmlNode enquiryAmountRecord = enquiryRecord.addChildElement(namespace + ':enquiry-amount', null, null);
                enquiryAmountRecord.setAttribute('currency-code', currenyCode);
                enquiryAmountRecord.addTextNode(loanAmount);
            }

            //Envelop Record -> Body Record -> Request Record -> Score Type Record -> What If Record
            // -> Enquiry Record -> Relationship Type Record
            if(!String.isBlank(relationshipCode)){
                enquiryRecord.addChildElement(namespace + ':relationship-code', null, null).addTextNode(relationshipCode);
            }

            //Envelop Record -> Body Record -> Request Record -> Score Type Record -> What If Record
            // -> Enquiry Record -> Enquiry Client Ref Record
            if(!String.isBlank(enquiryClientReference)){
                enquiryRecord.addChildElement(namespace + ':enquiry-client-reference', null, null).addTextNode(enquiryClientReference);
            }
            
            //Envelop Record -> Body Record -> Request Record -> Enquiry Data Record -> Subject Identity Record
            dom.XmlNode subjectIdentityRecord = requestRecord.addChildElement(namespace + ':subject-identity', null, null);   

            //Envelop Record -> Body Record -> Request Record -> Enquiry Data Record -> Individual Record
            // -> Current Name Record
            dom.XmlNode currentNameRecord = subjectIdentityRecord.addChildElement(namespace + ':current-name', null, null);
            if(!String.isBlank(salutation)){
                currentNameRecord.addChildElement(namespace + ':title', null, null).addTextNode(salutation);
            }
            if(!String.isBlank(lastName)){
                currentNameRecord.addChildElement(namespace + ':family-name', null, null).addTextNode(lastName);
            }
            if(!String.isBlank(firstName)){
                currentNameRecord.addChildElement(namespace + ':first-given-name', null, null).addTextNode(firstName);
            }
            if(!String.isBlank(middleName)){
                currentNameRecord.addChildElement(namespace + ':other-given-name', null, null).addTextNode(middleName);
            }
            
            //If address exist for the borrower
            if(addressList != null && addressList.size() > 0){
                //Envelop Record -> Body Record -> Request Record -> Enquiry Data Record -> Individual Record
                // -> Addresses Record
                dom.XmlNode addressesRecord = subjectIdentityRecord.addChildElement(namespace + ':addresses', null, null);

                //Envelop Record -> Body Record -> Request Record -> Enquiry Data Record -> Individual Record
                // -> Addresses Record -> Current Address Record
                if(currentAddress != null){
                    String addressType = (String) currentAddress.get('type');
                    String timeAtAddress = String.valueOf((Decimal) currentAddress.get('time-at-address'));
                    String property = (String) currentAddress.get('property');
                    String unitNumber = (String) currentAddress.get('unit-number');
                    String streetNumber = (String) currentAddress.get('street-number');
                    String streetName = (String) currentAddress.get('street-name');
                    String streetType = (String) currentAddress.get('street-type');
                    String suburb = (String) currentAddress.get('suburb');
                    String state = (String) currentAddress.get('state');
                    String postcode = (String) currentAddress.get('postcode');
                    String countryCode = (String) currentAddress.get('country-code');

                    dom.XmlNode currentAddressRecord = addressesRecord.addChildElement(namespace + ':address', null, null);
                    if(!String.isBlank(addressType)){
                        currentAddressRecord.setAttribute('type', addressType);
                    }
                    if(!String.isBlank(timeAtAddress)){
                        currentAddressRecord.setAttribute('time-at-address', timeAtAddress);
                    }
                    if(!String.isBlank(property)){
                        currentAddressRecord.addChildElement(namespace + ':property', null, null).addTextNode(property);
                    }
                    if(!String.isBlank(unitNumber)){
                        currentAddressRecord.addChildElement(namespace + ':unit-number', null, null).addTextNode(unitNumber);
                    }
                    if(!String.isBlank(streetNumber)){
                        currentAddressRecord.addChildElement(namespace + ':street-number', null, null).addTextNode(streetNumber);
                    }    
                    if(!String.isBlank(streetName)){
                        currentAddressRecord.addChildElement(namespace + ':street-name', null, null).addTextNode(streetName);
                    }
                    if(!String.isBlank(streetType)){
                        currentAddressRecord.addChildElement(namespace + ':street-type', null, null).addTextNode(streetType);
                    }
                    if(!String.isBlank(suburb)){
                        currentAddressRecord.addChildElement(namespace + ':suburb', null, null).addTextNode(suburb);
                    }
                    if(!String.isBlank(state)){
                        currentAddressRecord.addChildElement(namespace + ':state', null, null).addTextNode(state.toUpperCase());
                    }
                    if(!String.isBlank(postcode)){
                        currentAddressRecord.addChildElement(namespace + ':postcode', null, null).addTextNode(postcode);
                    }
                    if(!String.isBlank(countryCode)){
                        currentAddressRecord.addChildElement(namespace + ':country-code', null, null).addTextNode(countryCode.toUpperCase());
                    }
                }

                //Envelop Record -> Body Record -> Request Record -> Enquiry Data Record -> Individual Record
                // -> Addresses Record -> Previous Address Record
                if(previousAddress != null){
                    String addressType = (String) previousAddress.get('type');
                    String timeAtAddress = String.valueOf((Decimal) previousAddress.get('time-at-address'));
                    String property = (String) previousAddress.get('property');
                    String unitNumber = (String) previousAddress.get('unit-number');
                    String streetNumber = (String) previousAddress.get('street-number');
                    String streetName = (String) previousAddress.get('street-name');
                    String streetType = (String) previousAddress.get('street-type');
                    String suburb = (String) previousAddress.get('suburb');
                    String state = (String) previousAddress.get('state');
                    String postcode = (String) previousAddress.get('postcode');
                    String countryCode = (String) previousAddress.get('country-code');

                    dom.XmlNode previousAddressRecord = addressesRecord.addChildElement(namespace + ':address', null, null);
                    if(!String.isBlank(addressType)){
                        previousAddressRecord.setAttribute('type', addressType);
                    }
                    if(!String.isBlank(timeAtAddress)){
                        previousAddressRecord.setAttribute('time-at-address', timeAtAddress);
                    }
                    if(!String.isBlank(property)){
                        previousAddressRecord.addChildElement(namespace + ':property', null, null).addTextNode(property);
                    }
                    if(!String.isBlank(unitNumber)){
                        previousAddressRecord.addChildElement(namespace + ':unit-number', null, null).addTextNode(unitNumber);
                    }
                    if(!String.isBlank(streetNumber)){
                        previousAddressRecord.addChildElement(namespace + ':street-number', null, null).addTextNode(streetNumber);
                    }    
                    if(!String.isBlank(streetName)){
                        previousAddressRecord.addChildElement(namespace + ':street-name', null, null).addTextNode(streetName);
                    }
                    if(!String.isBlank(streetType)){
                        previousAddressRecord.addChildElement(namespace + ':street-type', null, null).addTextNode(streetType);
                    }
                    if(!String.isBlank(suburb)){
                        previousAddressRecord.addChildElement(namespace + ':suburb', null, null).addTextNode(suburb);
                    }
                    if(!String.isBlank(state)){
                        previousAddressRecord.addChildElement(namespace + ':state', null, null).addTextNode(state.toUpperCase());
                    }
                    if(!String.isBlank(postcode)){
                        previousAddressRecord.addChildElement(namespace + ':postcode', null, null).addTextNode(postcode);
                    }
                    if(!String.isBlank(countryCode)){
                        previousAddressRecord.addChildElement(namespace + ':country-code', null, null).addTextNode(countryCode.toUpperCase());
                    }
                }
            }
        
            //Envelop Record -> Body Record -> Request Record -> Enquiry Data Record -> Individual Record
            // -> Driver License Record
            if(!String.isBlank(drivingLicense)){
                dom.XmlNode driverLicenseRecord = subjectIdentityRecord.addChildElement(namespace + ':drivers-licence', null, null);
                driverLicenseRecord.addChildElement(namespace + ':number', null, null).addTextNode(drivingLicense);
            }
            
            //Envelop Record -> Body Record -> Request Record -> Enquiry Data Record -> Individual Record
            // -> Gender Record
            if(!String.isBlank(genderCode)){
                dom.XmlNode genderRecord = subjectIdentityRecord.addChildElement(namespace + ':gender-code', null, null);
                genderRecord.addTextNode(genderCode); 
            }

            //Envelop Record -> Body Record -> Request Record -> Enquiry Data Record -> Individual Record
            // -> Date of Birth Record
            if(!String.isBlank(dateOfBirth)){
                dom.XmlNode dobRecord = subjectIdentityRecord.addChildElement(namespace + ':date-of-birth', null, null);
                dobRecord.addTextNode(dateOfBirth); 
            }
            
            //If employment Information exist
            if(employmentList != null && employmentList.size() > 0){
                //Envelop Record -> Body Record -> Request Record -> Enquiry Data Record -> Individual Record
                // -> Employment Record
                dom.XmlNode employmentRecord = subjectIdentityRecord.addChildElement(namespace + ':employment', null, null);
         
                //Envelop Record -> Body Record -> Request Record -> Enquiry Data Record -> Individual Record
                // -> Employment Record -> Employer Name Record
                if(currentEmployment != null){
                    String employerStatus = (String) currentEmployment.get('type');
                    String employerName = (String) currentEmployment.get('name');
                    
                    if(!String.isBlank(employerName)){
                        dom.XmlNode employerRecord = employmentRecord.addChildElement(namespace + ':employer', null, null);
                        employerRecord.setAttribute('type', employerStatus);
                        employerRecord.addChildElement(namespace + ':name', null, null).addTextNode(employerName);
                    }
                }
                
                if(previousEmployment != null){
                    String employerStatus = (String) previousEmployment.get('type');
                    String employerName = (String) previousEmployment.get('name');
                    
                    if(!String.isBlank(employerName)){
                        dom.XmlNode employerRecord = employmentRecord.addChildElement(namespace + ':employer', null, null);
                        employerRecord.setAttribute('type', employerStatus);
                        employerRecord.addChildElement(namespace + ':name', null, null).addTextNode(employerName);
                    }    
                }
            }
            
            String finalRequestBody = doc.toXmlString().remove('<?xml version="1.0" encoding="UTF-8"?>');
            System.debug('Final Request Body:' + finalRequestBody);
            
            //Document doc1 = new Document();
            //doc1.body = Blob.valueOf(finalRequestBody);
            //doc1.name = 'ScoreseekBody';
            //doc1.folderid = '0055D000002YRpp';
            //insert doc1;
            
            return finalRequestBody;
        }
        catch(Exception e){
            System.debug('Partial request::::' + doc.toXmlString());
            System.debug('Exception: ' + e.getMessage());
            System.debug('Exception: ' + e.getStackTraceString());
            throw new ints.IntegrationException('Exception while creating request: ' + e.getMessage() + ', at line ' + e.getLineNumber());
        }
    }
    
    public static void createAttachment(String callType, String fileBody, ID parent){
        try{
            Attachment attachment = new Attachment();
            attachment.Body = Blob.valueOf(fileBody);
            attachment.Name = callType + '_' + 'EquifaxApply_' + CommonUtility.getDatetimeValue('');
            attachment.ParentId = parent;
            insert attachment;
        }
        catch(Exception e){
            throw new ints.IntegrationException('[ScoreSeekerAPIAdapter.createAttachment]Exception: ' +  e.getMessage());
        }
    }
    
    public static String parseScoreSeekerResponse(String response,Integer statusCode){
        try{
            scoreseeker__c scobj = new scoreseeker__c();
            DOM.Document doc = new DOM.Document();   
            doc.load(response);

            dom.XmlNode envelopeResponse = doc.getRootElement();
            System.debug(':::envelopeenvelope:::'+envelopeResponse);
            
            List<Dom.XMLNode> resbodyData = envelopeResponse.getChildren();
            
            for(Dom.XMLNode dataParse : resbodyData){
                if(dataParse.getName() == 'Body'){
                    List<Dom.XMLNode> resBody = dataParse.getChildren();
                    System.debug(':::resBody for child is::::'+resBody);
                    for(Dom.XMLNode searchBody : resBody){
                        if(searchBody.getName() == 'response'){
                            List<Dom.XMLNode> chData = searchBody.getChildren();
                            for(Dom.XMLNode respChData : chData){
                                Integer errorCount = 1;
                                if(respChData.getName() == 'score-data'){
                                    List<Dom.XMLNode> vsScoreData = respChData.getChildren();
                                    
                                    for(Dom.XMLNode childSData : vsScoreData){
                                        if(childSData.getName() == 'score'){                                              
                                            List<Dom.XMLNode> ScoreData = childSData.getChildren();
                                            for(Dom.XMLNode childScData : ScoreData){
                                                if(childScData.getName() == 'risk-odds'){
                                                    scobj.risk_odds__c = childScData.getText();
                                                }
                                                if(childScData.getName() == 'score-masterscale'){
                                                    scobj.score_masterscale__c = childScData.getText();
                                                }
                                                
                                                if(childScData.getName() == 'scorecard'){
                                                    scobj.score_cardid__c = childScData.getAttribute('id', null);
                                                    List<Dom.XMLNode> scoreData1 = childScData.getChildren();
                                                    System.debug('::::ScoreData:::::' + scoreData1);
                                                    for(Dom.XMLNode scData : scoreData1){
                                                        if(scData.getName() == 'version'){
                                                            scobj.version__c = Decimal.valueOf(scData.getText());
                                                        }
                                                        if(scData.getName() == 'name'){
                                                            scobj.score_name__c = scData.getText();
                                                        }
                                                        if(scData.getName() == 'type'){
                                                            scobj.score_cardtype__c = scData.getText();
                                                        }
                                                        if(scData.getName() == 'data-level'){
                                                            scobj.score_data_level__c = scData.getText();
                                                        }
                                                    }
                                                    
                                                }
                                            }
                                        }
                                    }
                                }
                                else if(respChData.getName() == 'errors'){
                                    List<Dom.XMLNode> vsErrorData = respChData.getChildren();
                                    List<String> listErrors = new List<String>();
                                    for(Dom.XMLNode childSData : vsErrorData){
                                        if(childSData.getName() == 'error'){                                              
                                            List<Dom.XMLNode> errorData = childSData.getChildren();
                                            String errorStr = '';
                                            for(Dom.XMLNode childScData : errorData){
                                                if(childScData.getName()=='fault-code'){
                                                    errorStr = '<ERR-' + errorCount++ + '>: ';
                                                    errorStr += 'fault-code = ' + childScData.getText() + ' : ';
                                                }
                                                else if(childScData.getName() == 'detail'){
                                                    errorStr += 'detail = ' + childScData.getText();
                                                }
                                            }
                                            if(!String.isBlank(errorStr)){
                                                listErrors.add(errorStr);
                                            }
                                        }
                                    }
                                    if(scobj.Error_Value__c == null){
                                        scobj.Error_Value__c = '';
                                    }
                                    scobj.Error_Value__c += string.join(listErrors,',');
                                }
                                else if(respChData.getName() == 'general-messages'){
                                    List<Dom.XMLNode> vsErrorData = respChData.getChildren();
                                    List<String> listErrors = new List<String>();
                                    for(Dom.XMLNode childSData : vsErrorData){
                                        if(childSData.getName() == 'message'){                                              
                                            String str = '<GENMSG-' + errorCount++ + '>: ';
                                            str += 'message = ' + childSData.getText() + ' : ';
                                            listErrors.add(str);
                                        }
                                    }
                                    if(scobj.Error_Value__c == null){
                                        scobj.Error_Value__c = '';
                                    }
                                    scobj.Error_Value__c += string.join(listErrors,',');
                                }
                            }
                        }
                        else if(searchBody.getName() == 'Fault'){
                            List<Dom.XMLNode> faultData = searchBody.getChildren();
                            String faultStr = '';
                            for(Dom.XMLNode childFaultData : faultData){
                                if(childFaultData.getName()=='faultcode'){
                                    faultStr += (String.isNotBlank(childFaultData.getText())?'Fault-code = ' + childFaultData.getText():'');
                                }
                                if(childFaultData.getName() == 'faultstring'){
                                    faultStr += (String.isNotBlank(childFaultData.getText())?' : Fault String = ' + childFaultData.getText():'');
                                }
                                if(childFaultData.getName() == 'detail'){
                                    faultStr += (String.isNotBlank(childFaultData.getText())?' : Detail = ' + childFaultData.getText():'');
                                }
                            }
                            if(String.isBlank(scobj.Error_Value__c)){
                                scobj.Error_Value__c = faultStr;
                            }
                        }
                    }// Parsing body ends here
                }
            }//Parsing response body for each ends here
            
            if(scobj != null){
                scobj.application__c = applicationId;
                scobj.apipull_datetime__c = System.now();
                scobj.account__c = accountId;
                scobj.Party_Type__c = partyTypeName;
                scobj.Is_Latest__c = true;
                scobj.Response_Status_Code__c=statusCode;
                scobj.Party__c = partyId;
                System.debug(':::Before adding the insert the score seeker::::'+scobj);                        
                
                //update recent Score seeker record's lookup on application
                List<scoreseeker__c> scoreseek = [SELECT id,
                                                         Name,
                                                         Is_Latest__c
                                                    FROM scoreseeker__c
                                                   WHERE application__c =: applicationId
                                                     AND account__c = :accountId
                                                     AND Party_Type__c = :partyTypeName
                                                     AND Is_Latest__c = true
                                                 ];
                if(scoreseek != null && scoreseek.size() > 0){
                    for(scoreseeker__c ss : scoreseek){
                        ss.Is_Latest__c = false;
                    }
                    update scoreseek;
                }
                
                insert scobj;
                
                //insert request and response XML as attachment of Score seeker record
                createAttachment('Request', reqBody, scobj.id);
                createAttachment('Response', response, scobj.id);
            }
            return 'SUCCESS';
        }
        catch(Exception e){
            throw new ints.IntegrationException('[ScoreSeekerAPIAdapter.parseScoreSeekerResponse]Exception: ' +  e.getMessage());
        }
    }
}