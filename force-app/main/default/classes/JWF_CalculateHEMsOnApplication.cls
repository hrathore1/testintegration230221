/**
 * @File Name          : JWF_CalculateHEMsOnApplication.cls
 * @Description        : 
 * @Author             : chirag.gupta@q2.com
 * @Group              : 
 * @Last Modified By   : chirag.gupta@q2.com
 * @Last Modified On   : 6/9/2020, 2:36:32 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    6/8/2020   chirag.gupta@q2.com     Initial Version
**/
public class JWF_CalculateHEMsOnApplication implements IFlowExecution {
    public void execute(Id applicationId,  JWF_Application_Execution_Log__c appExecutionLog){
        try{
            String hemCalcMessage = CalculateServicing.calculateHEM(applicationId);
            appExecutionLog.Message__c += ('HEM Calculation response:' + hemCalcMessage);

            Map<String, Object> mapExecutionLog = new Map<String, Object>();
            mapExecutionLog.put('appExecutionLog', appExecutionLog);

            ApplicationExecutionAction action = new ApplicationExecutionAction(applicationId);
            action.changeStatus(mapExecutionLog);
            action.doInvokeAction(null);                                                 
        }
        catch(Exception e){ExceptionLog.insertExceptionLog(e, 'HEM Calculation failed:::' + e.getMessage()+'---'+e.getStackTraceString(),applicationId);}          
    }
}