/**
 * @File Name          : StageExecutionInterface.cls
 * @Description        : 
 * @Author             : chirag.gupta@q2.com
 * @Group              : 
 * @Last Modified By   : chirag.gupta@q2.com
 * @Last Modified On   : 6/25/2020, 12:35:31 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    6/9/2020   chirag.gupta@q2.com     Initial Version
**/
public interface StageExecutionInterface {
    List<String> executeValidations(Id applicationId);
    String executeNextDeptActions(Id applicationId);
    String executePreviousDeptActions(Id applicationId);
}