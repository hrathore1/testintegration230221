/*********************************************************************
Description: Custom API to create community portal user.
# Input tags:
email
**********************************************************************/
global without sharing class RegisterPortalUserAPI implements clcommon.PortalCustomRemoteAPI1{
    global clcommon.Response invokeAction(String componentStrName,String[] disclosureNames,Map<String, Object> params){
        mfiflexUtil.VOLog voLogInstance = mfiflexUtil.VOLog.getInstance('Genesis');
        clcommon.Response returnRes = new clcommon.Response();
        //Savepoint sp;
        List<Contact> conList;
        list<user> u;
        String uname;
        try{ 
            
            if(params.containsKey('email') && String.isNotBlank((String)params.get('email'))){
                  
                //*****cehcking if user already exits using this username*************
                //if(((String)params.get('email')).containsIgnoreCase('racv')){
                //    uname = (String)params.get('email')+'.app';
                //}else{
                    uname = (String)params.get('email');
                //}
                u=[select Id 
                   from user 
                   where userName=:uname 
                         OR CommunityNickname=:uname];
                
                if(u!=null && u.size()>0){
                    throw new CustomException('User already exist with the email Id.');
                }
               
                
                
                
                         
                conList = [Select id, 
                                  AccountId, 
                                  FirstName, 
                                  LastName, 
                                  Email, 
                                  MobilePhone 
                           from contact 
                           where Email =: (String)params.get('email')];
                           
                           
                if(conList.size()==0 || conList == null){
                    throw new CustomException('No Contact record found');
                }/*else if(conList.size()>1){
                    throw new CustomException('More than one contact record is found against the email id');
                }*/
                else{
                    u=new list<user>{new user(userName=uname,Email= (String)params.get('email'))};
                }
            }else{
                throw new CustomException('Email not found as an input');
            }
            
            if(String.isNotBlank(conList[0].FirstName)){
                u[0].firstname=(conList[0].FirstName).capitalize();
            }else{
                throw new CustomException('First Name is missing.');
            }
            if(String.isNotBlank(conList[0].LastName)){
                u[0].lastName=(conList[0].LastName).capitalize();
            }else{
                throw new CustomException('Last Name is mising');
            }
            
            
            //***********assigning Community Profile ID******************
            try{
                if(params.containsKey('loanChannel') && String.isNotBlank((String)params.get('loanChannel'))){
                    String loanChannel = ((String)params.get('loanChannel')).toUpperCase();
                    List<Profiles_for_external_user__mdt> profiles = [SELECT id,
                                                                             Label,
                                                                             Profile_name__c 
                                                                        FROM Profiles_for_external_user__mdt
                                                                       WHERE Label =: loanChannel];
                    if(profiles != null && profiles.size() > 0 && profiles[0].Profile_name__c != null){
                        List<Profile> profileList = [SELECT ID FROM profile WHERE name=: profiles[0].Profile_name__c limit 1];
                        if(profileList != null && profileList.size() > 0){
                            u[0].profileID  = profileList[0].id;
                        }
                        else{
                            throw new CustomException('No profile found for this loan channel.');
                        }
                    }
                    else{
                        throw new CustomException('No metadata found for this loan channel.');
                    }
                    
                }
                else{
                    throw new CustomException('Loan channel is missing.');
                }
            }catch(Exception e){
                throw new CustomException('Community user profile not found');
            }
            //***********************************************************
            if(String.isNotBlank(conList[0].MobilePhone)){
                u[0].Phone=String.valueOf(conList[0].MobilePhone);
            }
            //u[0].CommunityNickname = uname;
            u[0].ContactId = conList[0].id;
            ID userId;

            String aliasName=conList[0].LastName.capitalize()+conList[0].FirstName.capitalize();
            u[0].Alias = aliasName.substring(0,2);
            u[0].emailencodingkey = 'UTF-8';
            u[0].languagelocalekey = 'en_US';
            u[0].localesidkey = 'en_AU';
            u[0].timezonesidkey = 'Australia/Sydney';
           
            u[0].CommunityNickname = 'User'+System.now().day()+System.now().month()+System.now().year()+
                                      System.now().hour()+System.now().minute()+System.now().second()+System.now().millisecond();
            Id newUserId = Site.createExternalUser(u[0], conList[0].AccountId);
            //email should not be sent if this class is being called from SaveAppRelatedDetailsAPI and sendCustomResetEmail is true
            //sendCustomResetEmail = true means email is sent from PricingDetailsAPI
            if(!params.containsKey('sendCustomResetEmail') || !(Boolean)params.get('sendCustomResetEmail')){
                String hashString = '1000' + String.valueOf(Datetime.now().formatGMT('yyyy-MM-dd HH:mm:ss.SSS'));
                Blob hash = Crypto.generateDigest('MD5', Blob.valueOf(hashString));
                String randomUniquesessionId = EncodingUtil.convertToHex(hash);
                system.debug('random Unique session Id:' + randomUniquesessionId);
                String password = randomUniquesessionId;
                //System.setPassword(existingUser[0].id, password);
                System.resetPasswordWithEmailTemplate(newUserId, true, System.label.CONSUMER_PORTAL_RESET_PASSWORD);
                List<User> userWithRegisterEmail = [SELECT id FROM User WHERE id =: u[0].id];
                if(userWithRegisterEmail != null && userWithRegisterEmail.size() > 0){
                    userWithRegisterEmail[0].Consumer_Portal_Registration_Email_Sent__c = true;
                    Database.update(userWithRegisterEmail[0],false);
                }
            }
            

            map<String,Object> fields=new map<String,Object>();
            fields.put('userId',userId);
            fields.put('contactId',conList[0].Id);
            fields.put('username',uname);
            List<clcommon.PortalCustomRemoteActionRecord> records = new List<clcommon.PortalCustomRemoteActionRecord>();
            records.add ( new clcommon.PortalCustomRemoteActionRecord( userId, fields));
            returnRes =clcommon.PortalActions.getCustomRemoteActionResponse(records);
                
        }catch(Exception e){
            String exMessage = e.getMessage();
            //Database.rollback(sp);
            returnRes.status = clcommon.Constants.ERROR;
            returnRes.errorCode = clcommon.Constants.API_EXCEPTION;
            returnRes.errorMessage = 'Something went wrong.';
            voLogInstance.logException(700, '[RegisterPortalUserAPI.invokeAction] Exception Occured'+e.getLineNumber(), e);
        }
        voLogInstance.commitToDb();
        return returnRes;
        
    }
    /*@future
    public static void SendEmail(String contactId, String accountId, String email){
        Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
        message.setTargetObjectId(contactId); 
        //message.setSenderDisplayName(âCompany Supportâ); 
        //message.setReplyTo(âno-reply@company.comâ);
        //message.setUseSignature(false); 
        //message.setBccSender(false); 
        //message.setSaveAsActivity(false); 
        EmailTemplate emailTemplate = [Select Id,Subject,Description,HtmlValue,DeveloperName,Body 
                                         from EmailTemplate 
                                        where DeveloperName =: System.label.CONSUMER_PORTAL_WELCOME_EMAIL];
        message.setTemplateID(emailTemplate.Id); 
        message.setWhatId(accountId); //This is important for the merge fields in template to work
        message.toAddresses = new String[] {email};
        Messaging.SingleEmailMessage[] messages = new List<Messaging.SingleEmailMessage> {message};
        Messaging.SendEmailResult[] results = Messaging.sendEmail(messages);
    }*/
}