/**************************************************************
 Description:- The callout class is used in skuid for dynamic screen of glassesguide.
***************************************************************/
global with sharing class VehicleSearchService_new{

    webservice static String makeCalloutForGlassesGuide(String url){
        Vehicle_Service__c vs = Vehicle_Service__c.getOrgDefaults();
        String fullUrl = vs.Base_URL_Glasses_Guide__c+url;
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        request.setEndpoint(fullUrl);
        request.setMethod('GET');
        request.setHeader('client_id',vs.client_id__c);
        request.setHeader('client_secret',vs.secret_id__c);
        request.setHeader('X-InitialSystem',vs.X_InitialSystem__c);
        
        HttpResponse response = http.send(request);
        // Parse the JSON response

        System.debug(response.getBody());
        String res = response.getBody();
        return res;
    }
    webservice static String makeCalloutForMotorWeb(String url){
        Vehicle_Service__c vs = Vehicle_Service__c.getOrgDefaults();
        String fullUrl = vs.Base_URL_MotorWeb__c+url;
        System.debug('fullUrl:' + fullUrl);
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        request.setEndpoint(fullUrl);
        request.setMethod('GET');
        request.setHeader('client_id',vs.client_id__c);
        request.setHeader('client_secret',vs.secret_id__c);
        request.setHeader('X-InitialSystem',vs.X_InitialSystem__c);
        
        HttpResponse response = http.send(request);
        // Parse the JSON response

        System.debug(response.getBody());
        String res = response.getBody();
        return res;
    }
}