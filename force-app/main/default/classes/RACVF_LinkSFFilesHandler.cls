public without sharing class RACVF_LinkSFFilesHandler {
    
    public void linkFilestoLibraryFolder(List<ContentDocumentLink> newDocs) {
        
        
        Set<Id> setDocIds = new Set<Id>();
        Set<Id> setKeyRecordIds = new Set<Id>();
        Set<Id> setLinkedEntityIds = new Set<Id>();
        Map<Id, ContentDocumentLink> mapToCategorizeDoc = new Map<Id, ContentDocumentLink>();
        List<ContentDocumentLink> contentDocLinktoInsert = new List<ContentDocumentLink>();
        
        for( ContentDocumentLink contentDocLinkNew : newDocs ){
            setDocIds.add(contentDocLinkNew.ContentDocumentId);
            setLinkedEntityIds.add(contentDocLinkNew.LinkedEntityId);
            
            // Link Document to Category if entityId is genesis__Applications__c
            String sObjName = contentDocLinkNew.LinkedEntityId.getSObjectType().getDescribe().getName();
            System.debug('sObjName 0 ---->'+sObjName);
            if(sObjName == 'genesis__Applications__c'){ 
                contentDocLinktoInsert = this.linkToCaseRelatedList(contentDocLinkNew.LinkedEntityId, setDocIds, contentDocLinktoInsert);
                mapToCategorizeDoc.put(contentDocLinkNew.LinkedEntityId, contentDocLinkNew);
            }
            
        }
        
        for(Id keyRecordId : setLinkedEntityIds){
        
            String sObjName = keyRecordId.getSObjectType().getDescribe().getName();
            System.debug('sObjName---->'+sObjName);
            if(sObjName == 'genesis__Applications__c' || sObjName == 'loan__Loan_Account__c' 
               || sObjName == 'collect__Loan_Account__c' || sObjName == 'Case' 
               || sObjName == 'Deposit_Contract__c'|| sObjName == 'EmailMessage' )
            {
                setKeyRecordIds.add(keyRecordId);
                System.debug('sObjName---->'+sObjName+'----LinkedEntityId---->'+keyRecordId);
            }
        }
  
        List<String> setContractNumber = new List<String>();
        List<Case> caseList;
        if( setKeyRecordIds.size()>0 ){
            
            List<genesis__Applications__c> appList = [Select CRN_Number__c FROM genesis__Applications__c where id IN:setKeyRecordIds];
            
            for( genesis__Applications__c application : appList ){
                
                setContractNumber.add(application.CRN_Number__c);
            }
            
            if( setContractNumber.size() == 0 ){
                
                List<loan__Loan_Account__c> loanList = [Select CRN_Number__c FROM loan__Loan_Account__c where id IN:setKeyRecordIds];
                
                for( loan__Loan_Account__c loan : loanList ){
                    
                    setContractNumber.add(loan.CRN_Number__c);
                }
            }
            
            if( setContractNumber.size() == 0 ){
                
                List<collect__Loan_Account__c> collList = [Select CRN_Number__c FROM collect__Loan_Account__c where id IN:setKeyRecordIds];
                
                for( collect__Loan_Account__c collection : collList ){
                    
                    setContractNumber.add(collection.CRN_Number__c);
                }
            }
            
            if( setContractNumber.size() == 0 ){
                
                List<Deposit_Contract__c> depositList = [Select Contract_Number__c  FROM Deposit_Contract__c where id IN:setKeyRecordIds];
                
                for( Deposit_Contract__c deposit : depositList ){
                    
                    setContractNumber.add(deposit.Contract_Number__c);
                }
            }
            // This is for Case Attachment
            if( setContractNumber.size() == 0 ){
                System.debug('In case ---->');
                caseList = [Select CRN_Number__c,Application__c , Collection_Contract__c ,Loan_Account__c  FROM Case 
                            WHERE Id IN:setKeyRecordIds];
                
                System.debug('In caseList ---->'+caseList.size());
                for( Case caseObj : caseList ){
                     System.debug('In caseObj.CRN_Number__c ---->'+caseObj.CRN_Number__c);
                    setContractNumber.add(caseObj.CRN_Number__c);
                }
            }
            
            // This is for incomming email Attachment
            if( setContractNumber.size() == 0 ){
                System.debug('In Email ---->');
                caseList = [Select CRN_Number__c,Application__c , Collection_Contract__c ,Loan_Account__c  FROM Case 
                            WHERE Id IN(Select ParentId from EmailMessage WHERE Id IN:setKeyRecordIds)];
                
                System.debug('In caseList Email ---->'+caseList.size());
                for( Case caseObj : caseList ){
                     System.debug('In caseObj.CRN_Number__c ---->'+caseObj.CRN_Number__c);
                    setContractNumber.add(caseObj.CRN_Number__c);
                }
            }
            
        }
        
        if( setContractNumber.size()>0 )
        {
            System.debug('setContractNumber---->'+setContractNumber.size());
            String strContractNumber = setContractNumber.get(0);
            System.debug('setContractNumber.get(0)---->'+setContractNumber.get(0));
            
            List<Contract_Document__c> contractDocList = [Select Id, Name From Contract_Document__c WHERE Name=:strContractNumber];
            
            Contract_Document__c  contractDocument;
            if( contractDocList.size() == 0)
            {
                contractDocument = new Contract_Document__c();
                contractDocument.Name = strContractNumber;
                insert contractDocument;
            }
            else
            {
                contractDocument = contractDocList.get(0);
            }
           
            
           
            // Getting Entity Id for Key object from Case
           	Set<Id> entityIds = new Set<Id>();
            if(caseList!=null && caseList.size()> 0)
            {
                for(Case caseObj: caseList){
                    
                    if(!String.isBlank(caseObj.Application__c)){
                        
                        entityIds.add(caseObj.Application__c);
                    }
                    if(!String.isBlank(caseObj.Collection_Contract__c)){
                       entityIds.add(caseObj.Application__c);
                    }
                    if(!String.isBlank(caseObj.Loan_Account__c)){
                        entityIds.add(caseObj.Application__c);
                    }
                }
            }
            
            Map<String, String> contentLinkMap = new Map<String, String>();
            List<ContentDocumentLink> contentLinkList = [SELECT LinkedEntityId, ContentDocumentId FROM ContentDocumentLink WHERE ContentDocumentId IN:setDocIds];
           
            
           for(ContentDocumentLink contentLinked : contentLinkList){
                contentLinkMap.put(contentLinked.LinkedEntityId+','+contentLinked.ContentDocumentId, contentLinked.LinkedEntityId+','+contentLinked.ContentDocumentId);
            }
            
            
            for( Id contentDocId : setDocIds ){
                System.debug(' contentDocId -----> '+contentDocId);
                ContentDocumentLink cdl = new ContentDocumentLink();
                
                if( !contentLinkMap.containsKey( contractDocument.Id+','+contentDocId) ){
                    
                    System.debug(' contentDocId2 -----> '+contentDocId);
                    cdl.ContentDocumentId = contentDocId;
                    cdl.ShareType = 'I';
                    cdl.Visibility = 'AllUsers';
                    cdl.LinkedEntityId = contractDocument.Id; 
                    contentDocLinktoInsert.add(cdl);
                }
            
                for( Id entityId: entityIds ){
                    System.debug(' entityId -----> '+entityId);
                    ContentDocumentLink cdl2 = new ContentDocumentLink();
                    cdl2.ContentDocumentId = contentDocId;
                    cdl2.ShareType = 'I';
                    cdl2.Visibility = 'AllUsers';
                    cdl2.LinkedEntityId = entityId; 
                     
                    // Link Document to Category if entityId is genesis__Applications__c
                    String sObjName = entityId.getSObjectType().getDescribe().getName();
                    if(sObjName == 'genesis__Applications__c'){ 
                        mapToCategorizeDoc.put(entityId, cdl);
                     
                    }else {
                        contentDocLinktoInsert.add(cdl2);
                    }
                    
                }
            }
            
            System.debug('contentDocLinktoInsert.size()----->'+ contentDocLinktoInsert.size());
            System.debug('contentDocLinktoInsert----->'+ contentDocLinktoInsert);
            if( contentDocLinktoInsert.size()>0)
            	RACVF_ContentDocumentLink_TriggerHelper.shareFile(contentDocLinktoInsert); 
            
            	for (Id ApplicationId : mapToCategorizeDoc.keySet())
				{
                    System.debug(ApplicationId);
                    System.debug(mapToCategorizeDoc.get(ApplicationId));
                    this.linkDocumentToCategory(ApplicationId, mapToCategorizeDoc.get(ApplicationId));
                }
        }
    }
    
    private void linkDocumentToCategory( Id ApplicationId, ContentDocumentLink conDocLink )
    {
        List< clcommon__Document_Category__c > dcList = [SELECT Id, Name , clcommon__Category_Name__c from clcommon__Document_Category__c 
                                                         WHERE genesis__Application__c =:ApplicationId AND clcommon__Category_Name__c='Uncategorised'];
        List<Id> conDocLinkIds = new List<Id>();
        if(dcList.size()>0){
            conDocLinkIds.add(conDocLink.Id);
            if(conDocLinkIds.size()>0){
                genesis.Response result = genesis.ApplicationDocumentManagement.linkAttachments(dcList[0].id, conDocLinkIds );
                System.debug('result====='+ result);
            }
            else {
                throw new CustomException('No attachment Id found');
            }
            
        } 
        else {
            throw new CustomException('No Document category found');
        }                      
    }
    
    private List<ContentDocumentLink> linkToCaseRelatedList( Id parentRecordId, Set<Id> setDocIds, List<ContentDocumentLink> contentDocLinktoInsert )
    {
        System.debug('linkToCaseRelatedList---->');
        try{
            
            Case objCase = [Select Id FROM Case WHERE Application__c =:parentRecordId ];
            
            if( objCase != null ) {
                
                for ( Id docId : setDocIds ) {
                    
                    ContentDocumentLink cdl = new ContentDocumentLink();
                    cdl.ContentDocumentId = docId;
                    cdl.ShareType = 'I';
                    cdl.Visibility = 'AllUsers';
                    cdl.LinkedEntityId = objCase.Id; 
                    contentDocLinktoInsert.add(cdl);
                    
                }
                
           }
         }Catch(Exception exp){
            
            System.debug('linkToCaseRelatedList---Exceptin ----> '+exp.getStackTraceString());
        }
        
        return contentDocLinktoInsert;
        
        
    }
    
    
}