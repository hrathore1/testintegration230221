/**
 * @description       : 
 * @author            : chirag.gupta@q2.com
 * @group             : 
 * @last modified on  : 09-28-2020
 * @last modified by  : chirag.gupta@q2.com
 * Modifications Log 
 * Ver   Date         Author                Modification
 * 1.0   09-28-2020   chirag.gupta@q2.com   RFCL-2888
**/
public class HEMCalculator {
    
    public HEMCalculator(){}
    
    /*
     * This method returns the HEM value basis the input parameters passed.
     */
    public static Decimal getHEMValue(String stateCode,
                                      String postCode,
                                      String incomeUnit,
                                      Decimal grossAnnualIncome,
                                      Integer numberOfDependents,
                                      Decimal borrowerSharePercentage) {

        if(String.isBlank(stateCode)){
            throw new MyException('State code is not provided');
        }

        if(String.isBlank(postCode)){
            throw new MyException('Postcode is not provided');
        }

        if(String.isBlank(incomeUnit)){
            throw new MyException('Income unit is not provided');
        }

        if(grossAnnualIncome == null || grossAnnualIncome == 0){
            throw new MyException('Invalid value for gross annual income.');
        }

        if(borrowerSharePercentage == null){
            borrowerSharePercentage = 0;
        }
        
        String noOfDependents = '0';
        //Integer noOfRemainingDependents = 0;
        Integer maxDependents = genesis__Org_parameters__c.getOrgDefaults().HEM_MaxDependents__c == null ? 0 : Integer.valueOf(genesis__Org_parameters__c.getOrgDefaults().HEM_MaxDependents__c);
        if(numberOfDependents == null){
            throw new MyException('Invalid value for number of dependents.');
        }
        else if(numberOfDependents < maxDependents){
            noOfDependents = String.valueOf(numberOfDependents);
        }
        else if(numberOfDependents >= maxDependents){
            noOfDependents = '>=' + maxDependents;
            //noOfRemainingDependents = numberOfDependents - 3;
        }
        
        //List<String> listCoupleValues = System.label.INCOME_UNIT_COUPLE.split(',');
        //String incomeUnit = listCoupleValues.contains(maritalStatus) ? 'Couple' : 'Single';
        
        //Query on postcode region using passed postcode
        List<Postcode_Region_Mapping__c> postCodeList = [SELECT id,
                                                                Name,
                                                                State__c,
                                                                Postcode__c,
                                                                Region_Name__c 
                                                           FROM Postcode_Region_Mapping__c
                                                          WHERE Postcode__c = :postCode
                                                            AND State__c = :stateCode
                                                        ];

        if(postCodeList == null || postCodeList.size() == 0){
            throw new MyException('Postcode value not available for passed values.');
        }
        
        Set<String> setRegionNames = new Set<String>();
        for(Postcode_Region_Mapping__c pcrm : postCodeList){
            setRegionNames.add(pcrm.Region_Name__c);
        }
        //String regionName = postCodeList.get(0).Region_Name__c;

        List<HEM_Value__c> hemList = [SELECT id,
                                             Name,
                                             Dependents__c,
                                             HEM_Value__c,
                                             Marital_Status__c,
                                             Maximum_Income__c,
                                             Minimum_Income__c,
                                             Region_Name__c,
                                             HEM_Living_Expenses_Result__c
                                        FROM HEM_Value__c 
                                       WHERE Dependents__c = :noOfDependents
                                         AND Marital_Status__c = :incomeUnit
                                         AND Region_Name__c IN :setRegionNames
                                         AND Maximum_Income__c >= :grossAnnualIncome
                                         AND Minimum_Income__c <= :grossAnnualIncome
                                       ORDER BY HEM_Living_Expenses_Result__c DESC
                                       LIMIT 1
                                     ];
            
        if(hemList != null && hemList.size() == 1){
            //Decimal amtToAdd = 0;
            //amtToAdd = HEM_Configuration__c.getOrgDefaults().Amount_to_add__c != null ? 
            //           HEM_Configuration__c.getOrgDefaults().Amount_to_add__c : 
            //           100;
                
            //amtToAdd *= noOfRemainingDependents;
            if(incomeUnit.equalsIgnoreCase('Single')){
                return (hemList.get(0).HEM_Living_Expenses_Result__c);// + amtToAdd);
            }
            else{
                Decimal sharePercentage = (borrowerSharePercentage == 0 ? 100 : borrowerSharePercentage);
                return ((sharePercentage * hemList[0].HEM_Living_Expenses_Result__c)/100);// + amtToAdd;
            }
        }
        else{
            throw new MyException('HEM Value not present for passed values');
        }
    }

    public class MyException extends Exception{}
}