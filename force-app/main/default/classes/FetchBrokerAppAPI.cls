/**
 * Company: CloudKaptan Consultancy Services Pvt. Ltd.
 * Description: This API is used to fetch all the application related to the logged in broker according to the hierarchy.
 * Developer : Atanu Saha.
 * Created Date : 10/12/2020
 * Last Modified By : Atanu Saha.
 * Last Modified Date : 11/12/2020
 */
 
global class FetchBrokerAppAPI
{
    //Roles
    public static final string PARENT_RELATION = 'Parent';
    public static final string GRANDPARENT_RELATION = 'Grandparent';
    public static final string CHILD_RELATION = 'Child';
    
    //Party Type
    public static final string BROKER_PARTY = 'BROKER';
    public static final string ORIGINATOR_PARTY = 'ORIGINATOR';
    
    //Custom Exception Messages
    public static final string RELATIONSHIP_FOUND_NULL = 'No related account found';
    public static final string ROLE_NOT_FOUND = 'Hierarchy role not found';

    //method to fetch the application list
    public static Set<Id> getApplications(Id accountId)
    {
        mfiflexUtil.VOLog voLogInstance = mfiflexUtil.VOLog.getInstance('Genesis');
        try
        {    
            Map<Id,genesis__Applications__c> appMap = new  Map<Id,genesis__Applications__c>();
            
            //List to store all role name in which to search for
            List<String> relationName = new List<String>{GRANDPARENT_RELATION,PARENT_RELATION,CHILD_RELATION};
            // List ti store all party type name in which to search for
            List<String> partyTypeName = new List<String>{BROKER_PARTY,ORIGINATOR_PARTY};
            
            //find the related account to the logged in user
            List<clcommon__Relationship__c> relatedRelationshipList = [SELECT Id,
                                                                              clcommon__Entity__c,
                                                                              clcommon__Related_Entity__c,
                                                                              clcommon__Entity_Role__c
                                                                       FROM clcommon__Relationship__c
                                                                       WHERE clcommon__Entity_Role__c IN: relationName
                                                                       AND clcommon__Related_Entity__c =: accountId];
            
            //according to new broker hierarchy model
            if(relatedRelationshipList.size()>0)
            {
                Id seachAccountId = relatedRelationshipList[0].clcommon__Entity__c;
                
                //geting apps based on Grandparent, parent or Child account Id
                appMap = new Map<Id,genesis__Applications__c>([SELECT Id,
                                                                      Grandparent_Broker__c,
                                                                      Parent_Broker__c,
                                                                      Broker_Originator__c
                                                               FROM genesis__Applications__c
                                                               WHERE Grandparent_Broker__c =: seachAccountId
                                                               OR Parent_Broker__c =: seachAccountId
                                                               OR Broker_Originator__c =: seachAccountId]); 
            }
            //according to old model
            else
            {
                //getting apps based on broker or originator party type attached to application
                appMap = new Map<Id,genesis__Applications__c>([SELECT Id,
                                                                      Grandparent_Broker__c,
                                                                      Parent_Broker__c,
                                                                      Broker_Originator__c
                                                               FROM genesis__Applications__c
                                                               WHERE Id IN (SELECT genesis__Application__c
                                                                            FROM clcommon__Party__c
                                                                            WHERE clcommon__Account__c =: accountId
                                                                            AND clcommon__Type__r.Name IN: partyTypeName)]);
                  
                //throw new CustomException(RELATIONSHIP_FOUND_NULL);
            }
            
           //returning the id of the applications 
           return appMap.keyset(); 
        }
        catch(Exception e) 
        {
            voLogInstance.logException(1001, '[FetchBrokerAppAPI.getApplications] Error: ' + e.getMessage() + ' (at line: ' + e.getLineNumber() + ')', e);
            voLogInstance.commitToDb();
            return null;
        }                                                                                           
    }
}