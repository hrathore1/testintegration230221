@isTest
public class DataMapperInitiatorJobSchedulesTest {
       public static clcommon__Chart_Of_Account__c createChartOfAccount(String chartOfAccountName,String chartOfAccountCode,String coaType,boolean commitToDb) {

        clcommon__Chart_Of_Account__c rec = new clcommon__Chart_Of_Account__c(Name=chartOfAccountName,
                                                          clcommon__Chart_Of_Account_Code__c = chartOfAccountCode,
                                                          clcommon__Type__c = coaType);

        if(commitToDb) {
           //insertRecords(rec);
           insert rec ;
        }

        return rec;
        }
    
        public static Map<String,clcommon__Chart_Of_Account__c> createChartOfAccountsWithCode(Map<String,String> codeNameMap,String coaType)  {
        Map<String,clcommon__Chart_Of_Account__c> retVal = new Map<String,clcommon__Chart_Of_Account__c>();

        for(String code : codeNameMap.keyset()) {
            retVal.put(code,createChartOfAccount(codeNameMap.get(code),code,coaType,false));
        }
        system.debug('retVal.values()====='+retVal.values());
        insert retVal.values();
        return retVal;
            
        }   
    
    @testSetup
    public static void createSeedData() {
    
    
        loan.TestHelper.createSeedDataForTesting();      
             loan__Org_Parameters__c org_parameter = new loan__Org_Parameters__c(
                SetupOwnerId = UserInfo.getOrganizationId(),
                loan__Adjust_Rounding_in_last_Payment__c = true,
                loan__Check_Teller_Balance_before_EOD__c = false,
                loan__Concurrent_BatchJobs__c = 1.0,
                loan__Custom_Fee_Calculator_Class__c = 'ATH_FeeCalculator',
                loan__Default_Tolerance_Rate__c = 0.0,
                loan__Digits_After_Decimals__c = 2.0,
                loan__Disable_Rounding_In_Amz_Schedule__c = false,
                loan__Disable_Triggers__c = false,
                loan__Draw_Amortization_Schedule__c = true,
                loan__Enable_Bank_Trust_Account_Validation__c = false,
                loan__Enable_Fractionalization__c = false,
                loan__Enable_Griaule_Biometrics__c = false,
                loan__Enable_M2SYS_Biometrics__c = false,
                loan__Enable_New_Spread_Option__c = true,
                loan__Enable_Teller_Management__c = false,
                loan__Exclude_Delinq_Grace_Days_In_Aging__c = false,
                loan__Final_Payment_Differ__c = true,
                loan__FinancialCalculatorVersion__c = 3.1,
                loan__Generate_Inv_AMZ_Schedule_on_Reschedule__c = true,
                loan__Integrate_Salesforce_CRM__c = true,
                loan__Interest_Accrual_Thresold_days__c = 90.0,
                loan__Keep_Same_Maturity_Date_on_FIT_Disbursal__c = true,
                loan__Log_Level__c = 'ERROR',
                loan__Mint_Theme__c = false,
                loan__Namespace_Prefix__c = 'loan',
                loan__Production__c = false,
                loan__Rounding_Mode__c = 'NEAREST',
                loan__Shift_Date_Before_Holiday__c = false,
                loan__Skip_Holidays_For_LF_Grace_Period__c = false,
                loan__Skip_Holidays__c = false,
                loan__Track_Account_History__c = false,
                loan__Track_Client_History__c = false,
                loan__Enable_Unit_Of_Work__c = true,
                peer__Auto_Invest_Functionality__c = false,
                peer__Disable_SMS_Verification__c = false,
                peer__Max_SMS__c = 20.0,
                peer__Max_Sms_Retry_Count__c = 5.0,
                peer__SMS_Send_Time_Limit__c = 30.0,
                peer__SMS_Timeout__c = 30.0,
                peer__Service_Fee_during_IO_Sale__c = false,
                //loan__Disable_Maintain_Delinquency_Reschedule__c = false,
                //ATH_Athena_Settlement_ID__c = '123',
                Id = [SELECT Id from loan__Org_Parameters__c ].Id);  
            loan__Currency__c dummyCurr = loan.TestHelper.createCurrency();
            loan__Office_Name__c dummyOffice = loan.TestHelper.createOffice();
            loan__MF_Account__c dummyIncAccount = loan.TestHelper.createMFAccount('DummyIncAccount','30000 - INCOME');
            loan__MF_Account__c dummyMFAccount = loan.TestHelper.createMFAccount('DummyMFAccount','10000 - ASSETS');
            loan__Fee__c dummyFee = loan.TestHelper.createFee(dummyCurr,dummyIncAccount ,dummyMFAccount);                                    
            loan__Fee_Set__c dummyFeeSet = loan.TestHelper.createFeeSet();
            loan__Fee_Junction__c dummyFeeJunction = loan.TestHelper.createFeeJunction(dummyFee,dummyFeeSet);
            loan__Loan_Purpose__c dummyLoanPurpose = loan.TestHelper.createLoanPurpose(); 
            loan__Loan_Product__c dummyLP = createLoanProductwithProductType
            ('Dummy Product', dummyOffice, dummyMFAccount, dummyCurr, dummyFeeSet,'Flat', 3, 1, null, loan.LoanConstants.AMZ);                                                 
            dummyLP.loan__Loan_Product_Type__c = 'Line of Credit';
            dummyLP.loan__Min_Loan_Amount__c = 100;
            dummyLP.loan__Max_Loan_Amount__c = 250000;
            
            update dummyLP;
        //Create Chart Of Accounts.
        Map<String,String> coaCodeName = new Map<String,String>();
        coaCodeName.put('10000','Product Control Account');
        coaCodeName.put('20000','Interest Income Account');
        coaCodeName.put('30000','Loan Loss Provision Account');
        coaCodeName.put('40000','Fee Income Account');
        coaCodeName.put('50000','Write-Off Recovery Account');

        Map<String,clcommon__Chart_Of_Account__c> chartOfAccounts = createChartOfAccountsWithCode(coaCodeName,clcommon.Constants.COA_ASSET);

        //Create Mapping Configuration for Transaction__c & Accrual_Entry__c objects using JSON stored in static resource
        StaticResource jsonResource = [SELECT Id,Body FROM StaticResource WHERE Name = 'Test_DataMapping_Config'];
        String jsonBody = jsonResource.body.toString();

        //replace COA codes with SFIDs in JSON
        jsonBody = jsonBody.replaceAll('10000',chartOfAccounts.get('10000').ID);
        jsonBody = jsonBody.replaceAll('20000',chartOfAccounts.get('20000').ID);
        jsonBody = jsonBody.replaceAll('30000',chartOfAccounts.get('30000').ID);
        jsonBody = jsonBody.replaceAll('40000',chartOfAccounts.get('40000').ID);

        System.debug(LoggingLevel.INFO,'JSON '+jsonBody);
        List<clcommon.DataMappingBusinessObject> dmObjects = (List<clcommon.DataMappingBusinessObject>)JSON.deserializeStrict(jsonBody,List<clcommon.DataMappingBusinessObject>.class);

        clcommon.MappingTemplateBuilder builder = new clcommon.MappingTemplateBuilder(dmObjects);
        builder.build();
        
        //Create Transaction records
        List<clcommon__Transaction__c> transactions = new List<clcommon__Transaction__c>();
        clcommon__Transaction__c txn;
        for(Integer i=0; i<2000; i++) {
            txn = new clcommon__Transaction__c();
            txn.clcommon__Debits__c = i+1;
            txn.clcommon__Credits__c = i+1;
            txn.clcommon__Transaction_Type__c = '';

            transactions.add(txn);
        }

        insert transactions;
    }
    @isTest static void testAccounting() {
        Test.startTest();
        
        DataMapperInitiatorJobSchedules sh1 = new DataMapperInitiatorJobSchedules();
        String sch = '0 0 23 * * ?'; 
        system.schedule('Test Territory Check', sch, sh1); 
        
        Test.stopTest();
        
    }
    
    /*@isTest static void testAccural() {
        Test.startTest();
        
        ATH_AccrualEntryJobSchedules sh1 = new ATH_AccrualEntryJobSchedules();
        String sch = '0 0 23 * * ?'; 
        system.schedule('Test Territory Check', sch, sh1); 
        
        Test.stopTest();
        
    }*/
    
    @TestVisible
    public static loan__Loan_Product__c createLoanProductwithProductType(
            String name,
            loan__Office_Name__c dummyOffice,
            loan__MF_Account__c dummyAccount,
            loan__Currency__c curr,
            loan__Fee_Set__c dummyFeeSet,
            String billingMethod,
            Integer defaultNoOfInstallments,
            Decimal defaultInterestRate,
            String loanCycleSetup,
            String productType) {
        if (loanCycleSetup == null || loanCycleSetup == '')
            loanCycleSetup = loan.LoanConstants.LOAN_ATTRIBUTES_SAME_FOR_ALL_LOANS;

        loan__Loan_Product__c dummyLP = new loan__Loan_Product__c(Name = name,
                loan__Loan_Product_Type__c = 'Loan',
                loan__Loan_Cycle_Setup__c = loanCycleSetup,
                loan__Currency__c = curr.ID,
                loan__Fee_Set__c = dummyFeeSet.ID,
                loan__Accrual_Based_Accounting__c = true,
                loan__Accrue_Income_On_Suspended_Account__c = true,
                loan__Approval_Date__c =  Date.today().addDays(-66),
                loan__Cash_Based_Accounting__c = false,
                loan__Default_Interest_Rate__c = defaultInterestRate,
                loan__Default_Number_of_Installments__c = defaultNoOfInstallments,
                loan__Interest_Rate_Type__c = billingMethod,
                loan__Maximum_Gap_Between_Installments__c = 1,
                loan__Minimum_Gap_Between_Installments__c = 1,
                loan__Max_Interest_Rate__c = defaultInterestRate + 5,
                loan__Max_Loan_Amount__c = 100000,
                loan__Max_Number_of_Installments__c = defaultNoOfInstallments + 6,
                loan__Max_Overdue_Interest_Rate__c = 24,
                loan__Minimum_Installment_Amount__c = 100,
                loan__Min_Interest_Rate__c = 1,
                loan__Min_Loan_Amount__c = 1000,
                loan__Min_Number_of_Installments__c = 0,
                loan__Min_Overdue_Interest_Rate__c = 10,
                loan__Product_Interest_Income_Account__c = dummyAccount.ID,
                loan__Product_Int_On_Overdue_Income_Acc__c = dummyAccount.ID,
                loan__Product_Loan_Control_Account__c = dummyAccount.ID,
                loan__Product_Loan_Loss_Provision_Account__c = dummyAccount.ID,
                loan__Product_Loan_Loss_Reserve_Account__c = dummyAccount.ID,
                loan__Product_Overdue_Interest_Account__c = dummyAccount.ID,
                loan__Product_Suspended_Interest_Account__c = dummyAccount.ID,
                loan__Product_Suspended_Int_On_Overdue_Acc__c = dummyAccount.ID,
                loan__Product_Write_Off_Recovery_Account__c = dummyAccount.ID,
                loan__Product_Excess_Account__c = dummyAccount.ID,
                loan__Recalculate_Interest_Flag__c = true,
                loan__Status__c = 'Active'//,
                //ATH_Redraw__c = true 
        );
        System.debug('dummyLP: ' + dummyLP.RecordTypeId);
        ID rt = null;
        if (productType.equals(loan.LoanConstants.Loan)) {
            rt = [SELECT ID, Name FROM RecordType WHERE DeveloperName = 'Loan_Product_Record_Type' AND SObjectType = 'loan__Loan_Product__c'].Id;
        } else if (productType.equals(loan.LoanConstants.AMZ)) {
            rt = [SELECT ID, Name FROM RecordType WHERE DeveloperName = :loan.LoanConstants.AMZ_PRODUCT_RT AND SObjectType = 'loan__Loan_Product__c'].Id;
        } else if (productType.equals(loan.LoanConstants.LOC)) {
            rt = [SELECT ID, Name FROM RecordType WHERE DeveloperName = 'Line_Of_Credit_Product_Record_Type'].Id;
        } else if (productType.equals(loan.LoanConstants.MCA)) {
            rt = [SELECT ID, Name FROM RecordType WHERE DeveloperName = 'Merchant_Cash_Advance'].Id;
        }

        dummyLP.RecordTypeId = rt;
        insert dummyLP;

        //Associate loan product with branch and create rules
        loan__Branch_Loan_Product__c dummyBLP = new loan__Branch_Loan_Product__c(loan__Branch__c = dummyOffice.ID,
                loan__Loan_Product__c = dummyLP.ID);

        insert dummyBLP;

        return dummyLP;
    }

}