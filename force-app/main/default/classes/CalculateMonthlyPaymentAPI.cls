global without sharing class CalculateMonthlyPaymentAPI{
    
    public Decimal calcMonthlyPayment(String freq, String liabType, Decimal repaymentAmount, Decimal facilityLimit, Decimal balance){
        String frequency = freq != null ? freq : '';
        if('yearly'.equalsIgnoreCase(freq)){
            frequency = 'Annual';
        }
        
        Decimal remTermInMonthsCS = genesis__Org_Parameters__c.getOrgDefaults().Liability_Remaining_Term_in_Months__c != null ?
                                    genesis__Org_Parameters__c.getOrgDefaults().Liability_Remaining_Term_in_Months__c :
                                    0;
        Decimal liabIntRateDefaultCS = genesis__Org_Parameters__c.getOrgDefaults().Liability_Interest_Rate_Default__c != null ?
                                       genesis__Org_Parameters__c.getOrgDefaults().Liability_Interest_Rate_Default__c :
                                       0;
        Decimal liabIntRateCS = genesis__Org_Parameters__c.getOrgDefaults().Liability_Interest_Rate__c != null ?
                                genesis__Org_Parameters__c.getOrgDefaults().Liability_Interest_Rate__c :
                                0;
        Decimal finalCalcTermInMonths = 0;
        Decimal finalCalcInterest = 0;
        Decimal finalCalcBalance = 0;
        Decimal returnMonthlyRepayment = 0;
        CalculatePricing calcPrice = new CalculatePricing();
        if('OVERDRAFT'.equalsIgnoreCase(liabType) || 'CREDIT CARD'.equalsIgnoreCase(liabType)){
            finalCalcTermInMonths = remTermInMonthsCS;
            finalCalcInterest = liabIntRateDefaultCS;
            finalCalcBalance = facilityLimit;
            if(finalCalcTermInMonths > 0 
               && finalCalcInterest > 0
               && finalCalcBalance > 0
               && !String.isBlank(frequency)){
                returnMonthlyRepayment = calcPrice.calculateEMIAmount(finalCalcBalance,
                                                                          finalCalcInterest,
                                                                          finalCalcTermInMonths,
                                                                          frequency
                                                                         );
            }
            else{
                returnMonthlyRepayment = repaymentAmount;
            }
        }
        else{
            returnMonthlyRepayment = repaymentAmount;
        } 
        return returnMonthlyRepayment; 
    }
    
}