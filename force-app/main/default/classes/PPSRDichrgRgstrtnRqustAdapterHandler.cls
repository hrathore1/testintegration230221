global class PPSRDichrgRgstrtnRqustAdapterHandler extends ints.AbstractMockAdapter{
    public static String CollateralId{get;set;}
    public static String CollateraLeinlId{get;set;}
    public static String requestBody = '';
   
    global override ints.HttpRequestObject createRequest(Map<String, Object> getRqstSrchCrtfctRqustValueMap, 
                                                         ints.IntegrationConfigurationDTO IntegrationConfiguration){
                                                             
        System.debug(':::::Inside the getRqstSrchCrtfctRqustValueMap:::::getRqstSrchCrtfctRqustValueMap data is:::' + getRqstSrchCrtfctRqustValueMap);
                                                             
        try{
            if(getRqstSrchCrtfctRqustValueMap != null){
                ints.HttpRequestObject httpReq = new ints.HttpRequestObject();
               
                httpReq.endpoint = 'callout:'+ IntegrationConfiguration.apiNamedCredential;
                httpReq.timeout = 60000 ;
                httpReq.method = 'POST';    
                                                                     
                /************************Creating and assigning header values******************************/
                Map<String,String> headerMap = new Map<String,String>();
                headerMap.put('Content-type','text/xml');
                httpReq.headerMap = (headerMap);
                                                                     
                /****************************Constructing request body************************************/
                
                String requestBody = constructRequestHandler(getRqstSrchCrtfctRqustValueMap);
                System.debug('requestBody ' + requestBody);
                //createAttachment('REQ', requestBody);
                if(requestBody != null && String.isNotBlank(requestBody)){
                    httpReq.body = requestBody;
                    System.debug('<< PPSRDichrgRgstrtnRqustAdapterHandler.createRequest>> requestBody ' + requestBody);
                    System.debug('HTTP :: ' + httpReq);
                    //return null;
                    return httpReq ;
                }
                else{
                    throw new ints.IntegrationException('[PPSRDichrgRgstrtnRqustAdapterHandler.createRequest] Generated request is blank.');
                }
            }
            else{
                throw new ints.IntegrationException(' [PPSRDichrgRgstrtnRqustAdapterHandler.createRequest] Scoreseeker request map in blank.');
            }
        }
        catch(Exception e){
            throw new ints.IntegrationException('[PPSRDichrgRgstrtnRqustAdapterHandler.createRequest] Exception :'+e.getMessage() + ' Line ' + e.getLineNumber());
        }                                                         
    }
    
    global override ints.IntegrationResponseObject parseResponse(HttpResponse response, ints.ThirdPartyRequest thirdPartyRequest){
        IntegrationAPIResponseParser respParser = new IntegrationAPIResponseParser();
        ints.IntegrationResponseObject integrationRes;
        
        String resp = response.getBody();
        System.debug('=====RESPONSE======'+resp);
        //createAttachment('RES', String.valueOf(response));
        if(response.getStatusCode() != 200){
            throw new ints.IntegrationException('Status code is: ' + response.getStatusCode());
        }
        else{
            if(resp != null && String.isNotBlank(resp)){
                String parserResponse = parsePPSRResponse(resp);
                //String parserResponse = responseXmlParseDML(resp);
                System.debug(':::parserResponse:::' + parserResponse);
                if(parserResponse != 'SUCCESS'){
                    //throw new ints.IntegrationException(parserResponse);
                }
            }
            else{
                throw new ints.IntegrationException('Response Body is null or blank.');
            }
        }
        
        system.debug('::::respParserrespParser:::'+respParser);
        
        integrationRes = respParser;
        return integrationRes;
    }
    
    global static String constructRequestHandler(Map<String, Object> getRqstSrchCrtfctRqustValueMap){
        try{
            System.debug('<<PPSRDichrgRgstrtnRqustAdapterHandler.constructRequest>> getRqstSrchCrtfctRqustValueMap ' + getRqstSrchCrtfctRqustValueMap);
            String Token;
            Long randomLong = Crypto.getRandomLong();
            
            String createdDateTimeString = datetime.now().format('yyyy-MM-dd\'T\'HH:mm:ss.SSS\'Z\'');
            String TargetEnvironment = isBlankCheck((String)getRqstSrchCrtfctRqustValueMap.get('TargetEnvironment'),'TargetEnvironment');
            //String CollateralLeinTableTag = isBlankCheck((String)getRqstSrchCrtfctRqustValueMap.get('PPSRRegistrationInformationTableTag'),'PPSRRegistrationInformationTableTag');
            String PPSRPasswordHistoryTag = isBlankCheck((String)getRqstSrchCrtfctRqustValueMap.get('PPSRPasswordHistoryTag'),'PPSRPasswordHistoryTag');
            String PPSRRegistrationInformationTableTag = isBlankCheck((String)getRqstSrchCrtfctRqustValueMap.get('PPSRRegistrationInformationTableTag'),'PPSRRegistrationInformationTableTag');
            
            //'Org Parameter' Custom Setting for Access code.
            String SecurePartyGroupNumber = isBlankCheck(genesis__Org_Parameters__c.getOrgDefaults().PPSRSecuredPartyGroupNumber__c ,'SecurePartyGroupNumber');
			String ppsrPartyGroupAccessCodeString = isBlankCheck(genesis__Org_Parameters__c.getOrgDefaults().PPSR_Party_Group_AccessCode__c,'Party Group Access Code');
            
            String customerRequestMessageIdString = '' + Integer.valueOf(Math.random() * 10000000);
            Token=  EncodingUtil.base64Encode(Blob.valueOf(String.valueOf(randomLong)));
            
            //Dynamic data
            List<Map<String, Object>> PPSRRegistrationInformationList = (List<Map<String, Object>>) getRqstSrchCrtfctRqustValueMap.get(PPSRRegistrationInformationTableTag);
            List<Map<String, Object>> PPSRPasswordHistoryList = (List<Map<String, Object>>) getRqstSrchCrtfctRqustValueMap.get(PPSRPasswordHistoryTag);
            
            System.debug(' PPSRRegistrationInformationList ' +PPSRRegistrationInformationList);
            Map<String, Object> PPSRRegistrationInformation = PPSRRegistrationInformationList.get(0);
            Map<String, Object> PPSRPasswordHistoryInfo = PPSRPasswordHistoryList.get(0);
           
            
            //CollateralId = isBlankCheck((String)PPSRRegistrationInformation.get('CollateralId'),'CollateralId');
            CollateraLeinlId = isBlankCheck((String)PPSRRegistrationInformation.get('ppsrCollateralId'),'ppsrCollateralId');
            String registrationNumberString = isBlankCheck((String) PPSRRegistrationInformation.get('ppsrCollateralRegirationNumber'),'ppsrCollateralRegirationNumber');
            String changeNumberString = isBlankCheck((String) PPSRRegistrationInformation.get('ppsrCollateralChangeNumber'),'ppsrCollateralChangeNumber');
            
            System.debug('registrationNumberString '+ registrationNumberString);
            System.debug('changeNumberString '+ changeNumberString);
            
            String Usernamestr = isBlankCheck((String) PPSRPasswordHistoryInfo.get('Username'),'Username');
            String Passwordstr = isBlankCheck((String) PPSRPasswordHistoryInfo.get('Password'),'Password');

            //String rgstrtnNmbrStrg = isBlankCheck((String) getRqstSrchCrtfctRqustValueMap.get('RegistrationNumber'),'RegistrationNumber');
            //String chngNmbrStrg = isBlankCheck((String) getRqstSrchCrtfctRqustValueMap.get('ChangeNumber'),'ChangeNumber');
           
            String s = '';
                        
            s= '<?xml version="1.0" encoding="UTF-8"?>';
            s+='<soap:Envelope xmlns:soap="http://www.w3.org/2003/05/soap-envelope" xmlns:data="http://schemas.ppsr.gov.au/2016/05/data" xmlns:data1="http://schemas.ppsr.gov.au/2011/04/data" xmlns:ser="http://schemas.ppsr.gov.au/2016/05/services">';
            s+=   '<soap:Header>';
            s+=      '<wsse:Security xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd" xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd" soap:mustUnderstand="true">';
            s+=         '<wsse:UsernameToken wsu:Id="UsernameToken-FE487D5534F2EDDE1515970361427691">';
            s+=            '<wsse:Username>'+Usernamestr+'</wsse:Username>';
            s+=            '<wsse:Password Type="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText">'+Passwordstr+'</wsse:Password>';
            s+=            '<wsse:Nonce EncodingType="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-soap-message-security-1.0#Base64Binary">'+Token+'</wsse:Nonce>';
            s+=            '<wsu:Created>'+createdDateTimeString+'</wsu:Created>';
            s+=         '</wsse:UsernameToken>';
            s+=      '</wsse:Security>';
            s+=      '<ser:TargetEnvironment>'+TargetEnvironment+'</ser:TargetEnvironment>';
            s+=   '</soap:Header>';
            s+=   '<soap:Body>';
            s+=       '<ser:DischargeRegistrationRequestMessage>';
            s+=         '<ser:DischargeRegistrationRequest>';
            s+=				'<data:CustomersRequestMessageId>' + customerRequestMessageIdString + '</data:CustomersRequestMessageId>';
            s+=				'<data:CustomersUserDefinedFields xsi:nil="true" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"/>';
            s+=				'<data:ChangeNumber> ' + changeNumberString + '</data:ChangeNumber>';
            s+=				'<data:RegistrationNumber> ' + registrationNumberString + '</data:RegistrationNumber>';
            s+=	            '<data:SecuredPartyGroupAccessCode>' + ppsrPartyGroupAccessCodeString + '</data:SecuredPartyGroupAccessCode>';
            s+=	            '<data:SecuredPartyGroupNumber>' + SecurePartyGroupNumber + '</data:SecuredPartyGroupNumber>';
            s+=			'</ser:DischargeRegistrationRequest>';
            s+=	      '</ser:DischargeRegistrationRequestMessage>';  
            s+=   '</soap:Body>';
            s+='</soap:Envelope>';
            
            System.debug('SOAP BODY===' + s);
            
            return s;
        }catch(Exception exp){
            System.debug('<<PPSRDichrgRgstrtnRqustAdapterHandler.constructRequest>> Error Message '+ exp.getMessage() + ' @ ' + exp.getLineNumber());
            throw new ints.IntegrationException(exp.getMessage() + ' Line ' + exp.getLineNumber());
        }
      
    }
    
    public static String isBlankCheck(String inputString, String key){
        if(String.isBlank(inputString)){
            throw new ints.IntegrationException(key+' is blank.');
        }
        return inputString;
    }
    
    public static String parsePPSRResponse(String response){
        try{
            PPSR_Registration_Information__c ppsrRegInfo = [SELECT id,
                                                            		Is_Latest__c 
                                                            FROM PPSR_Registration_Information__c
                                                            WHERE Collateral_Lien__c =: CollateraLeinlId
                                                            AND Is_Latest__c = True];
            DOM.Document doc = new DOM.Document();   
            doc.load(response);
    
            dom.XmlNode envelopeResponse = doc.getRootElement();
            System.debug(':::envelopeenvelope:::'+envelopeResponse);
            
            List<Dom.XMLNode> resbodyData = envelopeResponse.getChildren();
            
            for(Dom.XMLNode dataParse : resbodyData){
                if(dataParse.getName() == 'Body'){
                    List<Dom.XMLNode> resBody = dataParse.getChildren();
                    for(Dom.XMLNode searchBody : resBody){
                        if(searchBody.getName() == 'DischargeRegistrationResponseMessage'){
                            List<Dom.XMLNode> chData = searchBody.getChildren();
                            for(Dom.XMLNode respChData : chData){
                                if(respChData.getName() == 'DischargeRegistrationResponse'){
                                    List<Dom.XMLNode> ssData = respChData.getChildren();
                                    for(Dom.XMLNode respssData : ssData){
                                        if(respssData.getName() == 'PpsrRequestMessageId'){
                                            ppsrRegInfo.PD_Ppsr_Request_Message_Id__c = respssData.getText();
                                        }
                                        if(respssData.getName() == 'RequestProcessedDateTime'){
                                            ppsrRegInfo.PD_Request_Processed_Date_Time__c = respssData.getText();
                                        }
                                        if(respssData.getName() == 'ChangeNumber'){
                                            ppsrRegInfo.PD_Change_Number__c = respssData.getText();
                                        }
                                        if(respssData.getName() == 'PpsrTransactionId'){
                                            ppsrRegInfo.PD_Ppsr_Transaction_Id__c = respssData.getText();
                                        }
                                        if(respssData.getName() == 'RegistrationEndTime'){
                                            ppsrRegInfo.PD_Registration_End_Time__c = respssData.getText();
                                        }
                                        if(respssData.getName() == 'RegistrationNumber'){
                                            ppsrRegInfo.RegistrationNumber__c = respssData.getText();
                                        } 
                                    }
                                }
                            }
                        }
                    }
                }
            }
            
            if(ppsrRegInfo.PD_Ppsr_Request_Message_Id__c != null){
                update ppsrRegInfo;
                createAttachment('Request', requestBody, ppsrRegInfo.id);
                createAttachment('Response', response, ppsrRegInfo.id);
            }
            return System.label.SUCCESS_MESSAGE;
        }
        catch(Exception e){
            throw new ints.IntegrationException('[PPSRDichrgRgstrtnRqustAdapterHandler.parsePPSRResponse]Exception: ' +  e.getMessage() + ' @');
        }
    }
    public static void createAttachment(String callType, String fileBody, ID parent){
        try{
            Attachment attachment = new Attachment();
            attachment.Body = Blob.valueOf(fileBody);
            attachment.Name = callType + '_' + 'PPSRDischargeRegistration_' + CommonUtility.getDatetimeValue('');
            attachment.ParentId = parent;
            insert attachment;
        }
        catch(Exception e){
            throw new ints.IntegrationException('[PPSRDichrgRgstrtnRqustAdapterHandler.createAttachment]Exception: ' +  e.getMessage());
        }
    }
}