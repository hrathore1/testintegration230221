//@isTest
public with sharing class BankFileUtil_Test {
    /*public BankFileUtil_Test() {

    }

    static testmethod void testGetAPCData()
    {
        Test.startTest();
            loan__Automated_Payment_Configuration__c custbankaccount = new BankFileUtil().getAPCData(loan.LoanConstants.LOAN_DISBURSAL,'aARp0000000c0ybGAA');
            system.assertEquals(1,1,'Function_failed');
        Test.stopTest();
    }
    static testmethod void testGetCustomerBankAccounts()
    {
        Test.startTest();
            Map<Id, loan__Bank_Account__c> custbankaccount = new Map<Id, loan__Bank_Account__c>();
            Set<Id> customerAccIds = new Set<Id>();
            customerAccIds.add(RACVTestHelper.getAccount().Id);
            customerAccIds.add(RACVTestHelper.getAccount('Mr','RACT','Loan','Financial','ract@mailinator.com','0434848302','0444848302').Id);

            custbankaccount = new BankFileUtil().getCustomerBankAccounts(customerAccIds);
            system.assertEquals(1,1,'Function_failed');
        Test.stopTest();
    }


    static testmethod void testGetHeader()
    {
        Test.startTest();
            Map<Id, loan__Bank_Account__c> custbankaccount = new Map<Id, loan__Bank_Account__c>();
            Set<Id> customerAccIds = new Set<Id>();
            customerAccIds.add(RACVTestHelper.getAccount().Id);
            customerAccIds.add(RACVTestHelper.getAccount('Mr','RACT','Loan','Financial','ract@mailinator.com','0434848302','0444848302').Id);

            custbankaccount = new BankFileUtil().getCustomerBankAccounts(customerAccIds);
            system.assertEquals(1,1,'Function_failed');
        Test.stopTest();
    }

    static testmethod void testGetLineRecords()
    {
        Test.startTest();
            BankFileUtil.LineItemInfo line1 = new BankFileUtil.LineItemInfo();
            line1.bankAccountNumber='123456';
            line1.bsbNumber='062223';
            line1.txnAmount=20000;
            line1.nameOfEntity='CBA';
    
            List<BankFileUtil.LineItemInfo> listLineRecords = new List<BankFileUtil.LineItemInfo>();
            listLineRecords.add(line1);

            loan__Automated_Payment_Configuration__c apcRecord = new loan__Automated_Payment_Configuration__c();
            
            List<String> lines = new BankFileUtil().getLineRecords(listLineRecords,apcRecord);
            system.assertEquals(1,1,'Function_failed');
        Test.stopTest();
    }

    
    static testmethod void testGetTrailerRecord()
    {
        Test.startTest();

            loan__Automated_Payment_Configuration__c apcRecord = new loan__Automated_Payment_Configuration__c();
            
            String trailor = new BankFileUtil().getTrailerRecord(20000,30000,20,apcRecord);
            system.assertEquals(1,1,'Function_failed');
        Test.stopTest();
    }

    static testmethod void testisBankAccountValid_Disbursal_Txn_Distribution()
    {
        Test.startTest();
            loan__Disbursal_Txn_Distribution__c loan = new loan__Disbursal_Txn_Distribution__c();
            //Set field values

            boolean custbankaccount = new BankFileUtil().isBankAccountValid(loan);
            system.assertEquals(1,1,'Function_failed');
        Test.stopTest();
    }

    static testmethod void testisBankAccountValid_Bank_Account()
    {
        Test.startTest();
            loan__Bank_Account__c loan = new loan__Bank_Account__c();
            //Set field values
            
            boolean custbankaccount = new BankFileUtil().isBankAccountValid(loan);
            system.assertEquals(1,1,'Function_failed');
        Test.stopTest();
    }

    
    static testmethod void testisBankAccountValid_Loan_Payment_Transaction()
    {
        Test.startTest();
            loan__Loan_Payment_Transaction__c loan = new loan__Loan_Payment_Transaction__c();
            //Set field values

            boolean custbankaccount = new BankFileUtil().isBankAccountValid(loan);
            system.assertEquals(1,1,'Function_failed');
        Test.stopTest();
    }
*/

}