global without sharing class SSPortalAPI implements clcommon.PortalCustomRemoteAPI1 {
    global clcommon.Response invokeAction(String componentStrName, String[] disclosureNames, Map<String, Object> params) {           
        // Init parameter.
        //SavePoint dbSavePoint = Database.setSavepoint();
        clcommon.Response response = new clcommon.Response();
        String applicationId = (String)params.get('applicationId');
        String apiMsg = '';
        String errMsg = System.Label.CONSUMER_PORTAL_TRACKING_MSG_2;
        String refNumber = '';
        Boolean callNextAPI = true;
        Boolean ssSuccess = false;
        Boolean uploadDocuments = true;
        List<genesis__Applications__c> app = [SELECT id,
                                                     Name,
                                                     genesis__Account__c,
                                                     CRN_Number__c,
                                                     (SELECT id,
                                                             Name,
                                                             clcommon__Contact__c,
                                                             clcommon__Account__c,
                                                             clcommon__Contact__r.id,
                                                             clcommon__Account__r.id,
                                                             clcommon__Type__r.Name
                                                        FROM genesis__Parties__r
                                                       WHERE Party_Type_Name__c = 'BORROWER'
                                                    ORDER BY CreatedDate DESC
                                                    LIMIT 1
                                                     ) 
                                              FROM genesis__Applications__c 
                                             WHERE id =: applicationId];   
        Boolean isValidApp = (app != null && app.size() > 0) ? true : false ; 
        if(isValidApp){
            try {
                refNumber = app[0].CRN_Number__c;
                for(clcommon__Party__c party: app[0].genesis__Parties__r){
                    List<clcommon__Address__c> currentResidentialAddress = [SELECT Id, 
                                                                           Name,
                                                                           clcommon__account__c 
                                                                      FROM clcommon__Address__c 
                                                                     WHERE clcommon__Account__c =: party.clcommon__Account__c
                                                                       AND Is_Current__c = true
                                                                       AND Address_Type__c = 'Residential'
                                                                     ORDER BY CreatedDate DESC
                                                                     LIMIT 1
                                                                   ];
                    /*if(currentResidentialAddress == null || currentResidentialAddress.size() == 0){
                        response.status = clcommon.Constants.Pending;
                        return response;
                    }
                    else{*/
                        String res = EquifaxSystemCallouts.calloutSS_SyncForPartyWithValidations(app[0].id, party.clcommon__Account__c, party.id);
                        if(System.label.SUCCESS_MESSAGE.equalsIgnoreCase(res)){
                            List< scoreseeker__c > ssWithScorePresent = [SELECT id
                                                                           FROM scoreseeker__c
                                                                          WHERE score_masterscale__c != null
                                                                            AND application__c =: applicationId
                                                                            AND Party_Type__c =: party.clcommon__Type__r.Name
                                                                            AND score_masterscale__c != null
                                                                            AND score_masterscale__c != ''];
                            if(ssWithScorePresent != null && ssWithScorePresent.size() > 0){
                               ssSuccess = true;
                            }
                        }
                        if(!ssSuccess){
                            genesis__Applications__c updateApp = [SELECT id 
                                                                    FROM genesis__Applications__c 
                                                                   WHERE id =: applicationId];
                            updateApp.Portal_Decision__c = 'ScoreSeeker Failed';
                            update updateApp;
                        }
                    //}
                }
            } 
            catch (Exception e) {
                //return getErrorResponse(errMsg);   
                apiMsg = errMsg;            
            }
        }else{
            return getErrorResponse('Invalid Application Id');
        }   
        system.debug(apiMsg);
        Map<String,Object> fields = new Map<String,Object>();
        fields.put('message',apiMsg);
        fields.put('callNextAPI', String.valueOf(callNextAPI));
        fields.put('refNumber', refNumber);
        fields.put('uploadDocuments', uploadDocuments);
        //fields.put('showDetails', false);
        List<clcommon.PortalCustomRemoteActionRecord> respList = new List<clcommon.PortalCustomRemoteActionRecord>();
        respList.add(new clcommon.PortalCustomRemoteActionRecord('response', fields));
        response = clcommon.PortalActions.getCustomRemoteActionResponse(respList);
        /*if(!callNextAPI){
            //send set password email to user
            ID userId = UserInfo.getUserId();
            List<User> currUser = [SELECT id, AccountId FROM User WHERE id =: userId];
            if(currUser != null && currUser.size() > 0){
                //currUser[0].Consumer_Portal_Send_Reset_Email__c = true;
                //update currUser;
                System.resetPasswordWithEmailTemplate(currUser[0].id, true, System.label.CONSUMER_PORTAL_RESET_PASSWORD);
            }
        }*/
        asyncActions(applicationId);
        return response;
    }
    private clcommon.Response getErrorResponse(String errorMessage) {
        clcommon.Response response = new clcommon.Response();
        response.status = clcommon.Constants.ERROR;
        response.errorMessage = errorMessage;
        response.errorCode = clcommon.Constants.API_EXCEPTION;
        return response;
    }
    @future
    public static void asyncActions(String appId){
        StageAndTaskActions.triggerOmniWorkflow(appId);
    }
}