@IsTest
public class RACVF_LibraryCreateFolderTest {

    @testSetup static void setup() {
        
        List<String> contractNumbers = new List<String>();
        contractNumbers.add('Test-000011');
        contractNumbers.add('Test-000022');
        RACVF_LibraryCreateFolder.createLibraryFolder(contractNumbers);
    }
    
    static testMethod void testCheckFolderCreated() {
        
        Test.startTest();
        
        List<ContentWorkspace> contentList = [Select Id, Name From ContentWorkspace Where Name In('Test-000022','Test-000011')];
       
        System.assertEquals(contentList.size(), 2);
        
        Test.stopTest();
    }
    
    static testMethod void testCheckFolderAlreadyCreated() {
        
        Test.startTest();
        List<String> contractNumbers = new List<String>();
        contractNumbers.add('Test-000013');
        contractNumbers.add('Test-000022');
        RACVF_LibraryCreateFolder.createLibraryFolder(contractNumbers);
        List<ContentWorkspace> contentList = [Select Id, Name From ContentWorkspace Where Name In('Test-000022','Test-000011')];
       
        System.assertEquals(contentList.size(), 2);
        
        Test.stopTest();
    }
}