global class LiabilityCalculationHandler {

    @InvocableMethod
    webservice static void calculatePaymentAmount(List<Id> listIds){
        mfiflexUtil.VOLog voLogInstance = mfiflexUtil.VOLog.getInstance('Genesis');
        try{
            List<String> listLiabiltiyTypes = System.label.LIABILITY_TYPE_FOR_MONTH_PAY_CALC.split(',');
            List<genesis__Liability__c> listLiabities = [SELECT id,
                                                                name,
                                                                genesis__Liability_Type__c ,
                                                                Monthly_Repayments__c,
                                                                Final_Calc_Balance__c,
                                                                genesis__Payment_Frequency__c,
                                                                Final_Calc_Term_in_Months__c,
                                                                Final_Calc_Interest__c,
                                                                Party_Type__c
                                                           FROM genesis__Liability__c 
                                                          WHERE id IN :listIds
                                                            AND Type__c = 'Liability'
                                                            AND genesis__Liability_Type__c IN :listLiabiltiyTypes
                                                        ];

            CalculatePricing calcPrice = new CalculatePricing();
            for(genesis__Liability__c liab : listLiabities){
                String frequency = liab.genesis__Payment_Frequency__c != null ? liab.genesis__Payment_Frequency__c : '';
                if('yearly'.equalsIgnoreCase(liab.genesis__Payment_Frequency__c)){
                    frequency = 'Annual';
                }
                if(liab.Final_Calc_Term_in_Months__c > 0 
                   && liab.Final_Calc_Interest__c > 0
                   && liab.Final_Calc_Balance__c > 0
                   && !String.isBlank(liab.genesis__Payment_Frequency__c)){
                    liab.Monthly_Repayments__c = calcPrice.calculateEMIAmount(liab.Final_Calc_Balance__c,
                                                                              liab.Final_Calc_Interest__c,
                                                                              liab.Final_Calc_Term_in_Months__c,
                                                                              frequency
                                                                             );
                }
            }
            
            update listLiabities;
        }
        catch(Exception e){
            voLogInstance.logException(1001, '[LiabilityCalculationHandler.calculatePaymentAmount] Exception at line : ' + e.getLineNumber(), e);
            voLogInstance.commitToDB();
        }
    }
}