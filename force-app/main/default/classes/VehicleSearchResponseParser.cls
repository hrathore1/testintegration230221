global class VehicleSearchResponseParser{
    
    public static String parseJSON(String jsonBody, clcommon__Collateral__c collInst){
        try{
            /*Vehicle parseVehicleData;
            if(collInst != null && collInst.Call_Type__c != null && 'Glasses'.equalsIgnoreCase(collInst.Call_Type__c)){
                Vehicles parseVehicle = (Vehicles) System.JSON.deserialize(jsonBody, Vehicles.class);
                parseVehicleData = parseVehicle.vehicle != null ? parseVehicle.vehicle : parseVehicle.bike;
                System.debug('parseVehicleDataT2:' + parseVehicleData);
            }
            else{
                parseVehicleData = (Vehicle) System.JSON.deserialize(jsonBody, Vehicle.class);
            }
            */
            //***********************
            Vehicle parseVehicleData = (Vehicle) System.JSON.deserialize(jsonBody, Vehicle.class);
            System.debug('parseVehicleData:' + parseVehicleData);
            
            //collInst = getVehicleInstanceWithMappings(parseVehicleData, collInst.Id);
            
            collInst.nvic__c = parseVehicleData.nvic;
            collInst.vinNumber__c = parseVehicleData.vinNumber;
            collInst.modelTypeCode__c = parseVehicleData.modelTypeCode;
            collInst.nvicModel__c = parseVehicleData.nvicModel;
            collInst.Manufacturer_Code__c = parseVehicleData.manufacturerCode;
            collInst.Family_Code__c = parseVehicleData.familyCode;
            collInst.Variant_Code__c = parseVehicleData.variantCode;
            collInst.Series_Code__c = parseVehicleData.seriesCode;
            collInst.segmentCode__c = parseVehicleData.segmentCode;
            collInst.classCode__c = parseVehicleData.classCode;
            collInst.countryCode__c = parseVehicleData.countryCode;
            collInst.releaseDate__c = parseVehicleData.releaseDate;
            collInst.discontinueDate__c = parseVehicleData.discontinueDate;
            collInst.marketingModelCode__c = parseVehicleData.marketingModelCode;
            collInst.manufacturerName__c = parseVehicleData.manufacturerName;
            collInst.className__c = parseVehicleData.className;
            collInst.segmentName__c = parseVehicleData.segmentName;
            collInst.familyName__c = parseVehicleData.familyName;
            collInst.variantName__c = parseVehicleData.variantName;
            collInst.seriesName__c = parseVehicleData.seriesName;
            collInst.countryName__c = parseVehicleData.countryName;
            collInst.bodyName__c = parseVehicleData.bodyName;
            collInst.engineConfigName__c = parseVehicleData.engineConfigName;
            collInst.ccName__c = parseVehicleData.ccName;
            collInst.engineName__c = parseVehicleData.engineName;
            collInst.transmissionName__c = parseVehicleData.transmissionName;
            collInst.modelName__c = parseVehicleData.modelName;
            collInst.ident__c = parseVehicleData.ident;
            collInst.rrp__c = (String.isBlank(parseVehicleData.rrp) ? 0 : Decimal.valueOf(parseVehicleData.rrp));
            collInst.clcommon__Year__c = parseVehicleData.yearCreate;
            collInst.tradeLow__c = (String.isBlank(parseVehicleData.tradeLow) ? 0 : Decimal.valueOf(parseVehicleData.tradeLow));
            collInst.trade__c = (String.isBlank(parseVehicleData.trade) ? 0 : Decimal.valueOf(parseVehicleData.trade));
            collInst.retail__c = (String.isBlank(parseVehicleData.retail) ? 0 : Decimal.valueOf(parseVehicleData.retail));
            collInst.valveGearName__c = parseVehicleData.valveGearName;
            collInst.numberOfValves__c = parseVehicleData.numberOfValves;
            collInst.compressionRatio__c = parseVehicleData.compressionRatio;
            collInst.bore__c = parseVehicleData.bore;
            collInst.stroke__c = parseVehicleData.stroke;
            collInst.kw__c = parseVehicleData.kw;
            collInst.kwRpm__c = parseVehicleData.kwRpm;
            collInst.torque__c = parseVehicleData.torque;
            collInst.torqueRpm__c = parseVehicleData.torqueRpm;
            collInst.fuelName__c = parseVehicleData.fuelName;
            collInst.capacity__c = parseVehicleData.capacity;
            collInst.cityConsumption__c = parseVehicleData.cityConsumption;
            collInst.highwayConsumption__c = parseVehicleData.highwayConsumption;
            collInst.drivenWheelsName__c = parseVehicleData.drivenWheelsName;
            collInst.gearFinalRatio__c = parseVehicleData.gearFinalRatio;
            collInst.wheelRimsfront__c = parseVehicleData.wheelRimsfront;
            collInst.wheelRimsRear__c = parseVehicleData.wheelRimsRear;
            collInst.tyresFront__c = parseVehicleData.tyresFront;
            collInst.tyresRear__c = parseVehicleData.tyresRear;
            collInst.vinLocationName__c = parseVehicleData.vinLocationName;
            collInst.vinCompPlateCode__c = parseVehicleData.vinCompPlateCode;
            collInst.noOfDoors__c = parseVehicleData.noOfDoors;
            collInst.height__c = parseVehicleData.height;
            collInst.length__c = parseVehicleData.length;
            collInst.width__c = parseVehicleData.width;
            collInst.groundClearance__c = parseVehicleData.groundClearance;
            collInst.wheelBase__c = parseVehicleData.wheelBase;
            collInst.gcm__c = parseVehicleData.gcm;
            collInst.gvm__c = parseVehicleData.gvm;
            collInst.kerbWeight__c = parseVehicleData.kerbWeight;
            collInst.turningCircle__c = parseVehicleData.turningCircle;
            collInst.trackFront__c = parseVehicleData.trackFront;
            collInst.trackRear__c = parseVehicleData.trackRear;
            collInst.towCapBraked__c = parseVehicleData.towCapBraked;
            collInst.towCapUnBraked__c = parseVehicleData.towCapUnBraked;
            collInst.noOfSeats__c = parseVehicleData.noOfSeats;
            collInst.payLoad__c = parseVehicleData.payLoad;
            collInst.steeringName__c = parseVehicleData.steeringName;
            collInst.frontBrakes__c = parseVehicleData.frontBrakes;
            collInst.rearBrakes__c = parseVehicleData.rearBrakes;
            collInst.suspensionFront__c = parseVehicleData.suspensionFront;
            collInst.suspensionRear__c = parseVehicleData.suspensionRear;
            collInst.serviceMonths__c = parseVehicleData.serviceMonths;
            collInst.serviceKMS__c = parseVehicleData.serviceKMS;
            collInst.co2Emission__c = parseVehicleData.co2Emission;
            collInst.ancap__c = parseVehicleData.ancap;
            collInst.overAllRating__c = parseVehicleData.overAllRating;
            collInst.greenHouse__c = parseVehicleData.greenHouse;
            collInst.airPollution__c = parseVehicleData.airPollution;
            collInst.mth__c = parseVehicleData.mth;
            collInst.glassCode__c = parseVehicleData.glassCode;
            collInst.Is_Validated__c = true;
            collInst.regoStatus__c = parseVehicleData.regoStatus;
            collInst.clcommon__Registration_State__c = parseVehicleData.jurisdiction;
            collInst.Registration_Number__c = parseVehicleData.plate;
            collInst.colour__c = parseVehicleData.colour;
            collInst.averageKM__c = (String.isBlank(parseVehicleData.averageKM) ? 0 : Decimal.valueOf(parseVehicleData.averageKM));
            collInst.averageKMFOR__c = (String.isBlank(parseVehicleData.averageKMFOR) ? 0 : Decimal.valueOf(parseVehicleData.averageKMFOR));
            collInst.averageKMTHEN__c = (String.isBlank(parseVehicleData.averageKMTHEN) ? 0 : Decimal.valueOf(parseVehicleData.averageKMTHEN));
            collInst.guideKMCategoryCode__c = parseVehicleData.guideKMCategoryCode;
            collInst.weight__c = parseVehicleData.weight;
            collInst.dcCode__c = parseVehicleData.dcCode;
            collInst.marginCode__c = parseVehicleData.marginCode;
            collInst.imported__c = parseVehicleData.imported;
            collInst.towBallWeight__c = parseVehicleData.towBallWeight;
            collInst.towingHeight__c = parseVehicleData.towingHeight;
            collInst.bodyConstruction__c = parseVehicleData.bodyConstruction;
            collInst.axleName__c = parseVehicleData.axleName;
            collInst.berthName__c = parseVehicleData.berthName;
            
            return System.label.TAG_SUCCESS;
        }
        catch(Exception e){
            //delete collateral and app coll
            return ('Exception while parsing response:' + e.getMessage());
        }
    }
    
    public class VehicleParser{
        public List<Vehicles> vehicles;
        public List<Vehicles> Bike;
        public List<Vehicles> Caravan;
    }

    public class Vehicles {
        public Vehicle vehicle;
        public Vehicle bike;
        public Vehicle caravan;
    }
    
    public class Vehicle {
        public String jurisdiction;
        public String plate;
        public String regoStatus;
        public String colour;
        public String nvic;
        public String vinNumber;
        public String modelTypeCode;
        public String nvicModel;
        public String manufacturerCode;
        public String familyCode;
        public String variantCode;
        public String seriesCode;
        public String segmentCode;
        public String classCode;
        public String countryCode;
        public String releaseDate;
        public String discontinueDate;
        public String marketingModelCode;
        public String manufacturerName;
        public String className;
        public String segmentName;
        public String familyName;
        public String variantName;
        public String seriesName;
        public String countryName;
        public String bodyName;
        public String engineConfigName;
        public String ccName;
        public String engineName;
        public String transmissionName;
        public String modelName;
        public String ident;
        public String rrp;
        public String yearCreate;
        public String tradeLow;
        public String trade;
        public String retail;
        public String valveGearName;
        public String numberOfValves;
        public String compressionRatio;
        public String bore;
        public String stroke;
        public String kw;
        public String kwRpm;
        public String torque;
        public String torqueRpm;
        public String fuelName;
        public String capacity;
        public String cityConsumption;
        public String highwayConsumption;
        public String drivenWheelsName;
        public String gearFinalRatio;
        public String wheelRimsfront;
        public String wheelRimsRear;
        public String tyresFront;
        public String tyresRear;
        public String vinLocationName;
        public String vinCompPlateCode;
        public String noOfDoors;
        public String height;
        public String length;
        public String width;
        public String groundClearance;
        public String wheelBase;
        public String gcm;
        public String gvm;
        public String kerbWeight;
        public String turningCircle;
        public String trackFront;
        public String trackRear;
        public String towCapBraked;
        public String towCapUnBraked;
        public String noOfSeats;
        public String payLoad;
        public String steeringName;
        public String frontBrakes;
        public String rearBrakes;
        public String suspensionFront;
        public String suspensionRear;
        public String serviceMonths;
        public String serviceKMS;
        public String co2Emission;
        public String ancap;
        public String overAllRating;
        public String greenHouse;
        public String airPollution;
        public String mth;
        public String glassCode;
        public String averageKM;
        public String averageKMFOR;
        public String averageKMTHEN;
        public String guideKMCategoryCode;      
        public String weight;  
        public String dcCode;
        public String marginCode;
        public String imported;
        public String towBallWeight;
        public String towingHeight;
        public String bodyConstruction;
        public String axleName;
        public String berthName;
    }
    
    public static clcommon__Collateral__c getVehicleInstanceWithMappings(Vehicle parseVehicleData, String collateralId){
        try{
            clcommon__Collateral__c collInst = new clcommon__Collateral__c(Id = collateralId);
            collInst.nvic__c = parseVehicleData.nvic;
            collInst.vinNumber__c = parseVehicleData.vinNumber;
            collInst.modelTypeCode__c = parseVehicleData.modelTypeCode;
            collInst.nvicModel__c = parseVehicleData.nvicModel;
            collInst.Manufacturer_Code__c = parseVehicleData.manufacturerCode;
            collInst.Family_Code__c = parseVehicleData.familyCode;
            collInst.Variant_Code__c = parseVehicleData.variantCode;
            collInst.Series_Code__c = parseVehicleData.seriesCode;
            collInst.segmentCode__c = parseVehicleData.segmentCode;
            collInst.classCode__c = parseVehicleData.classCode;
            collInst.countryCode__c = parseVehicleData.countryCode;
            collInst.releaseDate__c = parseVehicleData.releaseDate;
            collInst.discontinueDate__c = parseVehicleData.discontinueDate;
            collInst.marketingModelCode__c = parseVehicleData.marketingModelCode;
            collInst.manufacturerName__c = parseVehicleData.manufacturerName;
            collInst.className__c = parseVehicleData.className;
            collInst.segmentName__c = parseVehicleData.segmentName;
            collInst.familyName__c = parseVehicleData.familyName;
            collInst.variantName__c = parseVehicleData.variantName;
            collInst.seriesName__c = parseVehicleData.seriesName;
            collInst.countryName__c = parseVehicleData.countryName;
            collInst.bodyName__c = parseVehicleData.bodyName;
            collInst.engineConfigName__c = parseVehicleData.engineConfigName;
            collInst.ccName__c = parseVehicleData.ccName;
            collInst.engineName__c = parseVehicleData.engineName;
            collInst.transmissionName__c = parseVehicleData.transmissionName;
            collInst.modelName__c = parseVehicleData.modelName;
            collInst.ident__c = parseVehicleData.ident;
            collInst.rrp__c = (String.isBlank(parseVehicleData.rrp) ? 0 : Decimal.valueOf(parseVehicleData.rrp));
            collInst.clcommon__Year__c = parseVehicleData.yearCreate;
            collInst.tradeLow__c = (String.isBlank(parseVehicleData.tradeLow) ? 0 : Decimal.valueOf(parseVehicleData.tradeLow));
            collInst.trade__c = (String.isBlank(parseVehicleData.trade) ? 0 : Decimal.valueOf(parseVehicleData.trade));
            collInst.retail__c = (String.isBlank(parseVehicleData.retail) ? 0 : Decimal.valueOf(parseVehicleData.retail));
            collInst.valveGearName__c = parseVehicleData.valveGearName;
            collInst.numberOfValves__c = parseVehicleData.numberOfValves;
            collInst.compressionRatio__c = parseVehicleData.compressionRatio;
            collInst.bore__c = parseVehicleData.bore;
            collInst.stroke__c = parseVehicleData.stroke;
            collInst.kw__c = parseVehicleData.kw;
            collInst.kwRpm__c = parseVehicleData.kwRpm;
            collInst.torque__c = parseVehicleData.torque;
            collInst.torqueRpm__c = parseVehicleData.torqueRpm;
            collInst.fuelName__c = parseVehicleData.fuelName;
            collInst.capacity__c = parseVehicleData.capacity;
            collInst.cityConsumption__c = parseVehicleData.cityConsumption;
            collInst.highwayConsumption__c = parseVehicleData.highwayConsumption;
            collInst.drivenWheelsName__c = parseVehicleData.drivenWheelsName;
            collInst.gearFinalRatio__c = parseVehicleData.gearFinalRatio;
            collInst.wheelRimsfront__c = parseVehicleData.wheelRimsfront;
            collInst.wheelRimsRear__c = parseVehicleData.wheelRimsRear;
            collInst.tyresFront__c = parseVehicleData.tyresFront;
            collInst.tyresRear__c = parseVehicleData.tyresRear;
            collInst.vinLocationName__c = parseVehicleData.vinLocationName;
            collInst.vinCompPlateCode__c = parseVehicleData.vinCompPlateCode;
            collInst.noOfDoors__c = parseVehicleData.noOfDoors;
            collInst.height__c = parseVehicleData.height;
            collInst.length__c = parseVehicleData.length;
            collInst.width__c = parseVehicleData.width;
            collInst.groundClearance__c = parseVehicleData.groundClearance;
            collInst.wheelBase__c = parseVehicleData.wheelBase;
            collInst.gcm__c = parseVehicleData.gcm;
            collInst.gvm__c = parseVehicleData.gvm;
            collInst.kerbWeight__c = parseVehicleData.kerbWeight;
            collInst.turningCircle__c = parseVehicleData.turningCircle;
            collInst.trackFront__c = parseVehicleData.trackFront;
            collInst.trackRear__c = parseVehicleData.trackRear;
            collInst.towCapBraked__c = parseVehicleData.towCapBraked;
            collInst.towCapUnBraked__c = parseVehicleData.towCapUnBraked;
            collInst.noOfSeats__c = parseVehicleData.noOfSeats;
            collInst.payLoad__c = parseVehicleData.payLoad;
            collInst.steeringName__c = parseVehicleData.steeringName;
            collInst.frontBrakes__c = parseVehicleData.frontBrakes;
            collInst.rearBrakes__c = parseVehicleData.rearBrakes;
            collInst.suspensionFront__c = parseVehicleData.suspensionFront;
            collInst.suspensionRear__c = parseVehicleData.suspensionRear;
            collInst.serviceMonths__c = parseVehicleData.serviceMonths;
            collInst.serviceKMS__c = parseVehicleData.serviceKMS;
            collInst.co2Emission__c = parseVehicleData.co2Emission;
            collInst.ancap__c = parseVehicleData.ancap;
            collInst.overAllRating__c = parseVehicleData.overAllRating;
            collInst.greenHouse__c = parseVehicleData.greenHouse;
            collInst.airPollution__c = parseVehicleData.airPollution;
            collInst.mth__c = parseVehicleData.mth;
            collInst.glassCode__c = parseVehicleData.glassCode;
            collInst.Is_Validated__c = true;
            collInst.regoStatus__c = parseVehicleData.regoStatus;
            collInst.clcommon__Registration_State__c = parseVehicleData.jurisdiction;
            collInst.Registration_Number__c = parseVehicleData.plate;
            collInst.colour__c = parseVehicleData.colour;
            collInst.weight__c = parseVehicleData.weight;
            collInst.dcCode__c = parseVehicleData.dcCode;
            collInst.marginCode__c = parseVehicleData.marginCode;
            collInst.imported__c = parseVehicleData.imported;
            collInst.towBallWeight__c = parseVehicleData.towBallWeight;
            collInst.towingHeight__c = parseVehicleData.towingHeight;
            collInst.bodyConstruction__c = parseVehicleData.bodyConstruction;
            collInst.axleName__c = parseVehicleData.axleName;
            collInst.berthName__c = parseVehicleData.berthName;
            return collInst;
        }
        catch(Exception e){
            throw new ints.IntegrationException('[VehicleSearchService.getVehicleInstanceWithMappings]Exception: ' +  e.getMessage());
        }
    }
}