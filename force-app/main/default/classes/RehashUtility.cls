/**
 * @File Name          : RehashUtility.cls
 * @Description        : 
 * @Author             : chirag.gupta@q2.com
 * @Group              : 
 * @Last Modified By   : chirag.gupta@q2.com
 * @Last Modified On   : 10-01-2020
 * @Modification Log   : 
 * Ver       Date            Author             Modification
 * 1.0    6/17/2020   chirag.gupta@q2.com     Initial Version
 * 2.0    9/21/2020   chirag.gupta@q2.com     Changes related to RFCL-2855
**/
global class RehashUtility {
    
    List<REHASH_Field_Mapping__mdt> listMetadataRecs;
    Map<String, Map<String, Schema.SObjectField>> mapObjectToFieldDef = new Map<String, Map<String, Schema.SObjectField>>();
    
    public genesis__Applications__c getData(String appId){
        try{
            listMetadataRecs = [SELECT id,
                                       MasterLabel,
                                       Source_Object__c,
                                       Source_Object_API_Name__c,
                                       Source_Field_API__c,
                                       Target_Object_API__c,
                                       Target_Field_API__c,
                                       Track_Field_Changes__c,
                                       Condition__c,
                                       Unique_Record_Identifier__c
                                  FROM REHASH_Field_Mapping__mdt
                               ];
            
            Map<String, List<String>> mapLocalSrcObjToFldsMapping = new Map<String, List<String>>();
            Map<String, String> mapObjectToCondition = new Map<String, String>();
            Map<String, String> mapSrcObjApiToActualAPI = new Map<String, String>();
            for(REHASH_Field_Mapping__mdt rfm : listMetadataRecs){
                if(!mapObjectToFieldDef.containsKey(rfm.Source_Object_API_Name__c)){
                    SObjectType sObjType = ((SObject)(Type.forName('Schema.' + rfm.Source_Object_API_Name__c).newInstance())).getSObjectType();
                    DescribeSObjectResult descResult = sObjType.getDescribe();
                    mapObjectToFieldDef.put(rfm.Source_Object_API_Name__c, descResult.fields.getMap());
                }
                
                if(mapLocalSrcObjToFldsMapping.containsKey(rfm.Source_Object__c)){
                    mapLocalSrcObjToFldsMapping.get(rfm.Source_Object__c).add(rfm.Source_Field_API__c);
                    continue;
                }
                mapLocalSrcObjToFldsMapping.put(rfm.Source_Object__c, new List<String>{rfm.Source_Field_API__c});
                mapObjectToCondition.put(rfm.Source_Object__c, rfm.Condition__c);
                mapSrcObjApiToActualAPI.put(rfm.Source_Object__c, rfm.Source_Object_API_Name__c);
            }
            
            if(!mapLocalSrcObjToFldsMapping.containsKey('genesis__Applications__c')){
                throw new CustomException('Invalid configuration data present');
            }
            
            mfiflexUtil.ExecutionContext ec = mfiflexUtil.ExecutionContext.getExecContext();
            mfiflexUtil.ObjectCache applicationData = ec.getObject('applicationData');
            if (applicationData != null) {
                ec.deleteObject('applicationData');
            }
            mfiflexUtil.ObjectCache entries = ec.createObject('applicationData', 'genesis__Applications__c', true);
            entries.addFields('Id, Name, genesis__CL_Product__c');
            for (String fieldAPIName : mapLocalSrcObjToFldsMapping.get('genesis__Applications__c')) {
                entries.addFields(fieldAPIName);
            }
            
            //Adding default all the fields of Rehash Metrics object in query
            mfiflexUtil.ObjectCache RMObjCache  = ec.createRelationship('Rehash_Metrics__r');
            SObjectType RMObject = Schema.getGlobalDescribe().get('Rehash_Metrics__c');
            Map<String, Schema.SObjectField> rmfields = RMObject.getDescribe().fields.getMap();
            RMObjCache.addFields('Base_Stage__r.name, Compared_With__r.name');
            for(String field: rmfields.keyset()) {
                RMObjCache.addFields(field);
            }
            entries.addRelationship(RMObjCache);

            if(!mapObjectToFieldDef.containsKey('Rehash_Metrics__c')){
                SObjectType sObjType = ((SObject)(Type.forName('Schema.Rehash_Metrics__c').newInstance())).getSObjectType();
                DescribeSObjectResult descResult = sObjType.getDescribe();
                mapObjectToFieldDef.put('Rehash_Metrics__c', descResult.fields.getMap());
            }
            //---------------------------------------------------------------
            
            for(String objectAPIName : mapLocalSrcObjToFldsMapping.keySet()){
                if(!objectAPIName.equalsIgnoreCase('genesis__Applications__c')) {
                    mfiflexUtil.ObjectCache tempObj  = ec.createRelationship(objectAPIName);
                    SObjectType tempSObj = Schema.getGlobalDescribe().get(mapSrcObjApiToActualAPI.get(objectAPIName));
                    Map<String, Schema.SObjectField> tempSObjFields = tempSObj.getDescribe().fields.getMap();
                    for(String field: tempSObjFields.keyset()) {
                        tempObj.addFields(field);
                    }
                    if(mapObjectToCondition.containsKey(objectAPIName) && mapObjectToCondition.get(objectAPIName) != null){
                        tempObj.setWhereClause(mapObjectToCondition.get(objectAPIName));
                    }                    
                    entries.addRelationship(tempObj);
                }
            }
          
            entries.addNamedParameter('applicationId', appId);
            entries.setWhereClause('Id = :applicationId');
            entries.buildQuery();
            
            System.debug('Dynamic Query :'+ entries.getQuery());
            return ((List<genesis__Applications__c>) entries.executeQuery().getRecords()).get(0);
        }
        catch(Exception e){
            throw e;
        }
    }

    webservice static String validateAppByRehash(String appId){
        try{
            List<genesis__Application_Department__c> currentAppDept = [SELECT id,
                                                                              genesis__Department__c 
                                                                         FROM genesis__Application_Department__c
                                                                        WHERE genesis__Application__c = :appId
                                                                          AND genesis__Status__c = 'Active'
                                                                      ];
            
            if(currentAppDept != null && currentAppDept.size() > 0){
                RehashUtility ru = new RehashUtility();
                String returnMsg = ru.triggerRehash(appId, currentAppDept.get(0).genesis__Department__c, null);

                genesis__Applications__c appInst = new genesis__Applications__c(Id = appId);
                if(System.label.NEXT_STAGE_SUBMIT_SUCCESS_MSG.equalsIgnoreCase(returnMsg) 
                    || System.label.TAG_SUCCESS.equalsIgnoreCase(returnMsg)){                
                    appInst.genesis__Status__c = 'RESUBMITTED - PASS';
                    update appInst;
                    return 'Rehash Executed: No resubmission required.';
                }
                
                appInst.genesis__Status__c = 'RESUBMITTED - REFERRED TO CREDIT';
                update appInst;
                return returnMsg;
            }
            else{
                return 'No active department present for application.';
            }
        }
        catch(Exception e){
            return ('Exception REHASH:' + e.getMessage());
        }
    }
    
    public String triggerRehash(String appId, String currentStageId, StageExecutionInterface classImplInst){
        try{
            if(String.isBlank(currentStageId)){
                return 'No active department present for application.';
            }
            
            String returnMsg = evaluateRehashFunction(appId, currentStageId, classImplInst);

            genesis__Applications__c appInst = new genesis__Applications__c(Id = appId);
            if(System.label.NEXT_STAGE_SUBMIT_SUCCESS_MSG.equalsIgnoreCase(returnMsg) 
                || System.label.TAG_SUCCESS.equalsIgnoreCase(returnMsg)){                
                return System.label.NEXT_STAGE_SUBMIT_SUCCESS_MSG;
            }
                
            appInst.genesis__Status__c = 'RESUBMITTED - REFERRED TO CREDIT';
            update appInst;
            return returnMsg;
        }
        catch(Exception e){
            return ('Exception REHASH:' + e.getMessage());
        }
    }
    
    public String evaluateRehashFunction(String appId, String currentStageId, StageExecutionInterface classImplInst){
        try{
            genesis__Org_Parameters__c orgParams = genesis__Org_Parameters__c.getOrgDefaults();
            genesis__Applications__c appInst = getData(appId);
            
            Rehash_Metrics__c currDeptRM = null;
            Rehash_Metrics__c baseDeptRM = null;
            for(Rehash_Metrics__c rm : appInst.Rehash_Metrics__r){
                if(String.valueOf(rm.Base_Stage__c).contains(currentStageId)){
                    currDeptRM = rm;
                    currDeptRM.Version__c += 1;
                    currDeptRM.Latest_Transaction_Datetime__c = Datetime.now();
                }
                if(rm.Base_Stage__r.name.equalsIgnoreCase(orgParams.Rehash_Base_Stage_Name__c)){
                    baseDeptRM = rm;
                    baseDeptRM.Latest_Transaction_Datetime__c = Datetime.now();
                }
                if(currDeptRM != null && baseDeptRM != null){
                    break;
                }
            }

            if(currDeptRM == null){
                currDeptRM = new Rehash_Metrics__c();
                currDeptRM.Application__c = appInst.Id;
                currDeptRM.Version__c = 1;
                currDeptRM.Base_Stage__c = currentStageId;
                currDeptRM.Transaction_Datetime__c = Datetime.now(); 
            }
            
            for(REHASH_Field_Mapping__mdt rfm : listMetadataRecs){
                if(rfm.Source_Object__c.equalsIgnoreCase('genesis__Applications__c')) {
                    currDeptRM.put(rfm.Target_Field_API__c, appInst.get(rfm.Source_Field_API__c));
                }
                else{
                    List<SObject> tempListInst = (List<SObject>) appInst.getSObjects(rfm.Source_Object__c);
                    if(tempListInst == null){
                        continue;
                    }
                    String fieldValue = '';
                    String uniqueRecordIdentifier = rfm.Unique_Record_Identifier__c;
                    for(SObject sObj : tempListInst){

                        String leftHandSideVal = '';
                        if(uniqueRecordIdentifier != null && !String.isBlank(uniqueRecordIdentifier)){
                            leftHandSideVal = sObj.get(uniqueRecordIdentifier) + ':';
                        }
                        //fieldValue += leftHandSideVal + (String) sObj.get(rfm.Source_Field_API__c) + ';';
                        fieldValue += leftHandSideVal + String.valueOf(sObj.get(rfm.Source_Field_API__c)) + ';';
                    }
                    currDeptRM.put(rfm.Target_Field_API__c, fieldValue);
                }
            }

            List<Rehash_Metrics__c> listRMToBeUpdated = new List<Rehash_Metrics__c>();
            if(baseDeptRM != null){
                baseDeptRM.Compared_With__c = currDeptRM.Base_Stage__c;
                copyDataFromCurrentToBase(currDeptRM, baseDeptRM);
                listRMToBeUpdated.add(baseDeptRM);
                //copyDataFromCurrentToBase(baseDeptRM, currDeptRM);
                //listRMToBeUpdated.add(baseDeptRM);
            }
            
            if(currDeptRM.Id == null || (baseDeptRM != null && currDeptRM.Id != baseDeptRM.Id)){
                listRMToBeUpdated.add(currDeptRM);
            }            
            UPSERT listRMToBeUpdated;
            
            if(currDeptRM != null && currDeptRM.Id != null){
                appInst.Rehash_Metrics__c = currDeptRM.Id;
                
                if(baseDeptRM != null && baseDeptRM.Id != null){
                    appInst.Rehash_Metrics_Base_Stage__c = baseDeptRM.Id;
                }
                update appInst;
            }
            
            if(baseDeptRM == null || (baseDeptRM != null && currDeptRM.Id == baseDeptRM.Id)){
                return System.label.NEXT_STAGE_SUBMIT_SUCCESS_MSG;//System.label.TAG_SUCCESS;
            }
            
            return takeAction(appInst, baseDeptRM, classImplInst);
        }
        catch(Exception e){
            ExceptionLog.insertExceptionLog(e, 'REHASH EXCEPTION', appId);
            return ('Exception REHASH:' + e.getMessage());
        }
    }    

    public String takeAction(genesis__Applications__c appInst, 
                             Rehash_Metrics__c baseDeptRM, 
                             StageExecutionInterface classImplInst){
        try{
            List<genesis__Rule__c> listRules = [SELECT Id,
                                                       Name 
                                                  FROM genesis__Rule__c 
                                                 WHERE genesis__Enabled__c = true 
                                                   AND Rule_Classification__c = 'RehashRules'
                                                   AND Id IN (SELECT genesis__Rule__c 
                                                                FROM genesis__Product_Rule_Association__c 
                                                               WHERE genesis__CL_Product__c = :appInst.genesis__CL_Product__c
                                                             )
                                               ];

            if(listRules == null || listRules.size() == 0){
                return System.label.NEXT_STAGE_SUBMIT_SUCCESS_MSG;
            }      
            
            //If any rule pass then submit the application back to credit stage...
            mfiflexUtil.ExecutionContext ec = mfiflexUtil.ExecutionContext.getExecContext();
            mfiflexUtil.ObjectCache sObjOC = ec.getObject('tempRuleCache');
            if(sObjOC != null){
                ec.deleteObject('tempRuleCache');
            }
            List<genesis__Checklist__c> checkList = genesis.RulesAPI.evaluateRules((SObject) appInst, listRules, false, false);
            System.debug(checkList);
            Boolean isCreditSubmissionRequired = false;
            for(genesis__Checklist__c chkList : checkList){
                if(chkList.genesis__Result__c){
                    isCreditSubmissionRequired = true;
                    break;
                }
            }
            
            if(isCreditSubmissionRequired){
                String returnMsg = StageAndTaskActions.submitToPreviousDepartment(appInst.Id, baseDeptRM.Base_Stage__c, classImplInst);
                return returnMsg + ' (There have been changes in the Application that require credit analysis.)';
            }
            return System.label.NEXT_STAGE_SUBMIT_SUCCESS_MSG;
        }
        catch(Exception e){
            throw e;
        }
    }

    public void copyDataFromCurrentToBase(Rehash_Metrics__c currentRM, Rehash_Metrics__c baseRM){
        try{
            Map<String, Schema.SObjectField> fieldsMap = mapObjectToFieldDef.get('Rehash_Metrics__c');
            
            String copyAPIIdentifier = 'Latest_';
            currentRM.Compared_With__c = baseRM.Base_Stage__c;
            for(REHASH_Field_Mapping__mdt rfm : listMetadataRecs){
                if(rfm.Track_Field_Changes__c){
                    String targetAPIName = copyAPIIdentifier + rfm.Target_Field_API__c;
                    if(currentRM.get(rfm.Target_Field_API__c) != null && fieldsMap.containsKey(targetAPIName)){
                        baseRM.put(targetAPIName, currentRM.get(rfm.Target_Field_API__c));
                    }
                }
            }
        }
        catch(Exception e){
            throw e;
        }
    }
}