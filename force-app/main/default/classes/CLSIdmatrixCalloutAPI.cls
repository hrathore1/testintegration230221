/**
 * @File Name          : CLSIdmatrixCalloutAPI.cls
 * @Description        : 
 * @Author             : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Group              : 
 * @Last Modified By   : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Last Modified On   : 6/4/2020, 3:27:52 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    6/4/2020   ChangeMeIn@UserSettingsUnder.SFDoc     Initial Version
**/
global class CLSIdmatrixCalloutAPI{
    /*webservice static String makeIdmatrixCallout(String partyId){ 
        System.debug('::::makeIdmatrixCallout Party Id is::::'+partyId);      
        try{
            List<clcommon__Party__c> partyInfo = [SELECT id,
                                                         name,
                                                         clcommon__Account__c, 
                                                         clcommon__Account__r.Electronic_Identification_Consent__c, 
                                                         genesis__Application__c
                                                    FROM clcommon__Party__c
                                                   WHERE id = :partyId
                                                 ];

            Id accountId;
            Id applicationId;

            if(partyInfo != null && partyInfo.size() > 0){
                accountId = partyInfo.get(0).clcommon__Account__c;
                applicationId = partyInfo.get(0).genesis__Application__c;

                if(!partyInfo.get(0).clcommon__Account__r.Electronic_Identification_Consent__c){
                    return 'Electronic Identification consent not provided.';
                }
            }
            else{
                return 'Invalid party information.';
            }

            Map<String,Object> accountValues = new Map<String,Object>();            
            accountValues.put('accId', accountId);
            accountValues.put('appId', applicationId);
            accountValues.put('Residential', 'Residential');
            
            Map<String,Object> borrowerValues1 = new Map<String,Object>();
            borrowerValues1.put('accountinfo',accountValues);
            borrowerValues1.put('AddressInfo',accountValues);
            borrowerValues1.put('applnpartydata',accountValues);
            borrowerValues1.put('prevAddressInfo',accountValues);
            borrowerValues1.put('accountId',accountId);
            borrowerValues1.put('ExecutionPriority',5);
            system.debug(borrowerValues1);
            
            List<ints__Integration_Configuration__c> integConfig = [SELECT id, 
                                                                           name,
                                                                           ints__Integration_Service__c,
                                                                           ints__Type__c                                    
                                                                      FROM ints__Integration_Configuration__c 
                                                                     WHERE ints__Integration_Service__c = 'Credit Check' 
                                                                       AND Name = 'idmatrixupdated'
                                                                    ];
            
            System.debug('::::makeIdmatrixCallout config data is::::'+integConfig);
            if(integConfig != null && integConfig.size() > 0){
                ints.AbstractMockService   mc  = ints.APIFactory.getPassthroughService();
                System.debug('makeIdmatrixCallout mcmcmcmc::::'+mc);
                
                ints.IntegrationResponseObject responseObject = mc.runPassthroughService(borrowerValues1, integConfig[0].ints__Integration_Service__c, '');
                system.debug('::final result is::::'+responseObject);
            }
            return 'SUCCESS';
        }
        catch(Exception ex){
            String str = ex.getMessage()+'~'+ex.getcause()+'~'+ex.getLineNumber()+'~'+ex.getStackTraceString()+'~'+ex.getTypeName();
            //IntegrationException.createLog(ex,'IDMatrixAPICallout for '+appObj.Name+' ');
            // throw new ints.IntegrationException('Failed: '+ ex.getMessage());
            // responseObject=String.valueOf(ex.getMessage());
            return 'Exception: ' + str;
        }
    }*/
}