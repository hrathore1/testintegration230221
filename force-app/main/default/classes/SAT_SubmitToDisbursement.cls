public class SAT_SubmitToDisbursement implements StageExecutionInterface {
    public List<String> executeValidations(Id applicationId){
        try{
            List<String> returnList = new List<String>();
            return returnList;            
        }
        catch(Exception e){
            throw e;
        }          
    }

    public String executeNextDeptActions(Id applicationId){
        try{
            return executeActions(applicationId);                
        }
        catch(Exception e){
            throw e;
        }          
    }

    public String executePreviousDeptActions(Id applicationId){
        try{
            //executeActions(applicationId);
            return System.label.NEXT_STAGE_SUBMIT_SUCCESS_MSG;
        }
        catch(Exception e){
            throw e;
        }          
    }

    public String executeActions(Id applicationId){
        try{
            genesis__Applications__c appInst = new genesis__Applications__c(Id = applicationId);
            appInst.genesis__Status__c = 'APPROVED - READY FOR DISBURSEMENT';
            update appInst;

            return System.label.NEXT_STAGE_SUBMIT_SUCCESS_MSG;                     
        }
        catch(Exception e){
            throw e;
        }
    }
}