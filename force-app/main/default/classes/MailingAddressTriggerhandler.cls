public class MailingAddressTriggerhandler {
    final String BORR_ADD_CHNG = 'Residential Address changed for Borrower';
    final String CO_BORR_ADD_CHNG = 'Residential Address changed for Co-Borrower';
    final String GUAR_ADD_CHNG = 'Residential Address changed for Gurantor';
    public static Id addressRecordTypeId = Schema.SObjectType.clcommon__Address__c.getRecordTypeInfosByName().get('Application').getRecordTypeId();

    mfiflexUtil.VOLog voLogInstance = mfiflexUtil.VOLog.getInstance('Genesis');

    public void afterInsert(List<clcommon__Address__c> newAddressList,Map<Id,clcommon__Address__c> newAddressMap, Map<Id,clcommon__Address__c> oldAddressMap) { 
        try{
        copyAddresstoAccount(newAddressList,newAddressMap,oldAddressMap);
        }catch(exception ex){
            System.debug('exception :'+ ex.getMessage());
        }
    }
    
    public void afterUpdate(List<clcommon__Address__c> newAddressList,Map<Id,clcommon__Address__c> newAddressMap, Map<Id,clcommon__Address__c> oldAddressMap) { 
        try{
        copyAddresstoAccount(newAddressList,newAddressMap,oldAddressMap);
        }catch(exception ex){
            System.debug('exception :'+ ex.getMessage());
        }
    }
    
    public void copyAddresstoAccount(List<clcommon__Address__c> newAddressList,Map<Id,clcommon__Address__c> newAddressMap, Map<Id,clcommon__Address__c> oldAddressMap){
        Savepoint sp = Database.setSavepoint();
        try
        {
            Map<Id, Account> accountIdToAccountMap = new Map<Id, Account>();
            Set<Id> partyIdSet = new Set<Id>();
            //Set<clcommon__Party__c> party = new Set<clcommon__Party__c>();
            Map<Id,List<String>> errorMsgMap = new Map<Id,List<String>>();
            Set<Id> appIdsList = new Set<Id>();
            List<genesis__Applications__c> updateAppErrorMsgAppList = new List<genesis__Applications__c>();
            String message = '';
            //Checking for Address changes of Parties After Insert/After Update.
            for(clcommon__Address__c addr : newAddressList){
                if((addr.Is_Current__c == true 
                                && addr.Address_Type__c == 'Residential' 
                                && addr.Postcode__c != null && oldAddressMap == null
                                && addr.RecordTypeId == addressRecordTypeId) 
                            || (oldAddressMap != null
                                && addr.Is_Current__c == true 
                                && oldAddressMap.get(addr.id).Is_Current__c == true 
                                && addr.Address_Type__c == 'Residential' 
                                && oldAddressMap.get(addr.id).Address_Type__c == 'Residential' 
                                && addr.Postcode__c != oldAddressMap.get(addr.id).Postcode__c
                                && addr.RecordTypeId == addressRecordTypeId
                                && oldAddressMap.get(addr.id).RecordTypeId == addressRecordTypeId))
                {
                    partyIdSet.add(addr.Party__c);
                } 
            }
            //Querying Party from the party Id set.
            List<clcommon__Party__c> party = [SELECT id,
                                                    name,
                                                    genesis__Application__c,
                                                    Party_Type_Name__c
                                                FROM clcommon__Party__c
                                                WHERE Id IN: partyIdSet
                                                AND genesis__Application__c != null];
            //Fetching Application Id from the Parties
            for(clcommon__Party__c partyId: party){
                appIdsList.add(partyId.genesis__Application__c);
            }
            Map<Id,genesis__Applications__c> partyAppMap = new Map<Id,genesis__Applications__c>([SELECT id,
                                                                                                        Name,
                                                                                                        HEM_Recalculate_Message__c,
                                                                                                        HEM_Value__c 
                                                                                                    FROM genesis__Applications__c 
                                                                                                    WHERE id in: appIdsList]);
            List<String> existingErrorMsg = new List<String>();
            genesis__Applications__c app;
            for(clcommon__Party__c partyId: party)
            {
                app = partyAppMap.get(partyId.genesis__Application__c);
                message = app.HEM_Recalculate_Message__c;
                if(String.isNotBlank(message))
                {
                    existingErrorMsg= message.split('<br>');
                }
                else {
                    existingErrorMsg = new List<String>();
                }
                if(app.HEM_Value__c != null)
                {
                    //Checking if the Application has already Error message in previous iterations
                    if(errorMsgMap.containsKey(partyId.genesis__Application__c) 
                        && errorMsgMap.get(partyId.genesis__Application__c).size()>0)
                    {
                        existingErrorMsg.addAll(errorMsgMap.get(partyId.genesis__Application__c));
                    }
                    if(partyId.Party_Type_Name__c == 'BORROWER' && !existingErrorMsg.contains(BORR_ADD_CHNG))
                    {
                        existingErrorMsg.add(BORR_ADD_CHNG);
                    }
                    else if(partyId.Party_Type_Name__c == 'CO-BORROWER' && !existingErrorMsg.contains(CO_BORR_ADD_CHNG))
                    {
                        existingErrorMsg.add(CO_BORR_ADD_CHNG);
                    }
                    else if(partyId.Party_Type_Name__c == 'GUARANTOR' && !existingErrorMsg.contains(GUAR_ADD_CHNG))
                    {
                        existingErrorMsg.add(GUAR_ADD_CHNG);
                    }
                    //Storing Error message list with their respective Application Id
                    errorMsgMap.put(partyId.genesis__Application__c,existingErrorMsg);
                }
            }
            for(genesis__Applications__c appList: [SELECT id,
                                                        Name,
                                                        HEM_Recalculate_Message__c
                                                    FROM genesis__Applications__c
                                                    WHERE id IN: appIdsList]){
                if(errorMsgMap.containsKey(appList.Id))
                {
                    if(!errorMsgMap.get(appList.Id).isEmpty())
                    {
                        //Storing Error message in HEM Recalculate message
                        appList.HEM_Recalculate_Message__c = String.join(errorMsgMap.get(appList.Id),'<br>');
                        updateAppErrorMsgAppList.add(appList);
                    }
                    
                }
            }
            if(updateAppErrorMsgAppList != null && updateAppErrorMsgAppList.size() > 0){
                update updateAppErrorMsgAppList;
            }
            for(clcommon__Address__c address : newAddressList){
                if(address.Is_Current__c && !String.isBlank(address.Address_Type__c) && !String.isBlank(address.clcommon__Account__c))
                {
                    Account account;
                    if(address.Address_Type__c.equalsignorecase('Mailing')){
                        if(!accountIdToAccountMap.containsKey(address.clcommon__Account__c)) {
                            account = new Account();
                            account.Id = address.clcommon__Account__c;
                            accountIdToAccountMap.put(account.id, account); 
                        }
                        else {
                            account = accountIdToAccountMap.get(address.clcommon__Account__c);
                        }
                        account.ShippingStreet = address.CustomerInfo_Addr_line_3__c;
                        account.ShippingCity = address.clcommon__City__c;
                        account.ShippingState = address.clcommon__State_Province__c;
                        account.ShippingCountry = address.clcommon__Country__c;
                        account.ShippingPostalCode = address.Postcode__c;
                    }
                    else if(address.Address_Type__c.equalsignorecase('Residential')){
                        if(!accountIdToAccountMap.containsKey(address.clcommon__Account__c)) {
                            account = new Account();
                            account.Id = address.clcommon__Account__c;
                            accountIdToAccountMap.put(account.id, account); 
                        }
                        else {
                            account = accountIdToAccountMap.get(address.clcommon__Account__c);
                        }
                        
                        String streetName = '';
                        if(!String.isBlank(address.Property_Type_Number__c)){
                            streetName = address.Property_Type_Number__c + '/';
                        }
                        account.BillingStreet = streetName + address.CustomerInfo_Addr_line_3__c;
                        account.BillingCity = address.clcommon__County__c;
                        account.BillingState = address.clcommon__State_Province__c;
                        account.BillingCountry = address.clcommon__Country__c;
                        account.BillingPostalCode = address.Postcode__c;
                    }
                }
            }
            CustomSecureDML.updateRecords(accountIdToAccountMap.values());
        }
        catch(Exception e){
            Database.rollback(sp);
            voLogInstance.logException(1001, '[MailingAddressTriggerhandler.copyAddresstoAccount] Exception at line : ' + e.getLineNumber(), e);
            voLogInstance.commitToDB();
        }
    }
}