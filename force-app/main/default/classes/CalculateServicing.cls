/**
 * @description       : 
 * @author            : chirag.gupta@q2.com
 * @group             : 
 * @last modified on  : 09-21-2020
 * @last modified by  : chirag.gupta@q2.com
 * Modifications Log 
 * Ver   Date         Author                Modification
 * 1.0   09-21-2020   chirag.gupta@q2.com   Initial Version
**/
global class CalculateServicing{

    public static mfiflexUtil.VOLog voLogInstance = mfiflexUtil.VOLog.getInstance('Genesis');
    
    //Method to calculate HEM Value and store it on application
    Webservice static String calculateHEM(ID appId){
        try{
            //Application query using method parameter
            List<genesis__Applications__c> appList = [SELECT id,
                                                             Name,
                                                             genesis__Account__c,
                                                             HEM_Value_Borrower__c,
                                                             HEM_Value_Co_Borrower__c,
                                                             HEM_Value_Guarantor__c,
                                                             Combine_HEM__c,
                                                             HEM_Recalculate_Message__c,
                                                             HEM_Value__c,
                                                             Gross_Annual_Income_Borrower__c,
                                                             Gross_Annual_Income_Co_Borrower__c,
                                                             Gross_Annual_Income_Guarantor__c,
                                                             (SELECT id,
                                                                     clcommon__Type__c,
                                                                     clcommon__Type__r.Name,
                                                                     clcommon__Account__c,
                                                                     Number_of_Dependents__c,
                                                                     Relationship_Status__c,
                                                                     Relationship_to_Applicant__c
                                                                FROM genesis__Parties__r
                                                               WHERE clcommon__Type__r.name IN ('BORROWER', 'CO-BORROWER', 'GUARANTOR')
                                                             )
                                                        FROM genesis__Applications__c 
                                                       WHERE id = :appId
                                                     ];  
            
            //If application found in the system and its post code in not null, then run the update logic
            if(appList != null && appList.size() > 0){
                genesis__Applications__c app = appList.get(0);
                List<clcommon__Party__c> listParties = app.genesis__Parties__r;
                Boolean hasSecondaryBorrower = false;
                Boolean coupleWithBorrower = false;
                Boolean isGuarantorPresent = false;
                List<String> coupleStatuses = System.label.COUPLE_RELATIONSHIP_STATUS.split(',');
                List<String> singleStatuses = System.label.SINGLE_RELATIONSHIP_STATUS.split(',');
                List<String> relationshipToBorr = System.label.COUPLE_RELATIONSHIP_TO_BORR_VALUES.split(',');
                String primMaritalStatus = '';
                String secMaritalStatus = '';
                String guarantorMaritalStatus = '';
                
                Map<Id, clcommon__Party__c> mapPartyIdsToParty = new Map<Id, clcommon__Party__c>();
                for(clcommon__Party__c party : listParties){
                    mapPartyIdsToParty.put(party.id, party);
                    if('BORROWER'.equalsIgnoreCase(party.clcommon__Type__r.Name)){
                        primMaritalStatus = party.Relationship_Status__c;
                    }
                    else if('CO-BORROWER'.equalsIgnoreCase(party.clcommon__Type__r.Name)){
                        hasSecondaryBorrower = true;
                        if(relationshipToBorr.contains(party.Relationship_to_Applicant__c)){
                            coupleWithBorrower = true;
                        }
                        secMaritalStatus = party.Relationship_Status__c;
                    }
                    else if('GUARANTOR'.equalsIgnoreCase(party.clcommon__Type__r.Name)){
                        guarantorMaritalStatus = party.Relationship_Status__c;
                        isGuarantorPresent = true;
                    }
                }

                List<clcommon__Address__c> residentialAddress = [SELECT id, 
                                                                        clcommon__Account__c,
                                                                        clcommon__Country__c,
                                                                        clcommon__State_Province__c,
                                                                        Postcode__c,
                                                                        Party__c
                                                                   FROM clcommon__Address__c
                                                                  WHERE Party__c IN :mapPartyIdsToParty.keySet()
                                                                    AND Address_Type__c = 'Residential'
                                                                    AND Is_Current__c = true
                                                                ];
                
                Map<Id, clcommon__Address__c> mapPartyIdToAddress = new Map<Id, clcommon__Address__c>();
                for(clcommon__Address__c addr : residentialAddress){
                    mapPartyIdToAddress.put(addr.Party__c, addr);
                }
                system.debug(mapPartyIdToAddress);
                Boolean boolCombineHEM = app.Combine_HEM__c;
                
                /****************************RFCL-1745******************************************************/
                Decimal grossIncomeFloor = genesis__Org_Parameters__c.getOrgDefaults().GrossIncomeFloor__c;
                Decimal primExpenseBenchmark = (app.Gross_Annual_Income_Borrower__c > grossIncomeFloor)?app.Gross_Annual_Income_Borrower__c :grossIncomeFloor;
                Decimal secExpenseBenchmark = (app.Gross_Annual_Income_Co_Borrower__c > grossIncomeFloor)?app.Gross_Annual_Income_Co_Borrower__c :grossIncomeFloor;
                Decimal guarantorExpenseBenchmark = (app.Gross_Annual_Income_Guarantor__c > grossIncomeFloor)?app.Gross_Annual_Income_Guarantor__c :grossIncomeFloor;
                
                app.GrossIncomeFloor__c = grossIncomeFloor;
                app.Expense_Benchmark_Borrower__c = primExpenseBenchmark;
                app.Expense_Benchmark_Co_Borrower__c = secExpenseBenchmark;
                
                Decimal primGrossAnnualIncome = boolCombineHEM ? (primExpenseBenchmark 
                                                                  + secExpenseBenchmark )
                                                                 : primExpenseBenchmark ;
                String primPostCode = '';
                String primStateCode = '';
                String primIncomeUnit = 'Single';
                if(boolCombineHEM){
                    String strMsg = '';
                    if(!hasSecondaryBorrower){
                        strMsg = 'Combined HEM is applicable only if a CO-BORROWER is present.';
                    }
                    else{
                        if(String.isBlank(primMaritalStatus)){
                            strMsg = 'Relationship Status is missing for Borrower.';
                        }
                        else if(String.isBlank(secMaritalStatus)){
                            strMsg = 'Relationship Status is missing for Co-Borrower.';
                        }
                        else if(!coupleStatuses.contains(primMaritalStatus) || !coupleStatuses.contains(secMaritalStatus)){
                            strMsg = 'Combine HEM is applicable if both the applicants have a Couple relationship.';
                        }
                        else if(!coupleWithBorrower){
                            strMsg = 'The relationship between borrower and coborrower is not applicable for Combine HEM.';
                        }
                    }
                    
                    if(!String.isBlank(strMsg)){
                        app.Combine_HEM__c = false;
                        update app;
                        return strMsg;
                    }
                    
                    primIncomeUnit = 'Couple';
                }
                else{
                    if(primMaritalStatus != null && String.isNotBlank(primMaritalStatus)){
                       if(singleStatuses.contains(primMaritalStatus)){
                           primIncomeUnit = 'Single';
                       }
                       else if(coupleStatuses.contains(primMaritalStatus)){
                           primIncomeUnit = 'Couple';
                       }
                       else{
                           return 'Relationship status configuration is missing for Borrower.';
                       }
                    }
                    else{
                        return 'Relationship Status is missing for Borrower.';
                    }
                }
                
                Integer primNumberOfDependents = 0;

                Decimal secGrossAnnualIncome = secExpenseBenchmark ;
                String secPostCode = '';
                String secStateCode = '';
                String secIncomeUnit = 'Single';
                if(hasSecondaryBorrower){
                    if(secMaritalStatus != null && String.isNotBlank(secMaritalStatus)){
                       if(singleStatuses.contains(secMaritalStatus)){
                           secIncomeUnit = 'Single';
                       }
                       else if(coupleStatuses.contains(secMaritalStatus)){
                           secIncomeUnit = 'Couple';
                       }
                       else{
                           return 'Relationship status configuration is missing for Co-Borrower.';
                       }
                    }
                    else{
                        return 'Relationship Status is missing for Co-Borrower.';
                    }
                }
                
                String guarantorIncomeUnit = '';
                if(isGuarantorPresent){
                    if(guarantorMaritalStatus != null && String.isNotBlank(guarantorMaritalStatus)){
                        if(singleStatuses.contains(guarantorMaritalStatus)){
                            guarantorIncomeUnit = 'Single';
                        }
                        else if(coupleStatuses.contains(guarantorMaritalStatus)){
                            guarantorIncomeUnit = 'Couple';
                        }
                        else{
                            return 'Relationship status configuration is missing for Guarantor.';
                        }
                    }
                    else{
                        return 'Relationship Status is missing for Guarantor.';
                    }
                }
                
                Integer secNumberOfDependents = 0;

                String guarantorPostCode = '';
                String guarantorStateCode = '';
                Integer guarantorNumberOfDependents = 0;
                for(Id idval : mapPartyIdToAddress.keySet()){
                    clcommon__Party__c partyInfo = mapPartyIdsToParty.get(idval);
                    clcommon__Address__c addr = mapPartyIdToAddress.get(idval);
                    if(partyInfo == null){
                        continue;
                    }
                    if('BORROWER'.equalsIgnoreCase(partyInfo.clcommon__Type__r.Name)){
                        primPostCode = addr.Postcode__c;
                        primStateCode = addr.clcommon__State_Province__c;
                        primNumberOfDependents = Integer.valueOf(partyInfo.Number_of_Dependents__c);
                    }
                    else if('CO-BORROWER'.equalsIgnoreCase(partyInfo.clcommon__Type__r.Name)){
                        secPostCode = addr.Postcode__c;
                        secStateCode = addr.clcommon__State_Province__c;
                        secNumberOfDependents = Integer.valueOf(partyInfo.Number_of_Dependents__c);
                    }
                    else if('GUARANTOR'.equalsIgnoreCase(partyInfo.clcommon__Type__r.Name)){
                        guarantorPostCode = addr.Postcode__c;
                        guarantorStateCode = addr.clcommon__State_Province__c;
                        guarantorNumberOfDependents = Integer.valueOf(partyInfo.Number_of_Dependents__c);
                    }
                }
                
                if(String.isBlank(primStateCode)){
                    return 'State value is missing in current residential address of Borrower.';




                }
                else if(String.isBlank(primPostCode)){
                    return 'Postcode value is missing in current residential address of Borrower.';
                }
                else if(String.isBlank(primIncomeUnit)){
                    return 'Relationship Status is missing for Borrower.';
                }
                else if(primGrossAnnualIncome <= 0 ){
                    return 'Income related financials are not yet provided for Borrower.';
                }
                else if(primNumberOfDependents == null){
                    return 'Number of dependents can not be blank for Borrower.';
                }

                app.HEM_Value_Borrower__c = HEMCalculator.getHEMValue(primStateCode,
                                                                      primPostCode,
                                                                      primIncomeUnit,
                                                                      primGrossAnnualIncome,
                                                                      primNumberOfDependents,
                                                                      100.00
                                                                     );
                
                app.HEM_Value__c = app.HEM_Value_Borrower__c;
                app.HEM_Value_Co_Borrower__c = 0;
                app.HEM_Value_Guarantor__c = 0;
                if(!boolCombineHEM && hasSecondaryBorrower){
                    if(String.isBlank(secStateCode)){
                        return 'State value is missing in current residential address of Co-Borrower.';




                    }
                    else if(String.isBlank(secPostCode)){
                        return 'Postcode value is missing in current residential address of Co-Borrower.';
                    }
                    else if(String.isBlank(secIncomeUnit)){
                        return 'Relationship Status is missing for Co-Borrower.';
                    }
                    else if(secGrossAnnualIncome <= 0 ){
                        return 'Income related financials are not yet provided for Co-Borrower.';
                    }
                    else if(secNumberOfDependents == null){
                        return 'Number of dependents can not be blank for Co-Borrower.';
                    }
                    
                    app.HEM_Value_Co_Borrower__c = HEMCalculator.getHEMValue(secStateCode,
                                                                             secPostCode,
                                                                             secIncomeUnit,
                                                                             secGrossAnnualIncome,
                                                                             secNumberOfDependents,
                                                                             100.00
                                                                            );
                    app.HEM_Value__c += app.HEM_Value_Co_Borrower__c;
                }
                
                if(isGuarantorPresent){
                        
                    if(String.isBlank(guarantorStateCode) ){
                        return 'State value is missing in current residential address of Guarantor.';
                    }
                    else if(String.isBlank(guarantorPostCode)){
                        return 'Postcode value is missing in current residential address of Guarantor.';
                    }
                    else if(String.isBlank(guarantorIncomeUnit) ){
                        return 'Relationship Status is missing for Guarantor.';
                    }
                    else if(guarantorExpenseBenchmark <= 0 ){
                        return 'Income related financials are not yet provided for Guarantor.';
                    }
                    else if(guarantorNumberOfDependents == null){
                        return 'Number of dependents can not be blank for Guarantor.';
                    }
                
                    app.HEM_Value_Guarantor__c = HEMCalculator.getHEMValue(guarantorStateCode,
                                                                           guarantorPostCode,
                                                                           guarantorIncomeUnit,
                                                                           guarantorExpenseBenchmark,
                                                                           guarantorNumberOfDependents,
                                                                           100.00
                                                                          );
                }
                app.HEM_Recalculate_Message__c='';
                update app; 
            }   
            return System.label.SUCCESS_MESSAGE;
        }    
        catch(Exception e){
            voLogInstance.logException(1001, '[CalculateServicing.calculate] Error: ' + e.getMessage() + ' (at line: ' + e.getLineNumber() + ')', e);
            voLogInstance.commitToDb();
            return 'Error: ' + e.getMessage() + ' (at line: ' + e.getLineNumber() + ')';
        }       
    }
    
    webservice static String financialDataValidations(ID appId, String partyTypes){
        try{
            if(String.isBlank(partyTypes)){
                return 'No party types to validate.';
            }
            
            List<String> listPartyTypes = partyTypes.split(',');
            Set<String> setPartyTypes = new Set<String>();
            for(String partyType : listPartyTypes){
                setPartyTypes.add(partyType);
            }
            
            Set<String> setActualParties = new Set<String>();
            for(String partyType : setPartyTypes){
                if(partyType.equalsIgnoreCase('JOINT')){
                    setActualParties.add('BORROWER');
                    setActualParties.add('CO-BORROWER');
                }
                else{
                    setActualParties.add(partyType);
                }
            }
            
            //Application query using method parameter
            List<genesis__Applications__c> appList = [SELECT id,
                                                             Name,
                                                             genesis__Account__c,
                                                             (SELECT id,
                                                                     clcommon__Type__c,
                                                                     clcommon__Type__r.Name,
                                                                     clcommon__Account__c
                                                                FROM genesis__Parties__r
                                                               WHERE clcommon__Type__r.name IN :setActualParties
                                                             )
                                                        FROM genesis__Applications__c 
                                                       WHERE id = :appId
                                                     ];  
            
            //If application found in the system and its post code in not null, then run the update logic
            if(appList != null && appList.size() > 0){
                genesis__Applications__c app = appList.get(0);
                List<clcommon__Party__c> listParties = app.genesis__Parties__r;
                
                Integer noOfGuarantors = 0;
                Integer noOfBorrowers = 0;
                Integer noOfCoBorrowers = 0;
                
                for(clcommon__Party__c party : listParties){
                    if('BORROWER'.equalsIgnoreCase(party.clcommon__Type__r.Name)){
                        noOfBorrowers++;
                    }    
                    else if('CO-BORROWER'.equalsIgnoreCase(party.clcommon__Type__r.Name)){
                        noOfCoBorrowers++;
                    }
                    else if('GUARANTOR'.equalsIgnoreCase(party.clcommon__Type__r.Name)){
                        noOfGuarantors++;
                    }
                }
                
                if(setActualParties.contains('BORROWER') && noOfBorrowers == 0){
                    return 'Please introduce borrower party first before providing financials as party type = (Borrower or Joint).';
                }    
                else if(setActualParties.contains('CO-BORROWER') && noOfCoBorrowers == 0){
                    return 'Please introduce co-borrower party first before providing financials as party type = Co-Borrower.';
                }
                else if(setActualParties.contains('GUARANTOR') && noOfGuarantors == 0){
                    return 'Please introduce guarantor party first before providing financials as party type = Guarantor.';
                }
            }   
            return System.label.SUCCESS_MESSAGE;
        }    
        catch(Exception e){
            return 'Error: ' + e.getMessage() + ' (at line: ' + e.getLineNumber() + ')';
        }       
    }
}