public class RACVF_ContentDocumentModel {
    
    @AuraEnabled
    public Id RecordId {get;set;}
    
    @AuraEnabled
    public Id ContentDocLinkId {get;set;}
    
    @AuraEnabled
    public String Title {get;set;}
    
    @AuraEnabled
    public String FileType {get;set;}
    
    @AuraEnabled
    public String FileSize {get;set;}
    
    @AuraEnabled
    public Id LinkedEntityId {get;set;}
    
    @AuraEnabled
    public String CreatedDate {get;set;}
    
    @AuraEnabled
    public String Createdby {get;set;}
    
    @AuraEnabled
    public String ModifiedDate {get;set;}
    
    @AuraEnabled
    public String LastViewDate {get;set;}
    
    @AuraEnabled
    public String DocumentCategory {get;set;}
    
    @AuraEnabled
    public String DocumentCategoryId {get;set;}
    
    @AuraEnabled
    public String DocumentCategoryAssocId {get;set;}

}