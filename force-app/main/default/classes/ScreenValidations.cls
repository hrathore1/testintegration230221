public class ScreenValidations {
    /*public static List<String> onSubmitToCredit(String applicationIdString) {
        List<String> returnList = new List<String>();
        try{
            if(String.isNotBlank(applicationIdString)){
                //Checking Rule Starts
                genesis__Applications__c applicationObject = new genesis__Applications__c(Id = applicationIdString);
                
                List<genesis__Rule__c> submitToCreditRuleList = new List<genesis__Rule__c>();
                submitToCreditRuleList = [SELECT id,
                                                 name 
                                            FROM genesis__Rule__c 
                                           WHERE genesis__Enabled__c = true 
                                             AND Rule_Classification__c = 'Submit to Credit'
                                         ];
                if(submitToCreditRuleList.size() > 0){
                    List<genesis__Checklist__c> submitToCreditRuleCheckList = genesis.RulesAPI.evaluateRules(applicationObject, submitToCreditRuleList, false, false);
                    
                    if(submitToCreditRuleCheckList != null){
                        for(genesis__Checklist__c checkListRecord : submitToCreditRuleCheckList){
                            if(checkListRecord.genesis__Result__c == false){
                                returnList.add(checkListRecord.genesis__Message__c);
                            }
                        }
                    }
                }
                
                //Checking Rule Ends
                
                //Custom Validation Starts
                //Address validation and Party type(Borrower) starts
                Set<String> partyTypeSet = new Set<String>{'GUARANTOR','CO-BORROWER','BORROWER'};
                List<clcommon__Party__c> partyList = [SELECT Id,
                                                             clcommon__Account__c,
                                                             clcommon__Account__r.Name,
                                                             clcommon__Account__r.Age__pc,
                                                             clcommon__Account__r.Credit_Check_Consent__c,
                                                             Party_Type_Name__c
                                                        FROM clcommon__Party__c
                                                       WHERE genesis__Application__c =: applicationIdString
                                                         AND Party_Type_Name__c IN :partyTypeSet
                                                         AND clcommon__Account__c != null
                                                     ];

                if(partyList != null && partyList.size() > 0){
                    Map<String, Set<Id>> partyTypeStringVsAccountIdSetMap = new Map<String, Set<Id>>();
                    Map<Id, Account> partyRelatedAccountIdVsAccountObjectMap = new Map<Id, Account>();
                    Map<Id, Decimal> partyRelatedAccountIdVsAccountAgeDecimalMap = new Map<Id, Decimal>();
                    
                    //Boolean borrowerFlagForBORROWERCheckBoolean = false;
                    Set<Id> accountIdSet = new Set<Id>();
                    Integer temparayCountForCOBORROWERCheckInteger = 0;
                    Integer temparayCountForBORROWERCheckInteger = 0;

                    for(clcommon__Party__c partyRecord :  partyList){
                        if(partyTypeStringVsAccountIdSetMap.get(partyRecord.Party_Type_Name__c) == null){
                            partyTypeStringVsAccountIdSetMap.put(partyRecord.Party_Type_Name__c, new Set<Id>());
                        }
                        partyTypeStringVsAccountIdSetMap.get(partyRecord.Party_Type_Name__c).add(partyRecord.clcommon__Account__c);
                        
                        if(partyRecord.Party_Type_Name__c == 'BORROWER'){// && !borrowerFlagForBORROWERCheckBoolean){
                            //borrowerFlagForBORROWERCheckBoolean = true;
                            temparayCountForBORROWERCheckInteger++;
                        }
                        if(partyRecord.Party_Type_Name__c == 'CO-BORROWER'){
                            temparayCountForCOBORROWERCheckInteger++;    
                        }
                        
                        if(partyRelatedAccountIdVsAccountObjectMap.get(partyRecord.clcommon__Account__c) == null){
                            Account accountTemporaryObject = new Account(Name = partyRecord.clcommon__Account__r.Name,
                                                                         Credit_Check_Consent__c = partyRecord.clcommon__Account__r.Credit_Check_Consent__c); 
                            partyRelatedAccountIdVsAccountObjectMap.put(partyRecord.clcommon__Account__c, accountTemporaryObject);
                            if(partyRecord.clcommon__Account__r.Age__pc != null)
                                partyRelatedAccountIdVsAccountAgeDecimalMap.put(partyRecord.clcommon__Account__c, partyRecord.clcommon__Account__r.Age__pc);
                        }
                    }
                    
                    //if(!borrowerFlagForBORROWERCheckBoolean){
                    if(temparayCountForBORROWERCheckInteger == 0){
                        //returnList.add('No Borrower party Found!');
                        //Getting message using custom label.
                        returnList.add(Label.Application_Borrow_Mandatory_Validation_Error_Message);
                    }
                    else if(temparayCountForBORROWERCheckInteger > 1){
                        returnList.add('More than one Party as BORROWER not allowed on application.');//Label.Application_Borrow_Mandatory_Validation_Error_Message);
                    }

                    //Check for more then one Co-Borrower
                    if(temparayCountForCOBORROWERCheckInteger > 1){
                        returnList.add(Label.Application_COBORROWER_Validation_Error_Message);
                    }

                    //Address Party type(Borrower) Ends
                    if(!partyTypeStringVsAccountIdSetMap.isEmpty()){
                        for(String tempatoryString : partyTypeStringVsAccountIdSetMap.keySet()){
                            accountIdSet.addAll(partyTypeStringVsAccountIdSetMap.get(tempatoryString));
                        }
                        if(accountIdSet.size() > 0){
                            Map<Id, Decimal> accountIdVsResidentialAddressTotalMonthsIntegerMap = new Map<Id, Decimal>();
                            Map<Id, Decimal> accountIdVsEmploymentInformationTotalMonthsDecimalMap = new Map<Id, Decimal>();
                            Map<Id, Boolean> accountIdVsCheckingMailingAdressBooleanMap = new Map<Id, Boolean>();
                            
                            Set<String> addressTypeSet = new Set<String>{'Residential', 'Mailing'};    
                            List<clcommon__Address__c> addressRelatedToAccountList = [SELECT Id,
                                                                                             Address_Type__c,
                                                                                             Tenure_Total_Months__c,
                                                                                             clcommon__Account__c,
                                                                                             Is_Current__c
                                                                                        FROM clcommon__Address__c
                                                                                       WHERE clcommon__Account__c IN: accountIdSet
                                                                                         AND Address_Type__c IN: addressTypeSet
                                                                                         AND Tenure_Total_Months__c > 0
                                                                                     ];
                            
                            System.debug('addressRelatedToAccountList:' + addressRelatedToAccountList);
                            if(addressRelatedToAccountList != null && addressRelatedToAccountList.size() > 0){                                
                                for(clcommon__Address__c addressRecord : addressRelatedToAccountList){
                                    if(addressRecord.Address_Type__c == 'Residential'){
                                        if(accountIdVsResidentialAddressTotalMonthsIntegerMap.get(addressRecord.clcommon__Account__c) == null){
                                            accountIdVsResidentialAddressTotalMonthsIntegerMap.put(addressRecord.clcommon__Account__c, 0);
                                        }
                                        if(addressRecord.Tenure_Total_Months__c != null && addressRecord.Tenure_Total_Months__c > 0)
                                            accountIdVsResidentialAddressTotalMonthsIntegerMap.put(addressRecord.clcommon__Account__c, accountIdVsResidentialAddressTotalMonthsIntegerMap.get(addressRecord.clcommon__Account__c) + addressRecord.Tenure_Total_Months__c);
                                    }
                                    else{ 
                                        //Getting for Mailing address data for validation Starts
                                        if(addressRecord.Is_Current__c){
                                            accountIdVsCheckingMailingAdressBooleanMap.put(addressRecord.clcommon__Account__c, true);
                                        }
                                        else{
                                            accountIdVsCheckingMailingAdressBooleanMap.put(addressRecord.clcommon__Account__c, false);
                                        }
                                        //Getting for Mailing address data for validation Ends
                                    }
                                }


                            }
                            
                            List<genesis__Employment_Information__c> employmentInformationRelatedToAccountList = [SELECT Id,
                                                                                                                         genesis__Contact__c,
                                                                                                                         genesis__Contact__r.accountId,
                                                                                                                         Total_Months_on_Job__c
                                                                                                                    FROM genesis__Employment_Information__c
                                                                                                                   WHERE genesis__Application__c =: applicationIdString
                                                                                                                     AND Total_Months_on_Job__c > 0
                                                                                                                 ];

                            if(employmentInformationRelatedToAccountList != null && employmentInformationRelatedToAccountList.size() > 0){
                                for(genesis__Employment_Information__c employmentInfomationRecord : employmentInformationRelatedToAccountList){
                                    if(!accountIdVsEmploymentInformationTotalMonthsDecimalMap.containsKey(employmentInfomationRecord.genesis__Contact__r.accountId)){
                                        accountIdVsEmploymentInformationTotalMonthsDecimalMap.put(employmentInfomationRecord.genesis__Contact__r.accountId, 0);
                                    }
                                    if(employmentInfomationRecord.Total_Months_on_Job__c != null && employmentInfomationRecord.Total_Months_on_Job__c > 0)
                                        accountIdVsEmploymentInformationTotalMonthsDecimalMap.put(employmentInfomationRecord.genesis__Contact__r.accountId, accountIdVsEmploymentInformationTotalMonthsDecimalMap.get(employmentInfomationRecord.genesis__Contact__r.accountId) + employmentInfomationRecord.Total_Months_on_Job__c);
                                }
                            }
                            
                            for(String partyTypeString : partyTypeStringVsAccountIdSetMap.keySet()){
                                for(Id partyRelatedAccountId : partyTypeStringVsAccountIdSetMap.get(partyTypeString)){
                                    Account temparayAccountRecord = partyRelatedAccountIdVsAccountObjectMap.get(partyRelatedAccountId);
                                    
                                    if(accountIdVsResidentialAddressTotalMonthsIntegerMap.containsKey(partyRelatedAccountId)){
                                        if(accountIdVsResidentialAddressTotalMonthsIntegerMap.get(partyRelatedAccountId) < 36){
                                            returnList.add('For Party Type ' + partyTypeString + ' Account ' + temparayAccountRecord.Name + ' (' + partyRelatedAccountId + ' ),' + Label.Application_Account_Residential_Address_Validation_Error_Message);
                                        }
                                    }
                                    else{
                                        returnList.add('For Party Type ' + partyTypeString + ' Account ' + temparayAccountRecord.Name + ' (' + partyRelatedAccountId + ' ),' + Label.Application_Account_Residential_Address_Validation_Error_Message_2);
                                    }
                                    
                                    //if(accountIdVsResidentialAddressTotalMonthsIntegerMap.get(partyRelatedAccountId) != null && 
                                    //   accountIdVsResidentialAddressTotalMonthsIntegerMap.get(partyRelatedAccountId) < 36){
                                    //       returnList.add('For Party Type ' + partyTypeString + ' Account ' + temparayAccountRecord.Name + ' (' + partyRelatedAccountId + ' ),' + Label.Application_Account_Residential_Address_Validation_Error_Message);
                                    //   }else if(accountIdVsResidentialAddressTotalMonthsIntegerMap.get(partyRelatedAccountId) == null){
                                    //       returnList.add('For Party Type ' + partyTypeString + ' Account ' + temparayAccountRecord.Name + ' (' + partyRelatedAccountId + ' ),' + Label.Application_Account_Residential_Address_Validation_Error_Message_2);
                                    //   }
                                    //System.debug('Mailing address map:'+ accountIdVsCheckingMailingAdressBooleanMap);
                                    //System.debug('Mailing address account:'+ partyRelatedAccountId);
                                    if(accountIdVsCheckingMailingAdressBooleanMap.containsKey(partyRelatedAccountId)){
                                        if(!accountIdVsCheckingMailingAdressBooleanMap.get(partyRelatedAccountId)){
                                            returnList.add('For Party Type ' + partyTypeString + ' Account ' + temparayAccountRecord.Name + ' (' + partyRelatedAccountId + ' ),' + Label.Application_Account_Mailing_Address_Validation_Error_Message);
                                        }
                                    }
                                    else{
                                        returnList.add('For Party Type ' + partyTypeString + ' Account ' + temparayAccountRecord.Name + ' (' + partyRelatedAccountId + ' ),' + Label.Application_Account_Mailing_Address_Validation_Error_Message_2);
                                    }

                                    //if(accountIdVsCheckingMailingAdressBooleanMap.get(partyRelatedAccountId) != null &&
                                    //  accountIdVsCheckingMailingAdressBooleanMap.get(partyRelatedAccountId) != true){
                                    //    returnList.add('For Party Type ' + partyTypeString + ' Account ' + temparayAccountRecord.Name + ' (' + partyRelatedAccountId + ' ),' + Label.Application_Account_Mailing_Address_Validation_Error_Message);
                                    //  }else if(accountIdVsCheckingMailingAdressBooleanMap.get(partyRelatedAccountId) == null){
                                    //      returnList.add('For Party Type ' + partyTypeString + ' Account ' + temparayAccountRecord.Name + ' (' + partyRelatedAccountId + ' ),' + Label.Application_Account_Mailing_Address_Validation_Error_Message_2);
                                    //  }
                                    
                                    System.debug(accountIdVsEmploymentInformationTotalMonthsDecimalMap);
                                    System.debug(partyRelatedAccountId);
                                    if(accountIdVsEmploymentInformationTotalMonthsDecimalMap.containsKey(partyRelatedAccountId)){
                                        if(accountIdVsEmploymentInformationTotalMonthsDecimalMap.get(partyRelatedAccountId) < 36){
                                            returnList.add('For Party Type ' + partyTypeString + ' Account ' + temparayAccountRecord.Name + ' (' + partyRelatedAccountId + ' ),' + Label.Application_Employment_Data_Validation_Error_Message);
                                        }
                                    }
                                    else{
                                        returnList.add('For Party Type ' + partyTypeString + ' Account ' + temparayAccountRecord.Name + ' (' + partyRelatedAccountId + ' ),' + Label.Application_Employment_Data_Validation_Error_Message_2);
                                    }

                                    //if(accountIdVsEmploymentInformationTotalMonthsDecimalMap.get(partyRelatedAccountId) != null &&
                                    //   accountIdVsEmploymentInformationTotalMonthsDecimalMap.get(partyRelatedAccountId) < 36){
                                    //       returnList.add('For Party Type ' + partyTypeString + ' Account ' + temparayAccountRecord.Name + ' (' + partyRelatedAccountId + ' ),' + Label.Application_Employment_Data_Validation_Error_Message);
                                    //   }else if(accountIdVsEmploymentInformationTotalMonthsDecimalMap.get(partyRelatedAccountId) == null){
                                    //       returnList.add('For Party Type ' + partyTypeString + ' Account ' + temparayAccountRecord.Name + ' (' + partyRelatedAccountId + ' ),' + Label.Application_Employment_Data_Validation_Error_Message_2);
                                    //   }

                                    if(!temparayAccountRecord.Credit_Check_Consent__c){
                                        returnList.add('For Party Type ' + partyTypeString + ' Account ' + temparayAccountRecord.Name + ' (' + partyRelatedAccountId + ' ),' + Label.Application_Account_Credit_Check_Consent_Validation_Error_Message);
                                    }

                                    if(!partyRelatedAccountIdVsAccountAgeDecimalMap.containsKey(partyRelatedAccountId) || 
                                       partyRelatedAccountIdVsAccountAgeDecimalMap.get(partyRelatedAccountId) <= 0){
                                           returnList.add('For Party Type ' + partyTypeString + ' Account ' + temparayAccountRecord.Name + ' (' + partyRelatedAccountId + ' ),' + Label.Application_Account_DOB_Validation_Error_Message);
                                       }
                                }
                            }
                        }
                    }
                }
                else{
                     returnList.add(Label.Application_Borrow_Mandatory_Validation_Error_Message);
                }
                //Address validation Ends
                
                //Security Check(Application Collaterals) Starts
                List<genesis__Application_Collateral__c> applicationCollateralList = new List<genesis__Application_Collateral__c>();
                applicationCollateralList = [SELECT Id
                                               FROM genesis__Application_Collateral__c 
                                              WHERE genesis__Application__c =: applicationIdString
                                                AND genesis__Collateral__r.Final_Collateral_Value__c > 0
                                            ];
                if(applicationCollateralList.size() == 0){
                    //returnList.add('Either no security present or retail value is not great zero.');
                    //Getting message using custom label.
                    returnList.add(Label.Application_Security_Validation_Error_Message);
                }
                
                //Security Check(Application Collaterals) Ends
                //Custom Validation Ends
            }
            else{
                returnList.add('Application Id is null or blank');
            }
        }
        catch(Exception e){
            System.debug('Exception: ' + e.getMessage() + ' at ' + e.getLineNumber());
            returnList.add('Exception:' + e.getMessage());
        }
        return returnList;
    }*/
}