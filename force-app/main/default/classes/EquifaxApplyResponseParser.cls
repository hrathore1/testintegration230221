/*
******************************************************************
Class Description : Custom parser to store the equifax apply response on 'Equifax Apply' object and application object
******************************************************************
*/

public class EquifaxApplyResponseParser {
    
    //Method to parse the veda response and store details on applications
    public String parseResponse(ints.VedaComprehensiveResponse res, Id appId, Id accId){
        system.debug(res);
        mfiflexUtil.VOLog voLogInstance = mfiflexUtil.VOLog.getInstance('Genesis') ;
        Map<String,Equifax_Apply_Metadata__c> codeToFieldName = new Map<String,Equifax_Apply_Metadata__c>();
        List<Attachment> pdfAtts= new List<Attachment>();
        String enqId = '';
        String reqXml = '';
        String resXml = '';
        String errorMsg ='';
        try{
            genesis__Applications__c app = [SELECT id
                                              FROM genesis__Applications__c 
                                             WHERE id = :appId
                                           ];
            Account acc = [SELECT id, 
                                  Name,
                                  Last_Equifax_Apply_call__c
                             FROM Account
                            WHERE id =: accId];
                            
            if(acc != null && app != null){


                    Equifax_Apply__c eqDynamicObject = new Equifax_Apply__c();
                    //Schema.sObjectType dynamicObject = eqInstance.getSObjectType();
                    //Equifax_Apply__c eqDynamicObject = (Equifax_Apply__c) dynamicObject.newSObject();
                    List<Equifax_Summary_Data__c> eqSummaryListInsert = new List<Equifax_Summary_Data__c>();
                    List<Equifax_Apply_Account__c> eqAccountListInsert = new List<Equifax_Apply_Account__c>();
                    List<Equifax_Apply_Account_Default__c> eqDefaultsInsert = new List<Equifax_Apply_Account_Default__c>();
                    Map<Decimal,List<Equifax_Apply_Account_Default__c>> internalIdToEqDefaults = new Map<Decimal,List<Equifax_Apply_Account_Default__c>>();
                    eqDynamicObject.Application__c = appId;
                    eqDynamicObject.Account__c = accId;
                    
                    insert eqDynamicObject;
                    //system.debug('check response = '+res);
                    eqDynamicObject.Response_Status_Code__c = Integer.ValueOf(res.integrationErrorStatusCode);
                    if(res.errors != null && res.errors.size()>0)
                    {
                        for(ints.VedaComprehensiveResponse.Error err : res.errors){
                            errorMsg = (err.faultCode!=null?'<ERR-1>:fault-code = '+err.faultCode:'')
                                     + (err.detail!=null?' : detail = '+err.detail:'') 
                                     + (err.faultString!=null?' : faultString = ' + err.faultString:'') ;
                        }
                        if(String.isNotBlank(errorMsg))
                        {
                            eqDynamicObject.Error_Value__c = errorMsg;
                        }
                    }
                    else{
                        //***** Employment Information *****
                        system.debug('Employment Details ========');
                        if(res.employers != null && res.employers.size() > 0){
                            for(ints.VedaComprehensiveResponse.employer emp: res.employers){
                                if(emp.firstReportedDate != null){
                                    system.debug(emp.firstReportedDate);
                                    eqDynamicObject.Employment_First_Reported_Date__c = emp.firstReportedDate;









                                }
                                if(emp.lastReportedDate != null){
                                    system.debug(emp.lastReportedDate);
                                    eqDynamicObject.Employment_Last_Reported_Date__c = emp.lastReportedDate;
                                }
                                if(emp.name != null){
                                    system.debug(emp.name);
                                    eqDynamicObject.Employer_Name__c = emp.name;
                                }
                                if(emp.occupation != null){
                                    system.debug(emp.occupation);
                                    eqDynamicObject.Occupation__c = emp.occupation;
                                }
                                if(emp.type != null){
                                    system.debug(emp.type);
                                    eqDynamicObject.Employment_Type__c = emp.type;
                                }
                                if(emp.idReferences != null && emp.idReferences.size() > 0){
                                    String refs = '';
                                    for(String idRef: emp.idReferences){
                                        system.debug(idRef);
                                        refs += (idRef+',');
                                    }
                                    eqDynamicObject.Employer_Reference_Id__c = refs.substringBeforeLast(',');
                                }
                            }
                        }

                    
                        //***** Product Information *****
                        system.debug('Product Information ========');
                        if(res.enquiryId != null && !String.isBlank(res.enquiryId)){
                            system.debug(res.enquiryId);
                            eqDynamicObject.Enquiry_Id__c = res.enquiryId;
                            enqId = res.enquiryId;





































































































                        }
                        if(res.datetimeRequested != null && !String.isBlank(res.datetimeRequested)){
                            system.debug(res.datetimeRequested);
                            eqDynamicObject.DateTime_Requested__c = res.datetimeRequested;
                        }
                        if(res.datetimeGenerated != null && !String.isBlank(res.datetimeGenerated)){
                            system.debug(res.datetimeGenerated);
                            eqDynamicObject.DateTime_Generated__c = res.datetimeGenerated;
                        }
                        if(res.clientReference != null && !String.isBlank(res.clientReference)){
                            system.debug(res.clientReference);
                            eqDynamicObject.Client_Reference__c = res.clientReference;
                        }
                        if(res.operatorId != null && !String.isBlank(res.operatorId)){
                            system.debug(res.operatorId);
                            eqDynamicObject.Operator_Id__c = res.operatorId;
                        }
                        if(res.operatorName != null && !String.isBlank(res.operatorName)){
                            system.debug(res.operatorName);
                            eqDynamicObject.Operator_Name__c= res.operatorName;
                        }
                        
                        // product-name missing
                        
                        if(res.permissionType != null && !String.isBlank(res.permissionType)){
                            system.debug(res.permissionType);
                            eqDynamicObject.Permission_Type__c = res.permissionType;
                        }
                        if(res.productDataLevel != null && !String.isBlank(res.productDataLevel)){
                            system.debug(res.productDataLevel);
                            eqDynamicObject.Product_DataLevel__c = res.productDataLevel;
                        }
                        if(res.productVersion != null && !String.isBlank(res.productVersion)){
                            system.debug(res.productVersion);
                            eqDynamicObject.Product_Version__c = res.productVersion;
                        }
                        if(res.primaryMatchType != null && !String.isBlank(res.primaryMatchType)){
                            system.debug(res.primaryMatchType);
                            eqDynamicObject.Primary_Match_Type__c = res.primaryMatchType;
                        }
                        if(res.bureauReference != null && !String.isBlank(res.bureauReference)){
                            system.debug(res.bureauReference);
                            eqDynamicObject.Bureau_Reference__c = res.bureauReference;
                        }
                        Integer accCount = 0;
                        if(res.accounts != null && res.accounts.size() > 0){
                            for(ints.VedaComprehensiveResponse.account charObj : res.accounts){
                                accCount++;
                                Equifax_Apply_Account__c eqAcc = new Equifax_Apply_Account__c();
                                eqAcc.Equifax_Apply__c = eqDynamicObject.id;
                                if(charObj.accountType != null){
                                    system.debug(charObj.accountType);
                                    eqAcc.Account_Type__c = charObj.accountType;
                                }
                                if(charObj.accountTypeCode != null){
                                    system.debug(charObj.accountTypeCode);
                                    eqAcc.Account_Type_Code__c = charObj.accountTypeCode;
                                }
                                if(charObj.accountId != null){
                                    system.debug(charObj.accountId);
                                    eqAcc.Account_Id__c = charObj.accountId;
                                }
                                if(charObj.originalCreditProviderName != null){
                                    system.debug(charObj.originalCreditProviderName);
                                    eqAcc.Original_Credit_Provider_Name__c = charObj.originalCreditProviderName;
                                }
                                if(charObj.latestCreditProviderName != null){
                                    system.debug(charObj.latestCreditProviderName);
                                    eqAcc.Latest_Credit_Provider_Name__c = charObj.latestCreditProviderName;
                                }
                                if(charObj.latestCreditProviderTransferDate!= null){
                                    system.debug(charObj.latestCreditProviderTransferDate);
                                    eqAcc.Latest_Credit_Provider_Transfer_Date__c = charObj.latestCreditProviderTransferDate;
                                }
                                if(charObj.relationship != null){
                                    system.debug(charObj.relationship);
                                    eqAcc.Account_Relationship__c = charObj.relationship;
                                }
                                if(charObj.relationshipCode != null){
                                    system.debug(charObj.relationshipCode);
                                    eqAcc.Account_Relationship_Code__c = charObj.relationshipCode;
                                }
                                if(charObj.accountHolderCount != null){
                                    system.debug(charObj.accountHolderCount);
                                    eqAcc.Account_Holder_Count__c = charObj.accountHolderCount ;
                                }
                                if(charObj.accountOpenDate != null){
                                    system.debug(charObj.accountOpenDate);
                                    eqAcc.Account_Open_Date__c = charObj.accountOpenDate ;
                                }
                                if(charObj.accountClosedDate != null){
                                    system.debug(charObj.accountClosedDate);
                                    eqAcc.Account_Closed_Date__c = charObj.accountClosedDate ;
                                }
                                if(charObj.loanPaymentMethod != null){
                                    system.debug(charObj.loanPaymentMethod);
                                    eqAcc.Loan_Payment_Method__c = charObj.loanPaymentMethod ;
                                }
                                if(charObj.termType != null){
                                    system.debug(charObj.termType);
                                    eqAcc.Term_Type__c = charObj.termType ;
                                }
                                if(charObj.securedCredit != null){
                                    system.debug(charObj.securedCredit);
                                    eqAcc.Secured_Credit__c = charObj.securedCredit ;
                                }
                                if(charObj.securedCreditCode != null){
                                    system.debug(charObj.securedCreditCode);
                                    eqAcc.Secured_Credit_Code__c = charObj.securedCreditCode ;
                                }
                                if(charObj.termOfLoan != null){
                                    system.debug(charObj.termOfLoan);
                                    eqAcc.Term_Of_Loan__c = charObj.termOfLoan ;
                                }
                                if(charObj.originalMaximumAmountCreditCurrencyCode != null){
                                    system.debug(charObj.originalMaximumAmountCreditCurrencyCode);
                                    eqAcc.OriginalMaximumAmountCreditCurrencyCode__c = charObj.originalMaximumAmountCreditCurrencyCode ;
                                }
                                if(charObj.originalMaximumAmountCredit != null){
                                    system.debug(charObj.originalMaximumAmountCredit);
                                    eqAcc.OriginalMaximumAmountCredit__c = charObj.originalMaximumAmountCredit ;
                                }
                                if(charObj.associationInformationIsSeriousCreditInfringement != null){
                                    eqAcc.Is_Serious_Credit_Infringement__c = charObj.associationInformationIsSeriousCreditInfringement;
                                }
                                eqAcc.Internal_Id__c = accCount;
                                //system.debug('defaults: '+charObj.defaults);
                                List<Equifax_Apply_Account_Default__c> eqDefaults = new List<Equifax_Apply_Account_Default__c>();
                                for(ints.VedaComprehensiveResponse.VedaComprehensivedefault defaults: charObj.defaults){
                                    //system.debug('defaults:- '+defaults);
                                    Equifax_Apply_Account_Default__c eaDef = new Equifax_Apply_Account_Default__c();
                                    if(defaults.currentDefaultDateRecorded != null){
                                        eaDef.Current_Default_Date_Recorded__c = defaults.currentDefaultDateRecorded;
                                    }
                                    if(defaults.currentDefaultDefaultAmount != null){
                                        eaDef.Current_Default_Amount__c = defaults.currentDefaultDefaultAmount;
                                    }
                                    if(defaults.currentDefaultDefaultAmountCurrencyCode != null){
                                        eaDef.Current_Default_Amount_CurrencyCode__c = defaults.currentDefaultDefaultAmountCurrencyCode;
                                    }
                                    if(defaults.currentDefaultReasonToReport != null){
                                        eaDef.Current_Default_Reason_To_Report__c = defaults.currentDefaultReasonToReport;
                                    }
                                    if(defaults.originalDefaultDateRecorded != null){
                                        eaDef.Original_Default_Date_Recorded__c = defaults.originalDefaultDateRecorded;
                                    }
                                    if(defaults.originalDefaultDefaultAmount != null){
                                        eaDef.Original_Default_Amount__c = defaults.originalDefaultDefaultAmount;
                                    }
                                    if(defaults.originalDefaultDefaultAmountCurrencyCode != null){
                                        eaDef.Original_Default_Amount_CurrencyCode__c = defaults.originalDefaultDefaultAmountCurrencyCode;
                                    }
                                    if(defaults.originalDefaultDefaultReasonToReport != null){
                                        eaDef.Original_Default_Reason_To_Report__c = defaults.originalDefaultDefaultReasonToReport;
                                    }
                                    if(defaults.status != null){
                                        eaDef.Status__c = defaults.status;
                                    }
                                    if(defaults.currentDefaultDateRecorded != null){
                                        eaDef.Current_Default_Date_Recorded__c = defaults.currentDefaultDateRecorded;
                                    }
                                    if(defaults.statusCode != null){
                                        eaDef.Status_Code__c = defaults.statusCode;
                                    }
                                    if(defaults.statusDate != null){
                                        eaDef.Status_Date__c = defaults.statusDate;
                                    }
                                    eqDefaults.add(eaDef);
                                    
                                }
                                internalIdToEqDefaults.put(accCount,eqDefaults);
                                eqAccountListInsert.add(eqAcc);
                            }
                        }
                        
                        //***** Equifax Apply Score *****
                        List<Contributing_Factor__c> cfList = new List<Contributing_Factor__c>();
                        if(res.scoreData != null && res.scoreData.size() > 0){
                            for(ints.VedaComprehensiveResponse.Score scoreObj : res.scoreData){
                                if(scoreObj != null){
                                    
                                    if(scoreObj.scoreId != null){
                                        System.debug('scoreId : '+ scoreObj.scoreId); // Positive
                                        eqDynamicObject.Scorecard_Id__c = scoreObj.scoreId;
                                    }
                                    if(scoreObj.scoreName != null){
                                        System.debug('scoreName : '+ scoreObj.scoreName);
                                        eqDynamicObject.Scorecard_Name__c = scoreObj.scoreName;
                                    }
                                    if(scoreObj.scoreVersion != null){
                                        System.debug('scoreVersion : '+ scoreObj.scoreVersion);
                                        eqDynamicObject.Scorecard_Version__c = scoreObj.scoreVersion;
                                    }
                                    if(scoreObj.scorType != null){
                                        System.debug('scorType : '+ scoreObj.scorType);
                                        eqDynamicObject.Scorecard_Type__c = scoreObj.scorType;
                                    }
                                    if(scoreObj.scoreDataLevel != null){
                                        System.debug('scoreDataLevel : '+ scoreObj.scoreDataLevel);
                                        eqDynamicObject.Scorecard_Datalevel__c = scoreObj.scoreDataLevel;
                                    }
                                    if(scoreObj.riskOdds != null){
                                        System.debug('riskOdds : '+ scoreObj.riskOdds);
                                        eqDynamicObject.Score_Risk_Odds__c = scoreObj.riskOdds;
                                    }
                                    if(scoreObj.scoreMasterscale != null && scoreObj.scoreMasterscale != null){
                                        System.debug('scoreMasterscale : '+ Decimal.valueOf(String.valueOf(scoreObj.scoreMasterscale)));
                                        eqDynamicObject.Score_Masterscale__c = scoreObj.scoreMasterscale;
                                    }
                                    if(scoreObj.contributingFactor != null && scoreObj.contributingFactor.size() > 0){
                                        for(ints.VedaComprehensiveResponse.ContributingFactor contFactor :scoreObj.contributingFactor){
                                            if(contFactor != null){
                                                Contributing_Factor__c cf = new Contributing_Factor__c();
                                                cf.Equifax_Apply__c = eqDynamicObject.id;
                                                if(contFactor.contributingFactorId != null){
                                                    system.debug(contFactor.contributingFactorId);
                                                    cf.Contributing_Factor_Id__c = contFactor.contributingFactorId;
                                                }
                                                if(contFactor.description != null){
                                                    system.debug(contFactor.description);
                                                    cf.Description__c = contFactor.description;
                                                }
                                                if(contFactor.name != null){
                                                    system.debug(contFactor.name);
                                                    cf.Name = contFactor.name;
                                                }
                                                if(contFactor.scoreImpactor != null){
                                                    system.debug(contFactor.scoreImpactor);
                                                    cf.Score_Impactor__c = contFactor.scoreImpactor;
                                                }
                                                cfList.add(cf);
                                            }

                                        }
                                    }
                                }
                            }
                        }
                        //***** Summary data *****
                        system.debug('Summary Data ==========');
                        if(res.summaryData != null && res.summaryData.size() > 0){
                            for(Equifax_Apply_Metadata__c eam: [SELECT id, 
                                                                    Name, 
                                                                    Grouping_Id__c,
                                                                    Equifax_Apply_Field_API_name__c 
                                                                FROM Equifax_Apply_Metadata__c]){
                                if(eam != null && eam.Name != null){
                                    codeToFieldName.put(eam.Name, eam);
                                }
                            }
                            for(ints.VedaComprehensiveResponse.DataBlock dataObj : res.summaryData){
                                if(dataObj.characteristic != null && dataObj.characteristic.size() > 0 && eqDynamicObject != null){
                                    
                                    for(ints.VedaComprehensiveResponse.Characteristic charObj : dataObj.characteristic){
                                        system.debug('characteristicId before check: '+charObj.characteristicId);
                                        
                                        if(charObj.characteristicId != null && codeToFieldName != null && 
                                            codeToFieldName.get(charObj.characteristicId) != null){
                                            Equifax_Summary_Data__c eqSummary = new Equifax_Summary_Data__c();
                                            
                                            eqSummary.Equifax_Apply__c = eqDynamicObject.id;
                                            
                                            system.debug('characteristicId : '+charObj.characteristicId);
                                            eqSummary.Characteristic_Id__c = charObj.characteristicId;
                                            if(codeToFieldName.get(charObj.characteristicId).Grouping_Id__c != null){
                                                eqSummary.Grouping_Id__c = codeToFieldName.get(charObj.characteristicId).Grouping_Id__c;















                                            }
                                            if(charObj.dataLevel != null){
                                                system.debug('dataLevel : '+charObj.dataLevel);
                                                eqSummary.Data_Level_Code__c = charObj.dataLevel;
                                            }
                                            if(charObj.description != null){
                                                system.debug('description : '+charObj.description);
                                                eqSummary.Description__c = charObj.description;
                                            }
                                            if(charObj.unit != null){
                                                system.debug('unit : '+charObj.unit);
                                                eqSummary.Unit__c = charObj.unit;
                                            }
                                            if(charObj.value != null){
                                                system.debug('value : '+charObj.value);
                                                eqSummary.Value__c =  charObj.value;
                                                /*if(codeToFieldName.get(charObj.characteristicId) != null ){//&& charObj.value != null && String.isNotBlank(charObj.value) && (charObj.value).isNumeric()){
                                                    
                                                    //eqDynamicObject.put(codeToFieldName.get(charObj.characteristicId),Decimal.valueOf(charObj.value));
                                                    eqDynamicObject.put(codeToFieldName.get(charObj.characteristicId),charObj.value);
                                                }*/
                                            }
                                            if(charObj.variableName != null){
                                                system.debug('variableName : '+charObj.variableName);
                                                eqSummary.Variable_Name__c = charObj.variableName;
                                            }
                                            eqSummaryListInsert.add(eqSummary);
                                        }
                                    }    
                                }      
                            }
                            
                            //update eqDynamicObject ;
                            insert eqSummaryListInsert;
                            insert cfList;
                            //insert eqAccountListInsert;
                            Database.SaveResult[] results = Database.insert(eqAccountListInsert);
                        
                            for(Equifax_Apply_Account__c eacc: [SELECT id, Internal_Id__c 
                                                                FROM Equifax_Apply_Account__c
                                                                WHERE Equifax_Apply__c =: eqDynamicObject.id]){
                                if(internalIdToEqDefaults != null && internalIdToEqDefaults.get(eacc.Internal_Id__c) != null){
                                    for(Equifax_Apply_Account_Default__c def: internalIdToEqDefaults.get(eacc.Internal_Id__c)){
                                        def.Equifax_Apply_Account__c = eacc.id;
                                        eqDefaultsInsert.add(def);
                                    }
                                }
                            }
                            if(eqDefaultsInsert != null && eqDefaultsInsert.size() > 0){
                                insert eqDefaultsInsert;
                            }
                            
                            //app.Equifax_Apply__c = eqDynamicObject.id;
                            //system.debug(app);
                            //update app;
                            //system.debug(app);
                            //if(String.isNotBlank(enqId)){
                            //    EquifaxPDFPull.httpCallout(enqId,app.id,acc.Name);
                            //}
                        }
                    }
                    
                    List<clcommon__Party__c> listParty = [SELECT id,
                                                                     Name,
                                                                     clcommon__Type__c,
                                                                     clcommon__Type__r.name
                                                                FROM clcommon__Party__c
                                                               WHERE genesis__Application__c = :appId
                                                                 AND clcommon__Account__c = :accId
                                                             ];
                        
                        if(listParty != null && listParty.size() > 0){
                            clcommon__Party__c party = listParty.get(0);
                            eqDynamicObject.Party_Type__c = party.clcommon__Type__r.name;
                            eqDynamicObject.Party__c = party.id;
                        }

                        eqDynamicObject.Is_Latest__c = true;

                        List<Equifax_Apply__c> existingEA = [SELECT id,
                                                                    name,
                                                                    Is_Latest__c
                                                               FROM Equifax_Apply__c
                                                              WHERE Application__c = :appId
                                                                AND Account__c = :accId
                                                                AND Party_Type__c = :eqDynamicObject.Party_Type__c
                                                            ];

                        for(Equifax_Apply__c ea : existingEA){
                            ea.Is_Latest__c = false;
                        }

                        if(existingEA != null && existingEA.size() > 0){
                            update existingEA;
                        }
                        
                    //Capture req and res as files
                    if(res.xmlResponse != null && !String.isBlank(res.xmlResponse)){
                        resXml = String.valueOf(res.xmlResponse);
                        Attachment resXmlFile = new Attachment();
                        resXmlFile.parentId = eqDynamicObject.Id;
                        resXmlFile.contentType = 'text/xml';
                        resXmlFile.body = Blob.valueOf(resXml);
                        resXmlFile.Name = 'EquifaxApply'+'_Response.xml';   
                        pdfAtts.add(resXmlFile);
























                    }
                    if(res.xmlRequest != null && !String.isBlank(res.xmlRequest)){
                        reqXml = String.valueOf(res.xmlRequest);
                        Attachment reqXmlFile = new Attachment();
                        reqXmlFile.parentId = eqDynamicObject.Id;
                        reqXmlFile.contentType = 'text/xml';
                        reqXmlFile.body = Blob.valueOf(reqXml);
                        reqXmlFile.Name = 'EquifaxApply' +'_Request.xml';
                        pdfAtts.add(reqXmlFile);
                    }
                    if(pdfAtts != null && pdfAtts.size() > 0 ){
                        insert pdfAtts;
                    }
                    update eqDynamicObject;
                    if(String.isNotBlank(eqDynamicObject.Error_Value__c))
                    {
                        return eqDynamicObject.Error_Value__c;
                    }
            }
            else{
                return 'Account Id or Application ID is missing.';
            }   
        }
        catch(Exception e){
            voLogInstance.logException(1001, '[EquifaxApplyResponseParser.parseResponse] Exception for app : ' + appId + ', at line : ' + e.getLineNumber(), e);
            voLogInstance.commitToDB();
            String exc = '';
            return genesis.Constants.API_EXCEPTION + ':' + e.getMessage();
        }
        return 'SUCCESS';
    }
}