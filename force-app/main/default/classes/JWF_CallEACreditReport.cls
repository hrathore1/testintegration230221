public class JWF_CallEACreditReport implements IFlowExecution{
    public void execute(Id applicationId,  JWF_Application_Execution_Log__c appExecutionLog){
        try{
            EACreditReportBatch job = new EACreditReportBatch(applicationId, appExecutionLog);
            Database.executeBatch(job, 1);                               
        }
        catch(Exception e){ExceptionLog.insertExceptionLog(e, 'Exception JWF_CallEACreditReport:' + e.getMessage(), applicationId);}          
    }
}