global class IncomeDetailsUtil{
    /*
    public String assets;
    public String incomes;
    
    
    public class AssetInfo{
        public String assetType;
        public Decimal assetValue;
        public String assetOwner;
        public String genesisIncome_Typec;
        public Decimal genesisAmountc;
        public String additionalInfo;
        public String Additional_Infoc;
        public String Party_Typec;
    }
    public class IncomeInfo{
        public String incomeType;
        public String incomeFreq;
        public Decimal incomeVal;
        public String incomeOwner;
        public String genesisIncome_Typec;
        public String genesisFrequencyc;
        public Decimal genesisAmountc;
        public String additionalInfo;
        public String Additional_Infoc;
        public String Party_Typec;
    }
    */
    public class ReturnIncome{
        public List<genesis__Income__c> incDets;
    }
    
    public static ReturnIncome assignIncomeVals(ApplicationCreationParser incData, genesis__Applications__c app, Boolean isLoggedIn){
        
        ReturnIncome returnInc = new ReturnIncome();
        mfiflexUtil.VOLog voLogInstance = mfiflexUtil.VOLog.getInstance('Genesis');
        List<genesis__Income__c> retIncList = new List<genesis__Income__c>();
        //String jsonModified = json.replace('__','');
        try{
            //IncomeDetailsUtil incData = (IncomeDetailsUtil) System.JSON.deserialize(jsonModified,IncomeDetailsUtil.class);
            List<ApplicationCreationParser.AssetInfo> assetsDeSer;
            List<ApplicationCreationParser.IncomeInfo> incomesDeSer;
            if(incData.assets != null)
            assetsDeSer = (List<ApplicationCreationParser.AssetInfo>) System.JSON.deserialize(String.valueOf(incData.assets), List<ApplicationCreationParser.AssetInfo>.class);
            if(incData.incomes != null)
            incomesDeSer = (List<ApplicationCreationParser.IncomeInfo>) System.JSON.deserialize(String.valueOf(incData.incomes), List<ApplicationCreationParser.IncomeInfo>.class);
            
            system.debug('assets ==== '+assetsDeSer );
            system.debug('incomes ==== '+incomesDeSer );
            //**************************** asset details *************************************
            if(app != null){
                if(assetsDeSer != null && assetsDeSer.size() > 0){
                    for(ApplicationCreationParser.AssetInfo asset: assetsDeSer){
                        genesis__Income__c retInc1 = new genesis__Income__c();
                        retInc1.Type__c = 'Asset';
                        if(isLoggedIn){
                            retInc1.genesis__Income_Type__c = asset.genesisIncome_Typec;
                            retInc1.genesis__Amount__c = asset.genesisAmountc;
                            retInc1.genesis__Application__c = app.id;
                            retInc1.Party_Type__c = asset.Party_Typec;
                            if(asset.Additional_Infoc != null){
                                retInc1.Additional_Info__c = asset.Additional_Infoc;
                            }
                        }
                        else{
                            retInc1.genesis__Income_Type__c = asset.assetType;
                            retInc1.genesis__Amount__c = asset.assetValue;
                            retInc1.genesis__Application__c = app.id;
                            if(asset.additionalInfo != null){
                                retInc1.Additional_Info__c = asset.additionalInfo;
                            }
                            if(asset.assetOwner != null && String.isNotBlank(asset.assetOwner)){
                                retInc1.Party_Type__c = asset.assetOwner;
                            }
                            else{
                                retInc1.Party_Type__c = 'BORROWER';
                            }
                        }
                        retIncList.add(retInc1);
                    }
                }
            }
            
            //**************************** Income details *************************************
            if(app != null){
                if(incomesDeSer != null && incomesDeSer.size() > 0){
                    for(ApplicationCreationParser.IncomeInfo inc: incomesDeSer){
                        genesis__Income__c retInc = new genesis__Income__c();
                        retInc.Type__c = 'Income';
                        if(isLoggedIn){
                            retInc.genesis__Income_Type__c = inc.genesisIncome_Typec;
                            retInc.genesis__Amount__c = inc.genesisAmountc;
                            retInc.genesis__Frequency__c = inc.genesisFrequencyc;
                            if(inc.Additional_Infoc != null){
                                retInc.Additional_Info__c = inc.Additional_Infoc;
                            }
                            if(inc.Party_Typec != null && String.isNotBlank(inc.Party_Typec)){
                                retInc.Party_Type__c = inc.Party_Typec;
                            }
                            else{
                                retInc.Party_Type__c = 'BORROWER';
                            }
                        }
                        else{
                            retInc.genesis__Income_Type__c = inc.incomeType;
                            retInc.genesis__Amount__c = inc.incomeVal;
                            retInc.genesis__Frequency__c = inc.incomeFreq;
                            if(inc.additionalInfo != null){
                                retInc.Additional_Info__c = inc.additionalInfo;
                            }
                            if(inc.incomeOwner != null && String.isNotBlank(inc.incomeOwner)){
                                retInc.Party_Type__c = inc.incomeOwner;
                            }
                            else{
                                retInc.Party_Type__c = 'BORROWER';
                            }
                        }
                        retInc.genesis__Application__c = app.id;
                        retIncList.add(retInc);
                    }
                }
            }
            
            returnInc.incDets = retIncList;
        }
        catch(Exception e){
            voLogInstance.logException(1001, '[IncomeDetailsUtil.assignIncomeVals] Error: ' + e.getMessage() + ' (at line: ' + e.getLineNumber() + ')', e);
            voLogInstance.commitToDb();
            throw new CustomException('Error: '+ e.getMessage());
        }
        
        return returnInc;
    }
}