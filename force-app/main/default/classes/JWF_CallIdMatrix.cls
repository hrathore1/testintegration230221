/**
 * @File Name          : JWF_CallEquifaxApply.cls
 * @Description        : 
 * @Author             : chirag.gupta@q2.com
 * @Group              : 
 * @Last Modified By   : chirag.gupta@q2.com
 * @Last Modified On   : 6/25/2020, 10:31:29 AM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    6/3/2020   chirag.gupta@q2.com     Initial Version
**/
public class JWF_CallIdMatrix implements IFlowExecution{
    public void execute(Id applicationId,  JWF_Application_Execution_Log__c appExecutionLog){
        try{
            IDMatrixCallBatch job = new IDMatrixCallBatch(applicationId, appExecutionLog);
            Database.executeBatch(job, 1);
        }
        catch(Exception e){ExceptionLog.insertExceptionLog(e, 'Exception JWF_CallIdMatrix:' + e.getMessage(), applicationId);}          
    }
}