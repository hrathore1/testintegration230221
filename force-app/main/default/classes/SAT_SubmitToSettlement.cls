public class SAT_SubmitToSettlement implements StageExecutionInterface {
    public List<String> executeValidations(Id applicationId){
        try{
            List<String> returnList = new List<String>();
            return returnList;            
        }
        catch(Exception e){throw e;}          
    }

    public String executeNextDeptActions(Id applicationId){
        try{
            String str = GenerateContract.createApplicationDisbursementData(applicationId);
            System.enqueueJob(new RegenerateAmortisationSchdlClass(applicationId));
            if(System.label.TAG_SUCCESS.equalsIgnoreCase(str)){
                return executeActions(applicationId);
            }
            return str;                     
        }
        catch(Exception e){throw e;}          
    }

    public String executePreviousDeptActions(Id applicationId){
        try{
            //executeActions(applicationId);
            return System.label.NEXT_STAGE_SUBMIT_SUCCESS_MSG;
        }
        catch(Exception e){throw e;}          
    }

    public String executeActions(Id applicationId){
        try{
            genesis__Applications__c appInst = new genesis__Applications__c(Id = applicationId);
            appInst.genesis__Status__c = 'APPROVED-PAPERWORK';
            update appInst;

            return System.label.NEXT_STAGE_SUBMIT_SUCCESS_MSG;                     
        }
        catch(Exception e){throw e;}
    }
    
    public class RegenerateAmortisationSchdlClass implements Queueable{
        String appId;
        public RegenerateAmortisationSchdlClass (String applicationId){
            appId = applicationId;
        }
        public void execute(QueueableContext context) {
            try{
                SkuidActionsCtrl.generateSchedule(appId);
            }
            catch(Exception e){
                ExceptionLog.insertExceptionLog(e, 'Regenerating amortisation schedule failed for app: '+ appId, appId);
            }   
        }
    }
}