public class RACVF_ContractFilePreviewerController {
    
    @AuraEnabled
    public static List<RACVF_ContentDocumentModel> getContentDocument(Id caseId)
    {
        String applicationId = [Select CRN_Number__c, Application__c FROM Case WHERE Id=:caseId].Application__c;
        
        /*
ContentWorkspace ws  = [SELECT Id, Name FROM ContentWorkspace 
WHERE Name =:contratNumber
LIMIT 1];

List<ContentDocumentLink> contentDocLinks = [SELECT ContentDocument.Id, ContentDocument.title, ContentDocument.FileType, ContentDocument.ContentSize, ContentDocument.CreatedDate, LinkedEntityId 
FROM ContentDocumentLink
WHERE LinkedEntityId =:ws.Id];
*/
        
        
        List<clcommon__Category_Attachment_Association__c> categoryAssociation = [Select clcommon__Document_Category__r.Id, clcommon__Document_Category__r.clcommon__Category_Name__c, clcommon__Document_Category__r.genesis__Application__c, CreatedBy.Name, clcommon__Attachment_Id__c 
                                                                                  FROM clcommon__Category_Attachment_Association__c WHERE clcommon__Document_Category__r.genesis__Application__c=:applicationId];
        Set<Id> contAssocIds = new Set<Id>();
        for( clcommon__Category_Attachment_Association__c catAssoc :categoryAssociation )
        {
            contAssocIds.add(catAssoc.clcommon__Attachment_Id__c);
        }
        List<ContentDocumentLink> contentDocLinks = [SELECT Id, ContentDocument.Id, ContentDocument.title, ContentDocument.FileType, ContentDocument.ContentSize
                                                     , ContentDocument.CreatedDate,ContentDocument.LastViewedDate , ContentDocument.ContentModifiedDate , LinkedEntityId, ContentDocument.CreatedBy.Name 
                                                     FROM ContentDocumentLink
                                                     WHERE Id =:contAssocIds];
        
        List<RACVF_ContentDocumentModel> contentDocumentModelList = setContentDocument(contentDocLinks, categoryAssociation);
        return contentDocumentModelList;
    }
    public static List<RACVF_ContentDocumentModel> setContentDocument(List<ContentDocumentLink> contentDocLinks, List<clcommon__Category_Attachment_Association__c> categoryAssociations)
    {
        List<RACVF_ContentDocumentModel> contentDocumentModelList = new List<RACVF_ContentDocumentModel>();
        
        
        
        if(contentDocLinks!=null && contentDocLinks.size()> 0)
        {
            for(clcommon__Category_Attachment_Association__c categoryAssociation: categoryAssociations)
            {
                
                for(ContentDocumentLink contentDocLink: contentDocLinks)
                {
                    if(contentDocLink.Id == categoryAssociation.clcommon__Attachment_Id__c)
                    {
                        RACVF_ContentDocumentModel model = new RACVF_ContentDocumentModel();
                        model.RecordId = contentDocLink.ContentDocument.Id;
                        model.ContentDocLinkId = contentDocLink.Id;
                        model.Title = contentDocLink.ContentDocument.title;
                        model.CreatedBy = contentDocLink.ContentDocument.CreatedBy.Name;
                        model.FileType= contentDocLink.ContentDocument.FileType;
                        model.FileSize = String.valueOf(contentDocLink.ContentDocument.ContentSize);
                        model.CreatedDate = formatDate(contentDocLink.ContentDocument.CreatedDate);
                        model.ModifiedDate = formatDate(contentDocLink.ContentDocument.ContentModifiedDate);
                        model.LastViewDate = formatDate(contentDocLink.ContentDocument.LastViewedDate);
                        model.LinkedEntityId = contentDocLink.LinkedEntityId;  
                        model.DocumentCategory = categoryAssociation.clcommon__Document_Category__r.clcommon__Category_Name__c;
                        model.DocumentCategoryId = categoryAssociation.Id;
                        contentDocumentModelList.add(model);
                    }
                    
                }
            }
            
        }
        // contentDocumentModelList.sort();
        return contentDocumentModelList;
    }
    
    @AuraEnabled
    public static String getDocumentInformation(Id contentDocLnkId){
        System.debug('getDocumentInformation--->'+ contentDocLnkId);
        RACVF_ContentDocumentModel model = new RACVF_ContentDocumentModel();
        WrapperClass wrapper = new WrapperClass();
        
        List<ContentDocumentLink> contentDocLinks = [SELECT Id, ContentDocument.Id, ContentDocument.title, ContentDocument.CreatedDate, ContentDocument.CreatedBy.Name, LinkedEntityId 
                                                     FROM ContentDocumentLink
                                                     WHERE Id =:contentDocLnkId ];
        if( contentDocLinks.size() > 0)
        {
            
            for (ContentDocumentLink contentDocLink : contentDocLinks)
            {
                model.ContentDocLinkId = contentDocLink.Id;
                model.RecordId = contentDocLink.ContentDocument.Id;
                model.Title = contentDocLink.ContentDocument.title;
                model.CreatedBy = contentDocLink.ContentDocument.CreatedBy.Name;
                model.CreatedDate = formatDate(contentDocLink.ContentDocument.CreatedDate);
                model.LinkedEntityId = contentDocLink.LinkedEntityId;
               
                
            }
            
            List<clcommon__Category_Attachment_Association__c> categoryAssociation = new List<clcommon__Category_Attachment_Association__c>();
            System.debug('contentDocLinkId--->'+ contentDocLnkId);
             
            categoryAssociation = [Select Id,clcommon__Document_Category__r.genesis__Application__c, clcommon__Document_Category__r.Id, clcommon__Document_Category__r.clcommon__Category_Name__c, clcommon__Attachment_Id__c 
                                   FROM clcommon__Category_Attachment_Association__c WHERE clcommon__Attachment_Id__c=:contentDocLnkId];
            
            System.debug('categoryAssociation.size()--->'+ categoryAssociation.size());
            Id applicationId;
            if( categoryAssociation.size() >0 )
            {
                model.DocumentCategoryAssocId = categoryAssociation[0].Id;
                model.DocumentCategory = categoryAssociation[0].clcommon__Document_Category__r.clcommon__Category_Name__c;
                model.DocumentCategoryId = categoryAssociation[0].clcommon__Document_Category__r.Id;
                applicationId = categoryAssociation[0].clcommon__Document_Category__r.genesis__Application__c;
            }
            List<clcommon__Document_Category__c> docCategories = [Select Id, clcommon__Category_Name__c From clcommon__Document_Category__c WHERE genesis__Application__c=:applicationId];
            
            wrapper.documentInfo = model;
            wrapper.categories = docCategories;
        }
        
        
        return JSON.serialize(wrapper);
    }
    
    @AuraEnabled
    public static void linkDocumentToCategory( Id contentDocumentAccocId, Id documentCategoryId, Id conDocLinkId )
    { 
        /*
List<Id> conDocLinkIds = new List<Id>();
conDocLinkIds.add(conDocLinkId);
genesis.Response result = genesis.ApplicationDocumentManagement.linkAttachments(documentCategoryId, conDocLinkIds );
System.debug('result====='+ result);
*/
        clcommon__Category_Attachment_Association__c categoryAssociation = new clcommon__Category_Attachment_Association__c();
        
        categoryAssociation = [Select Id,  clcommon__Attachment_Id__c, clcommon__Document_Category__c  , Name
                               FROM clcommon__Category_Attachment_Association__c WHERE Id=:contentDocumentAccocId];
        
       
        clcommon__Category_Attachment_Association__c newRecord = categoryAssociation.clone(false);
        newRecord.clcommon__Document_Category__c =documentCategoryId;	// new master id
        insert newRecord;
        delete categoryAssociation;
       
    }
    
    
    /*
* This Method is for unlinking file from Contrat Document Folder.
* 
*/
    @AuraEnabled
    public static Integer deleteFileFromFolder(Id entityId, Id contentDocId)  
    {
        List<ContentDocumentLink> contentDocLinks = [Select Id FROM ContentDocumentLink 
                                                     WHERE LinkedEntityId=:entityId AND ContentDocument.Id=:contentDocId];
        
        if( contentDocLinks.size() >0 )
        {
            delete contentDocLinks;
            return 1;
        }
        return 0;
    }
    
    //RACVF_ContentDocumentModel
    // Helper Method to format date
    public static String formatDate(DateTime formateDate)
    {
        String dateStr;
        if(formateDate!=null)
        {
            dateStr = formateDate.format('dd/MM/yyyy hh:mm:ss');
            //DateTime dt = DateTime.newInstance(formateDate.year(), formateDate.month(),formateDate.day());
            //dateStr = dt.format('dd/MM/yyyy');
        }
        return dateStr;
    }
    
    public class WrapperClass{
        
        @AuraEnabled public List<clcommon__Document_Category__c> categories {get;set;}
        @AuraEnabled public RACVF_ContentDocumentModel documentInfo{get;set;}
        
    }
    
}