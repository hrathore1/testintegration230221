global class PPSRRegistrationAdapter extends ints.AbstractMockAdapter{
    public static String CollateralId{get;set;}
    public static String CollateraLeinlId{get;set;}
    public static String requestBody = '';
    global PPSRRegistrationAdapter(){}
    global override ints.HttpRequestObject createRequest(Map<String, Object> getRqstSrchCrtfctRqustValueMap, 
                                                         ints.IntegrationConfigurationDTO IntegrationConfiguration){
                                                             
        System.debug(':::::Inside the getRqstSrchCrtfctRqustValueMap:::::getRqstSrchCrtfctRqustValueMap data is:::' + getRqstSrchCrtfctRqustValueMap);
                                                             
        try{
            if(getRqstSrchCrtfctRqustValueMap != null){
                ints.HttpRequestObject httpReq = new ints.HttpRequestObject();
               
                httpReq.endpoint = 'callout:'+ IntegrationConfiguration.apiNamedCredential;
                httpReq.timeout = 60000 ;
                httpReq.method = 'POST';    
                                                                     
                /************************Creating and assigning header values******************************/
                Map<String,String> headerMap = new Map<String,String>();
                headerMap.put('Content-type','text/xml');
                httpReq.headerMap = (headerMap);
                                                                     
                /****************************Constructing request body************************************/
                
                requestBody = constructRequestHandler(getRqstSrchCrtfctRqustValueMap);
                //createAttachment('REQ', requestBody);
                if(requestBody != null && String.isNotBlank(requestBody)){
                    httpReq.body = requestBody;
                    System.debug('<< PPSRRegistrationAdapter.createRequest>> requestBody ' + requestBody);
                    System.debug('HTTP :: ' + httpReq);
                    //return null;
                    return httpReq ;
                }
                else{
                    throw new ints.IntegrationException('[PPSRRegistrationAdapter.createRequest] Generated request is blank.');
                }
            }
            else{
                throw new ints.IntegrationException(' [PPSRRegistrationAdapter.createRequest] Scoreseeker request map in blank.');
            }
        }
        catch(Exception e){
            throw new ints.IntegrationException('[PPSRRegistrationAdapter.createRequest] Exception :'+e.getMessage());
        }                                                         
    }
    
    global override ints.IntegrationResponseObject parseResponse(HttpResponse response, ints.ThirdPartyRequest thirdPartyRequest){
        IntegrationAPIResponseParser respParser = new IntegrationAPIResponseParser();
        ints.IntegrationResponseObject integrationRes;
        
        String resp = response.getBody();
        System.debug('=====RESPONSE======'+resp);
        //createAttachment('RES', String.valueOf(response));
        if(response.getStatusCode() != 200){
            throw new ints.IntegrationException('Status code is: ' + response.getStatusCode());
        }
        else{
            if(resp != null && String.isNotBlank(resp)){
                String parserResponse = parsePPSRResponse(resp);
                //String parserResponse = responseXmlParseDML(resp);
                System.debug(':::parserResponse:::' + parserResponse);
                if(parserResponse != 'SUCCESS'){
                    //throw new ints.IntegrationException(parserResponse);
                }
            }
            else{
                throw new ints.IntegrationException('Response Body is null or blank.');
            }
        }
        
        system.debug('::::respParserrespParser:::'+respParser);
        
        integrationRes = respParser;
        return integrationRes;
    }
    
    global static String constructRequestHandler(Map<String, Object> getRqstSrchCrtfctRqustValueMap){
        try{
            System.debug('<<PPSRServiceAdaptorHandler.constructRequest>> getRqstSrchCrtfctRqustValueMap ' + getRqstSrchCrtfctRqustValueMap);
            String Token;
            Long randomLong = Crypto.getRandomLong();
            
            String Created = isBlankCheck((String)getRqstSrchCrtfctRqustValueMap.get('Created'),'Created');
            String TargetEnvironment = isBlankCheck((String)getRqstSrchCrtfctRqustValueMap.get('TargetEnvironment'),'TargetEnvironment');
            String CollateralLeinTableTag = isBlankCheck((String)getRqstSrchCrtfctRqustValueMap.get('CollateralLeinTableTag'),'CollateralLeinTableTag');
            String PPSRPasswordHistoryTag = isBlankCheck((String)getRqstSrchCrtfctRqustValueMap.get('PPSRPasswordHistoryTag'),'PPSRPasswordHistoryTag');
            
            String SecurePartyGroupNumber = isBlankCheck(genesis__Org_Parameters__c.getOrgDefaults().PPSRSecuredPartyGroupNumber__c ,'SecurePartyGroupNumber');

            
            String customerRequestMessageIdString = '' + Integer.valueOf(Math.random() * 10000000);
            Token=  EncodingUtil.base64Encode(Blob.valueOf(String.valueOf(randomLong)));
            
            //Dynamic data
            List<Map<String, Object>> collateralLeinList = (List<Map<String, Object>>) getRqstSrchCrtfctRqustValueMap.get(CollateralLeinTableTag);
            List<Map<String, Object>> PPSRPasswordHistoryList = (List<Map<String, Object>>) getRqstSrchCrtfctRqustValueMap.get(PPSRPasswordHistoryTag);
                        
            Map<String, Object> collateralLeinInfo = collateralLeinList.get(0);
            Map<String, Object> PPSRPasswordHistoryInfo = PPSRPasswordHistoryList.get(0);
           
            
            CollateralId = isBlankCheck((String)collateralLeinInfo.get('CollateralId'),'CollateralId');
            CollateraLeinlId = isBlankCheck((String)collateralLeinInfo.get('CollateraLeinlId'),'CollateraLeinlId');
            String SerialNumber = isBlankCheck((String) collateralLeinInfo.get('ppsrSerialNumber'),'ppsrSerialNumber');
            String SerialNumberType = isBlankCheck((String) collateralLeinInfo.get('ppsrSerialNumberType'),'ppsrSerialNumberType');
            String ppsrCollateralClassType = isBlankCheck((String) collateralLeinInfo.get('ppsrCollateralClassType'),'ppsrCollateralClassType');
            String CollateralLeinName = isBlankCheck((String) collateralLeinInfo.get('CollateralLeinName'),'CollateralLeinName');
            String Usernamestr = isBlankCheck((String) PPSRPasswordHistoryInfo.get('Username'),'Username');
            String Passwordstr = isBlankCheck((String) PPSRPasswordHistoryInfo.get('Password'),'Password');
            
            Integer RegistrationEndTimeYear = (genesis__Org_Parameters__c.getOrgDefaults().RegistrationEndTime_Year__c != null)?
                                               (Integer)genesis__Org_Parameters__c.getOrgDefaults().RegistrationEndTime_Year__c : 8;
            
            Date d = System.today().addYears(RegistrationEndTimeYear);
            Datetime myDT = datetime.newInstance(d.year(), d.month(),d.day());
            String RegistrationEndTime = myDT.format('yyyy-MM-dd\'T\'HH:mm:ss');
            system.debug('RegistrationEndTime======== '+RegistrationEndTime);
            
            
            String s = '';
            
            String IsTransitional = 'false';
            String AreProceedsClaimed = 'false';
            String IsSubordinate = 'false';
            List < PPSR_Search_Result__c > ppsesearchresultList = [SELECT id,
                                                                              IsTransitional__c,
                                                                              AreProceedsClaimed__c,
                                                                              IsSubordinate__c,
                                                                                  Is_Latest__c 
                                                                           FROM  PPSR_Search_Result__c
                                                                           WHERE Collateral_Lien__c =: CollateraLeinlId
                                                                           AND Is_Latest__c = True];
            
            
            if(ppsesearchresultList.size()>0 ){
                IsTransitional = (ppsesearchresultList[0].IsTransitional__c != null) ? ppsesearchresultList[0].IsTransitional__c : 'false';
                AreProceedsClaimed = (ppsesearchresultList[0].AreProceedsClaimed__c != null) ? ppsesearchresultList[0].AreProceedsClaimed__c : 'false';
                IsSubordinate = (ppsesearchresultList[0].IsSubordinate__c != null) ? ppsesearchresultList[0].IsSubordinate__c : 'false';
            }
            
        
            s= '<?xml version="1.0" encoding="UTF-8"?>';
            s+='<soap:Envelope xmlns:soap="http://www.w3.org/2003/05/soap-envelope" xmlns:data="http://schemas.ppsr.gov.au/2016/05/data" xmlns:data1="http://schemas.ppsr.gov.au/2011/04/data" xmlns:ser="http://schemas.ppsr.gov.au/2016/05/services">';
            s+=   '<soap:Header>';
            s+=      '<wsse:Security xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd" xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd" soap:mustUnderstand="true">';
            s+=         '<wsse:UsernameToken wsu:Id="UsernameToken-FE487D5534F2EDDE1515970361427691">';
            s+=            '<wsse:Username>'+Usernamestr+'</wsse:Username>';
            s+=            '<wsse:Password Type="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText">'+Passwordstr+'</wsse:Password>';
            s+=            '<wsse:Nonce EncodingType="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-soap-message-security-1.0#Base64Binary">'+Token+'</wsse:Nonce>';
            s+=            '<wsu:Created>'+Created+'</wsu:Created>';
            s+=         '</wsse:UsernameToken>';
            s+=      '</wsse:Security>';
            s+=      '<ser:TargetEnvironment>'+TargetEnvironment+'</ser:TargetEnvironment>';
            s+=   '</soap:Header>';
            s+=   '<soap:Body>';
            s+=       '<ser:CreateRegistrationsRequestMessage>';
            s+=          '<ser:CreateRegistrationsRequest>';
            s+=             '<data:CustomersRequestMessageId>'+customerRequestMessageIdString+'</data:CustomersRequestMessageId>';
            s+=             '<data:CustomersUserDefinedFields xsi:nil="true" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"/>';
            s+=             '<data:CollateralType>Consumer</data:CollateralType>';
            s+=             '<data:IsTransitional>'+IsTransitional+'</data:IsTransitional>'; //??
            s+=             '<data:NewRegistrations>';
            s+=                '<!--Zero or more repetitions:-->';
            s+=                '<data1:NewCollateralRegistration>';
            s+=                   '<data1:AreProceedsClaimed>'+AreProceedsClaimed+'</data1:AreProceedsClaimed>';//??
            s+=                   '<data1:CollateralClassType>'+ppsrCollateralClassType+'</data1:CollateralClassType>';
            s+=                   '<data1:GivingOfNoticeIdentifier>'+CollateralLeinName+'</data1:GivingOfNoticeIdentifier>'; 
            s+=                   '<data1:IsPMSI>true</data1:IsPMSI>';
            s+=                   '<data1:IsSubordinate>'+IsSubordinate+'</data1:IsSubordinate>';//??
            s+=                   '<data1:NewRegistrationSequenceNumber>1</data1:NewRegistrationSequenceNumber>'; //1 for all
            s+=                   '<data1:RegistrationEndTime>'+RegistrationEndTime+'</data1:RegistrationEndTime>'; // today + 8year(json)
            s+=                   '<data1:SerialNumberDetails>';
            s+=                      '<data1:SerialNumber>'+SerialNumber+'</data1:SerialNumber>';
            s+=                      '<data1:SerialNumberType>'+SerialNumberType+'</data1:SerialNumberType>';
            s+=                   '</data1:SerialNumberDetails>';
            s+=                '</data1:NewCollateralRegistration>';
            s+=             '</data:NewRegistrations>';
            s+=             '<data:SecuredPartyGroupNumber>'+SecurePartyGroupNumber+'</data:SecuredPartyGroupNumber>';
            s+=          '</ser:CreateRegistrationsRequest>';
            s+=       '</ser:CreateRegistrationsRequestMessage>';
            s+=   '</soap:Body>';
            s+='</soap:Envelope>';
            
            System.debug('SOAP BODY===' + s);
       

            
            return s;
        }catch(Exception exp){
            System.debug('<<PPSRRegistrationAdapter.constructRequest>> Error Message '+ exp.getMessage() + ' @ ' + exp.getLineNumber());
            throw new ints.IntegrationException(exp.getMessage());
        }
      
    }
    
    public static String isBlankCheck(String inputString, String key){
        if(String.isBlank(inputString)){
            throw new ints.IntegrationException(key+' is blank.');
        }
        return inputString;
    }
    
    public static String parsePPSRResponse(String response){
        try{
            system.debug('CollateraLeinlId ======'+CollateraLeinlId);
           
    
            
            PPSR_Registration_Information__c ppsrRegInfo = new PPSR_Registration_Information__c();
            DOM.Document doc = new DOM.Document();   
            doc.load(response);
    
            dom.XmlNode envelopeResponse = doc.getRootElement();
            System.debug(':::envelopeenvelope:::'+envelopeResponse);
            
            List<Dom.XMLNode> resbodyData = envelopeResponse.getChildren();
            
            for(Dom.XMLNode dataParse : resbodyData){
                if(dataParse.getName() == 'Body'){
                    List<Dom.XMLNode> resBody = dataParse.getChildren();
                    System.debug(':::resBody for child is::::'+resBody);
                    for(Dom.XMLNode searchBody : resBody){
                        if(searchBody.getName() == 'createregistrationsresponsemessage'){
                            List<Dom.XMLNode> chData = searchBody.getChildren();
                            for(Dom.XMLNode respChData : chData){
                                if(respChData.getName() == 'createregistrationsresponse'){
                                    List<Dom.XMLNode> ssData = respChData.getChildren();
                                    for(Dom.XMLNode respssData : ssData){
                                        if(respssData.getName() == 'registrations'){
                                            List<Dom.XMLNode> srData = respssData.getChildren();
                                            for(Dom.XMLNode respsrData : srData){
                                                if(respsrData.getName() == 'registrationinfo'){
                                                    List<Dom.XMLNode> nevData = respsrData.getChildren();
                                                    for(Dom.XMLNode respnevData : nevData){
                                                        if(respnevData.getName() == 'changenumber'){
                                                            ppsrRegInfo.Registration_ChangeNumber__c = respnevData.getText();
                                                        }
                                                        if(respnevData.getName() == 'isregistrationendtimechanged'){
                                                            ppsrRegInfo.RegistrationEndTime__c = respnevData.getText();
                                                        }
                                                        if(respnevData.getName() == 'newregistrationsequencenumber'){
                                                            ppsrRegInfo.NewRegistrationSequenceNumber__c = respnevData.getText();
                                                        }
                                                        if(respnevData.getName() == 'ppsrtransactionid'){
                                                            ppsrRegInfo.PPSR_Registration_Transaction_Id__c = respnevData.getText();
                                                        }
                                                        if(respnevData.getName() == 'registrationendtime'){
                                                            ppsrRegInfo.RegistrationEndTime__c = respnevData.getText();
                                                        }
                                                        if(respnevData.getName() == 'registrationnumber'){
                                                            ppsrRegInfo.RegistrationNumber__c = respnevData.getText();
                                                        }
                                                        if(respnevData.getName() == 'registrationstarttime'){
                                                            ppsrRegInfo.RegistrationStartTime__c = respnevData.getText();
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            
            system.debug('CollateraLeinlId ======'+CollateraLeinlId);
           
            if(ppsrRegInfo != null){ 
                ppsrRegInfo.Collateral_Lien__c   = CollateraLeinlId;
                ppsrRegInfo.Is_Latest__c   = True; 
                
                List < PPSR_Registration_Information__c > ppsrRegInfoList = [SELECT id,
                                                                              Is_Latest__c 
                                                                       FROM  PPSR_Registration_Information__c
                                                                       WHERE Collateral_Lien__c =: CollateraLeinlId
                                                                       AND Is_Latest__c = True];
                if(ppsrRegInfoList.size() >0){
                    for(PPSR_Registration_Information__c psr : ppsrRegInfoList){
                        psr.Is_Latest__c = False;
                    }
                    update ppsrRegInfoList;
                }
                
                insert ppsrRegInfo;
                
                //insert request and response XML
                createAttachment('Request', requestBody, ppsrRegInfo.id);
                createAttachment('Response', response, ppsrRegInfo.id);
            }
            //Temporary code
           
           /* createAttachment('Request', requestBody, CollateraLeinlId);
            createAttachment('Response', response, CollateraLeinlId);*/
            
            return System.label.SUCCESS_MESSAGE;
        }
        catch(Exception e){
            throw new ints.IntegrationException('[PPSRRegistrationAdapter.parsePPSRResponse]Exception: ' +  e.getMessage());
        }
    }
    public static void createAttachment(String callType, String fileBody, ID parent){
        try{
            Attachment attachment = new Attachment();
            attachment.Body = Blob.valueOf(fileBody);
            attachment.Name = callType + '_' + 'PPSRRegistration_' + CommonUtility.getDatetimeValue('');
            attachment.ParentId = parent;
            insert attachment;
        }
        catch(Exception e){
            throw new ints.IntegrationException('[PPSRRegistrationAdapter.createAttachment]Exception: ' +  e.getMessage());
        }
    }
}