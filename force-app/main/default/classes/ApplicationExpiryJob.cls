/**
 * @description       : 
 * @author            : chirag.gupta@q2.com
 * @group             : 
 * @last modified on  : 10-05-2020
 * @last modified by  : chirag.gupta@q2.com
 * Modifications Log 
 * Ver   Date         Author                Modification
 * 1.0   10-05-2020   chirag.gupta@q2.com   Initial Version
 * 2.0   08-01-2021   nadin.mandal@q2.com   Added broker quote expiry logic.
**/
global class ApplicationExpiryJob implements Schedulable, 
                                             Database.Batchable<sObject>, 
                                             Database.AllowsCallouts, 
                                             Database.Stateful {

    public Date currentBusinessDate = CommonUtility.getSystemDate();
    
    // Start Method
    global Database.QueryLocator start(Database.BatchableContext bc) {
        List<String> listStatuses = System.label.APPLICATION_EXPIRY_STATUSES.split(',');

        return Database.getQueryLocator([SELECT id,
                                                name,
                                                genesis__Status__c,
                                                Approval_Date__c,
                                                Broker_Quote_Expiry_Date__c
                                           FROM genesis__Applications__c 
                                          WHERE genesis__Status__c NOT IN :listStatuses
                                            AND ( Expiry_Date__c <= :currentBusinessDate
                                            OR Broker_Quote_Expiry_Date__c <= :currentBusinessDate )
                                            AND CL_Contract__c = null
                                        ]);
    }

    global void execute(SchedulableContext sc) {
        ApplicationExpiryJob job = new ApplicationExpiryJob();
        Database.executeBatch(job, 200);
    }

    public void Execute(Database.BatchableContext bc, List <sObject> scope) {
        try {
            List<genesis__Applications__c> applicationList = (List<genesis__Applications__c>) scope;
            for (genesis__Applications__c appLine: applicationList) {
                if(appLine.Broker_Quote_Expiry_Date__c <= currentBusinessDate){
                    appLine.genesis__Status__c = 'QUOTE - EXPIRED'; 
                }
                else{
                    appLine.genesis__Status__c = 'EXPIRED';
                }
            }

            update applicationList;
        } catch (Exception e) {
            mfiflexUtil.VOLog voLogInstance = mfiflexUtil.VOLog.getInstance('Genesis');
            voLogInstance.logException(9001, '[ApplicationExpiryJob.Execute()] : ' + ', at line : ' + e.getLineNumber(), e);voLogInstance.commitToDb();
        }
    }
    
    public void Finish(Database.BatchableContext bc) {}
}