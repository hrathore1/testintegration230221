public class AddressHandler {
    @InvocableMethod
    public static void getApp(List< clcommon__Address__c> addressList){
        List<clcommon__Address__c> updateaddressList = new List<clcommon__Address__c>();
        Set<Id> residentialAddressSet = new Set<Id>();
        Set<Id> mailingAddressSet = new Set<Id>();
        //List<clcommon__Address__c> otherAddressList = new List<clcommon__Address__c>();
        mfiflexUtil.VOLog voLogInstance = mfiflexUtil.VOLog.getInstance('Genesis');
        try{
            system.debug(addressList[0].Party__c);
            Set<Id> setAdds = new Map<Id, clcommon__Address__c>(addressList).keySet();
            List <clcommon__Address__c> mailingAddrToInsert = new List <clcommon__Address__c>();
            Set<Id> partyIds = new Set<Id>();
            Id appAddressId = Schema.SObjectType.clcommon__Address__c.getRecordTypeInfosByName().get('Application').getRecordTypeId();
            for(clcommon__Address__c add: [SELECT id, 
                                                Name,
                                                Is_Current__c,
                                                Street_Type__c,
                                                clcommon__Account__c,
                                                Party__c,
                                                Country_Picklist__c,
                                                Address_Type__c,
                                                Mailing_Address_Created__c,
                                                RecordTypeId
                                             FROM clcommon__Address__c
                                            WHERE id IN: setAdds]){
                
                //updateaddressList.add(add);
                if(add.Party__c != null && add.RecordTypeId == appAddressId){
                    partyIds.add(add.Party__c);
                    if(add.Address_Type__c != null && add.Address_Type__c == 'Residential'){
                        residentialAddressSet.add(add.Party__c);
                    }
                    else if(add.Address_Type__c != null && add.Address_Type__c == 'Mailing'){
                        mailingAddressSet.add(add.Party__c);
                    }
                }
                else{
                    add = putAddressCodes(add);
                    updateaddressList.add(add);
                }
            }
            
            if(residentialAddressSet != null && residentialAddressSet.size() > 0){
                Set<clcommon__Address__c> resiAddrs = adjustAddress(residentialAddressSet, 'Residential', setAdds);
                if(resiAddrs != null && resiAddrs.size() > 0)
                updateaddressList.addAll(resiAddrs); 
            }
            if(mailingAddressSet != null && mailingAddressSet.size() > 0){
                Set<clcommon__Address__c> mailAddrs = adjustAddress(mailingAddressSet, 'Mailing', setAdds);
                if(mailAddrs != null && mailAddrs.size() > 0)
                updateaddressList.addAll(mailAddrs);
            }
            if(updateaddressList != null && updateaddressList.size() > 0){
                system.debug(updateaddressList);
                upsert updateaddressList;
            }
        } 
        catch(Exception e){
            voLogInstance.logException(1001, '[AddressHandler.getApp] Error: ' + e.getMessage() + ' (at line: ' + e.getLineNumber() + ')', e);
            voLogInstance.commitToDb();
        }
    }
    public static clcommon__Address__c putAddressCodes(clcommon__Address__c add){
        if(add != null){
            //for(clcommon__Address__c add: addList){
            if(add.Street_Type__c != null && String.isNotBlank(add.Street_Type__c)){
                List<Address_Code__mdt> streetTypeCode = [SELECT id,
                                                                 DeveloperName,
                                                                 Full_Name__c
                                                            FROM Address_Code__mdt
                                                           WHERE Type__c = 'Street Type'
                                                             AND Full_Name__c =: add.Street_Type__c];
                if(streetTypeCode != null && streetTypeCode.size() > 0){
                    add.Street_Type_Code__c = streetTypeCode[0].DeveloperName;
                }
            }
            if(add.Country_Picklist__c != null && String.isNotBlank(add.Country_Picklist__c)){
                List<Address_Code__mdt> countryCode = [SELECT id,
                                                             DeveloperName,
                                                             Full_Name__c
                                                        FROM Address_Code__mdt
                                                       WHERE Type__c = 'Country'
                                                         AND Full_Name__c =: add.Country_Picklist__c];
                if(countryCode != null && countryCode.size() > 0){
                    add.Country_Code__c = countryCode[0].DeveloperName;
                } 
            }
            //}
        } 
        return add;
    }
    public Static Set<clcommon__Address__c> adjustAddress(Set<Id> partyIds, String addrType, Set<Id> contextAddr){
        Set<clcommon__Address__c> returnAddrList = new Set<clcommon__Address__c>();
        for(clcommon__Party__c party: [Select Id ,
                                         (Select Id ,
                                         Is_Current__c,
                                         clcommon__Account__c,
                                         DPID__c,
                                         TenureYears__c,
                                         TenureMonths__c,
                                         Start_Date__c,
                                         Expiry_date__c,
                                         Is_Current_Mailing_Address__c,
                                         Street_Type__c,
                                         Country_Picklist__c,
                                         Mailing_Address_Created__c,
                                         Address_Type__c,
                                         Address_Validated__c,
                                         Property_Type_Picklist__c,
                                         Building_Name__c,
                                         Street_Direction__c,
                                         Property_Type_Number__c,
                                         Street_Number__c,
                                         Street_Name__c,
                                         clcommon__County__c,
                                         clcommon__State_Province__c,
                                         Postcode__c,
                                         Property_Type_Picklist_2__c,
                                         Property_Type_Number_2__c,
                                         Property_Type_Picklist_3__c,
                                         Property_Type_Number_3__c
                                         from Addresses__r 
                                         where Address_Type__c =: addrType
                                         order by Is_Current__c desc, LastModifiedDate desc)          
                                  From clcommon__Party__c
                                  Where id IN: partyIds]){
            Integer count = 0;
            Date currentStartDate = System.Today();
            for(clcommon__Address__c addr: party.Addresses__r){
                if(contextAddr.contains(addr.id)){
                    addr = putAddressCodes(addr);
                    if(addr.Is_Current_Mailing_Address__c == True && addr.Address_Type__c.equals('Residential') && !addr.Mailing_Address_Created__c){
                        clcommon__Address__c  clonedMailingAddr = addr.clone(false, false, false, false);
                        clonedMailingAddr.Address_Type__c = 'Mailing';
                        clonedMailingAddr.Is_Current__c = True;
                        clonedMailingAddr.Is_Current_Mailing_Address__c  = False;
                        returnAddrList.add(clonedMailingAddr);
                    }
                    addr.Mailing_Address_Created__c = true;
                }
                if(addr.Is_Current__c && count == 0){
                    //if(count == 0){
                    Integer Yr = addr.TenureYears__c != null ? Integer.valueOf(addr.TenureYears__c) : 0;
                    Integer Mn = addr.TenureMonths__c != null ? Integer.valueOf(addr.TenureMonths__c) : 0;
                    Integer totalM = (Yr*12) + Mn;
                    Integer totalD = totalM * 30;
                    addr.Start_Date__c = date.Today().addMonths(-1*totalM);
                    addr.Expiry_Date__c = date.valueOf('2099-12-31');
                    
                    if(addr.Start_Date__c != null && count == 0)
                    currentStartDate = addr.Start_Date__c;
                    //}
                    /*else{
                        addr.Is_Current__c = False;
                        addr.Expiry_date__c = currentStartDate != null ? (currentStartDate - 1) : null;
                        Integer Yr = addr.TenureYears__c != null ? Integer.valueOf(addr.TenureYears__c) : 0;
                        Integer Mn = addr.TenureMonths__c != null ? Integer.valueOf(addr.TenureMonths__c) : 0;
                        Integer totalM = (Yr*12) + Mn;
                        Integer totalD = totalM * 30;
                        if(addr.Expiry_date__c != null)
                        addr.Start_Date__c = addr.Expiry_date__c.addMonths(-1*totalM);
                    }*/
                }
                else{
                    addr.Is_Current__c = False;
                    addr.Expiry_date__c = currentStartDate != null ? (currentStartDate - 1) : null;
                    Integer Yr = addr.TenureYears__c != null ? Integer.valueOf(addr.TenureYears__c) : 0;
                    Integer Mn = addr.TenureMonths__c != null ? Integer.valueOf(addr.TenureMonths__c) : 0;
                    Integer totalM = (Yr*12) + Mn;
                    Integer totalD = totalM * 30;
                    if(addr.Expiry_date__c != null)
                    addr.Start_Date__c = addr.Expiry_date__c.addMonths(-1*totalM);
                }
                count++;
                returnAddrList.add(addr);
            }
        }
        system.debug(returnAddrList);
        return returnAddrList;
    }
}