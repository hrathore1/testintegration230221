/**
 * @File Name          : GeneratePricing.cls
 * @Description        : 
 * @Author             : chirag.gupta@q2.com
 * @Group              : 
 * @Last Modified By   : chirag.gupta@q2.com
 * @Last Modified On   : 6/10/2020, 4:37:02 PM
 * @Modification Log   : 
 * Ver       Date            Author                 Modification
 * 1.0    6/10/2020   chirag.gupta@q2.com     Initial Version
**/
global class GeneratePricing {
    
    public static void recalculateNumberOfInstallments(genesis__Applications__c appInst){
        try{
            String frequency = appInst.genesis__Payment_Frequency__c;
            Integer term = Integer.valueOf(appInst.Application_Term__c);
            
            if(term <= 0 || term == null){
                term = Integer.valueOf(appInst.Loan_term_years__c) * 12;
                appInst.Application_Term__c = term;
            }
            else if(String.isBlank(frequency)){
                new PricingException('Application frequency can\'t be blank.');
            }
            
            //If monthly frequency no calculation needs to be done
            Date expectedStartDate = (appInst.genesis__Expected_Start_Date__c == null ?  CommonUtility.getSystemDate() : appInst.genesis__Expected_Start_Date__c);
            Integer daysBetween = expectedStartDate.daysBetween(expectedStartDate.addMonths(term));
            Integer noOfInstallments = term;
            
            if('MONTHLY'.equalsIgnoreCase(frequency)){
                noOfInstallments = term;
                appInst.genesis__Expected_First_Payment_Date__c = expectedStartDate.addMonths(1);
                //appInst.genesis__Expected_First_Payment_Date__c = loan.HolidayUtil.INSTANCE.getNextWorkingDate(appInst.genesis__Expected_First_Payment_Date__c);
                //appInst.genesis__Due_Day__c = appInst.genesis__Expected_First_Payment_Date__c.day();
                //return;
            }
            else if('WEEKLY'.equalsIgnoreCase(frequency)){
                noOfInstallments = (daysBetween/7);
                appInst.genesis__Expected_First_Payment_Date__c = expectedStartDate.addDays(7);
                
            }
            else if('FORTNIGHTLY'.equalsIgnoreCase(frequency) || 'BI-WEEKLY'.equalsIgnoreCase(frequency)){
                noOfInstallments = (daysBetween/14);
                appInst.genesis__Expected_First_Payment_Date__c = expectedStartDate.addDays(14);
            }
            
            /*clcommon__CL_Product__c appProduct = [SELECT id, 
                                                         genesis__Bank_Hours__c,
                                                         genesis__Schedule_Adjustment_Method__c,
                                                         genesis__Move_Across_Month__c 
                                                    FROM clcommon__CL_Product__c 
                                                   WHERE id = :appInst.genesis__CL_Product__c 
                                                   LIMIT 1
                                                 ];
            BusinessHours bh = [SELECT id,name FROM BusinessHours WHERE id = :appProduct.genesis__Bank_Hours__c];                                     
            clcommon.HolidayUtil holidayHelper = new clcommon.HolidayUtil(bh,
                                                                          appProduct.genesis__Schedule_Adjustment_Method__c,
                                                                          appProduct.genesis__Move_Across_Month__c);
            
            appInst.genesis__Expected_First_Payment_Date__c = holidayHelper.getWorkingDate(appInst.genesis__Expected_First_Payment_Date__c);*/
            appInst.genesis__Due_Day__c = appInst.genesis__Expected_First_Payment_Date__c.day();
            appInst.genesis__Term__c = noOfInstallments;
        }
        catch(Exception e){throw e;}
    }
    
    public static void recalculateDiscountRate(genesis__Applications__c appInst){
        try{
            if(appInst.Id != null){
                appInst.genesis__Discount_Rate__c = getDiscountRate(appInst, true);
            }
        }
        catch(Exception e){throw e;}
    }
    
    public static void recalculateInterestRate(genesis__Applications__c appInst){
        try{
            if(appInst.Id != null){
                appInst.genesis__Discount_Rate__c = getDiscountRate(appInst, true);
            }
            appinst.genesis__Interest_Rate__c = (appinst.Pricing_Interest__c == null ? 0 : appinst.Pricing_Interest__c)
                                                    - (appinst.genesis__Discount_Rate__c == null ? 0 : appinst.genesis__Discount_Rate__c) 
                                                    + (appinst.Discretion__c == null ? 0 : appinst.Discretion__c);
        }
        catch(Exception e){throw e;}
    }
    
    public static void recalculateLoanAmount(genesis__Applications__c appInst){
        try{
            Decimal totalLoanAmount = 0;
            
            if(appInst.Primary_Loan_Amount__c != null){
                totalLoanAmount += appInst.Primary_Loan_Amount__c;
            }
            if(appInst.Secondary_Loan_Amount__c != null){
                totalLoanAmount += appInst.Secondary_Loan_Amount__c;
            }
            if(appInst.Vehicle_related_costs__c != null){
                totalLoanAmount += appInst.Vehicle_related_costs__c;
            }
            if(appInst.Trade_in_value__c != null){
                totalLoanAmount -= appInst.Trade_in_value__c;
            }
            if(appInst.Trade_In_Payout__c!= null){
                totalLoanAmount += appInst.Trade_In_Payout__c;
            }
            if(appInst.Net_Cash_Deposit__c != null){
                totalLoanAmount -= appInst.Net_Cash_Deposit__c;
            }
            if(appInst.Capitalised_Fee__c != null ){
                totalLoanAmount += appInst.Capitalised_Fee__c;
            }
            if(appInst.Balloon_Percentage__c != null){
                appInst.genesis__Balloon_Payment__c = (appInst.Balloon_Percentage__c * totalLoanAmount)/100;
            }
            else{
                appInst.Balloon_Percentage__c = 0;
                appInst.genesis__Balloon_Payment__c = 0;
            }
            
            appInst.genesis__Loan_Amount__c = totalLoanAmount;
        }
        catch(Exception e){throw e;}
    }
    
    public static Decimal calculateEMIAmount(Decimal totalLoanAmount,
                                             Decimal finalInterestRate, 
                                             Decimal numberOfInstallments, 
                                             String paymentFrequency,
                                             Decimal balloonPayment,
                                             Date startDate,
                                             Date firstPaymentDate,
                                             String clProductId){//(genesis__Applications__c appInst){
        try{
            List<genesis__Amortization_Schedule__c> emiScheduleNew = getInstallments(totalLoanAmount,
                                                                                     finalInterestRate, 
                                                                                     numberOfInstallments, 
                                                                                     paymentFrequency, 
                                                                                     balloonPayment,
                                                                                     startDate,
                                                                                     firstPaymentDate,
                                                                                     clProductId); 
            if(emiScheduleNew.size() > 0){
                return emiScheduleNew.get(0).genesis__Total_Due_Amount__c;
            }
            throw new PricingException('Error while calculating payment amount.');
        }
        catch(Exception e){throw e;}
    }
    
    public static List<genesis__Amortization_Schedule__c> getInstallments(Decimal totalLoanAmount,
                                                                          Decimal finalInterestRate, 
                                                                          Decimal numberOfInstallments, 
                                                                          String paymentFrequency,
                                                                          Decimal balloonPayment,
                                                                          Date startDate,
                                                                          Date firstPaymentDate,
                                                                          String clProductId){
        try{
            System.debug('getInstallments ' + totalLoanAmount);
            //String dayConvention = genesis.LendingConstants.ACCRUAL_METHOD_CODE_30_360; 
            Date expectedStartDate = (startDate == null ? Date.Today() : startDate); 
            String loanFrequency = ('FORTNIGHTLY'.equalsIgnoreCase(paymentFrequency) ? 'BI-WEEKLY' : paymentFrequency);
            Decimal interestRate = finalInterestRate;
            Decimal term = numberOfInstallments;
            //Decimal loanAmount = totalLoanAmount;
            Decimal balloonPaymentAmount = (balloonPayment == null ? 0 : balloonPayment);
            
            if(firstPaymentDate == null){
                if('WEEKLY'.equalsIgnoreCase(loanFrequency)){
                    firstPaymentDate = expectedStartDate.addDays(7);
                }
                else if('BI-WEEKLY'.equalsIgnoreCase(loanFrequency) || 'FORTNIGHTLY'.equalsIgnoreCase(loanFrequency)){
                    firstPaymentDate = expectedStartDate.addDays(14);
                    loanFrequency = 'BI-WEEKLY';
                }
                else{
                    firstPaymentDate = expectedStartDate.addMonths(1);
                }
            }

            Id bankHours;
            String scheduleAdjustmentMethod;
            Boolean moveAcrossMonth;
            String roundingMethod;
            Decimal digitsAfterDecimal;
            String interestPeriodCalculation;

            if(!String.isBlank(clProductId)){
                List<clcommon__CL_Product__c> product = new List<clcommon__CL_Product__c>([SELECT genesis__Bank_Hours__c, 
                                                                                                  genesis__Schedule_Adjustment_Method__c, 
                                                                                                  genesis__Move_Across_Month__c, 
                                                                                                  genesis__Rounding_Method__c, 
                                                                                                  genesis__Digits_After_Decimal__c, 
                                                                                                  genesis__Interest_Period_Calculation__c
                                                                                             FROM clcommon__CL_Product__c 
                                                                                            WHERE Id = :clProductId 
                                                                                            LIMIT 1
                                                                                         ]);

                if (product.size() > 0) {
                    if(product[0].genesis__Bank_Hours__c != null){
                        bankHours = product[0].genesis__Bank_Hours__c;
                    }
                    if (product[0].genesis__Schedule_Adjustment_Method__c != null) {
                        scheduleAdjustmentMethod = product[0].genesis__Schedule_Adjustment_Method__c;
                    }
                    if (product[0].genesis__Move_Across_Month__c != null) {
                        moveAcrossMonth = product[0].genesis__Move_Across_Month__c;
                    }
                    if (product[0].genesis__Rounding_Method__c != null) {
                        roundingMethod = product[0].genesis__Rounding_Method__c;
                    }
                    if (product[0].genesis__Digits_After_Decimal__c != null) {
                        digitsAfterDecimal = product[0].genesis__Digits_After_Decimal__c;
                    }
                    if (product[0].genesis__Interest_Period_Calculation__c != null) {
                        interestPeriodCalculation = product[0].genesis__Interest_Period_Calculation__c;
                    }
                }
            }
            
            genesis__Lending_Calculator__c calc = new genesis__Lending_Calculator__c(
                                                                            //genesis__Accrual_Base_Method_Code__c = dayConvention,
                                                                            genesis__Action__c = 'CALCULATE_ALL',
                                                                            genesis__Additional_Interest_Amount__c = 0,
                                                                            genesis__Amortization_Calculation_Method_Code__c = 'NONE',
                                                                            genesis__APR__c = 0,
                                                                            genesis__Balance_Amount__c = 0,
                                                                            genesis__Balloon_Method_Code__c = 'DUMMY',
                                                                            genesis__Balloon_Payment_Amount__c = balloonPaymentAmount,
                                                                            genesis__Billing_Method_Code__c = genesis.LendingConstants.REPAYMENT_INT_CALC_METHOD_DEC_BAL,
                                                                            genesis__Contract_Date__c = expectedStartDate,
                                                                            genesis__Final_Payment_Amount__c = 0,
                                                                            genesis__Financed_Amount__c = 0,
                                                                            genesis__Financed_Fees__c = 0,
                                                                            genesis__First_Payment_Date__c = firstPaymentDate,
                                                                            genesis__First_Period_Calender_Days__c = 0,
                                                                            genesis__First_Period_Interest__c = 0,
                                                                            genesis__Flexible_Repayment_Flag__c = false,
                                                                            genesis__Installment_Method_Code__c = 'UNDEFINED',
                                                                            genesis__Interest_Amount__c = 0,
                                                                            genesis__Interest_Only_Period__c = 0,
                                                                            genesis__Loan_Amount__c = totalLoanAmount,
                                                                            genesis__Payment_Amount__c = 0,
                                                                            genesis__Payment_Frequency_Code__c = loanFrequency,
                                                                            genesis__Payment_Frequency_Multiplier__c = 1,
                                                                            genesis__Prepaid_Fees__c = 0,
                                                                            genesis__Principal_Payment_Amount__c = 0,
                                                                            genesis__Rate__c = interestRate,
                                                                            genesis__Repayment_Type_Code__c = 'UNDEFINED',
                                                                            genesis__Total_Finance_Charge__c = 0,
                                                                            genesis__Total_Financed_Amount__c = 0,
                                                                            genesis__Total_Of_Payments__c = 0,
                                                                            genesis__Due_Day__c = firstPaymentDate.day(),
                                                                            //genesis__Expected_Second_Pay_Day_Date__c = secondPayDayDate,
                                                                            genesis__Amortization_Term__c = term,
                                                                            genesis__Repayment_Procedure__c = 'Equal Monthly Installments',
                                                                            //genesis__Interest_Compounding_Frequency__c = application.Interest_Compounding_Frequency__c,
                                                                            //genesis__Payment_Amount_Step_Up_Type__c = application.Payment_Amount_Step_Up_Type__c,
                                                                            genesis__Bank_Hours__c = bankHours,
                                                                            genesis__Schedule_Adjustment_Method__c = scheduleAdjustmentMethod,
                                                                            genesis__Move_Across_Month__c = moveAcrossMonth,
                                                                            genesis__Rounding_Method__c = roundingMethod,
                                                                            genesis__Digits_After_Decimal__c = digitsAfterDecimal,
                                                                            genesis__Interest_Period_Calculation__c = interestPeriodCalculation,
                                                                            genesis__Time_Counting_Method_Code__c = genesis.LendingConstants.TIME_COUNTING_ACTUAL_DAYS_366,
                                                                            genesis__Term__c = term
                                                                        );       
            System.debug('calc ' + calc);
            List<genesis__Amortization_Schedule__c> emiScheduleNew = new List<genesis__Amortization_Schedule__c>();
            if(!Test.isRunningTest()){
                emiScheduleNew = genesis.LendingCalculator.calculate(calc, NULL); 
            }
            else{
                genesis__Amortization_Schedule__c amort = new genesis__Amortization_Schedule__c();
                emiScheduleNew.add(amort);
            }
            if(emiScheduleNew.size() > 0){
                return emiScheduleNew;
            }
            throw new PricingException('Error while calculating payment amount.');
        }
        catch(Exception e){throw e;}
    }
        
    public static Decimal getDiscountRate(String appId){
        try{
            System.debug('Inside discount function...');
            List<genesis__Applications__c> appRecords = [SELECT id,
                                                                Pricing_Interest__c,
                                                                Loan_Channel__c,
                                                                Franchise__c,
                                                                (SELECT id,
                                                                        Name,
                                                                        clcommon__Account__r.Membership_card_colour__c
                                                                   FROM genesis__Parties__r
                                                                  WHERE clcommon__Type__r.Name IN ('BORROWER','CO-BORROWER','GUARANTOR')
                                                                )
                                                           FROM genesis__Applications__c
                                                          WHERE id = :appId
                                                        ];  
            
            if(appRecords != null && appRecords.size() > 0){
                return getDiscountRate(appRecords.get(0), false);
            }
            throw new PricingException('No application record found for id:' + appId);
        }
        catch(Exception e){throw e;}
    }
    
    public static Decimal getDiscountRate(genesis__Applications__c appInst, Boolean reQueryPartyDetails){
        try{
            System.debug('Inside discount function...');
                                                        
            if(appInst != null){
                genesis__Applications__c appRecord = appInst;
                String franchise = appRecord.Franchise__c;
                String loanChannel = appRecord.Loan_Channel__c;
                Decimal baseInterestRate = (appRecord.Pricing_Interest__c != null ? appRecord.Pricing_Interest__c : 0); 
                Set<String> setMembershipCardColor = new Set<String>();
                
                List<clcommon__Party__c> partyList = new List<clcommon__Party__c>();
                if(reQueryPartyDetails){
                    partyList = [SELECT id,
                                        name,
                                        clcommon__Account__r.Membership_card_colour__c 
                                   FROM clcommon__Party__c 
                                  WHERE genesis__Application__c = :appInst.Id
                                    AND clcommon__Type__r.Name IN ('BORROWER','CO-BORROWER','GUARANTOR')
                                ];  
                }  
                else{
                    partyList = appInst.genesis__Parties__r;
                }
                
                for(clcommon__Party__c partyRecord : partyList){
                    if(partyRecord.clcommon__Account__c != null && !String.isBlank(partyRecord.clcommon__Account__r.Membership_card_colour__c)){
                        setMembershipCardColor.add(partyRecord.clcommon__Account__r.Membership_card_colour__c);
                    }
                }
            
                System.debug('setMembershipCardColor:' + setMembershipCardColor);
                if(setMembershipCardColor.size() == 0 || String.isBlank(franchise) || String.isBlank(loanChannel)){
                    return 0;
                }
                else{
                    List<Interest_Discount__c> intDiscounts = [SELECT id,
                                                                      Discount__c,
                                                                      Is_Discount_on_Base_Rate__c,
                                                                      Membership_Colour__c
                                                                 FROM Interest_Discount__c
                                                                WHERE Franchise__c = :franchise
                                                                  AND (Loan_Channel__c = :loanChannel
                                                                   OR Loan_Channel__c = 'All')
                                                                  AND Membership_Colour__c in :setMembershipCardColor
                                                                ORDER BY Discount__c DESC
                                                                LIMIT 1
                                                              ];
                                            
                    System.debug(intDiscounts );
                    if(intDiscounts != null && intDiscounts.size() > 0){
                        if(intDiscounts.size() == 1){
                            Interest_Discount__c interestDiscount = intDiscounts.get(0);
                            Decimal discount = interestDiscount.Discount__c/100;
                            Boolean isDiscountonBaseRate = interestDiscount.Is_Discount_on_Base_Rate__c;
                            if(isDiscountonBaseRate) {
                                discount = discount * baseInterestRate;
                            }
                            return discount.setScale(2);
                        }
                    }
                }
                return 0;
            }
            throw new PricingException('No application record found for application:' + appInst);
        }
        catch(Exception e){throw e;}
    }
    
    webservice static String generateAndApplyPricing(String appId){
        try{
            String pricingMsg = genesis.SkuidPricingCtrl.generatePricing(appId);
            System.debug('Rate card response:' +  pricingMsg);
            if(!pricingMsg.equalsIgnoreCase('Pricing Generated...')){
                return pricingMsg;
            }
            
            String applyPricingMsg = applyPricing(appId, null);
            if(!System.label.SUCCESS_MESSAGE.equalsIgnoreCase(applyPricingMsg)){
                return applyPricingMsg;
            }
            
            return System.label.SUCCESS_MESSAGE;
        }
        catch(Exception e){return ('Exception: ' + e.getMessage());}
    }
    
    webservice static String applyPricing(String appId, String pricingId){
        try{
            List<genesis__Applications__c> appRecords = [SELECT id,
                                                                name,
                                                                Application_Term__c,
                                                                Pricing_Interest__c,
                                                                Pricing_Term__c,
                                                                genesis__Discount_Rate__c,
                                                                genesis__Interest_Rate__c,
                                                                Discretion__c,
                                                                Franchise__c,
                                                                Loan_Channel__c,
                                                                (SELECT id,
                                                                        genesis__Term__c,
                                                                        genesis__Interest_Rate__c,
                                                                        genesis__Selected__c
                                                                   FROM genesis__Pricing_Details__r
                                                                )
                                                           FROM genesis__Applications__c
                                                          WHERE Id = :appId
                                                        ];
            
            if(appRecords != null && appRecords.size() > 0){
                Boolean pricingFound = false;
                genesis__Applications__c appRec = appRecords.get(0);
                Integer term = Integer.valueOf(appRec.Pricing_Term__c);
                List<genesis__Application_Pricing_Detail__c> pricingRecords = appRec.genesis__Pricing_Details__r;
                for(genesis__Application_Pricing_Detail__c pricingRecord : pricingRecords){
                    if((pricingId == null && pricingRecord.genesis__Term__c == term) 
                          || (pricingId != null && String.valueOf(pricingRecord.Id).startsWith(pricingId))){
                        appRec.Pricing_Interest__c = pricingRecord.genesis__Interest_Rate__c;
                        appRec.Pricing_Term__c = pricingRecord.genesis__Term__c;
                        pricingRecord.genesis__Selected__c = true;
                        
                        if(pricingId != null){
                            appRec.Application_Term__c = pricingRecord.genesis__Term__c;          
                        }     
                        pricingFound = true;
                    }
                    else{
                        pricingRecord.genesis__Selected__c = false;
                    }
                }
            
                if(!pricingFound){
                    throw new PricingException('No pricing record found for term: ' + term);
                }
                
                recalculateInterestRate(appRec);
                update appRec;
                update pricingRecords;
            }
            else{
                throw new PricingException('No application found for appId: ' + appId);
            }
            return System.label.SUCCESS_MESSAGE;
        }
        catch(Exception e){throw e;}
    }
    
    public class PricingException extends Exception{}
}