@RestResource(urlMapping='/reconcileBankFiles/*')
global class WSReconcileBankFiles {
    global class Response{
        public String status;
        public String errorCode;
        public String errorMessage;
        public String statusCode ;
        public Integer fileCount;
    }

    public class FileNames{
        public List<String> fileNames;
    }

    @HttpPost
    global static Response inboundFileMethod(){
        try{
            RestRequest request = Restcontext.Request;
            FileNames reqParams = (FileNames) System.JSON.deserialize(request.requestBody.toString(), FileNames.class);
            
            Response res = new Response();
            if(reqParams == null) {
                res.status = System.label.TAG_ERROR;
                res.errorCode = 'INVALID_REQUEST';
                res.errorMessage = 'Request parameters passed are either null or invalid.';
                res.statusCode = '400';
                return res;
            }   

            List<String> listFileNames = reqParams.fileNames;

            if(listFileNames == null || listFileNames.size() == 0){
                res.status = System.label.TAG_ERROR;
                res.errorCode = 'INVALID_REQUEST';
                res.errorMessage = 'Error : File list is blank.';
                res.statusCode = '400';
                return res;
            }

            Integer initialFileSize = listFileNames.size();
            Banking_Config__c bankingConfig = Banking_Config__c.getOrgDefaults();
            String folderName = bankingConfig.Bank_Files_Processed_Folder__c;

            Folder folderInfo = [SELECT id,
                                        name
                                   FROM Folder
                                  WHERE name = :folderName     
                                ];
            
            List<Document> listDocs = [SELECT id,
                                              name,
                                              folderid
                                         FROM Document
                                        WHERE Name IN :listFileNames
                                          AND folderid != :folderInfo.id
                                      ];
            
            if(listDocs == null || listDocs.size() == 0){
                res.status = System.label.TAG_ERROR;
                res.errorCode = 'INVALID_REQUEST';
                res.errorMessage = 'Error : No files found in the system.';
                res.statusCode = '400';
                return res;
            }
            
            Integer actualFileSize = listDocs.size();
            List<String> listFileNamesProcessed = new List<String>();
            for(Document doc : listDocs){
                doc.folderid = folderInfo.id;
                listFileNamesProcessed.add(doc.name + '(' + doc.Id + ')');
            }

            update listDocs;
            
            if(initialFileSize != actualFileSize){
                //res.status = System.label.TAG_ERROR;
                //res.errorCode = 'INVALID_FILE_NAMES';
                res.status = System.label.TAG_SUCCESS;
                res.fileCount = actualFileSize;
                String errorMsg = 'WARNING: Processed file count is not matching. Expected:';
                errorMsg += initialFileSize + ', Found:' + actualFileSize + '.';
                String errorMsgLong = errorMsg + ' (File names processed:' + listFileNamesProcessed + ').';
                res.errorMessage = errorMsg;
                res.statusCode = '200';
                
                peer__Bank_Recon_Exception__c exceptionRec = new peer__Bank_Recon_Exception__c();
                exceptionRec.peer__Recon_Message__c = errorMsg;
                exceptionRec.peer__Name__c = 'WARNING: RECONCILIATION API';   
                exceptionRec.peer__Reconciliation_Message__c = errorMsgLong;   
                exceptionRec.peer__Transaction_Date__c = System.today();
                insert exceptionRec;
                
                return res;
            }
            
            res.fileCount = listDocs.size();
            res.status = System.label.TAG_SUCCESS;
            res.statusCode = '200';
            return res;
        }
        catch(Exception e){
            Response res = new Response();
            res.status = System.label.TAG_EXCEPTION;
            res.errorCode = 'TECHNICAL_EXCEPTION';
            res.errorMessage = 'Exception: ' + e.getMessage();
            res.statusCode = '404';
            return res;
        }
    }
}