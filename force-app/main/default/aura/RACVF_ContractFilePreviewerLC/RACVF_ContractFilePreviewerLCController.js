({
    /*call apex controller method "getContentDocument" to get salesforce file records*/
    doInit : function(component, event, helper) {
        
        var actions = [
            { label: 'Delete', name: 'delete' },
            { label: 'Edit', name: 'edit' }
        ]
        component.set('v.columns', [
            {label: 'Title', fieldName: 'linkName', type: 'url', sortable:true, initialWidth: 380,
             typeAttributes: {label: { fieldName: 'Title' }, target: '_blank'}},
            { label: 'Doc Category', fieldName: 'DocumentCategory', type: 'text',sortable:true, initialWidth: 160 },
            { label: 'Created Date', fieldName: 'CreatedDate', type: 'text',sortable:true, initialWidth: 150 },
            { label: 'Created By', fieldName: 'Createdby', type: 'text',sortable:true, initialWidth: 150 },
            { type: 'action', typeAttributes: { rowActions: actions } }
        ]);
        helper.getContractDocument(component); // Calling Helper method 
    },
    
    handleRowAction: function (cmp, event, helper) {
        var action = event.getParam('action');
        var row = event.getParam('row');
        
        switch (action.name) {
            case 'delete':
                helper.deleteFile(cmp, row);
                break;
            case 'edit':
                helper.editCategory(cmp, row);
                break;
            default:
                break;
        }
    },
    
    saveEditModal : function(component, event, helper) {
        helper.updateDocumentCategory(component,event, helper);
    },
    closeEditModal: function(component, event, helper) {
        
        component.set("v.picklistOpts",null);
        component.set("v.list",null);
        var cmpTarget = component.find('editDialog');
        var cmpBack = component.find('dialogBack');
        $A.util.removeClass(cmpTarget, 'slds-fade-in-open');
        $A.util.removeClass(cmpBack, 'slds-backdrop--open'); 
        
    },
    
    // this function automatic call by aura:waiting event  
    showSpinner: function(component, event, helper) {
        // make Spinner attribute true for display loading spinner 
        component.set("v.Spinner", true); 
    },
    handleSort: function(cmp, event, helper) {
        helper.handleSort(cmp, event);
    },
    
    // this function automatic call by aura:doneWaiting event 
    hideSpinner : function(component,event,helper){
        // make Spinner attribute to false for hide loading spinner    
        component.set("v.Spinner", false);
    },
    
    getSelected : function(component,event,helper){
        // display modle and set seletedDocumentId attribute with selected record Id   
        component.set("v.hasModalOpen" , true);
        component.set("v.selectedDocumentId" , event.currentTarget.getAttribute("data-Id")); 
        
    },
    closeModel: function(component, event, helper) {
        // for Close Model, set the "hasModalOpen" attribute to "FALSE"  
        component.set("v.hasModalOpen", false);
        component.set("v.selectedDocumentId" , null); 
    }
})