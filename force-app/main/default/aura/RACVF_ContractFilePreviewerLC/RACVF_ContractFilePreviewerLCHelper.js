({
    getContractDocument : function( component ) {
        var action = component.get("c.getContentDocument");
        console.log('responseObj--->5');
        action.setParams({"caseId": component.get("v.recordId")});
        action.setCallback(this, function(response) {
            var state = response.getState();
            var result = JSON.stringify(response.getReturnValue());
            console.log(result);
            
            if (state === "SUCCESS") {
                
                var records =response.getReturnValue();
                records.forEach(function(record){
                    record.linkName = '/'+record.RecordId;
                });
                
                component.set('v.lstContentDoc', response.getReturnValue());
                component.set("v.totalNumberOfRows", records.length);
            }
            else if (state === "INCOMPLETE") {
                // do something
            }
                else if (state === "ERROR") {
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + 
                                        errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }
        });
        $A.enqueueAction(action);
    },
    
    editCategory:function(component, row ){
        
        var options = [];
        var action = component.get("c.getDocumentInformation");
        console.log('editCategory rowIndex2---->'+row.RecordId);
        
        console.log('editCategory row.ContentDocLinkId---->'+row.ContentDocLinkId);
        var categoryId='';
        action.setParams({"contentDocLnkId": row.ContentDocLinkId});
        console.log('editCategory');
        action.setCallback(this, function(response) {
            //store state of response
            var state = response.getState();
            if (state === "SUCCESS") {
                // Parsing JSON string into js object
                var responseObj = JSON.parse(response.getReturnValue());
                console.log('responseObj--->3'+responseObj);
                component.set('v.contentDocument', responseObj.documentInfo);
                
                categoryId = responseObj.documentInfo.DocumentCategoryId;
                
                responseObj.categories.forEach(function(element) {
                    console.log(element.Id);
                    console.log(element.clcommon__Category_Name__c);
                    options.push({ value: element.Id, label: element.clcommon__Category_Name__c });
                });
                
                component.set("v.picklistOpts", options);
                component.find("categoryId").set("v.value", categoryId);
                var cmpTarget = component.find('editDialog');
                var cmpBack = component.find('dialogBack');
                $A.util.addClass(cmpTarget, 'slds-fade-in-open');
                $A.util.addClass(cmpBack, 'slds-backdrop--open'); 
                
            }
        });
        $A.enqueueAction(action);
    },
    
    updateDocumentCategory : function(component, event, helper) {
        
        var action = component.get("c.linkDocumentToCategory");
        var categoryId = component.find("categoryId").get("v.value");
        var contentDocument = component.get('v.contentDocument');
        console.log('categoryId--->'+categoryId);
        console.log('contentDocumentLink Id--->'+contentDocument.ContentDocLinkId);
        console.log('contentDocumentLink Id--->'+contentDocument.DocumentCategoryAssocId);
        
        action.setParams({"contentDocumentAccocId":contentDocument.DocumentCategoryAssocId , "documentCategoryId": categoryId, "conDocLinkId": contentDocument.ContentDocLinkId});
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            console.log(state);
            var isSuccess =  response.getReturnValue();
            console.log('isSuccess---->'+isSuccess);
            
            component.set("v.picklistOpts",null);
            component.set("v.contentDocument",null);
            var cmpTarget = component.find('editDialog');
            var cmpBack = component.find('dialogBack');
            $A.util.removeClass(cmpTarget, 'slds-fade-in-open');
            $A.util.removeClass(cmpBack, 'slds-backdrop--open'); 
            console.log('responseObj--->3');
            this.getContractDocument(component);
            console.log('responseObj--->4');
        });
        $A.enqueueAction(action);
        
    },
    deleteFile: function (component, row) {
        var rows = component.get('v.lstContentDoc');
        var rowIndex = rows.indexOf(row);
        
        var action = component.get("c.deleteFileFromFolder");
        action.setParams({"entityId": rows[rowIndex].LinkedEntityId, "contentDocId": rows[rowIndex].RecordId});
        action.setCallback(this, function(response) {
            var state = response.getState();
            var result = JSON.stringify(response.getReturnValue());
            console.log(result);
            if (state === "SUCCESS") {
                //var records =response.getReturnValue();
                rows.splice(rowIndex, 1);
                component.set('v.lstContentDoc', rows);
                component.set("v.totalNumberOfRows", rows.length);
            }
            else if (state === "INCOMPLETE") {
                // do something
            }
                else if (state === "ERROR") {
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + 
                                        errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }
        });
        $A.enqueueAction(action);
        
    },
    sortBy: function(field, reverse, primer) {
        var key = primer
            ? function(x) {
                  return primer(x[field]);
              }
            : function(x) {
                  return x[field];
              };

        return function(a, b) {
            a = key(a);
            b = key(b);
            return reverse * ((a > b) - (b > a));
        };
    },
    handleSort: function(cmp, event) {
         
        var sortedBy = event.getParam('fieldName');
        var sortDirection = event.getParam('sortDirection');
        var cloneData = cmp.get('v.lstContentDoc');
        
        cloneData.sort((this.sortBy(sortedBy, sortDirection === 'asc' ? 1 : -1)));
        
        cmp.set('v.lstContentDoc', cloneData);
        cmp.set('v.sortDirection', sortDirection);
        cmp.set('v.sortedBy', sortedBy);
    }
})